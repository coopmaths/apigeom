import type Figure from '../../Figure'
import type Segment from './Segment'

import type { OptionsElement2D } from '../interfaces'
import LinePerpendicular from './LinePerpendicular'

class PerpendicularBisector extends LinePerpendicular {
  notationSegment: string
  constructor(
    figure: Figure,
    { segment, ...options }: { segment: Segment } & OptionsElement2D,
  ) {
    const middlePoint = figure.create('Middle', {
      isChild: true,
      isSelectable: true,
      isVisible: false,
      point1: segment.point1,
      point2: segment.point2,
      shape: '',
    })
    super(figure, { line: segment, point: middlePoint, ...options })
    this.type = 'PerpendicularBisector'
    this.notationSegment = segment.notation
  }

  toJSON(): object {
    return {
      ...this.jsonOptions(),
      idSegment: this.line.id,
    }
  }

  get description(): string {
    return `Médiatrice du segment $${this.notationSegment}$`
  }

  get notation(): string {
    return `\\text{médiatrice de }${this.notationSegment}`
  }
}

export default PerpendicularBisector
