import { expect, test } from 'vitest'
import { describe } from 'vitest'
import Figure from '../src/Figure'

describe('Rotation', () => {
  test("Image d'un point après déplacement", () => {
    const figure = new Figure()
    const A = figure.create('Point', { x: -2, y: 0, label: 'A' })
    const B = figure.create('Point', { x: -4, y: 0, label: 'B' })
    const O = figure.create('Point', { x: 0, y: 0, label: 'M' })
    const A2 = A.rotate(O, -90)
    const B2 = B.rotate(O, -90)
    const M = figure.create('Middle', { point1: A2, point2: B2 })
    expect(A.x).toBe(-2)
    expect(A2.x).toBeCloseTo(0)
    expect(A2.y).toBeCloseTo(2)
    expect(M.x).toBeCloseTo(0)
    expect(M.y).toBeCloseTo(3)
    A.moveTo(0, 2)
    B.moveTo(0, 5)
    expect(M.x).toBeCloseTo(3.5)
    expect(M.y).toBeCloseTo(0)
  })

  test('Cercle circonscrit avec deux symétries centrales', () => {
    const figure = new Figure()
    const A = figure.create('Point', { x: -2, y: 0, label: 'A' })
    const B = figure.create('Point', { x: 6, y: 0, label: 'B' })
    const C = figure.create('Point', { x: 3, y: 8, label: 'C' })
    const triangle = figure.create('Polygon', { points: [A, B, C] })
    const medAB = figure.create('PerpendicularBisectorByPoints', {
      point1: A,
      point2: B,
    })
    const medBC = figure.create('PerpendicularBisectorByPoints', {
      point1: B,
      point2: C,
    })
    const medAC = figure.create('PerpendicularBisectorByPoints', {
      point1: A,
      point2: C,
    })
    const O1 = figure.create('PointIntersectionLL', {
      line1: medAB,
      line2: medBC,
    })
    const c = figure.create('CircleCenterPoint', { center: O1, point: A })

    const O = figure.create('Point', { x: 8, y: 5, label: 'O' })
    const O2 = figure.create('Point', { x: 5, y: 9, label: "O'" })

    figure.options.color = 'green'
    const triangle2 = triangle.rotate(O, -30)
    const medAB2 = medAB.rotate(O, -30)
    const medBC2 = medBC.rotate(O, -30)
    const medAC2 = medAC.rotate(O, -30)
    const c2 = c.rotate(O, -30)

    figure.options.color = 'orange'

    const triangle3 = triangle2.rotate(O, -30)
    const medAB3 = medAB2.rotate(O, -30)
    const medBC3 = medBC2.rotate(O, -30)
    const medAC3 = medAC2.rotate(O, -30)
    const c3 = c2.rotate(O, -30)

    const A3B3 = triangle3.segments[0]
    O.moveTo(O.x + 1, O.y + 1)
    O2.moveTo(O2.x + 1, O2.y + 1)
    expect(c3.radius).toBeCloseTo(c.radius)
    expect(A3B3.angleWithHorizontalInDegres).toBeCloseTo(-60)
  })
})
