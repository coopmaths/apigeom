import { interpret } from 'xstate'

import Figure from './Figure'
import ui from './uiMachine'

class SuperFigure extends Figure {
  constructor({
    border = false,
    dx = 1,
    dy = 1,
    height = 400,
    isDynamic = true,
    pixelsPerUnit = 30,
    scale = 1,
    snapGrid = false,
    width = 400,
    xMin = -10,
    xScale = 1,
    yMin = -6,
    yScale = 1,
  }: {
    border?: boolean
    dx?: number
    dy?: number
    height?: number
    isDynamic?: boolean
    pixelsPerUnit?: number
    scale?: number
    snapGrid?: boolean
    width?: number
    xMin?: number
    xScale?: number
    yMin?: number
    yScale?: number
  } = {}) {
    super({
      border,
      dx,
      dy,
      height,
      isDynamic,
      pixelsPerUnit,
      scale,
      snapGrid,
      width,
      xMin,
      xScale,
      yMin,
      yScale,
    })
    const machineWithContext = ui.withContext({
      figure: this,
      temp: { elements: [], htmlElement: [], values: [] },
    })
    this.ui = interpret(machineWithContext).start()
  }
}

export default SuperFigure
