import type Figure from '../../Figure'
import type Segment from '../lines/Segment'
import type Point from '../points/Point'

import { round } from '../../lib/format'
import Element2D from '../Element2D'
import { labelCoords } from '../calculus/Coords'
import type TextByPosition from '../text/TextByPosition'

class Vector extends Element2D {
  private _label?: string
  private _labelColor: string
  elementTextLabel?: TextByPosition
  /** Extrémité de la représentation du vecteur */
  end!: Point
  labelDxInPixels?: number
  labelDyInPixels?: number
  /** Origine pour la représentation du vecteur */
  readonly origin: Point
  /** Représentation du vecteur */
  representation?: Segment
  x: number
  y: number
  constructor(
    figure: Figure,
    {
      color,
      id,
      isChild,
      isDashed,
      isVisible = true,
      label,
      labelColor,
      labelDxInPixels,
      labelDyInPixels,
      origin,
      opacity,
      thickness,
      x,
      y,
      ...options
    }: {
      color?: string
      isVisible?: boolean
      label?: string
      labelColor?: string
      labelDxInPixels?: number
      labelDyInPixels?: number
      id?: string
      isChild?: boolean
      isDashed?: boolean
      origin: Point
      opacity?: number
      thickness?: number
      x: number
      y: number
    },
  ) {
    super(figure, {
      color,
      id,
      isChild,
      isDashed,
      thickness,
      isVisible,
      ...options,
    })
    this.type = 'Vector'
    this.x = x
    this.y = y
    this.origin = origin
    this._labelColor = labelColor ?? this._color
    this.label = label
    this.labelDxInPixels = labelDxInPixels ?? 0
    this.labelDyInPixels = labelDyInPixels ?? 0
  }

  draw(): void {
    if (this.origin !== undefined) {
      this.origin.subscribe(this)
      this.end = this.figure.create('Point', {
        isChild: true,
        isFree: false,
        isSelectable: true,
        isVisible: this.isVisible,
        shape: '',
        x: this.origin.x + this.x,
        y: this.origin.y + this.y,
      })
      this.representation = this.figure.create('Segment', {
        color: this.color,
        isChild: true,
        isDashed: this.isDashed,
        isSelectable: true,
        isVisible: this.isVisible,
        point1: this.origin,
        point2: this.end,
        shape: '->',
        thickness: this.thickness,
      })
    }
  }

  toJSON(): object {
    return {
      ...this.jsonOptions(),
      idOrigin: this.origin?.id,
      x: round(this.x),
      y: round(this.y),
      label: this.label,
      labelColor: this.labelColor,
    }
  }

  update(): void {
    this.notify()
    if (this.origin !== undefined && this.end !== undefined) {
      this.end.x = this.origin.x + this.x
      this.end.y = this.origin.y + this.y
    }
    if (this.elementTextLabel !== undefined) {
      const { x, y } = labelCoords(this.origin, this.end)
      this.elementTextLabel.x = x
      this.elementTextLabel.y = y
    }
  }

  get color(): string {
    return this._color
  }

  set color(color: string) {
    this._color = color
    if (this.representation !== undefined) {
      this.representation.color = color
    }
  }

  get isDashed(): boolean {
    return this._isDashed
  }

  set isDashed(isDashed: boolean) {
    this._isDashed = isDashed
    if (this.representation !== undefined) {
      this.representation.isDashed = isDashed
    }
  }

  get isVisible(): boolean {
    return this._isVisible
  }

  set isVisible(isVisible: boolean) {
    this._isVisible = isVisible
    if (this.representation !== undefined) {
      this.representation.isVisible = isVisible
    }
    if (this.elementTextLabel !== undefined) {
      this.elementTextLabel.isVisible = isVisible
    }
  }

  get label(): string {
    if (this._label === undefined) return ''
    return this._label
  }

  set label(label: string | undefined) {
    this._label = label
    if (label !== undefined) {
      if (this.elementTextLabel === undefined) {
        const xEnd = this.origin.x + this.x
        const yEnd = this.origin.y + this.y
        const { x, y } = labelCoords(this.origin, { x: xEnd, y: yEnd })
        this.elementTextLabel = this.figure.create('TextByPosition', {
          color: this._labelColor ?? this._color,
          dxInPixels: this.labelDxInPixels,
          dyInPixels: this.labelDyInPixels,
          id: `${this.id}_label`,
          isChild: true,
          isSelectable: true,
          isVisible: this.isVisible,
          x,
          y,
          text: `$${label}$`,
        })
      } else {
        this.elementTextLabel.text = `$${label}$`
        this.elementTextLabel.color = this._labelColor ?? 'currentColor'
      }
    }
  }

  get labelColor(): string {
    return this._labelColor
  }

  set labelColor(color: string) {
    this._labelColor = color
    if (this.elementTextLabel !== undefined) {
      this.elementTextLabel.color = color
    }
  }

  get notation(): string {
    if (this.label !== '') return this.label
    return this.id
  }

  get opacity(): number {
    return this._opacity
  }

  set opacity(opacity: number) {
    this._opacity = opacity
    if (this.representation !== undefined) {
      this.representation.opacity = opacity
    }
    if (this.elementTextLabel !== undefined) {
      this.elementTextLabel.opacity = opacity
    }
  }

  get thickness(): number {
    return this._thickness
  }

  set thickness(thickness: number) {
    this._thickness = thickness
    if (this.representation !== undefined) {
      this.representation.thickness = thickness
    }
  }
}

export default Vector
