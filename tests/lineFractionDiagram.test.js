import { expect, test } from 'vitest'
import Figure from '../src/Figure'
import fs from 'node:fs/promises'
import sharp from 'sharp'
import nodeHtmlToImage from 'node-html-to-image'

const figure = new Figure({ xMin: -0.5, yMin: -2, width: 570, height: 120 })
figure.divUserMessage.style.display = 'none'
figure.options.automaticUserMessage = false
figure.options.color = 'blue'

figure.create('LineFractionDiagram', { denominator: 5, max: 3, width: 6, height: 0.8, thickHeightInPixels:20 })
// 6 x 3 = 18 unités, plus 1 donne 19 unités
// pixelsPerUnit = 30,
// 19 * 30 * scale * scalex = 570

const container = document.createElement('div')
figure.isDynamic = false
figure.setContainer(container)  




// pnpm test ./tests/lineFractionDiagram.test.js
test('LineFractionDiagram', async () => {

  let zoom = 1
  figure.zoom(zoom, { changeHeight: true, changeWidth: true, changeLeft: false, changeBottom: false })
  await exportPNG(zoom)
  await exportHTML(zoom)

  zoom = 2
  figure.zoom(zoom, { changeHeight: true, changeWidth: true, changeLeft: false, changeBottom: false })
  await exportPNG(zoom)
  await exportHTML(zoom)

  zoom = 3
  figure.zoom(zoom, { changeHeight: true, changeWidth: true, changeLeft: false, changeBottom: false })
  await exportPNG(zoom)
  await exportHTML(zoom)

}, 30000)


async function exportHTML(zoom) {
    const IMA_FOLDER = 'imag_png'
    try {
        await fs.access(IMA_FOLDER)
    } catch (err) {
        await fs.mkdir(IMA_FOLDER)
    }

    const style =`<head>
      <style>
        body {
          width: ${figure.width}px;
          height: ${figure.height}px;
        }
      </style>
    </head>`

    await nodeHtmlToImage({
        output: `${IMA_FOLDER}/LineFractionDiagram_HTML_z_${zoom}.png`,
        html: `<html><body>${style}<div>${figure.container.innerHTML}</div></body></html>`
    }).then(() => console.info('The image was created successfully!'))
}

async function exportPNG(zoom) {
    const IMA_FOLDER = 'imag_png'
    try {
        await fs.access(IMA_FOLDER)
    } catch (err) {
        await fs.mkdir(IMA_FOLDER)
    }
    const serializer = new XMLSerializer()
    const svgString = serializer.serializeToString(figure.svg)
    const filePath = `${IMA_FOLDER}/LineFractionDiagram_z_${zoom}.png`;
    await sharp(Buffer.from(svgString))
        .png() // Conversion en PNG
        .toFile(filePath) // Sauvegarde le fichier en PNG
        .then(info => {
            console.info('Image PNG créée avec succès :', info)
        })
        .catch(err => {
            console.error('Erreur lors de la conversion :', err)
        })
}

