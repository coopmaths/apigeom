import type Figure from '../../Figure'
import type Circle from '../lines/Circle'
import type Line from '../lines/Line'

import { intersectionLCCoord } from '../calculus/Coords'
import type { OptionsElement2D, OptionsIntersectionLC } from '../interfaces'
import Point from './Point'

/**
 * Créé les points à l'intersection de deux cercles
 */
export class PointIntersectionLC extends Point {
  /** Deuxième cercle */
  circle: Circle
  /** Premier cercle */
  line: Line
  /** Numéro de l'intersection */
  n: 1 | 2
  constructor(
    figure: Figure,
    { circle, line, n, ...options }: OptionsIntersectionLC & OptionsElement2D,
  ) {
    const coords = intersectionLCCoord(line, circle, n)
    super(figure, { isFree: false, x: coords.x, y: coords.y, ...options })
    this.type = 'PointIntersectionLC'
    this.line = line
    this.circle = circle
    this.n = n ?? 1
    this.line.subscribe(this)
    this.circle.subscribe(this)
  }

  toJSON(): object {
    return {
      ...this.jsonOptions(),
      idCircle: this.circle.id,
      idLine: this.line.id,
      label: this.label,
      n: this.n,
      shape: this.shape,
    }
  }

  update(): void {
    if (this.line === undefined || this.circle === undefined) return
    try {
      const { x, y } = intersectionLCCoord(this.line, this.circle, this.n)
      this._x = x
      this._y = y
      if (!this.isOnline(this.line)) {
        this._x = Number.NaN
        this._y = Number.NaN
      }
      super.update()
    } catch (error) {
      console.error('Erreur dans PointIntersectionLL.update()', error)
    }
    this.notify()
  }

  get description(): string {
    const lineName = this.line.notation
    const circleName = this.circle.id
    return `Intersection de la droite $${lineName}$ et du cercle $${circleName}$`
  }
}

export default PointIntersectionLC
