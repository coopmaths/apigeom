import type Figure from '../Figure'
import type Point from '../elements/points/Point'

import { angleOriented } from '../elements/calculus/Coords'
import DynamicNumber from './DynamicNumber'

class Angle extends DynamicNumber {
  center: Point
  end: Point
  start: Point
  constructor(
    figure: Figure,
    { center, end, start }: { center: Point; end: Point; start: Point },
  ) {
    super(figure, { isChild: false })
    this.start = start
    this.center = center
    this.end = end
    this.start.subscribe(this)
    this.center.subscribe(this)
    this.end.subscribe(this)
    this.update()
  }

  toJSON(): object {
    return {
      id: this.id,
      idCenter: this.center.id,
      idEnd: this.end.id,
      idStart: this.start.id,
      isChild: this.isChild,
      type: this.type,
    }
  }

  update(): void {
    this._value = angleOriented(this.start, this.center, this.end)
    this.notify()
  }
}

export default Angle
