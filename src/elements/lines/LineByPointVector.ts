import type Figure from '../../Figure'
import type Point from '../points/Point'
import type Vector from '../vector/Vector'

import type { OptionsElement2D } from '../interfaces'
import Line from './Line'

class LineByPointVector extends Line {
  point: Point
  vector: Vector
  constructor(
    figure: Figure,
    {
      point,
      vector,
      ...options
    }: { point: Point; vector: Vector } & OptionsElement2D,
  ) {
    const point2 = point.translateVector(vector)
    point2.isVisible = false
    point2.isSelectable = true
    point2.isVisible = false
    point2.shape = point.shape
    super(figure, { point1: point, point2, shape: '', ...options })
    this.type = 'LineByPointVector'
    this.point = point
    this.vector = vector
  }

  toJSON(): object {
    return {
      ...this.jsonOptions(),
      idPoint: this.point.id,
      idVector: this.vector.id,
    }
  }
}

export default LineByPointVector
