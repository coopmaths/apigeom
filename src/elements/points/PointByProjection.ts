import type Figure from '../../Figure'
import type Segment from '../lines/Segment'

import { orthogonalProjectionCoord } from '../calculus/Coords'
import type { OptionsElement2D } from '../interfaces'
import Point from './Point'

class PointByProjection extends Point {
  line: Segment
  origin: Point
  constructor(
    figure: Figure,
    {
      line,
      origin,
      ...options
    }: { line: Segment; origin: Point, shape?: '' | '|' | 'o' | 'x', sizeInPixels?: number } & OptionsElement2D,
  ) {
    const { x, y } = orthogonalProjectionCoord(origin, line)
    super(figure, { isFree: false, x, y, ...options })
    this.type = 'PointByProjection'
    this.origin = origin
    this.line = line
    this.origin.subscribe(this)
    this.line.subscribe(this)
  }

  toJSON(): object {
    return {
      ...this.jsonOptions(),
      idLine: this.line.id,
      idOrigin: this.origin.id,
      label: this.label,
      shape: this.shape,
      sizeInPixels: this.sizeInPixels,
    }
  }

  update(): void {
    const { x, y } = orthogonalProjectionCoord(this.origin, this.line)
    this._x = x
    this._y = y
    super.update()
  }

  get description(): string {
    const lineName = this.line.notation
    const originName =
      this.origin.label !== '' ? this.origin.label : this.origin.id
    return `Projection orthogonale de $${originName}$ sur $${lineName}$`
  }
}

export default PointByProjection
