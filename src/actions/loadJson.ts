import type Figure from '../Figure'
import type Distance from '../dynamicNumbers/Distance'
import type DynamicNumber from '../dynamicNumbers/DynamicNumber'
import type { FigureOptions, SaveJson } from '../elements/interfaces'
import type Circle from '../elements/lines/Circle'
import type Line from '../elements/lines/Line'
import type Segment from '../elements/lines/Segment'
import type Point from '../elements/points/Point'
import { showSelectedColor } from '../userInterface/addColorPalette'
import { showSelectedStyle } from '../userInterface/addDashedChoice'

/**
 * Analyse l'objet de la sauvegarde et si le type est pris en charge alors on créé l'élément
 * @param figure - Espace de travail
 * @param json - Objet
 * @param eraseHistory - Faut-il repartir d'un historique vide ? Oui pour un chargement, non pour l'utilisation de undo et redo
 * @returns - Tableau avec tous les éléments créés
 */
export function loadJson(
  figure: Figure,
  json: SaveJson,
  eraseHistory = false,
): void {
  if (eraseHistory) {
    figure.stackUndo = []
    figure.stackRedo = []
  }
  figure.elements.clear()
  if (figure.divFigure != null) figure.divFigure.innerHTML = ''
  figure.clearHtml()
  figure.divFigure?.appendChild(figure.svg)
  if (json.options != null) {
    const options = json.options as FigureOptions
    for (const [key, value] of Object.entries(options)) {
      if (key in figure.options) {
        // @ts-expect-error Je ne sais pas typer cela
        figure.options[key] = value
      }
    }
  }
  showSelectedColor(figure)
  showSelectedStyle(figure)
  figure.pointer = figure.create('Point', {
    isChild: true,
    isFree: false,
    isVisible: false,
    shape: '',
    x: 0,
    y: 0,
  })
  figure.pointer.type = 'pointer'
  for (const options of Object.values(json)) {
    // Clone map figure.elements
    // const elements = new Map()
    // for (const [key, value] of figure.elements) {
    //   elements.set(key, value)
    // }
    // console.info(elements)
    // console.info(options.type)
    if (options.type === 'Point') {
      figure.create('Point', { x: options.x, y: options.y, ...options })
    } else if (options.type === 'Arc') {
      const center = figure.elements.get(options.idCenter) as Point
      const start = figure.elements.get(options.idStart) as Point
      const dynamicAngle = figure.elements.get(
        options.idDynamicAngle,
      ) as DynamicNumber
      figure.create('Arc', { center, dynamicAngle, start, ...options })
    } else if (options.type === 'ArcByCenterRadiusAndAngles') {
      const center = figure.elements.get(options.idCenter) as Point
      const radius = options.radius
      const startAngle = options.startAngle
      const endAngle = options.endAngle
      figure.create('ArcByCenterRadiusAndAngles', {
        center,
        endAngle,
        radius,
        startAngle,
        ...options,
      })
    } else if (options.type === 'BisectorByPoints') {
      const origin = figure.elements.get(options.idOrigin) as Point
      const pointOnSide1 = figure.elements.get(options.idPointOnSide1) as Point
      const pointOnSide2 = figure.elements.get(options.idPointOnSide2) as Point
      figure.create('BisectorByPoints', {
        origin,
        pointOnSide1,
        pointOnSide2,
        ...options,
      })
    } else if (options.type === 'Circle') {
      const center = figure.elements.get(options.idCenter) as Point
      figure.create('Circle', { center, radius: options.radius, ...options })
    } else if (options.type === 'CircleCenterPoint') {
      const center = figure.elements.get(options.idCenter) as Point
      const point = figure.elements.get(options.idPoint) as Point
      figure.create('CircleCenterPoint', { center, point, ...options })
    } else if (options.type === 'CircleDynamicRadius') {
      const center = figure.elements.get(options.idCenter) as Point
      const radius = figure.elements.get(options.idRadius) as Distance
      figure.create('CircleCenterDynamicRadius', { center, radius, ...options })
    } else if (options.type === 'CircleFractionDiagram') {
      const indicesSectorsInColor = new Set(options.arrayIndicesSectorsInColor)
      figure.create('CircleFractionDiagram', {
        indicesSectorsInColor,
        ...options,
      })
    } else if (options.type === 'Distance') {
      const point1 = figure.elements.get(options.idPoint1) as Point
      const point2 = figure.elements.get(options.idPoint2) as Point
      figure.create('Distance', { point1, point2, ...options })
    } else if (options.type === 'DynamicX') {
      const point = figure.elements.get(options.idPoint) as Point
      figure.create('DynamicX', { point, ...options })
    } else if (options.type === 'DynamicY') {
      const point = figure.elements.get(options.idPoint) as Point
      figure.create('DynamicY', { point, ...options })
    } else if (options.type === 'PointOnLine') {
      const line = figure.elements.get(options.idLine) as Line
      figure.create('PointOnLine', { line, ...options })
    } else if (options.type === 'PointOnCircle') {
      const circle = figure.elements.get(options.idCircle) as Circle
      figure.create('PointOnCircle', { circle, ...options })
    }  else if (options.type === 'GraphByParts') {
      figure.create('GraphByParts', { ...options })
    } else if (options.type === 'Graph') {
      figure.create('Graph', { ...options })
    }else if (options.type === 'GraduatedLine') {
      figure.create('GraduatedLine', { ...options })
    } else if (options.type === 'Grid') {
      figure.create('Grid', { ...options })
    } else if (options.type === 'Line') {
      const point1 = figure.elements.get(options.idPoint1) as Point
      const point2 = figure.elements.get(options.idPoint2) as Point
      figure.create('Line', { point1, point2, ...options })
    } else if (options.type === 'Middle') {
      const point1 = figure.elements.get(options.idPoint1) as Point
      const point2 = figure.elements.get(options.idPoint2) as Point
      figure.create('Middle', { point1, point2, ...options })
    } else if (options.type === 'LineByPointVector') {
      const vector = figure.elements.get(options.idVector)
      const point = figure.elements.get(options.idPoint)
      figure.create('LineByPointVector', { point, vector, ...options })
    } else if (options.type === 'LineFractionDiagram') {
      const indicesRectanglesInColor = new Set(
        options.arrayIndicesRectanglesInColor,
      )
      figure.create('LineFractionDiagram', {
        indicesRectanglesInColor,
        ...options,
      })
    } else if (options.type === 'RectangleFractionDiagram') {
      const indicesRectanglesInColor = new Set(
        options.arrayIndicesRectanglesInColor,
      )
      figure.create('RectangleFractionDiagram', {
        indicesRectanglesInColor,
        ...options,
      })
    } else if (options.type === 'LineParallel') {
      const line = figure.elements.get(options.idLine)
      const point = figure.elements.get(options.idPoint)
      figure.create('LineParallel', { line, point, ...options })
    } else if (options.type === 'LinePerpendicular') {
      const line = figure.elements.get(options.idLine)
      const point = figure.elements.get(options.idPoint)
      figure.create('LinePerpendicular', { line, point, ...options })
    } else if (options.type === 'TextByPosition') {
      figure.create('TextByPosition', {
        text: options.text,
        x: options.x,
        y: options.y,
        anchor: options.anchor,
        ...options,
      })
    } else if (options.type === 'MarkBetweenPoints') {
      const point1 = figure.elements.get(options.idPoint1) as Point
      const point2 = figure.elements.get(options.idPoint2) as Point
      figure.create('MarkBetweenPoints', {
        text: options.text,
        point1,
        point2,
        ...options,
      })
    } else if (options.type === 'MarkRightAngle') {
      const point = figure.elements.get(options.idPoint) as Point
      const directionPoint = figure.elements.get(options.idDirectionPoint) as Point
      figure.create('MarkRightAngle', {
        point,
        directionPoint,
        ...options,
      })
    } else if (options.type === 'PerpendicularBisector') {
      const segment = figure.elements.get(options.idSegment)
      figure.create('PerpendicularBisector', { segment, ...options })
    } else if (options.type === 'PerpendicularBisectorByPoints') {
      const point1 = figure.elements.get(options.idPoint1) as Point
      const point2 = figure.elements.get(options.idPoint2) as Point
      figure.create('PerpendicularBisectorByPoints', {
        point1,
        point2,
        ...options,
      })
    } else if (options.type === 'ElementByReflect') {
      const origin = figure.elements.get(
        options.idTransformationOrigin,
      ) as Point
      const center = figure.elements.get(
        options.idTransformationCenter,
      ) as Point
      origin.reflect(center, options)
    } else if (options.type === 'ElementByReflectOverLine') {
      const origin = figure.elements.get(
        options.idTransformationOrigin,
      ) as Point
      const line = figure.elements.get(options.idTransformationLine) as Line
      origin.reflectOverLine(line, options)
    } else if (options.type === 'ElementByDilate') {
      const origin = figure.elements.get(
        options.idTransformationOrigin,
      ) as Point
      const center = figure.elements.get(
        options.idTransformationCenter,
      ) as Point
      const factor = options.transformationFactor
      origin.dilate(center, factor, options)
    } else if (options.type === 'ElementByRotation') {
      const origin = figure.elements.get(
        options.idTransformationOrigin,
      ) as Point
      const center = figure.elements.get(
        options.idTransformationCenter,
      ) as Point
      const angle = options.transformationAngle
      origin.rotate(center, angle, options)
    } else if (options.type === 'ElementByTranslation') {
      const origin = figure.elements.get(
        options.idTransformationOrigin,
      ) as Point
      const point1 = figure.elements.get(
        options.idTransformationPoint1,
      ) as Point
      const point2 = figure.elements.get(
        options.idTransformationPoint2,
      ) as Point
      origin.translate(point1, point2, options)
    } else if (options.type === 'PointByProjection') {
      const origin = figure.elements.get(options.idOrigin) as Point
      const line = figure.elements.get(options.idLine) as Segment
      figure.create('PointByProjection', { line, origin, ...options })
    } else if (options.type === 'PointByProjectionOnAxisX') {
      const origin = figure.elements.get(options.idOrigin) as Point
      figure.create('PointByProjectionOnAxisX', { origin, ...options })
    } else if (options.type === 'PointByProjectionOnAxisY') {
      const origin = figure.elements.get(options.idOrigin) as Point
      figure.create('PointByProjectionOnAxisY', { origin, ...options })
    } else if (options.type === 'PointIntersectionLL') {
      const line1 = figure.elements.get(options.idLine1) as Line
      const line2 = figure.elements.get(options.idLine2) as Line
      figure.create('PointIntersectionLL', { line1, line2, ...options })
    } else if (options.type === 'PointIntersectionCC') {
      const circle1 = figure.elements.get(options.idCircle1) as Circle
      const circle2 = figure.elements.get(options.idCircle2) as Circle
      figure.create('PointIntersectionCC', { circle1, circle2, ...options })
    } else if (options.type === 'PointIntersectionLC') {
      const line = figure.elements.get(options.idLine) as Line
      const circle = figure.elements.get(options.idCircle) as Circle
      figure.create('PointIntersectionLC', { circle, line, ...options })
    } else if (options.type === 'PointOnGraph') {
      const graph = figure.elements.get(options.idGraph)
      figure.create('PointOnGraph', { graph, ...options })
    } else if (options.type === 'PointOnPolyline'){
      const polyline = figure.elements.get(options.idPolyline )
      figure.create('PointOnPolyline', {polyline, ...options})
    } else if (options.type === 'Segment') {
      const point1 = figure.elements.get(options.idPoint1) as Point
      const point2 = figure.elements.get(options.idPoint2) as Point
      figure.create('Segment', { point1, point2, ...options })
    } else if (options.type === 'Polyline') {
      const points = []
      for (const idPoint of options.idPoints) {
        points.push(figure.elements.get(idPoint))
      }
      figure.create('Polyline', { points, ...options })
    } else if (options.type === 'Polygon') {
      const points = []
      for (const idPoint of options.idPoints) {
        points.push(figure.elements.get(idPoint))
      }
      figure.create('Polygon', { points, ...options })
    } else if (options.type === 'Ray') {
      const point1 = figure.elements.get(options.idPoint1) as Point
      const point2 = figure.elements.get(options.idPoint2) as Point
      figure.create('Ray', { point1, point2, ...options })
    } else if (options.type === 'TextByPosition') {
      figure.create('TextByPosition', {
        text: options.text,
        x: options.x,
        y: options.y,
        ...options,
      })
    } else if (options.type === 'TextByPoint') {
      const point = figure.elements.get(options.idPoint) as Point
      figure.create('TextByPoint', { point, text: options.text, ...options })
    } else if (options.type === 'TextDynamicByPosition') {
      const dynamicNumber = figure.elements.get(
        options.idDynamicNumber,
      ) as DynamicNumber
      figure.create('TextDynamicByPosition', { dynamicNumber, ...options })
    } else if (options.type === 'Vector') {
      const origin = figure.elements.get(options.idOrigin)
      figure.create('Vector', { origin, ...options })
    } else if (options.type === 'VectorByPoints') {
      const origin = figure.elements.get(options.idOrigin)
      const point1 = figure.elements.get(options.idPoint1)
      const point2 = figure.elements.get(options.idPoint2)
      figure.create('VectorByPoints', { origin, point1, point2, ...options })
    } else if (options.type === 'VectorPerpendicular') {
      const origin = figure.elements.get(options.idOrigin)
      const line = figure.elements.get(options.idLine)
      figure.create('VectorPerpendicular', { line, origin, ...options })
    }
  }
  // Pour la navigation dans l'historique on ne sauvegarde que le premier chargement
  // les autres chargements proviennent de goBack() ou de goForward()
  if (figure.stackUndo.length === 0) figure.saveState()
  if (eraseHistory) {
    if ('history' in json && json.history !== null) {
      const history = json.history as string[]
      figure.stackUndo = [...history]
      figure.handleUndoRedoButtons()
      // Comportement irrégulier sans ce Undo que je ne m'explique pas
      // figure.undo()
    }
  }
}
