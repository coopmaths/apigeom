import type { availableButtons } from '../userInterface/availableButtons'

// Couleurs
export const colors = {
  back: '#f5f1f3',
  backcorrection: '#E0A588',
  backdark: '#dadbdf',
  backdarker: '#cecfd4',
  backnav: '#F15929',
  backnavlight: '#f56d45',
  dark: '#F45E27',
  darkest: '#E64A10',
  darkmode: '#2e2e2b',
  darkmodelight: '#363633',
  DEFAULT: '#F15929',
  light: '#f56d45',
  lightest: '#f87f5c',
  title: '#342A34',
  titleexercise: '#F15929',
  titlelight: '#f5f1f3',
  titlemenu: '#F15929',
}

// Distance entre l'endroit où on clique et les élément
export const defaultDistanceClick = 15

// Taille de l'historique pour la navigation avec le bouton précédent
export const defaultHistorySize = 100

// Nombre de chiffres après la virgule dans les affichages
export const displayDigits = 2

// Pente d'une droite verticale
export const defaultMaxSlope = 10 ** 4

// Pente d'une droite horizontale
export const defaultMinSlope = 10 ** -4

// Ecart entre le lieu du clic et la position de la fenêtre modale en unité de la figure
export const defaultDeltaXModal = 1

// Espace horizontal laissé pour les boutons
export const defaultButtonsWidth = 300

// Espace vertical laissé pour le footer
export const defaultFooterHeight = 30

// Largeur minimale du svg
export const defaultMinWidth = 30

// Hauteur minimale du svg
export const defaultMinHeight = 30

// Style du divUserMessage
export const defaultDivUserMessageHeight = '50px'
export const defaultDivUserMessageFontSize = 'large'

// Déplacement en unités lors du dragAll
export const defaultDragAllDelta = 0.1

export const defaultZoomInFactor = 1.1
export const defaultZoomOutFactor = 0.9

export const marks = ['', '|', '||', '|||', '✖︎', 'O', '◼︎', '▲', '★']

export const orangeMathalea = '#F15929'
export const orangeMathaleaLight = 'rgba(241, 89, 41, 0.5)'

const defaultOptions = {
  animationStepInterval: 3000,
  automaticUserMessage: true,
  borderSize: 0.2,
  color: 'currentColor',
  colorPointPolygon: 'none',
  changeColorChangeActionToSetOptions: false,
  discFillOpacity: 0.2,
  displayGrid: false,
  distanceWithoutNewPoint: 0.2,
  fillColor: 'none',
  fillColorAndBorderColorAreSame: false,
  fillOpacity: 0.2,
  gridWithTwoPointsOnSamePosition: true,
  fontSize: '1em',
  isDashed: false,
  labelAutomaticForPoints: undefined,
  labelDxInPixels: 15,
  labelDyInPixels: 15,
  latexHeight: 12,
  labelIsVisible: true,
  latexWidth: 18,
  limitNumberOfElement: new Map(),
  mark: '||',
  moveTextGrid: 15,
  pointDescriptionWithCoordinates: true,
  pointSize: 5,
  thickness: 1,
  shape: 'x' as '' | '|' | 'o' | 'x',
  shapeForPolygon: 'x' as '' | '|' | 'o' | 'x',
  thicknessForPoint: 2,
  tmpColor: 'gray',
  tmpFillColor: orangeMathaleaLight,
  tmpFillOpacity: 0.2,
  tmpIsDashed: true,
  tmpThickness: 1,
  tmpShape: 'x' as '' | '|' | 'o' | 'x',
}

export const defaultTools = [
  'SAVE',
  'OPEN',
  'UNDO',
  'REDO',
  'DRAG',
  'DOWNLOAD_LATEX_SVG',
  'HIDE',
  'REMOVE',
  'POINT',
  'POINT_ON',
  'POINT_INTERSECTION',
  'MIDDLE',
  'SEGMENT',
  'LINE',
  'RAY',
  'POLYGON',
  'LINE_PARALLEL',
  'LINE_PERPENDICULAR',
  'PERPENDICULAR_BISECTOR',
  'BISECTOR_BY_POINTS',
  'CIRCLE_CENTER_POINT',
  'CIRCLE_RADIUS',
  'REFLECTION_OVER_LINE',
  'REFLECTION',
  'ROTATE',
  'TRANSLATION',
  'DILATE',
  'VECTOR',
  'GRID',
  'SET_OPTIONS',
  'FILL',
  'NAME_POINT',
  'MOVE_LABEL',
  'MARK_BETWEEN_POINTS',
  'DESCRIPTION',
  'SHAKE',
  'ZOOM_IN',
  'ZOOM_OUT',
] as availableButtons[]

export default defaultOptions
