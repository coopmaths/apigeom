import type Figure from '../Figure'

import type { OptionsDynamicNumber } from '../elements/interfaces'
import DynamicNumber from './DynamicNumber'

class DynamicCalcul extends DynamicNumber {
  calculus: (x: number[]) => number
  dynamicNumbers: DynamicNumber[]
  constructor(
    figure: Figure,
    {
      calculus,
      dynamicNumbers,
      ...options
    }: {
      calculus: (x: number[]) => number
      dynamicNumbers: DynamicNumber[]
    } & OptionsDynamicNumber,
  ) {
    super(figure, { ...options })
    this.calculus = calculus
    this.dynamicNumbers = dynamicNumbers
    for (const dynamicNumber of this.dynamicNumbers) {
      dynamicNumber.subscribe(this)
    }
    this.update()
  }

  update(): void {
    const values = []
    for (const dynamicNumber of this.dynamicNumbers) {
      values.push(dynamicNumber.value)
    }
    this._value = this.calculus(values)
    this.notify()
  }
}

export default DynamicCalcul
