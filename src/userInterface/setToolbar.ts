import type Figure from '../Figure'
import { defaultTools } from '../elements/defaultValues'
import addMarkPalette from './addMarkPalette'

import type { availableButtons } from './availableButtons'

export default function setToolbar(
  figure: Figure,
  {
    nbCols,
    position = 'left',
    showStyles,
    tools = defaultTools,
  }: {
    nbCols?: number
    position?: 'left' | 'top' | 'none'
    showStyles?: boolean
    tools?: availableButtons[]
  },
): void {
  figure.divButtons.innerHTML = ''
  if (position === 'none') {
    figure.divButtons.style.display = 'none'
    return
  }
  if (position === 'left') {
    if (showStyles === undefined) showStyles = true
    if (nbCols === undefined) nbCols = 4
    figure.divButtons.style.display = 'grid'
    if (window.matchMedia('(min-width: 768px)').matches) {
      figure.container.style.gridTemplateColumns = '230px 1fr'
    } else {
      figure.container.style.gridTemplateColumns = '50px 1fr'
    }
    figure.container.style.display = 'grid'
    if (window.matchMedia('(min-width: 768px)').matches) {
      figure.divButtons.style.gridTemplateColumns = `repeat(${nbCols}, auto)`
    } else {
      figure.divButtons.style.gridTemplateColumns = 'auto'
    }
    figure.divButtons.style.gridAutoRows = 'min-content'
    figure.divButtons.style.justifyContent = 'start'
  }
  if (position === 'top') {
    if (showStyles === undefined) showStyles = false
    if (nbCols === undefined) nbCols = 8
    figure.container.style.display = 'block'
    figure.divButtons.style.display = 'grid'
    figure.divButtons.style.gridTemplateColumns = `repeat(${nbCols}, auto)`
    figure.divButtons.style.gridAutoRows = 'min-content'
    if (tools.length < 6) {
      figure.divButtons.style.justifyContent = 'start'
    } else {
      figure.divButtons.style.justifyContent = 'center'
    }
  }
  if (showStyles === false) {
    const index = tools.indexOf('SET_OPTIONS')
    if (index > -1) {
      tools.splice(index, 1)
    }
  }
  const imgs = figure.addButtons(tools.join(' '))
  figure.divButtons.append(...imgs)
  for (const img of imgs) {
    figure.divButtons.appendChild(img)
  }
  if (showStyles === true) {
    const divStyles = document.createElement('div')
    divStyles.style.gridColumn = '1 / -1'
    figure.divButtons.appendChild(divStyles)
    divStyles.appendChild(figure.addColorPalette(['black', 'blue', 'red']))
    divStyles.appendChild(
      figure.addColorPalette(['green', 'orange', 'lightgray']),
    )
    divStyles.appendChild(figure.addDashedChoice())
    divStyles.appendChild(figure.addThicknessChoice())
  }
  if (tools.indexOf('MARK_BETWEEN_POINTS')) {
    figure.divMarkPalette = document.createElement('div')
    figure.divMarkPalette.style.display = 'flex'
    figure.divToolbar2.appendChild(figure.divMarkPalette)
    addMarkPalette({ figure, container: figure.divMarkPalette })
    figure.divMarkPalette.style.display = 'none'
  }
}
