import type Figure from '../../Figure'
import type Line from '../lines/Line'
import type Point from '../points/Point'

import type { OptionsVectorPerpendicular } from '../interfaces'
import Vector from './Vector'

class VectorUnitPerpendicular extends Vector {
  /** Droite perpendiculaire au vecteur */
  line: Line
  /** Origine de la représentation vecteur */
  origin: Point
  constructor(
    figure: Figure,
    { line, origin, ...options }: OptionsVectorPerpendicular,
  ) {
    const [x, y] = line.equation
    super(figure, { origin, x, y, ...options })
    this.type = 'VectorPerpendicular'
    this.line = line
    this.origin = origin
    line.subscribe(this)
    origin.subscribe(this)
  }

  toJSON(): object {
    return {
      ...this.jsonOptions(),
      idLine: this.line.id,
      idOrigin: this.origin.id,
    }
  }

  update(): void {
    this.notify()
    const [x, y] = this.line.equation
    this.x = x
    this.y = y
    if (this.end !== undefined) {
      this.end.x = this.origin.x + this.x
      this.end.y = this.origin.y + this.y
    }
  }
}

export default VectorUnitPerpendicular
