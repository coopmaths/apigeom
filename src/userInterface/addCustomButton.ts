import type Figure from '../Figure'
import { orangeMathaleaLight } from '../elements/defaultValues'
import setButtonStyle from './setButtonStyle'

/**
 *
 * action : fonction à exécuter lors du clic sur le bouton
 * figure : figure à laquelle le bouton est associé
 * tooltip : texte affiché au survol du bouton
 * url : url de l'image du bouton
 * text : texte du bouton si url est null
 * container : conteneur du bouton
 * record : si true, le bouton est enregistré dans la liste des boutons de la figure et s'allumera lorsqu'il est cliqué et s'eteindra lorsqu'un autre bouton de figure.buttons est cliqué
 * @returns
 */
export default function addCustomButton({
  action,
  figure,
  tooltip = '',
  url,
  text,
  container,
  record = true,
  userMessage = '',
}: {
  action: () => void
  figure: Figure
  tooltip?: string
  url?: string
  text?: string
  container?: HTMLDivElement
  record?: boolean
  userMessage?: string
}): HTMLElement {
  if (url == null && text == null) {
    throw new Error('addCustomButton doit spécifier une url ou un texte')
  }
  let element: HTMLImageElement | HTMLDivElement
  if (url != null) {
    element = document.createElement('img') as HTMLImageElement
    const img = element as HTMLImageElement
    img.src = url
  } else {
    element = document.createElement('div')
    const div = element as HTMLDivElement
    div.textContent = text as string
    element.style.textAlign = 'center'
    element.style.lineHeight = '40px'
    element.style.fontSize = '20px'
    element.style.letterSpacing = '1px'
    element.style.fontFamily = 'Arial'
  }
  element.title = tooltip
  setButtonStyle(element)
  if (container == null) {
    figure.divButtons.appendChild(element)
  } else {
    container.appendChild(element)
  }
  element.onclick = () => {
    action()
    figure.userMessage = userMessage
  }
  if (record) {
    const id =
      window.crypto.randomUUID !== undefined
        ? window.crypto.randomUUID()
        : getNumberId()
    figure.buttons.set(id as string, element)
    element.addEventListener('click', () => {
      for (const button of figure.buttons.values()) {
        button.style.backgroundColor = 'white'
      }
      element.style.backgroundColor = orangeMathaleaLight
    })
  }
  return element
}

/* MGu : HACK pour les connexions HTTP 
car window.crypto.randomUUID est undefined pour les connexions HTTP
*/
let numberId = 0
function getNumberId() {
  numberId++
  return numberId
}
