import type Figure from '../../Figure'

import { similitudeCoord } from '../calculus/Coords'
import type { OptionsElement2D, OptionsPointBy } from '../interfaces'
import Line from '../lines/Line'
import Ray from '../lines/Ray'
import Segment from '../lines/Segment'
import Point from './Point'

class PointOnLine extends Point {
  line: Line
  center: Point
  origin: Point
  k: number
  constructor(
    figure: Figure,
    {
      k,
      line,
      shape = '|',
      ...options
    }: { k?: number; line: Line } & OptionsPointBy & OptionsElement2D,
  ) {
    if (k === undefined) k = Math.random()
    if (shape === undefined) shape = '|'
    super(figure, {
      x: 99,
      y: 99,
      shape,
      ...options,
    })
    this.center = line.point1
    this.origin = line.point2
    this.k = k
    this.type = 'PointOnLine'
    this.line = line
    this.isFree = true
    this.line.subscribe(this)
  }

  /** Déplace le point */
  moveTo(x: number, y: number): void {
    // Lieu du clic
    const origin = { x, y }
    // On calcule le nouveau coefficient de l'homothétie par rapport au projeté du lieu où on clic
    try {
      const [a, b, c] = this.line.equation
      const k = 1 / (a * a + b * b)
      let x: number | undefined
      let y: number | undefined
      if (a === 0) {
        x = origin.x
        y = -c / b
      } else if (b === 0) {
        y = origin.y
        x = -c / a
      } else {
        if (origin.x === undefined || origin.y === undefined) {
          x = Number.NaN
          y = Number.NaN
        } else {
          x = k * (b * b * origin.x - a * b * origin.y - a * c)
          y =
            k * (-a * b * origin.x + a * a * origin.y + (a * a * c) / b) - c / b
        }
      }
      if (this.line instanceof Segment) {
        const line = this.line
        if (this.line instanceof Line) {
          let isPositive = 1
          if (this.line.point1.x < this.line.point2.x && x < this.line.point1.x)
            isPositive = -1
          else if (
            this.line.point1.x > this.line.point2.x &&
            x > this.line.point1.x
          )
            isPositive = -1
          else if (
            this.line.point1.y < this.line.point2.y &&
            y < this.line.point1.y
          )
            isPositive = -1
          else if (
            this.line.point1.y > this.line.point2.y &&
            y > this.line.point1.y
          )
            isPositive = -1
          this.k =
            (isPositive *
              Math.hypot(x - this.line.point1.x, y - this.line.point1.y)) /
            Math.hypot(
              this.line.point2.x - this.line.point1.x,
              this.line.point2.y - this.line.point1.y,
            )
          //
        } else if (line instanceof Ray) {
          const ray = this.line as Ray
          if (ray.point1.x < ray.point2.x && x < ray.point1.x) this.k = 0
          else if (ray.point1.x > ray.point2.x && x > ray.point1.x) this.k = 0
          else if (ray.point1.y < ray.point2.y && y < ray.point1.y) this.k = 0
          else if (ray.point1.y > ray.point2.y && y > ray.point1.y) this.k = 0
          else
            this.k =
              Math.hypot(x - ray.point1.x, y - ray.point1.y) /
              Math.hypot(
                ray.point2.x - ray.point1.x,
                ray.point2.y - ray.point1.y,
              )
        } else if (line instanceof Segment) {
          const segment = this.line as Segment
          if (x < Math.min(segment.point1.x, segment.point2.x))
            this.k = segment.point1.x < segment.point2.x ? 0 : 1
          else if (x > Math.max(segment.point1.x, segment.point2.x))
            this.k = segment.point1.x < segment.point2.x ? 1 : 0
          else if (y < Math.min(segment.point1.y, segment.point2.y))
            this.k = segment.point1.y < segment.point2.y ? 0 : 1
          else if (y > Math.max(segment.point1.y, segment.point2.y))
            this.k = segment.point1.y < segment.point2.y ? 1 : 0
          else
            this.k =
              Math.hypot(x - segment.point1.x, y - segment.point1.y) /
              Math.hypot(
                segment.point2.x - segment.point1.x,
                segment.point2.y - segment.point1.y,
              )
        }
      }
    } catch (error) {
      console.error('Erreur dans le moveTo PointOnLine')
    }
    this.update()
  }

  toJSON(): object {
    return {
      ...this.jsonOptions(),
      idLine: this.line.id,
      k: this.k,
      label: this.label,
      shape: this.shape,
    }
  }

  update(): void {
    this._x =
      this.center.x + this.k * ((this.origin as Point).x - this.center.x)
    this._y =
      this.center.y + this.k * ((this.origin as Point).y - this.center.y)
    // La marque du segment doit être perpendiculaire à la droite donc on surcharge le update par défaut
    if (this.shape === '|') {
      const a = { x: this.line.point1.x, y: this.line.point1.y }
      const o = { x: this._x, y: this._y }
      const l = Math.hypot(
        this.line.point1.x - this._x,
        this.line.point1.y - this._y,
      )
      let { x: x1, y: y1 } = similitudeCoord(
        a,
        o,
        (((1 / l) * this.sizeInPixels) / this.figure.pixelsPerUnit) *
          Math.sqrt(2),
        90,
      )
      let { x: x2, y: y2 } = similitudeCoord(
        a,
        o,
        (((1 / l) * this.sizeInPixels) / this.figure.pixelsPerUnit) *
          Math.sqrt(2),
        -90,
      )
      // Le trait ne se trace pas si le point est confondu avec l'origine donc on "change d'origine" pour le calcul
      if (
        !Number.isFinite(x1) ||
        !Number.isFinite(x2) ||
        !Number.isFinite(y1) ||
        !Number.isFinite(y2)
      ) {
        const { x: newX1, y: newY1 } = similitudeCoord(
          o,
          a,
          (((1 / l) * this.sizeInPixels) / this.figure.pixelsPerUnit) *
            Math.sqrt(2),
          90,
        )
        const { x: newX2, y: newY2 } = similitudeCoord(
          o,
          a,
          (((1 / l) * this.sizeInPixels) / this.figure.pixelsPerUnit) *
            Math.sqrt(2),
          -90,
        )
        x1 = newX1
        x2 = newX2
        y1 = newY1
        y2 = newY2
      }
      if (
        Number.isFinite(x1) &&
        Number.isFinite(x2) &&
        Number.isFinite(y1) &&
        Number.isFinite(y2)
      ) {
        const x1Svg = this.figure.xToSx(x1).toString()
        const x2Svg = this.figure.xToSx(x2).toString()
        const y1Svg = this.figure.yToSy(y1).toString()
        const y2Svg = this.figure.yToSy(y2).toString()
        this.svgLine1.setAttribute('x1', x1Svg)
        this.svgLine1.setAttribute('y1', y1Svg)
        this.svgLine1.setAttribute('x2', x2Svg)
        this.svgLine1.setAttribute('y2', y2Svg)
        this.groupSvg.appendChild(this.svgLine1)
      } else console.error('Erreur dans le update de PointOnLine')
    }
    super.update()
  }

  get description(): string {
    return `Point sur ${this.line.notation}`
  }

  get x(): number {
    return this._x
  }

  set x(x: number) {
    const [a, b, c] = this.line.equation
    const y = (-a * x - c) / b
    this.moveTo(x, y)
  }

  get y(): number {
    return this._y
  }

  set y(y: number) {
    const [a, b, c] = this.line.equation
    const x = (-b * y - c) / a
    this.moveTo(x, y)
  }
}

export default PointOnLine
