import type Figure from '../../Figure'
import type { OptionsElement2D } from '../interfaces'
import type Point from '../points/Point'

import Distance from '../../dynamicNumbers/Distance'
import type Circle from './Circle'
import CircleCenterDynamicRadius from './CircleCenterDyamicRadius'
import type Segment from './Segment'

/**
 * Trace un cercle défini par un centre et une distance dynamique
 * Cela servira au report de longueur ou pour tracer un cercle dont le rayon est géré par un curseur
 */
class CircleCenterPoint extends CircleCenterDynamicRadius {
  point: Point
  constructor(
    figure: Figure,
    {
      center,
      point,
      ...options
    }: { center: Point; point: Point } & OptionsElement2D,
  ) {
    const radius = new Distance(figure, {
      isChild: true,
      point1: center,
      point2: point,
    })
    super(figure, { center, radius, ...options })
    this.type = 'CircleCenterPoint'
    this.point = point
    this.point.subscribe(this)
  }

  dilate(
    center: Point,
    factor: number,
    options?: OptionsElement2D,
  ): CircleCenterPoint {
    const newCenter = this.center.dilate(center, factor, {
      isChild: true,
      isVisible: false,
    })
    const newPoint = this.point.dilate(center, factor, {
      isChild: true,
      isVisible: false,
    })
    const newCircle = this.figure.create('CircleCenterPoint', {
      center: newCenter,
      point: newPoint,
      ...options,
    })
    newCircle.transformation = { center, factor, origin: this, type: 'dilate' }
    return newCircle
  }

  reflect(center: Point, options?: OptionsElement2D): CircleCenterPoint {
    const newCenter = this.center.reflect(center, {
      isChild: true,
      isVisible: false,
    })
    const newPoint = this.point.reflect(center, {
      isChild: true,
      isVisible: false,
    })
    const newCircle = this.figure.create('CircleCenterPoint', {
      center: newCenter,
      point: newPoint,
      ...options,
    })
    newCircle.transformation = { center, origin: this, type: 'reflect' }
    return newCircle
  }

  reflectOverLine(
    line: Segment,
    options?: OptionsElement2D,
  ): CircleCenterPoint {
    const newCenter = this.center.reflectOverLine(line, {
      isChild: true,
      isVisible: false,
    })
    const newPoint = this.point.reflectOverLine(line, {
      isChild: true,
      isVisible: false,
    })
    const newCircle = this.figure.create('CircleCenterPoint', {
      center: newCenter,
      point: newPoint,
      ...options,
    })
    newCircle.transformation = { line, origin: this, type: 'reflectOverLine' }
    return newCircle
  }

  rotate(center: Point, angle: number, options?: OptionsElement2D): Circle {
    const newCenter = this.center.rotate(center, angle, {
      isChild: true,
      isVisible: false,
    })
    const newPoint = this.point.rotate(center, angle, {
      isChild: true,
      isVisible: false,
    })
    const newCircle = this.figure.create('CircleCenterPoint', {
      center: newCenter,
      point: newPoint,
      ...options,
    })
    newCircle.transformation = { center, angle, origin: this, type: 'rotation' }
    return newCircle
  }

  toJSON(): object {
    return {
      ...this.jsonOptions(),
      fillColor: this.fillColor,
      fillOpacity: this.fillOpacity,
      idCenter: this.center.id,
      idPoint: this.point.id,
    }
  }

  translate(
    point1: Point,
    point2: Point,
    options?: OptionsElement2D,
  ): CircleCenterPoint {
    const newCenter = this.center.translate(point1, point2, {
      isChild: true,
      isVisible: false,
    })
    const newPoint = this.point.translate(point1, point2, {
      isChild: true,
      isVisible: false,
    })
    const newCircle = this.figure.create('CircleCenterPoint', {
      center: newCenter,
      point: newPoint,
      ...options,
    })
    newCircle.transformation = {
      origin: this,
      point1,
      point2,
      type: 'translation',
    }
    return newCircle
  }

  get description(): string {
    if (this.transformation?.type === 'reflect') {
      return `Image du cercle $${(this.transformation.origin as Circle).notation}$ dans la symétrie de centre $${this.transformation?.center.notation}$`
    }
    if (this.transformation?.type === 'reflectOverLine') {
      return `Image du cercle $${(this.transformation.origin as Circle).notation}$ dans la symétrie d'axe $${this.transformation?.line.notation}$`
    }
    if (this.transformation?.type === 'dilate') {
      return `Image du cercle $${(this.transformation.origin as Circle).notation}$ dans l'homothétie de centre $${this.transformation?.center.notation}$ et de rapport $${this.transformation.factor}$`
    }
    if (this.transformation?.type === 'rotation') {
      return `Image du cercle $${(this.transformation.origin as Circle).notation}$ dans la rotation de centre $${this.transformation.center.notation}$ et d'angle $${Math.abs(this.transformation.angle)}\\degree$ dans le sens ${this.transformation.angle > 0 ? 'anti-horaire' : 'horaire'}`
    }
    if (this.transformation?.type === 'translation') {
      return `Image du cercle $${(this.transformation.origin as Circle).notation}$ dans la translation qui transforme $${this.transformation.point1.notation}$ en $${this.transformation.point2.notation}$`
    }
    const centerName =
      this.center.label !== '' ? this.center.label : this.center.id
    const pointName = this.point.label !== '' ? this.point.label : this.point.id
    return `Cercle de centre ${centerName} et passant par le point ${pointName}`
  }
}

export default CircleCenterPoint
