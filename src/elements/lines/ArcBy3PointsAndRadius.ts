import type Figure from '../../Figure'
import type Point from '../points/Point'

import type { OptionsElement2D } from '../interfaces'
import Arc from './Arc'

class ArcBy3PointsAndRadius extends Arc {
  radius: number
  constructor(
    figure: Figure,
    {
      addBorders = true,
      center,
      end,
      radius,
      start,
      ...options
    }: {
      addBorders?: boolean
      center: Point
      end: Point
      radius: number
      start: Point
    } & OptionsElement2D,
  ) {
    const tempLine = figure.create('Segment', {
      isChild: true,
      isSelectable: true,
      isVisible: false,
      point1: start,
      point2: center,
    })
    const newStart = figure.create('PointOnLineAtDistance', {
      distance: radius,
      isChild: true,
      isVisible: false,
      line: tempLine,
      shape: start.shape,
    })
    const dynamicAngle = figure.create('Angle', {
      center,
      end,
      start: newStart,
    })
    super(figure, {
      addBorders,
      center,
      dynamicAngle,
      start: newStart,
      ...options,
    })
    this.radius = radius
  }
}

export default ArcBy3PointsAndRadius
