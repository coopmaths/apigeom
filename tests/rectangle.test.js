import { expect, test } from 'vitest'
import Figure from '../src/Figure'

const figure = new Figure()
const A = figure.create('Point', { label: 'A', x: 0, y: 12 })
const B = figure.create('Point', { label: 'B', x: 5, y: 16 })
const sAB = figure.create('Segment', { point1: A, point2: B })
const dBC = figure.create('LinePerpendicular', { line: sAB, point: B })
const dAD = figure.create('LinePerpendicular', { line: sAB, point: A })
const C = figure.create('PointOnLine', {
  k: 0.5,
  label: 'C',
  line: dBC,
  shape: 'x',
})
const dCD = figure.create('LinePerpendicular', { line: dBC, point: C })
figure.create('PointIntersectionLL', { label: 'D', line1: dAD, line2: dCD })

test('Rectangle', () => {
  const { isValid } = figure.checkAngle({
    angle: 90,
    label1: 'A',
    label2: 'B',
    label3: 'C',
  })
  expect(isValid).toBe(true)
  const { isValid: isValid2 } = figure.checkAngle({
    angle: 90,
    label1: 'B',
    label2: 'C',
    label3: 'D',
  })
  expect(isValid2).toBe(true)
  const { isValid: isValid3 } = figure.checkAngle({
    angle: 90,
    label1: 'C',
    label2: 'D',
    label3: 'A',
  })
  expect(isValid3).toBe(true)
  const { isValid: isValid4 } = figure.checkAngle({
    angle: 90,
    label1: 'D',
    label2: 'A',
    label3: 'B',
  })
  expect(isValid4).toBe(true)
})
