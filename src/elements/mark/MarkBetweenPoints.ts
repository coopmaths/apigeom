import type Figure from '../../Figure'
import Element2D from '../Element2D'
import { angleWithHorizontalInDegres } from '../calculus/Coords'
import type Point from '../points/Point'

import TextByPosition from '../text/TextByPosition'

class MarkSegment extends TextByPosition {
  point1: Point
  point2: Point
  constructor(
    figure: Figure,
    {
      color = figure.options.color,
      fontSize = figure.options.fontSize,
      point1,
      point2,
      text = figure.options.mark,
    }: {
      color?: string
      fontSize?: string
      point1: Point
      point2: Point
      text?: string
    },
  ) {
    const x = (point1.x + point2.x) / 2
    const y = (point1.y + point2.y) / 2
    super(figure, { color, fontSize, text, x, y })
    this.type = 'MarkSegment'
    this.fontSize = '30px'
    this._color = color
    this.point1 = point1
    this.point2 = point2
    this.point1.subscribe(this)
    this.point2.subscribe(this)
    const angle = angleWithHorizontalInDegres(this.point1, this.point2)
    this.angleWithHorizontalInDegres = -angle
    this.type = 'MarkBetweenPoints'
    this.erasePreviousMarks()
  }

  draw(): void {
    super.draw()
    this.update()
  }

  erasePreviousMarks(): void {
    const marks = [...this.figure.elements.values()].filter(
      (e) => e instanceof Element2D && e.type === 'MarkBetweenPoints',
    ) as MarkSegment[]
    for (const mark of marks) {
      if (mark.id === this.id) {
        continue
      }
      if (
        mark.point1.id === this.point1.id &&
        mark.point2.id === this.point2.id
      ) {
        mark.remove()
      } else if (
        mark.point1.id === this.point2.id &&
        mark.point2.id === this.point1.id
      ) {
        mark.remove()
      }
    }
  }

  update(): void {
    this.x = (this.point1.x + this.point2.x) / 2
    this.y = (this.point1.y + this.point2.y) / 2
    if (this.div !== undefined) {
      if (this.text !== '||' && this.text !== '|||') {
        this.div.style.fontFamily = 'Courier New'
        // Police qui permet de centrer verticalement les symboles
        // mais qui met trop d'espacement entre les | pour ||
      }
      this.div.style.fontWeight = 'bold'
      this.div.style.fontSize = this.fontSize
      this.div.style.transformOrigin = 'center center'
      this.div.style.transformBox = 'fill-box'
      this.div.style.transform = 'translate(-50%, -50%)'
      this.div.classList.add('-tracking-2')
      const angle = angleWithHorizontalInDegres(this.point1, this.point2)
      this.div.style.transform += `rotate(${-angle}deg)`
    }
    this.notify()
  }

  toJSON(): object {
    return {
      ...this.jsonOptions(),
      color: this.color,
      text: this.text,
      idPoint1: this.point1.id,
      idPoint2: this.point2.id,
    }
  }
}

export default MarkSegment
