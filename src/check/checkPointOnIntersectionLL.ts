import type Figure from '../Figure'
import type Line from '../elements/lines/Line'
import PointIntersectionLL from '../elements/points/PointIntersectionLL'

/**
 * Vérifie si un point appartient à une intersection de deux lignes (droites, demi-droites ou segments)
 * @param {Figure} figure // La figure
 * @param {string} labelPt // Nom du point
 * @param {string[]|string} nameLine1 // Nom de la ligne1 ou bien tableau contenant les noms de la ligne1
 * @param {string[]|string} nameLine2 // Nom de la ligne2 ou bien tableau contenant les noms de la ligne2
 * @example Voir exemples plus adaptés dans Figure.ts
 * @author Eric Elter
 * @returns {{ isValid: boolean, message: string }}
 */
export default function checkPointOnIntersectionLL({
  figure,
  labelPt,
  nameLine1,
  nameLine2,
}: {
  figure: Figure
  labelPt: string
  nameLine1: string[] | string
  nameLine2: string[] | string
}): { isValid: boolean; message: string } {
  let points = [...figure.elements.values()].filter(
    (e) =>
      e.type !== 'pointer' &&
      e.type.includes('Point') &&
      'label' in e &&
      e.label === labelPt,
  )
  if (points.length === 0)
    return {
      isValid: false,
      message: `Il n'existe pas de point ${labelPt} sur la figure.`,
    }
  if (points.length > 1)
    return {
      isValid: false,
      message: `Il existe trop de points ${labelPt} sur la figure.`,
    }

  const lines = [...figure.elements.values()].filter(
    (e) =>
      e.type.includes('Line') ||
      e.type.includes('Segment') ||
      e.type.includes('Ray'),
  ) as Line[]
  let lines1: Line[]
  let lines2: Line[]

  const nomLigne1 = Array.isArray(nameLine1) ? nameLine1[0] : nameLine1
  const nomLigne2 = Array.isArray(nameLine2) ? nameLine2[0] : nameLine2

  if (lines.length !== 0) {
    lines1 = lines.filter((e) => nameLine1.includes(e.notation))
    if (lines1.length === 0)
      return {
        isValid: false,
        message: `${nomLigne1} n'existe pas sur la figure.`,
      }
    lines2 = lines.filter((e) => nameLine2.includes(e.notation))
    if (lines2.length === 0)
      return {
        isValid: false,
        message: `${nomLigne2} n'existe pas sur la figure.`,
      }
  }

  points = [...figure.elements.values()].filter(
    (e) =>
      e instanceof PointIntersectionLL &&
      e.type !== 'pointer' &&
      e.type.includes('PointIntersectionLL') &&
      'label' in e &&
      e.label === labelPt &&
      lines1.some((line) => e.line1 === line || e.line2 === line) &&
      lines2.some((line) => e.line1 === line || e.line2 === line),
  )
  if (points.length === 0)
    return {
      isValid: false,
      message: `Le point ${labelPt} n'est pas à l'intersection de ${nomLigne1} et de ${nomLigne2}.`,
    }
  return {
    isValid: true,
    message: `Le point ${labelPt} est à l'intersection de ${nomLigne1} et de ${nomLigne2}.`,
  }
}
