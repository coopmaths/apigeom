import type Figure from '../../Figure'
import type Polygon from '../lines/Polygon'
import type Point from '../points/Point'

import Element2D from '../Element2D'

class RectangleFractionDiagram extends Element2D {
  denominator: number
  gap: number
  height: number
  numberOfRectangles: number
  _indicesRectanglesInColor: Set<number> = new Set()
  origin: Point
  origins: Point[] = []
  rectangles: Polygon[] = []
  width: number
  constructor(
    figure: Figure,
    {
      denominator = 4,
      gap = 1,
      height = 1,
      numberOfRectangles = 5,
      indicesRectanglesInColor = new Set(),
      numerator = 0,
      width = 4,
      x = 0,
      y = 0,
      ...options
    }: {
      denominator?: number
      gap?: number
      height?: number
      indicesRectanglesInColor?: Set<number>
      numberOfRectangles?: number
      numerator?: number
      width?: number
      x?: number
      y?: number
    } = {},
  ) {
    super(figure, { ...options })
    this.origin = this.figure.create('Point', {
      isChild: true,
      isFree: false,
      shape: '',
      x,
      y,
    })
    this.width = width
    this.height = height
    this.denominator = denominator
    this.gap = gap
    this.numberOfRectangles = numberOfRectangles
    this.numerator = numerator
    if (numerator !== undefined && indicesRectanglesInColor.size === 0) {
      for (let i = 0; i < numerator; i++) {
        this.indicesRectanglesInColor.add(i)
      }
    } else {
      this.indicesRectanglesInColor = indicesRectanglesInColor
    }
    this.type = 'RectangleFractionDiagram'
  }

  draw(): void {
    this.origins = [this.origin]
    this.rectangles = []
    const fractionWidth = this.width / this.denominator
    for (let n = 0; n < this.numberOfRectangles; n++) {
      if (n > 0)
        this.origins.push(
          this.figure.create('Point', {
            isChild: true,
            isSelectable: false,
            shape: '',
            x: this.origin.x + n * (this.width + this.gap),
            y: this.origin.y,
          }),
        )
      for (let i = 0; i < this.denominator; i++) {
        this.rectangles.push(
          createRectangleByOriginWidthAndHeight({
            height: this.height,
            origin: this.origins[n],
            width: fractionWidth,
            xTranslate: i * fractionWidth,
          }),
        )
      }
    }
    for (const rectangle of this.rectangles) {
      rectangle.distancePointer = function (_: number, __: number): number {
        if (
          this.figure.currentState === 'FILL' &&
          this.figure.pointer.isInPolygon(this)
        )
          return 0
        return Number.POSITIVE_INFINITY
      }
    }
    this.indicesRectanglesInColor = this._indicesRectanglesInColor
  }

  redraw(): void {
    for (const origin of this.origins) {
      origin.remove()
    }
    for (const rectangle of this.rectangles) {
      rectangle.remove()
    }
    this.draw()
  }

  toJSON(): object {
    return {
      ...this.jsonOptions(),
      denominator: this.denominator,
      numerator: this.numerator,
      arrayIndicesRectanglesInColor: Array.from(this.indicesRectanglesInColor),
      height: this.height,
      width: this.width,
      gap: this.gap,
      x: this.origin.x,
      y: this.origin.y,
    }
  }

  get indicesRectanglesInColor(): Set<number> {
    for (let i = 0; i < this.rectangles.length; i++) {
      if (this.rectangles[i].fillColor !== 'none') {
        this._indicesRectanglesInColor.add(i)
      } else {
        this._indicesRectanglesInColor.delete(i)
      }
    }
    return this._indicesRectanglesInColor
  }

  set indicesRectanglesInColor(indicesRectanglesInColor: Set<number>) {
    this._indicesRectanglesInColor = indicesRectanglesInColor
    this.rectangles.forEach((rectangle, index) => {
      if (this._indicesRectanglesInColor.has(index)) {
        rectangle.fillColor = this.color
      } else {
        rectangle.fillColor = 'none'
      }
    })
  }

  get numerator(): number {
    return this.rectangles.filter((rectangle) => rectangle.fillColor !== 'none')
      .length
  }

  set numerator(numerator: number) {
    this.rectangles.forEach((rectangle, index) => {
      if (index < numerator) {
        rectangle.fillColor = this.color
      } else {
        rectangle.fillColor = 'none'
      }
    })
  }
}

function createRectangleByOriginWidthAndHeight({
  height,
  origin,
  width,
  xTranslate,
}: {
  height: number
  origin: Point
  width: number
  xTranslate: number
}): Polygon {
  const newPoints = []
  const figure = origin.figure
  newPoints.push(
    figure.create('Point', {
      isChild: true,
      isFree: false,
      shape: '',
      x: origin.x + xTranslate,
      y: origin.y,
    }),
  )
  newPoints.push(
    figure.create('Point', {
      isChild: true,
      isFree: false,
      shape: '',
      x: origin.x + xTranslate + width,
      y: origin.y,
    }),
  )
  newPoints.push(
    figure.create('Point', {
      isChild: true,
      isFree: false,
      shape: '',
      x: origin.x + xTranslate + width,
      y: origin.y + height,
    }),
  )
  newPoints.push(
    figure.create('Point', {
      isChild: true,
      isFree: false,
      shape: '',
      x: origin.x + xTranslate,
      y: origin.y + height,
    }),
  )
  return figure.create('Polygon', { isChild: true, points: newPoints })
}

export default RectangleFractionDiagram
