import renderMathInElement from 'katex/dist/contrib/auto-render.js'

import type Figure from '../Figure'

import Element2D from '../elements/Element2D'
import {
  colors,
  defaultDeltaXModal,
  defaultDistanceClick
} from '../elements/defaultValues'
import Circle from '../elements/lines/Circle'
import Segment from '../elements/lines/Segment'
import Point from '../elements/points/Point'
import TextByPosition from '../elements/text/TextByPosition'
import { distance } from '../elements/calculus/Coords'

export default function handlePointerAction(
  figure: Figure,
  event: PointerEvent,
): void {
  const [pointerX, pointerY] = figure.getPointerCoord(event)
  const [x, y] = [pointerX, pointerY]
  if (x > figure.xMax || x < figure.xMin || y > figure.yMax || y < figure.yMin)
    return
  if (figure.currentState === 'POINT') {
    const pointsOnFigure = [...figure.elements.values()].filter((e) =>
      e.type.includes('Point'),
    ) as Point[]
      for (const point of  pointsOnFigure) {
        if (distance({x, y}, {x: point.x, y: point.y}) < figure.options.distanceWithoutNewPoint) {
          return
        }
      }
  }
  const possibleElements = []
  figure.modal?.remove()
  const elements = [...figure.elements.values()].filter(
    (e) => e instanceof Element2D,
  ) as Element2D[]
  const elementsFiltered = elements
    .filter(figure.filter)
    .filter((e) => e.type !== 'pointer' && e.isVisible && e.isSelectable)
  for (const element of elementsFiltered) {
    if (
      element.distancePointer(pointerX, pointerY) * figure.pixelsPerUnit <
        defaultDistanceClick &&
      element.isSelectable
    ) {
      possibleElements.push(element)
    }
  }
  if (possibleElements.length === 1) {
    sendToMachine(figure, { element: possibleElements[0], x, y })
  } else if (
    figure.currentState === 'POINT_INTERSECTION' &&
    possibleElements.length === 2 &&
    (possibleElements[0] instanceof Segment ||
      possibleElements[0] instanceof Circle) &&
    (possibleElements[1] instanceof Segment ||
      possibleElements[1] instanceof Circle)
  ) {
    sendToMachine(figure, { element: undefined, possibleElements, x, y })
  } else if (possibleElements.length > 1) {
    const points = possibleElements.filter((e) => e instanceof Point)
    if (points.length === 1) {
      sendToMachine(figure, { element: points[0], x, y })
      return
    }
    const elementText = new TextByPosition(figure, {
      isChild: true,
      isSelectable: false,
      text: '',
      x: pointerX + defaultDeltaXModal,
      y: Math.min(pointerY, figure.yMax - 2),
    })
    elementText.draw()
    figure.modal = elementText.div
    figure.modal.style.pointerEvents = 'auto'
    figure.modal.style.padding = '10px'
    figure.modal.style.overflowY = 'auto'
    figure.modal.style.backgroundColor = colors.titlelight
    figure.modal.style.color = colors.title
    figure.modal.style.boxShadow =
      '0 4px 6px -1px rgb(0 0 0 / 0.1), 0 2px 4px -2px rgb(0 0 0 / 0.1)'
    figure.modal.style.touchAction = 'none'
    figure.modal.innerText = 'Quel élément souhaitez-vous sélectionner ?'
    const divs = []
    for (const element of possibleElements) {
      const div = document.createElement('div')
      div.innerText =
        element.description !== ''
          ? element.description
          : `${element.type} ${element.id}`
      div.addEventListener('click', () => {
        elementText.remove()
        sendToMachine(figure, { element, x, y })
      })
      div.addEventListener('mouseenter', () => {
        div.style.backgroundColor = colors.lightest
      })
      div.addEventListener('mouseleave', () => {
        div.style.backgroundColor = colors.titlelight
      })
      div.style.marginTop = '5px'
      div.style.marginLeft = '10px'
      div.style.userSelect = 'none'
      div.style.cursor = 'default'
      divs.push(div)
      renderMathInElement(div, {
        delimiters: [
          { display: true, left: '\\[', right: '\\]' },
          { display: false, left: '$', right: '$' },
        ],
        errorColor: '#CC0000',
        preProcess: (chaine: string) =>
          chaine.replaceAll(String.fromCharCode(160), '\\,'),
        strict: 'warn',
        throwOnError: true,
        trust: false,
      })
    }
    for (const div of divs) {
      figure.modal.appendChild(div)
    }
    sendToMachine(figure, {
      element: undefined,
      possibleElements,
      waitingWithModal: true,
      x,
      y,
    })
  } else {
    sendToMachine(figure, { element: undefined, x, y })
  }
}

function sendToMachine(
  figure: Figure,
  {
    element,
    possibleElements,
    waitingWithModal,
    x,
    y,
  }: {
    element?: Element2D
    possibleElements?: Element2D[]
    waitingWithModal?: boolean
    x: number
    y: number
  },
): void {
  if (figure.ui !== undefined) {
    figure.ui.send('clickLocation', {
      element,
      possibleElements,
      waitingWithModal: waitingWithModal ?? false,
      x,
      y,
    })
  }
}
