import { interpret } from 'xstate'

import Figure from './Figure'
import ui from './uiMachine'

// On créé le svg dans un div
const div = document.querySelector('#app') as HTMLDivElement
const figure = new Figure({ yMin: -10, xMin: -10, width: 600, height: 600 })
// const figure = new Figure()
figure.options.labelAutomaticBeginsWith = 'A'
figure.options.color = 'blue'
figure.options.automaticUserMessage = true
figure.snapGrid = false
// figure.pixelsPerUnit = 10

const machineWithContext = ui.withContext({
  figure,
  temp: { elements: [], htmlElement: [], values: [] },
})
figure.ui = interpret(machineWithContext).start()
// Création de la figure
figure.setContainer(div)
if (window.location.hostname === 'localhost') {
  figure.autoAdjustSize()
  figure.divToolbar2.appendChild(figure.addButtons('INFORMATIONS')[0])
}
