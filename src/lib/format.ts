export function round(value: number, precision = 4): number {
  return Math.round(value * 10 ** precision) / 10 ** precision
}

export function displayNumber(value: number, precision = 4): string {
  if (round(value, precision).toLocaleString('fr-FR') === '-0') return '0'
  return round(value, precision).toLocaleString('fr-FR').replace(',', '{,}')
}
