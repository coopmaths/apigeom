/* eslint-disable @typescript-eslint/no-misused-promises */
import { type AnyEventObject, createMachine } from 'xstate'

import type Figure from './Figure'
import type Grid from './elements/grid/Grid'

import Element2D from './elements/Element2D'
import { distance } from './elements/calculus/Coords'
import Graph from './elements/calculus/Graph'
import {
  defaultZoomInFactor,
  defaultZoomOutFactor,
  orangeMathaleaLight,
} from './elements/defaultValues'
import Arc from './elements/lines/Arc'
import Circle from './elements/lines/Circle'
import Polygon from './elements/lines/Polygon'
import Segment from './elements/lines/Segment'
import Point from './elements/points/Point'
import {
  createDialoxBoxAngle,
  createDialoxBoxK,
  createDialoxBoxName,
  createDialoxBoxRadius,
  createModalDescription,
} from './userInterface/handleDialog'
import selectionRectangle from './userInterface/selectionRectangle'

interface MyContext {
  figure: Figure
}

export type eventName =
  | 'ANGLE'
  | 'BISSECTOR_BY_POINTS'
  | 'CIRCLE_CENTER_POINT'
  | 'CIRCLE_RADIUS'
  | 'clickLocation'
  | 'COLOR'
  | 'DESCRIPTION'
  | 'DILATE_COEF'
  | 'DILATE'
  | 'DISC_CENTER_POINT'
  | 'DISC_RADIUS'
  | 'DOWNLOAD_LATEX_SVG'
  | 'DRAG'
  | 'FILL'
  | 'GRID'
  | 'HIDE'
  | 'INFORMATIONS'
  | 'INIT'
  | 'LATEX'
  | 'LINE_PARALLEL'
  | 'LINE_PERPENDICULAR'
  | 'LINE'
  | 'MARK_BETWEEN_POINTS'
  | 'MIDDLE'
  | 'MOVE_LABEL'
  | 'NAME_POINT'
  | 'OPEN'
  | 'PAUSE'
  | 'PERPENDICULAR_BISECTOR'
  | 'PLAY_SKIP_BACK'
  | 'PLAY_SKIP_FORWARD'
  | 'PLAY'
  | 'POINT_INTERSECTION'
  | 'POINT_ON'
  | 'POINT'
  | 'POLYGON'
  | 'RADIUS'
  | 'RAY'
  | 'REDO'
  | 'REFLECTION_OVER_LINE'
  | 'REFLECTION'
  | 'REMOVE'
  | 'RESTART'
  | 'ROTATE'
  | 'SAVE'
  | 'SEGMENT'
  | 'SELECTION_AREA_TO_LATEX'
  | 'SELECTION_AREA_TO_SVG'
  | 'SET_OPTIONS'
  | 'SHAKE'
  | 'TEXT_FROM_DIALOG'
  | 'TRANSLATION'
  | 'UNDO'
  | 'VECTOR'
  | 'ZOOM_IN'
  | 'ZOOM_OUT'

export type eventOptions =
  | { angle: number }
  | { coefficient: number }
  | {
      element?: Element2D
      possibleElements?: Element2D[]
      waitingWithModal: boolean
      x: number
      y: number
    }
  | { height: number; width: number; xMin: number; yMax: number }
  | { radius: string }
  | { text: string }
  | { text: string }

interface MyEvent extends AnyEventObject {
  element?: Element2D
  text?: string
  waitingWithModal?: boolean
  x?: number
  y?: number
}

interface Context {
  figure: Figure
  intervalId?: number
  temp: {
    elements: Element2D[]
    htmlElement: HTMLElement[]
    values: number[]
  }
}

const ui = createMachine<Context>(
  {
    /* eslint-disable-next-line @typescript-eslint/consistent-type-assertions */
    context: {
      figure: {} as Figure,
      temp: { elements: [], htmlElement: [], values: [] },
    },
    entry: ['highlightButton', 'eraseTempElements', 'eraseUserMessage'],
    id: 'apiGeomUI',
    initial: 'INIT',
    on: {
      BISECTOR_BY_POINTS: 'BISECTOR_BY_POINTS',
      CIRCLE_CENTER_POINT: 'CIRCLE_CENTER_POINT',
      CIRCLE_RADIUS: 'CIRCLE_RADIUS',
      COLOR: 'COLOR',
      DESCRIPTION: 'DESCRIPTION',
      DILATE: 'DILATE',
      DISC_CENTER_POINT: 'DISC_CENTER_POINT',
      DISC_RADIUS: 'DISC_RADIUS',
      DOWNLOAD_LATEX_SVG: 'DOWNLOAD_LATEX_SVG',
      DRAG: 'DRAG',
      FILL: 'FILL',
      GRID: 'GRID',
      HIDE: 'HIDE',
      INFORMATIONS: 'INFORMATIONS',
      LATEX: 'LATEX',
      LINE: 'LINE',
      LINE_PARALLEL: 'LINE_PARALLEL',
      LINE_PERPENDICULAR: 'LINE_PERPENDICULAR',
      MARK_BETWEEN_POINTS: 'MARK_BETWEEN_POINTS',
      MIDDLE: 'MIDDLE',
      MOVE_LABEL: 'MOVE_LABEL',
      NAME_POINT: 'NAME_POINT',
      OPEN: 'OPEN',
      PAUSE: 'PAUSE',
      PERPENDICULAR_BISECTOR: 'PERPENDICULAR_BISECTOR',
      PLAY: 'PLAY',
      PLAY_SKIP_BACK: 'PLAY_SKIP_BACK',
      PLAY_SKIP_FORWARD: 'PLAY_SKIP_FORWARD',
      POINT: 'POINT',
      POINT_INTERSECTION: 'POINT_INTERSECTION',
      POINT_ON: 'POINT_ON',
      POLYGON: 'POLYGON',
      RAY: 'RAY',
      REDO: 'REDO',
      REFLECTION: 'REFLECTION',
      REFLECTION_OVER_LINE: 'REFLECTION_OVER_LINE',
      REMOVE: 'REMOVE',
      RESTART: 'RESTART',
      ROTATE: 'ROTATE',
      SAVE: 'SAVE',
      SEGMENT: 'SEGMENT',
      SET_OPTIONS: 'SET_OPTIONS',
      SHAKE: 'SHAKE',
      TRANSLATION: 'TRANSLATION',
      UNDO: 'UNDO',
      VECTOR: 'VECTOR',
      ZOOM_IN: 'ZOOM_IN',
      ZOOM_OUT: 'ZOOM_OUT',
    },
    /** @xstate-layout N4IgpgJg5mDOIC5QEMAOBLA4mA9gWwFUBJAYgAUB5IgOQBUBtABgF1FRUdZ0AXdHAOzYgAHogAsAJgA0IAJ7iAjBIB0CgMwB2BQA4NAVgC+BmWiy5CpADI0Aok1ZIQHLrwFDRCSTPkINYxsoSemKahsYgptj4xCQAyjaYALI2dPZCzjx8go4eXnKICowAbEXKYoVFQUYmGFEWJAAiAEoAgphpjhmu2aC50vkI6kV6ysOMEozaatMzEtURteYxZDZNK9QNRADCBJYtTR3snJluOeL9PgpF46piYTVm0aRk+y2WljaWh07H3e7n3gKAE5GGJlNpGEp7gtHvVKJYAJqYCjUb5dLL-TwXRASCRA0qaIGheaRJakLZEJpbD5o34Ys5YwEIIISUraCTlEpcrnaEmLJ4kLYUSwUA4sdJ0069AEDCQaNQBEG4oFXblFXnhUkClZrFKbHZ7JoAfQAQkRYvEtrRRbSXPTpYzZUSwUCIZy1eq+bCYjRaKtLbQiCjbSceiIZT41KyFKo1B7uRqHnVllQ6Ebg+LOpKw30mQoVcpIWINCr4xovcmiMpmm0SABjAA26DrAGtLDg68huiG-gy1HoRkENNooXmORoytpdNCtRZlJRfcoAO7ITL8KAAMRwACd253uvWm62912sj37eHmYxB9oQZU9Ez-CNGK6oRWycp4kkUrRl6veOuW7bhu6DbrA3A2A2YB4GA-DcIezZth2p4COeUqXiEKhiNoIT6HmCiFMoKrFFUmr8nOX7JHQf5rpuO6xGAdYCBAkHQbB8GNohJ7dpmRx2uhuRRpOuEPgMhQKGC0ykUmH4UlSHw0QBdHbls7FgNuCHHshPEOHxoaYpoMZ3Goo6ynoWjKFG7oeomMKVsocnUjYinoIBO5kDgrkcUeSH7mevE-PxOaIIZZR6CZQRMuOYJ+Nc1k8u+TzKNY1DOSutFASBYEQVBMFwZpvkofwaHBQgahAioI6RQMELaIEwTEmR3pVilaX-q5ykMUx-Asbl7EFdx-m6YF+l9hV4KmT4WgxkUGj3olc46us+q7PsLlucBoHgaxeXeVx2lDRKQWYthYISDoeFiUUYilNdM7kcQ86rMt2yrU062dYxzE7f1nFaX5qEBeiAniDhgQXaJlwDgExTqvoC2PS8rTvJ8H2ZVtOVsflf2FTpR2jQ6ISSSJeZAmIQKWZC0l2R+SNvB8lho-RX09T92M+YNgPDcDpVE5ZJNidoegqBoEgRfdzVPbqGyvYaprmgGopM5t2Vs3t-1FSVmLmaUKr+PeUVAiWk6gqqaq2bOiPPXqsv7PLFo2FaSvpUpQFdd9fXs-tAPFUD2aYtdahEXNegG7KU4qEqeJmwlTX2fCSIonE1pkAnyKon7x0MhMpTmVVkM4hIUzgiWjUyUlacosork8CQWsMgOMYh2HPjncbuhEvDce08KifUB9asDQdXP472DpTDFagC63Vx1VcJYx7H5eLb36cD576u42eCjc-7Dd6E3wwt4XegBCWcWL8MCNVpX-exCnt915nBOXo3yjN9VrcKjGeIXx6Ev2V9P6R2gYq4uw6ujVWG8h4+3roTIoFNyokQLmVKMFMhjxhKBbB6VYgFNADEGfu4CNru1ZtAnGnNfa7yzvAxBd5P4hTFioAcQt4pYOvvOVMtB0zUBgZrZ+Y9Lz9kHHnSaiARzKGFkYcI-AcAQDgEIS2RBR4Xg8AAWiKEyNRIxXSMD0fogxeixAcJrJgFRINBg3SIudaceYp4U1htODhC46DmNKuoOe+sGHMg0AEPQr5qZKM4YuYhylKFuMxDoUoElkFMlPrdOagScGfgSFRWgESGQKH8aMKM+cmQVTZFGABH5KI-mVllbaG8MkOgmBOIouSxGDHGBOYc7Jo5qnLN3JKpTqKhLdizXqWNuDVNfiCVQ3jCgKkCBHdp3JOnL0eo5D4IyPAxMpjdWxso1C6ECDdU2NkOFLLahlHcqk4LqRWQUKc6y4YoI5NciYCgNC+KmDMaYhzKROWVh5LylyEDCwcZ3O55Mz6QmeZMN57yulzlan8giFN8QvlCHE55qhEnFKSq1cpGM1ZwtmkRa4QKmSixGOofMC8PQcKxX05m3VBm7T+dDMK4tiVyhyeS0sapnHWxlgafYfzzoxjjFoTZlwbrMKwV3BZN8eUrUNNiqBQyBXZOFRDPMocg7C1ZPsuZ3LpZyrWjS7cpD6XsT+S86xaqxKh1KOMbVbD5k0wrq8FGlg8VGQqlay4IdAhizfNCxGLqGYKsqUqrMNDLwTApuTGxl1LjaBKGiqcwRF6OqCXTV1ysTW4vDS-DwwwHEbLjVc9UoxWkpo6Xql6fLjRmgdk7JofyFRglibKEsIx-DKkvtgyWS0bY1vtord6RqKmYwZbmwRHh+wqFba3W8BI9AQgmK8t5Vb+1vUHSA527USEDJzXpSd4jISjCeV6nEt46pCyXRCyFzjV4ojhYuyRw5Gk5wnB3MuTqV6IjXjXYZE7VEhR2fKaeOIskjEJJ+9N96iE7qgPukah6EC6CwuULxdzWQU1ijqhMd6f1V3vhQVOMGm0SNPaKwu1wS5Ao4XgghD6AMWPzLnJBx8yovjqteVh3baN0GAVaQhIax1msY6VDkYJwr0JQVGTQ-M5o4fYQG3BfH8EgME0a7NVTRMBzZYwFlAwozXVUBCIFhi1B3t9DwuFZGRXFsGMXDjShyZAhc65nt9khQikbdpzJodLUUZ8YwCcUdOW4ekUAA */
    predictableActionArguments: true,
    states: {
      BISECTOR_BY_POINTS: {
        initial: 'waitingForFirstElement',
        states: {
          waitingForFirstElement: {
            entry: (context) => {
              userMessage(
                "Cliquer sur un premier point (autre que le sommet de l'angle).",
                context.figure,
              )
              context.figure.filter = (e) => e instanceof Point
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  context.figure.selectedElements[0] = event.element as Point
                },
                cond: (_, event) => event.element !== undefined,
                target: 'waitingForSecondElement',
              },
            },
          },
          waitingForSecondElement: {
            entry: (context) => {
              userMessage("Cliquer sur le sommet de l'angle.", context.figure)
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  context.figure.selectedElements[1] = event.element as Point
                },
                cond: (_, event) => event.element !== undefined,
                target: 'waitingForThirdElement',
              },
            },
          },
          waitingForThirdElement: {
            entry: (context) => {
              userMessage('Cliquer sur un troisième point.', context.figure)
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  context.figure.selectedElements[2] = event.element as Point
                  const [pointOnSide1, origin, pointOnSide2] = context.figure
                    .selectedElements as Point[]
                  context.figure.create('BisectorByPoints', {
                    origin,
                    pointOnSide1,
                    pointOnSide2,
                  })
                  context.figure.saveState()
                },
                cond: (_, event) => event.element !== undefined,
                target: 'waitingForFirstElement',
              },
            },
          },
        },
      },
      CIRCLE_CENTER_POINT: {
        initial: 'waitingForCenter',
        states: {
          waitingForCenter: {
            entry: (context) => {
              userMessage('Cliquer sur le centre du cercle.', context.figure)
              context.figure.filter = (e) =>
                e instanceof Point || e instanceof Segment
              context.figure.currentState = 'POINT_INTERSECTION'
            },
            exit: (context) => {
              context.figure.currentState = ''
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  const newPoint = getExistingPointOrCreatedPoint(
                    context,
                    event,
                  )
                  if (newPoint !== undefined) {
                    context.figure.selectedElements[0] = newPoint
                    context.figure.tempCreate('CircleCenterPoint', {
                      center: context.figure.selectedElements[0] as Point,
                      point: context.figure.pointer,
                    })
                  }
                  context.figure.filter = (e) => e instanceof Point
                },
                cond: (_, event) =>
                  getExistingPointOrCreatetPoindWasASuccess(event),
                target: 'waitingForPoint',
              },
            },
          },
          waitingForPoint: {
            entry: (context) => {
              userMessage('Cliquer sur un point du cercle.', context.figure)
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  const newPoint = getExistingPointOrCreatedPoint(
                    context,
                    event,
                  )
                  if (newPoint !== undefined) {
                    context.figure.selectedElements[1] = newPoint
                    const [center, point] = context.figure.selectedElements as [
                      Point,
                      Point,
                    ]
                    context.figure.create('CircleCenterPoint', {
                      center,
                      point,
                    })
                    context.figure.saveState()
                  }
                },
                cond: (_, event) =>
                  getExistingPointOrCreatetPoindWasASuccess(event),
                target: 'waitingForCenter',
              },
            },
          },
        },
      },
      CIRCLE_RADIUS: {
        initial: 'waitingForCenter',
        states: {
          waitingForCenter: {
            entry: (context) => {
              userMessage('Cliquer sur le centre du cercle.', context.figure)
              context.figure.filter = (e) =>
                e instanceof Point || e instanceof Segment
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  const newPoint = getExistingPointOrCreatedPoint(
                    context,
                    event,
                  )
                  if (newPoint !== undefined) {
                    context.figure.selectedElements[0] = newPoint
                    const dialog = createDialoxBoxRadius(context.figure)
                    dialog.showModal()
                  }
                  context.figure.filter = (e) => e instanceof Point
                },
                cond: (_, event) =>
                  getExistingPointOrCreatetPoindWasASuccess(event),
                target: 'waitingForRadius',
              },
            },
          },
          waitingForRadius: {
            on: {
              RADIUS: {
                actions: (context, event) => {
                  const radius = event.radius
                  const center = context.figure.selectedElements[0] as Point
                  const points = testRadiusIsSegment(
                    event.radius,
                    context.figure,
                  )
                  if (points !== undefined) {
                    const dynamicRadius = context.figure.create('Distance', {
                      point1: points[0],
                      point2: points[1],
                    })
                    context.figure.create('CircleCenterDynamicRadius', {
                      center,
                      radius: dynamicRadius,
                    })
                    context.figure.saveState()
                  } else if (radius > 0) {
                    context.figure.create('Circle', { center, radius })
                    context.figure.saveState()
                  }
                },
                target: 'waitingForCenter',
              },
            },
          },
        },
      },
      COLOR: {
        entry: (context, event) => {
          context.figure.options.color = event.text
        },
      },
      DESCRIPTION: {
        entry: (context) => {
          createModalDescription(context.figure)
        },
      },
      DILATE: {
        entry: (context) => {
          context.figure.filter = (e) => e instanceof Point
        },
        exit: (context) => {
          context.figure.selectedElements = []
          for (const e of context.figure.tmpElements) {
            e.remove()
          }
        },
        initial: 'waitingForCenter',
        states: {
          waitingForCenter: {
            entry: (context) => {
              userMessage(
                "Cliquer sur le centre de l'homothétie.",
                context.figure,
              )
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  const newPoint = getExistingPointOrCreatedPoint(
                    context,
                    event,
                  )
                  if (newPoint !== undefined) {
                    context.figure.selectedElements[0] = newPoint
                    const dialog = createDialoxBoxK(context.figure)
                    dialog.showModal()
                  }
                },
                cond: (_, event) =>
                  getExistingPointOrCreatetPoindWasASuccess(event),
                target: 'waitingForCoefficient',
              },
            },
          },
          waitingForCoefficient: {
            on: {
              DILATE_COEF: {
                actions: (context, event) => {
                  context.temp.values[0] = event.coefficient
                },
                target: 'waitingForElement',
              },
            },
          },
          waitingForElement: {
            entry: (context) => {
              context.figure.filter = (e) =>
                e instanceof Point ||
                e instanceof Segment ||
                e instanceof Circle ||
                e instanceof Polygon
              const center = context.figure.selectedElements[0] as Point
              const k = context.temp.values[0]
              context.figure.tmpElements.push(
                context.figure.pointer.dilate(center, k, {
                  color: context.figure.options.tmpColor,
                  isChild: true,
                }),
              )
              userMessage(
                "Cliquer sur l'élément à transformer.",
                context.figure,
              )
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  const center = context.figure.selectedElements[0] as Point
                  const origin = event.element
                  const k = context.temp.values[0]
                  origin.dilate(center, k)
                  context.figure.saveState()
                  context.figure.selectedElements[0] = center
                },
                cond: (_, event) => event.element !== undefined,
                target: 'waitingForElement',
              },
            },
          },
        },
      },
      DISC_CENTER_POINT: {
        initial: 'waitingForCenter',
        states: {
          waitingForCenter: {
            entry: (context) => {
              userMessage('Cliquer sur le centre du disque.', context.figure)
              context.figure.filter = (e) =>
                e instanceof Point || e instanceof Segment
              context.figure.currentState = 'POINT_INTERSECTION'
            },
            exit: (context) => {
              context.figure.currentState = ''
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  const newPoint = getExistingPointOrCreatedPoint(
                    context,
                    event,
                  )
                  if (newPoint !== undefined) {
                    context.figure.selectedElements[0] = newPoint
                    context.figure.tempCreate('CircleCenterPoint', {
                      center: context.figure.selectedElements[0] as Point,
                      point: context.figure.pointer,
                    })
                  }
                  context.figure.filter = (e) => e instanceof Point
                },
                cond: (_, event) =>
                  getExistingPointOrCreatetPoindWasASuccess(event),
                target: 'waitingForPoint',
              },
            },
          },
          waitingForPoint: {
            entry: (context) => {
              userMessage('Cliquer sur un point du disque.', context.figure)
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  const newPoint = getExistingPointOrCreatedPoint(
                    context,
                    event,
                  )
                  if (newPoint !== undefined) {
                    context.figure.selectedElements[1] = newPoint
                    const [center, point] = context.figure.selectedElements as [
                      Point,
                      Point,
                    ]
                    context.figure.create('CircleCenterPoint', {
                      center,
                      point,
                      fillColor: context.figure.options.color,
                      fillOpacity: context.figure.options.discFillOpacity,
                    })
                    context.figure.saveState()
                  }
                },
                cond: (_, event) =>
                  getExistingPointOrCreatetPoindWasASuccess(event),
                target: 'waitingForCenter',
              },
            },
          },
        },
      
      },
      DISC_RADIUS: {
        initial: 'waitingForCenter',
        states: {
          waitingForCenter: {
            entry: (context) => {
              userMessage('Cliquer sur le centre du disque.', context.figure)
              context.figure.filter = (e) =>
                e instanceof Point || e instanceof Segment
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  const newPoint = getExistingPointOrCreatedPoint(
                    context,
                    event,
                  )
                  if (newPoint !== undefined) {
                    context.figure.selectedElements[0] = newPoint
                    const dialog = createDialoxBoxRadius(context.figure)
                    dialog.showModal()
                  }
                  context.figure.filter = (e) => e instanceof Point
                },
                cond: (_, event) =>
                  getExistingPointOrCreatetPoindWasASuccess(event),
                target: 'waitingForRadius',
              },
            },
          },
          waitingForRadius: {
            on: {
              RADIUS: {
                actions: (context, event) => {
                  const radius = event.radius
                  const center = context.figure.selectedElements[0] as Point
                  const points = testRadiusIsSegment(
                    event.radius,
                    context.figure,
                  )
                  if (points !== undefined) {
                    const dynamicRadius = context.figure.create('Distance', {
                      point1: points[0],
                      point2: points[1],
                    })
                    context.figure.create('CircleCenterDynamicRadius', {
                      center,
                      radius: dynamicRadius,
                      fillColor: context.figure.options.color,
                      fillOpacity: context.figure.options.discFillOpacity
                    })
                    context.figure.saveState()
                  } else if (radius > 0) {
                    context.figure.create('Circle', { center, radius, fillColor: context.figure.options.color, fillOpacity: context.figure.options.discFillOpacity })
                    context.figure.saveState()
                  }
                },
                target: 'waitingForCenter',
              },
            },
          },
        },
      },
      DOWNLOAD_LATEX_SVG: {
        entry: (context) => {
          context.temp.htmlElement[0] = selectionRectangle(context.figure)
          // const svg = context.figure.svg.outerHTML
        },
        on: {
          SELECTION_AREA_TO_LATEX: {
            actions: (context, event) => {
              const xMin =
                context.figure.xMin + context.figure.sxTox(event.xMin)
              const xMax = xMin + context.figure.sxTox(event.width)
              const yMax =
                context.figure.yMax + context.figure.syToy(event.yMax)
              const yMin = yMax + context.figure.syToy(event.height)
              context.temp.htmlElement[0].remove()
              const latex = context.figure.latex({ xMax, xMin, yMax, yMin })
              const blob = new Blob([latex], { type: 'text/plain' })
              const url = window.URL.createObjectURL(blob)
              const a = document.createElement('a')
              a.style.display = 'none'
              a.href = url
              a.download = 'figure.tex'
              document.body.appendChild(a)
              a.click()
              window.URL.revokeObjectURL(url)
              document.body.removeChild(a)
              context.figure.buttons.get('DRAG')?.click()
            },
            target: 'DRAG',
          },
          SELECTION_AREA_TO_SVG: {
            actions: (context, event) => {
              const xMin = context.figure.xToSx(
                context.figure.xMin + context.figure.sxTox(event.xMin),
              )
              const yMax = context.figure.yToSy(
                context.figure.yMax + context.figure.syToy(event.yMax),
              )
              context.temp.htmlElement[0].remove()
              const svg = context.figure.svg.cloneNode(true) as SVGElement
              svg.setAttribute('width', event.width.toString())
              svg.setAttribute('height', event.height.toString())
              svg.setAttribute(
                'viewBox',
                `${xMin} ${yMax} ${event.width as number} ${event.height as number}`,
              )
              const blob = new Blob([svg.outerHTML], { type: 'image/svg+xml' })
              const url = window.URL.createObjectURL(blob)
              const a = document.createElement('a')
              a.style.display = 'none'
              a.href = url
              a.download = 'figure.svg'
              document.body.appendChild(a)
              a.click()
              window.URL.revokeObjectURL(url)
              document.body.removeChild(a)
              context.figure.buttons.get('DRAG')?.click()
            },
            target: 'DRAG',
          },
        },
      },
      DRAG: {
        entry: (context) => {
          if (context.figure.elements.size > 1) {
            userMessage(
              'Cliquer sur un point pour le déplacer.',
              context.figure,
            )
          } else {
            userMessage('', context.figure)
          }
          context.figure.filter = (e) => e instanceof Point && e.isFree
        },
        exit: (context) => {
          context.figure.divFigure.style.cursor = 'default'
          context.figure.inDrag = undefined
        },
        on: {
          clickLocation: {
            actions: (context, event) => {
              context.figure.inDrag = event.element
              context.figure.divFigure.style.cursor = 'move'
            },
            cond: (_, event) => event.element !== undefined,
            target: 'DRAG',
          },
        },
      },
      FILL: {
        entry: (context) => {
          context.figure.filter = (e) =>
            e instanceof Polygon || e instanceof Circle || e instanceof Arc
          context.figure.currentState = 'FILL'
        },
        exit: (context) => {
          context.figure.currentState = ''
          for (const e of context.figure.tmpElements) {
            e.isSelected = false
          }
        },
        on: {
          clickLocation: {
            actions: (context, event) => {
              const element = event.element
              if (element !== undefined) {
                if (element.fillColor === context.figure.options.color) {
                  element.fillColor = 'none'
                } else {
                  element.fillColor = context.figure.options.color
                }
                context.figure.saveState()
              }
            },
          },
        },
      },
      GRID: {
        entry: (context) => {
          if (context.figure.grid === undefined) {
            context.figure.grid = context.figure.create('Grid', {
              color: 'black',
            }) as Grid
          } else {
            if (context.figure.grid.isVisible) {
              context.figure.grid.isVisible = false
              context.figure.snapGrid = false
            } else {
              context.figure.grid.isVisible = true
              context.figure.snapGrid = true
            }
          }
          context.figure.saveState()
        },
      },
      HIDE: {
        entry: (context) => {
          context.figure.currentState = 'HIDE'
          userMessage(
            "Cliquer sur l'élément à masquer ou à démasquer.",
            context.figure,
          )
          context.figure.filter = (e) => e.isVisible
          for (const e of context.figure.elements) {
            if (e instanceof Element2D && e.isHidden) {
              e.tempShow()
            }
          }
        },
        exit: (context) => {
          context.figure.currentState = ''
          for (const e of context.figure.elements) {
            if (e instanceof Element2D && e.isHidden) {
              e.hide()
            }
          }
        },
        on: {
          clickLocation: {
            actions: (context, event) => {
              if (event.element.isHidden === true) {
                event.element.show()
                event.element.isHidden = false
              } else {
                event.element.tempShow()
                event.element.isHidden = true
              }
              context.figure.saveState()
            },
            cond: (_, event) => event.element !== undefined,
            target: 'HIDE',
          },
        },
      },
      INFORMATIONS: {
        entry: (context) => {
          userMessage("Cliquer sur l'élément", context.figure)
          context.figure.filter = (e) => e.isVisible
        },
        on: {
          clickLocation: {
            actions: (_, event) => {
              console.info(event.element)
            },
            cond: (_, event) => event.element !== undefined,
            target: 'INFORMATIONS',
          },
        },
      },
      INIT: {
        entry: (context) => {
          context.figure.userMessage = ''
        },
      },
      LATEX: {
        entry: (context) => {
          const latex = context.figure.latex
          const blob = new Blob([latex()], { type: 'text/plain' })
          const url = window.URL.createObjectURL(blob)
          const a = document.createElement('a')
          a.style.display = 'none'
          a.href = url
          a.download = 'figure.tex'
          document.body.appendChild(a)
          a.click()
          window.URL.revokeObjectURL(url)
          document.body.removeChild(a)
          context.figure.buttons.get('DRAG')?.click()
        },
      },
      LINE: {
        entry: (context) => {
          context.figure.filter = (e) => e instanceof Point
        },
        initial: 'waitingForFirstElement',
        states: {
          waitingForFirstElement: {
            entry: (context) => {
              userMessage(
                'Cliquer sur un premier point de la droite.',
                context.figure,
              )
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  const newPoint = getExistingPointOrCreatedPoint(
                    context,
                    event,
                  )
                  if (newPoint !== undefined) {
                    context.figure.selectedElements[0] = newPoint
                    context.figure.tempCreate('Line', {
                      point1: context.figure.selectedElements[0] as Point,
                      point2: context.figure.pointer,
                    })
                  }
                },
                cond: (_, event) =>
                  getExistingPointOrCreatetPoindWasASuccess(event),
                target: 'waitingForSecondElement',
              },
            },
          },
          waitingForSecondElement: {
            entry: (context) => {
              userMessage(
                'Cliquer sur un deuxième point de la droite.',
                context.figure,
              )
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  const newPoint = getExistingPointOrCreatedPoint(
                    context,
                    event,
                  )
                  if (newPoint !== undefined) {
                    context.figure.selectedElements[1] = newPoint
                    const [point1, point2] = context.figure
                      .selectedElements as [Point, Point]
                    context.figure.create('Line', { point1, point2 })
                    context.figure.saveState()
                  }
                },
                cond: (_, event) =>
                  getExistingPointOrCreatetPoindWasASuccess(event),
                target: 'waitingForFirstElement',
              },
            },
          },
        },
      },
      LINE_PARALLEL: {
        initial: 'waitingForFirstElement',
        states: {
          waitingForFirstElement: {
            entry: (context) => {
              userMessage('Cliquer sur un point ou une droite.', context.figure)
              context.figure.filter = (e) =>
                e instanceof Segment || e instanceof Point
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  context.figure.selectedElements[0] = event.element
                },
                cond: (_, event) => event.element !== undefined,
                target: 'waitingForSecondElement',
              },
            },
          },
          waitingForSecondElement: {
            entry: (context) => {
              if (context.figure.selectedElements[0] instanceof Point) {
                userMessage('Cliquer sur une droite.', context.figure)
                context.figure.filter = (e) => e instanceof Segment
              } else {
                userMessage('Cliquer sur un point.', context.figure)
                context.figure.filter = (e) => e instanceof Point
              }
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  context.figure.selectedElements[1] = event.element
                  let point: Point
                  let line: Segment
                  if (context.figure.selectedElements[0] instanceof Point) {
                    ;[point, line] = context.figure.selectedElements as [
                      Point,
                      Segment,
                    ]
                  } else {
                    ;[line, point] = context.figure.selectedElements as [
                      Segment,
                      Point,
                    ]
                  }
                  context.figure.create('LineParallel', { line, point })
                  context.figure.saveState()
                },
                cond: (_, event) => event.element !== undefined,
                target: 'waitingForFirstElement',
              },
            },
          },
        },
      },
      LINE_PERPENDICULAR: {
        initial: 'waitingForFirstElement',
        states: {
          waitingForFirstElement: {
            entry: (context) => {
              userMessage('Cliquer sur un point ou une droite.', context.figure)
              context.figure.filter = (e) =>
                e instanceof Segment || e instanceof Point
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  context.figure.selectedElements[0] = event.element
                },
                cond: (_, event) => event.element !== undefined,
                target: 'waitingForSecondElement',
              },
            },
          },
          waitingForSecondElement: {
            entry: (context) => {
              if (context.figure.selectedElements[0] instanceof Point) {
                userMessage('Cliquer sur une droite.', context.figure)
                context.figure.filter = (e) => e instanceof Segment
              } else {
                userMessage('Cliquer sur un point.', context.figure)
                context.figure.filter = (e) => e instanceof Point
              }
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  context.figure.selectedElements[1] = event.element
                  let point: Point
                  let line: Segment
                  if (context.figure.selectedElements[0] instanceof Point) {
                    ;[point, line] = context.figure.selectedElements as [
                      Point,
                      Segment,
                    ]
                  } else {
                    ;[line, point] = context.figure.selectedElements as [
                      Segment,
                      Point,
                    ]
                  }
                  context.figure.create('LinePerpendicular', { line, point })
                  context.figure.saveState()
                },
                cond: (_, event) => event.element !== undefined,
                target: 'waitingForFirstElement',
              },
            },
          },
        },
      },
      MARK_BETWEEN_POINTS: {
        initial: 'waitingForFirstElement',
        states: {
          waitingForFirstElement: {
            entry: (context) => {
              userMessage(
                'Cliquer sur la première extrémité du segment ou sur un segment.',
                context.figure,
              )
              if (context.figure?.divMarkPalette !== undefined) {
                context.figure.divMarkPalette.style.display = 'flex'
              }
              context.figure.filter = (e) =>
                e instanceof Point || e instanceof Segment
            },
            on: {
              clickLocation: [
                {
                  actions: (context, event) => {
                    context.figure.selectedElements[0] = event.element
                  },
                  cond: (_, event) =>
                    event.element !== undefined &&
                    event.element instanceof Point,
                  target: 'waitingForSecondElement',
                },
                {
                  actions: (context, event) => {
                    const segment = event.element as Segment
                    const point1 = segment.point1
                    const point2 = segment.point2
                    context.figure.create('MarkBetweenPoints', {
                      point1,
                      point2,
                    })
                    context.figure.saveState()
                  },
                  cond: (_, event) =>
                    event.element !== undefined &&
                    event.element instanceof Segment,
                  target: 'waitingForFirstElement',
                },
              ],
            },
            exit: (context) => {
              if (context.figure?.divMarkPalette !== undefined) {
                context.figure.divMarkPalette.style.display = 'none'
              }
            },
          },
          waitingForSecondElement: {
            entry: (context) => {
              if (context.figure?.divMarkPalette !== undefined) {
                context.figure.divMarkPalette.style.display = 'flex'
              }
              userMessage(
                'Cliquer sur la deuxième extrémité du segment.',
                context.figure,
              )
              context.figure.filter = (e) => e instanceof Point
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  context.figure.selectedElements[1] = event.element
                  const [point1, point2] = context.figure.selectedElements as [
                    Point,
                    Point,
                  ]
                  context.figure.create('MarkBetweenPoints', {
                    point1,
                    point2,
                  })
                  context.figure.saveState()
                },
                cond: (_, event) => event.element !== undefined,
                target: 'waitingForFirstElement',
              },
            },
          },
        },
      },
      MIDDLE: {
        initial: 'waitingForFirstElement',
        states: {
          waitingForFirstElement: {
            entry: (context) => {
              userMessage(
                'Cliquer sur la première extrémité du segment ou sur un segment.',
                context.figure,
              )
              context.figure.filter = (e) =>
                e instanceof Point || e instanceof Segment
            },
            on: {
              clickLocation: [
                {
                  actions: (context, event) => {
                    context.figure.selectedElements[0] = event.element
                  },
                  cond: (_, event) =>
                    event.element !== undefined &&
                    event.element instanceof Point,
                  target: 'waitingForSecondElement',
                },
                {
                  actions: (context, event) => {
                    const segment = event.element as Segment
                    const point1 = segment.point1
                    const point2 = segment.point2
                    context.figure.create('Middle', { point1, point2 })
                  },
                  cond: (_, event) =>
                    event.element !== undefined &&
                    event.element instanceof Segment,
                  target: 'waitingForFirstElement',
                },
              ],
            },
          },
          waitingForSecondElement: {
            entry: (context) => {
              userMessage(
                'Cliquer sur la deuxième extrémité du segment.',
                context.figure,
              )
              context.figure.filter = (e) => e instanceof Point
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  context.figure.selectedElements[1] = event.element
                  const [point1, point2] = context.figure.selectedElements as [
                    Point,
                    Point,
                  ]
                  context.figure.create('Middle', { point1, point2 })
                  context.figure.saveState()
                },
                cond: (_, event) => event.element !== undefined,
                target: 'waitingForFirstElement',
              },
            },
          },
        },
      },
      MOVE_LABEL: {
        entry: (context) => {
          userMessage(
            'Cliquer sur le point pour déplacer son nom.',
            context.figure,
          )
          context.figure.filter = (e) => e instanceof Point
        },
        initial: 'waitingForPoint',
        states: {
          waitingForPoint: {
            on: {
              clickLocation: {
                actions: (context, event) => {
                  const point = event.element
                  context.figure.inDrag = point.elementTextLabel
                },
                target: 'waitingForPoint',
              },
            },
          },
        },
      },
      NAME_POINT: {
        initial: 'waitingForPoint',
        states: {
          waitingForName: {
            entry: (context) => {
              const dialog = createDialoxBoxName(context.figure)
              dialog.showModal()
            },
            on: {
              TEXT_FROM_DIALOG: {
                actions: (context, event) => {
                  const point = context.temp.elements[0] as Point
                  point.label = event.text
                  context.figure.saveState()
                },
                target: 'waitingForPoint',
              },
            },
          },
          waitingForPoint: {
            entry: (context) => {
              context.figure.filter = (e) => e instanceof Point
              userMessage('Cliquer sur un point à renommer.', context.figure)
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  const newPoint = getExistingPointOrCreatedPoint(
                    context,
                    event,
                  )
                  if (newPoint !== undefined) {
                    context.temp.elements[0] = newPoint
                  }
                },
                cond: (_, event) =>
                  getExistingPointOrCreatetPoindWasASuccess(event),
                target: 'waitingForName',
              },
            },
          },
        },
      },
      OPEN: {
        entry: (context) => {
          const input = document.createElement('input')
          input.type = 'file'
          input.accept = '.json'
          input.onchange = (e: Event) => {
            const file = (e.target as HTMLInputElement).files?.[0]
            if (file === undefined) {
              return
            }
            const reader = new FileReader()
            reader.readAsText(file, 'UTF-8')
            reader.onload = (readerEvent: ProgressEvent<FileReader>) => {
              const content =
                (readerEvent?.target?.result as null | string) ?? ''
              if (typeof content === 'string' && content !== '') {
                context.figure.loadJson(JSON.parse(content), true)
              }
            }
          }
          input.click()
          // context.figure.buttons.get('DRAG')?.click()
        },
      },
      PAUSE: {
        entry: (context) => {
          userMessage('', context.figure)
          if (context.intervalId !== undefined) {
            clearInterval(context.intervalId)
          }
        },
      },
      PERPENDICULAR_BISECTOR: {
        initial: 'waitingForFirstElement',
        states: {
          waitingForFirstElement: {
            entry: (context) => {
              userMessage(
                'Cliquer sur la première extrémité du segment.',
                context.figure,
              )
              context.figure.filter = (e) => e instanceof Point
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  context.figure.selectedElements[0] = event.element
                  context.figure.tempCreate('PerpendicularBisectorByPoints', {
                    point1: context.figure.selectedElements[0] as Point,
                    point2: context.figure.pointer,
                  })
                },
                cond: (_, event) => event.element !== undefined,
                target: 'waitingForSecondElement',
              },
            },
          },
          waitingForSecondElement: {
            entry: (context) => {
              userMessage(
                'Cliquer sur la deuxième extrémité du segment.',
                context.figure,
              )
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  context.figure.selectedElements[1] = event.element
                  const [point1, point2] = context.figure.selectedElements as [
                    Point,
                    Point,
                  ]
                  context.figure.create('PerpendicularBisectorByPoints', {
                    point1,
                    point2,
                  })
                  context.figure.saveState()
                },
                target: 'waitingForFirstElement',
              },
            },
          },
        },
      },
      PLAY: {
        entry: (context) => {
          userMessage('', context.figure)
          clearInterval(context.intervalId)
          context.figure.redo()
          context.intervalId = setInterval(() => {
            if (context.figure.stackRedo.length === 0) {
              context.figure.buttons.get('PAUSE')?.click()
              clearInterval(context.intervalId)
              return
            }
            context.figure.redo()
          }, context.figure.options.animationStepInterval)
        },
      },
      PLAY_SKIP_BACK: {
        entry: (context) => {
          userMessage('', context.figure)
          context.figure.undo()
        },
      },
      PLAY_SKIP_FORWARD: {
        entry: (context) => {
          userMessage('', context.figure)
          context.figure.redo()
        },
      },
      POINT: {
        entry: (context) => {
          userMessage('Cliquer pour créer un point.', context.figure)
          context.figure.filter = (e) => e instanceof Point
          context.figure.currentState = 'POINT'
        },
        initial: 'waitingForLocation',
        states: {
          waitingForLocation: {
            on: {
              clickLocation: {
                actions: (context, event) => {
                  if (context.figure.hoverPoints.size > 0) {
                    const point = [...context.figure.hoverPoints][0] as Point
                    context.figure.inDrag = point
                    context.figure.divFigure.style.cursor = 'move'
                  } else {
                    const { x, y } = event
                    const limitNumberOfPoint =
                      context.figure.options.limitNumberOfElement?.get(
                        'Point',
                      ) ?? Number.POSITIVE_INFINITY
                    const noLimit =
                      limitNumberOfPoint === undefined ||
                      !Number.isFinite(limitNumberOfPoint)
                    const limitReached =
                      [...context.figure.elements.values()].filter(
                        (e) => e instanceof Point,
                      ).length > limitNumberOfPoint
                    const samePosition =
                      !context.figure.options.gridWithTwoPointsOnSamePosition &&
                      [...context.figure.elements.values()].filter(
                        (e) =>
                          e instanceof Point &&
                          e.isSelectable &&
                          e.isVisible &&
                          e.x ===
                            Math.round(x / context.figure.dx) *
                              context.figure.dx &&
                          e.y ===
                            Math.round(y / context.figure.dy) *
                              context.figure.dy,
                      ).length > 0
                    if (!samePosition && (noLimit || !limitReached)) {
                      if (
                        context.figure.options.labelAutomaticBeginsWith !==
                        undefined
                      ) {
                        let indiceName =
                          context.figure.options.labelAutomaticBeginsWith.charCodeAt(
                            0,
                          )
                        let label =
                          context.figure.options.labelAutomaticBeginsWith
                        while (
                          [...context.figure.elements.values()].some((e) => {
                            if (!('label' in e)) return false
                            return e.label === label
                          })
                        ) {
                          label = String.fromCharCode(++indiceName)
                        }
                        context.figure.create('Point', { label, x, y })
                      } else {
                        context.figure.create('Point', { x, y })
                      }
                    }
                  }
                  context.figure.saveState()
                },
                target: 'waitingForLocation',
              },
            },
          },
        },
        exit: (context) => {
          context.figure.currentState = ''
        }
      },
      POINT_INTERSECTION: {
        entry: (context) => {
          context.figure.filter = (e) =>
            e instanceof Segment || e instanceof Circle
          context.figure.currentState = 'POINT_INTERSECTION'
        },
        exit: (context) => {
          context.figure.selectedElements = []
          for (const e of context.figure.tmpElements) {
            e.remove()
          }
          context.figure.currentState = ''
        },
        initial: 'waitingForFirstElement',
        states: {
          waitingForFirstElement: {
            entry: (context) => {
              userMessage(
                'Cliquer sur une droite, une demi-droite, un segment ou un cercle.',
                context.figure,
              )
            },
            exit: (context) => {
              if (context.figure.selectedElements[0]?.isSelected)
                context.figure.selectedElements[0].isSelected = false
            },
            on: {
              clickLocation: [
                {
                  actions: (context, event) => {
                    context.figure.selectedElements[0] = event.element
                    event.element.isSelected = true
                  },
                  cond: (_, event) => event.element !== undefined,
                  target: 'waitingForSecondElement',
                },
                {
                  actions: (context, event) => {
                    context.figure.selectedElements[0] =
                      event.possibleElements[0]
                    context.figure.selectedElements[1] =
                      event.possibleElements[1]
                    const [element1, element2] = context.figure.selectedElements
                    let label = ''
                    if (
                      context.figure.options.labelAutomaticBeginsWith !==
                      undefined
                    ) {
                      let indiceName =
                        context.figure.options.labelAutomaticBeginsWith.charCodeAt(
                          0,
                        )
                      label = context.figure.options.labelAutomaticBeginsWith
                      while (
                        [...context.figure.elements.values()].some((e) => {
                          if (!('label' in e)) return false
                          return e.label === label
                        })
                      ) {
                        label = String.fromCharCode(++indiceName)
                      }
                    }
                    if (element1 instanceof Segment) {
                      if (element2 instanceof Segment) {
                        context.figure.create('PointIntersectionLL', {
                          line1: element1,
                          line2: element2,
                          label,
                        })
                      }
                      if (element2 instanceof Circle) {
                        context.figure.create('PointsIntersectionLC', {
                          circle: element2,
                          line: element1,
                        })
                      }
                      context.figure.saveState()
                    } else if (element1 instanceof Circle) {
                      if (element2 instanceof Segment) {
                        context.figure.create('PointsIntersectionLC', {
                          circle: element1,
                          line: element2,
                        })
                      }
                      if (element2 instanceof Circle) {
                        context.figure.create('PointsIntersectionCC', {
                          circle1: element1,
                          circle2: element2,
                        })
                      }
                      context.figure.saveState()
                    }
                  },
                  cond: (_, event) => event.possibleElements.length === 2,
                  target: 'waitingForFirstElement',
                },
              ],
            },
          },
          waitingForSecondElement: {
            exit: (context) => {
              for (const e of context.figure.tmpElements) {
                e.isSelected = false
              }
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  context.figure.selectedElements[1] = event.element
                  const [element1, element2] = context.figure.selectedElements
                  if (element1 instanceof Segment) {
                    if (element2 instanceof Segment) {
                      context.figure.create('PointIntersectionLL', {
                        line1: element1,
                        line2: element2,
                      })
                    }
                    if (element2 instanceof Circle) {
                      context.figure.create('PointsIntersectionLC', {
                        circle: element2,
                        line: element1,
                      })
                    }
                    context.figure.saveState()
                  } else if (element1 instanceof Circle) {
                    if (element2 instanceof Segment) {
                      context.figure.create('PointsIntersectionLC', {
                        circle: element1,
                        line: element2,
                      })
                    }
                    if (element2 instanceof Circle) {
                      context.figure.create('PointsIntersectionCC', {
                        circle1: element1,
                        circle2: element2,
                      })
                    }
                    context.figure.saveState()
                  }
                },
                cond: (_, event) => event.element !== undefined,
                target: 'waitingForFirstElement',
              },
            },
          },
        },
      },
      POINT_ON: {
        entry: (context) => {
          context.figure.filter = (e) =>
            e instanceof Segment || e instanceof Circle || e instanceof Graph
          userMessage(
            'Cliquer sur un segment, un cercle ou une courbe.',
            context.figure,
          )
        },
        on: {
          clickLocation: {
            actions: (context, event) => {
              context.figure.selectedElements[0] = event.element
              if (event.element instanceof Segment) {
                const [point1, point2] = [
                  event.element.point1,
                  event.element.point2,
                ]
                const k =
                  ((event.x - point1.x) * (point2.x - point1.x) +
                    (event.y - point1.y) * (point2.y - point1.y)) /
                  distance(point1, point2) ** 2
                let label = ''
                if (
                  context.figure.options.labelAutomaticBeginsWith !== undefined
                ) {
                  let indiceName =
                    context.figure.options.labelAutomaticBeginsWith.charCodeAt(
                      0,
                    )
                  label = context.figure.options.labelAutomaticBeginsWith
                  while (
                    [...context.figure.elements.values()].some((e) => {
                      if (!('label' in e)) return false
                      return e.label === label
                    })
                  ) {
                    label = String.fromCharCode(++indiceName)
                  }
                }
                context.figure.create('PointOnLine', {
                  k,
                  line: event.element,
                  label,
                })
                context.figure.saveState()
              } else if (event.element instanceof Circle) {
                const angleWithHorizontal = Math.atan2(
                  event.y - event.element.center.y,
                  event.x - event.element.center.x,
                )
                context.figure.create('PointOnCircle', {
                  angleWithHorizontal,
                  circle: event.element,
                })
                context.figure.saveState()
              } else if (event.element instanceof Graph) {
                const x = event.x
                context.figure.create('PointOnGraph', {
                  x,
                  graph: event.element,
                })
                context.figure.saveState()
              }
            },
            target: 'POINT_ON',
          },
        },
      },
      POLYGON: {
        id: 'polygon',
        initial: 'init',
        on: {
          STOPPOLYGON: '#polygon.STOPPOLYGON',
        },
        states: {
          init: {
            always: 'waitingElement',
            entry: (context) => {
              userMessage('Cliquer sur un sommet du polygone.', context.figure)
              context.figure.filter = (e) => e instanceof Point
            },
          },
          STOPPOLYGON: {
            always: {
              target: '#polygon.waitingElement',
            },
            entry: (context) => {
              const polygon = context.figure.create('Polygon', {
                points: context.figure.selectedElements as Point[],
                fillColor: context.figure.options.fillColor,
              })
              for (const point of polygon.points) {
                point.color = context.figure.options.colorPointPolygon
              }
              userMessage(
                'Cliquer sur un sommet pour créer un autre polygone.',
                context.figure,
              )
              context.figure.saveState()
            },
          },
          waitingElement: {
            exit: (context) => {
              for (const e of context.figure.tmpElements) {
                e.remove()
              }
            },
            on: {
              clickLocation: [
                {
                  actions: (context, event) => {
                    if (context.figure.selectedElements.length > 0) {
                      userMessage(
                        'Cliquer sur un nouveau sommet ou sur le premier sommet pour terminer.',
                        context.figure,
                      )
                    } else {
                      userMessage(
                        'Cliquer sur un sommet du polygone.',
                        context.figure,
                      )
                    }
                    const newPoint = getExistingPointOrCreatedPoint(
                      context,
                      event,
                    )
                    if (newPoint !== undefined) {
                      newPoint.shape = context.figure.options.tmpShape
                      newPoint.color = context.figure.options.tmpColor
                      context.figure.selectedElements.push(newPoint)
                      const points = [
                        ...context.figure.selectedElements,
                        context.figure.pointer,
                      ] as Point[]
                      context.figure.tempCreate('Polygon', {
                        isBuiltWithSegments: false,
                        isChild: true,
                        color: context.figure.options.tmpColor,
                        fillColor: context.figure.options.tmpFillColor,
                        points,
                      })
                    }
                  },
                  cond: (context, event) => {
                    if (context.figure.selectedElements.length < 2) {
                      return getExistingPointOrCreatetPoindWasASuccess(event)
                    }
                    const last = event.element as Point
                    const first = context.figure.selectedElements[0] as Point
                    const isClickOnFirstPoint = last?.id === first?.id
                    return (
                      getExistingPointOrCreatetPoindWasASuccess(event) &&
                      !isClickOnFirstPoint
                    )
                  },
                  target: 'waitingElement',
                },
                {
                  cond: (context, event) => {
                    if (context.figure.selectedElements.length > 1) {
                      const last = event.element as Point
                      const first = context.figure.selectedElements[0] as Point
                      return last?.id === first?.id
                    }
                    return false
                  },
                  target: 'STOPPOLYGON',
                },
              ],
            },
          },
        },
      },
      RAY: {
        initial: 'waitingForFirstElement',
        states: {
          waitingForFirstElement: {
            entry: (context) => {
              userMessage(
                "Cliquer sur l'origine de la demi-droite.",
                context.figure,
              )
              context.figure.filter = (e) => e instanceof Point
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  const newPoint = getExistingPointOrCreatedPoint(
                    context,
                    event,
                  )
                  if (newPoint !== undefined) {
                    context.figure.selectedElements[0] = newPoint
                    context.figure.tempCreate('Ray', {
                      point1: context.figure.selectedElements[0] as Point,
                      point2: context.figure.pointer,
                    })
                  }
                },
                cond: (_, event) =>
                  getExistingPointOrCreatetPoindWasASuccess(event),
                target: 'waitingForSecondElement',
              },
            },
          },
          waitingForSecondElement: {
            entry: (context) => {
              userMessage('Cliquer un point de la demi-droite.', context.figure)
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  const newPoint = getExistingPointOrCreatedPoint(
                    context,
                    event,
                  )
                  if (newPoint !== undefined) {
                    context.figure.selectedElements[1] = newPoint
                    const [point1, point2] = context.figure
                      .selectedElements as [Point, Point]
                    context.figure.create('Ray', { point1, point2 })
                    context.figure.saveState()
                  }
                },
                cond: (_, event) =>
                  getExistingPointOrCreatetPoindWasASuccess(event),
                target: 'waitingForFirstElement',
              },
            },
          },
        },
      },
      REDO: {
        entry: (context) => {
          context.figure.redo()
          context.figure.buttons.get('DRAG')?.click()
        },
      },
      REFLECTION: {
        entry: (context) => {
          userMessage('Cliquer sur le centre de la symétrie.', context.figure)
          context.figure.filter = (e) => e instanceof Point
        },
        exit: (context) => {
          context.figure.selectedElements = []
          for (const e of context.figure.tmpElements) {
            e.remove()
          }
        },
        initial: 'waitingForCenter',
        states: {
          waitingForCenter: {
            on: {
              clickLocation: {
                actions: (context, event) => {
                  context.figure.selectedElements[0] = event.element
                },
                cond: (_, event) => event.element !== undefined,
                target: 'waitingForElement',
              },
            },
          },
          waitingForElement: {
            entry: (context) => {
              userMessage(
                "Cliquer sur l'élément à transformer.",
                context.figure,
              )
              context.figure.filter = (e) =>
                e instanceof Point ||
                e instanceof Segment ||
                e instanceof Circle ||
                e instanceof Polygon
              const center = context.figure.selectedElements[0] as Point
              if (center !== undefined) {
                context.figure.selectedElements[0] = center
                context.figure.tmpElements.push(
                  context.figure.pointer.reflect(center, {
                    color: context.figure.options.tmpColor,
                    isChild: true,
                    isSelectable: false,
                  }),
                )
              }
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  const origin = event.element as
                    | Circle
                    | Point
                    | Polygon
                    | Segment
                  const center = context.figure.selectedElements[0] as Point
                  if (origin !== undefined) {
                    origin.reflect(center)
                    context.figure.saveState()
                    // Le saveState initialise selectedElements à []
                    context.figure.selectedElements[0] = center
                  }
                },
                target: 'waitingForElement',
              },
            },
          },
        },
      },
      REFLECTION_OVER_LINE: {
        entry: (context) => {
          userMessage("Cliquer sur l'axe de la symétrie.", context.figure)
          context.figure.filter = (e) => e instanceof Segment
        },
        exit: (context) => {
          context.figure.selectedElements = []
          for (const e of context.figure.tmpElements) {
            e.remove()
          }
        },
        initial: 'waitingForAxis',
        states: {
          waitingForAxis: {
            on: {
              clickLocation: {
                actions: (context, event) => {
                  context.figure.selectedElements[0] = event.element
                },
                cond: (_, event) => event.element !== undefined,
                target: 'waitingForOrigin',
              },
            },
          },
          waitingForOrigin: {
            entry: (context) => {
              userMessage(
                "Cliquer sur l'élément à transformer.",
                context.figure,
              )
              context.figure.filter = (e) =>
                e instanceof Point ||
                e instanceof Segment ||
                e instanceof Circle ||
                e instanceof Polygon
              const line = context.figure.selectedElements[0] as Segment
              if (line !== undefined) {
                context.figure.selectedElements[0] = line
                context.figure.tmpElements.push(
                  context.figure.pointer.reflectOverLine(line, {
                    color: context.figure.options.tmpColor,
                    isChild: true,
                    isSelectable: false,
                  }),
                )
              }
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  const origin = event.element as
                    | Circle
                    | Point
                    | Polygon
                    | Segment
                  const line = context.figure.selectedElements[0] as Segment
                  if (origin !== undefined) {
                    origin.reflectOverLine(line)
                    context.figure.saveState()
                    // Le saveState initialise selectedElements à []
                    context.figure.selectedElements[0] = line
                  }
                },
                cond: (_, event) => event.element !== undefined,
                target: 'waitingForOrigin',
              },
            },
          },
        },
      },
      REMOVE: {
        entry: (context) => {
          userMessage("Cliquer sur l'élément à supprimer.", context.figure)
          context.figure.filter = (e) => e.isVisible
        },
        on: {
          clickLocation: {
            actions: (context, event) => {
              event.element.remove()
              context.figure.saveState()
            },
            cond: (_, event) => event.element !== undefined,
            target: 'REMOVE',
          },
        },
      },
      RESTART: {
        entry: (context) => {
          userMessage('', context.figure)
          context.figure.restart()
        },
      },
      ROTATE: {
        entry: (context) => {
          context.figure.filter = (e) =>
            e instanceof Point || e instanceof Segment
        },
        exit: (context) => {
          context.figure.selectedElements = []
          for (const e of context.figure.tmpElements) {
            e.remove()
          }
        },
        initial: 'waitingForCenter',
        states: {
          waitingForAngle: {
            on: {
              ANGLE: {
                actions: (context, event) => {
                  const angle = event.angle
                  context.temp.values[0] = angle
                },
                target: 'waitingForOrigin',
              },
            },
          },
          waitingForCenter: {
            entry: (context) => {
              userMessage('Cliquer sur le centre de rotation.', context.figure)
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  const newPoint = getExistingPointOrCreatedPoint(
                    context,
                    event,
                  )
                  if (newPoint !== undefined) {
                    context.figure.selectedElements[0] = newPoint
                    const dialog = createDialoxBoxAngle(context.figure)
                    dialog.showModal()
                  }
                },
                cond: (_, event) =>
                  getExistingPointOrCreatetPoindWasASuccess(event),
                target: 'waitingForAngle',
              },
            },
          },
          waitingForOrigin: {
            entry: (context) => {
              const center = context.figure.selectedElements[0] as Point
              const angle = context.temp.values[0]
              context.figure.tmpElements.push(
                context.figure.pointer.rotate(center, angle, {
                  color: context.figure.options.tmpColor,
                  isChild: true,
                  isSelectable: false,
                }),
              )
              userMessage(
                "Cliquer sur l'élément à transformer.",
                context.figure,
              )
              context.figure.filter = (e) =>
                e instanceof Point ||
                e instanceof Segment ||
                e instanceof Circle ||
                e instanceof Polygon
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  const center = context.figure.selectedElements[0] as Point
                  const origin = event.element as
                    | Circle
                    | Point
                    | Polygon
                    | Segment
                  const angle = context.temp.values[0]
                  origin.rotate(center, angle)
                  context.figure.saveState()
                  context.figure.selectedElements[0] = center
                },
                cond: (_, event) => event.element !== undefined,
                target: 'waitingForOrigin',
              },
            },
          },
        },
      },
      SAVE: {
        entry: (context) => {
          const jsonContent = context.figure.getJsonWithHistory()
          const blob = new Blob([jsonContent], { type: 'text/plain' })
          const url = window.URL.createObjectURL(blob)

          const a = document.createElement('a')
          a.style.display = 'none'
          a.href = url
          a.download = 'figure.json'
          document.body.appendChild(a)
          a.click()
          window.URL.revokeObjectURL(url)
          document.body.removeChild(a)
          context.figure.buttons.get('DRAG')?.click()
        },
      },
      SEGMENT: {
        initial: 'waitingForFirstElement',
        states: {
          waitingForFirstElement: {
            entry: (context) => {
              userMessage(
                'Cliquer sur la première extrémité du segment.',
                context.figure,
              )
              context.figure.filter = (e) => e instanceof Point
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  const newPoint = getExistingPointOrCreatedPoint(
                    context,
                    event,
                  )
                  if (newPoint !== undefined) {
                    context.figure.selectedElements[0] = newPoint
                    context.figure.tempCreate('Segment', {
                      point1: context.figure.selectedElements[0] as Point,
                      point2: context.figure.pointer,
                    })
                  }
                },
                cond: (_, event) =>
                  getExistingPointOrCreatetPoindWasASuccess(event),
                target: 'waitingForSecondElement',
              },
            },
          },
          waitingForSecondElement: {
            entry: (context) => {
              userMessage(
                'Cliquer sur la deuxième extrémité du segment.',
                context.figure,
              )
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  const newPoint = getExistingPointOrCreatedPoint(
                    context,
                    event,
                  )
                  if (newPoint !== undefined) {
                    context.figure.selectedElements[1] = newPoint
                    const [point1, point2] = context.figure
                      .selectedElements as [Point, Point]
                    context.figure.create('Segment', { point1, point2 })
                    context.figure.saveState()
                  }
                },
                cond: (_, event) =>
                  getExistingPointOrCreatetPoindWasASuccess(event),
                target: 'waitingForFirstElement',
              },
            },
          },
        },
      },
      SET_OPTIONS: {
        entry: (context) => {
          context.figure.filter = (e) => e instanceof Element2D
          context.figure.currentState = 'SET_OPTIONS'
        },
        on: {
          clickLocation: {
            actions: (context, event) => {
              const element = event.element
              if (element !== undefined) {
                element.color = context.figure.options.color
                element.thickness = context.figure.options.thickness
                if ('isDashed' in element)
                  element.isDashed = context.figure.options.isDashed
                if (context.figure.options.fillColorAndBorderColorAreSame) {
                  if ('fillColor' in element && element.fillColor !== 'none') element.fillColor = context.figure.options.color
                  // if ('fillOpacity' in element) element.fillOpacity = context.figure.options.fillOpacity
                }
              }
            },
          },
        },
      },
      SHAKE: {
        always: 'DRAG',
        entry: async (context) => {
          await context.figure.shake()
          context.figure.saveState()
        },
      },
      TRANSLATION: {
        exit: (context) => {
          for (const e of context.figure.tmpElements) {
            e.isSelectable = false
          }
        },
        initial: 'waitingForFistPoint',
        states: {
          waitingForFistPoint: {
            entry: (context) => {
              context.figure.filter = (e) => e instanceof Point
              userMessage(
                'Cliquer sur le premier point qui définit de la translation.',
                context.figure,
              )
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  const point1 = event.element as Point
                  context.temp.elements[0] = point1
                  point1.isSelected = true
                },
                cond: (_, event) => event.element !== undefined,
                target: 'waitingForSecondPoint',
              },
            },
          },
          waitingForOrigin: {
            entry: (context) => {
              userMessage("Cliquer sur l'objet à transformer.", context.figure)
              context.figure.filter = (e) =>
                e instanceof Point ||
                e instanceof Segment ||
                e instanceof Circle ||
                e instanceof Polygon
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  const [point1, point2] = context.temp.elements as [
                    Point,
                    Point,
                  ]
                  const origin = event.element
                  origin.translate(point1, point2)
                  context.figure.saveState()
                },
                cond: (_, event) => event.element !== undefined,
                target: 'waitingForOrigin',
              },
            },
          },
          waitingForSecondPoint: {
            entry: (context) => {
              userMessage(
                'Cliquer sur le deuxième point qui définit de la translation.',
                context.figure,
              )
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  const point2 = event.element as Point
                  context.temp.elements[1] = point2
                  point2.isSelected = true
                },
                cond: (_, event) => event.element !== undefined,
                target: 'waitingForOrigin',
              },
            },
          },
        },
      },
      UNDO: {
        entry: (context) => {
          context.figure.undo()
          context.figure.buttons.get('DRAG')?.click()
        },
      },
      VECTOR: {
        initial: 'waitingForFirstElement',
        states: {
          waitingForFirstElement: {
            entry: (context) => {
              userMessage(
                "Cliquer sur l'origine de la représentation du vecteur.",
                context.figure,
              )
              context.figure.filter = (e) => e instanceof Point
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  const newPoint = getExistingPointOrCreatedPoint(
                    context,
                    event,
                  )
                  if (newPoint !== undefined) {
                    context.figure.selectedElements[0] = newPoint
                    context.figure.tempCreate('Segment', {
                      point1: context.figure.selectedElements[0] as Point,
                      point2: context.figure.pointer,
                      shape: '->',
                    })
                  }
                },
                cond: (_, event) =>
                  getExistingPointOrCreatetPoindWasASuccess(event),
                target: 'waitingForSecondElement',
              },
            },
          },
          waitingForSecondElement: {
            entry: (context) => {
              userMessage(
                "Cliquer sur l'extrémité de la représentation du vecteur.",
                context.figure,
              )
            },
            on: {
              clickLocation: {
                actions: (context, event) => {
                  const newPoint = getExistingPointOrCreatedPoint(
                    context,
                    event,
                  )
                  if (newPoint !== undefined) {
                    context.figure.selectedElements[1] = newPoint
                    const [point1, point2] = context.figure
                      .selectedElements as [Point, Point]
                    context.figure.create('VectorByPoints', {
                      point1,
                      point2,
                    })
                    point1.color = ''
                    point2.color = ''
                    context.figure.saveState()
                  }
                },
                cond: (_, event) =>
                  getExistingPointOrCreatetPoindWasASuccess(event),
                target: 'waitingForFirstElement',
              },
            },
          },
        },
      },
      ZOOM_IN: {
        initial: 'zoom_in',
        states: {
          zoom_in: {
            entry: (context) => {
              context.figure.zoomLevel *= defaultZoomInFactor
              context.figure.zoom(context.figure.zoomLevel)
            },
          },
        },
      },
      ZOOM_OUT: {
        initial: 'zoom_out',
        states: {
          zoom_out: {
            entry: (context) => {
              context.figure.zoomLevel *= defaultZoomOutFactor
              context.figure.zoom(context.figure.zoomLevel)
            },
          },
        },
      },
    },
  },
  {
    actions: {
      eraseTempElements: (context) => {
        for (const e of context.figure.tmpElements) {
          e.remove()
        }
      },
      highlightButton: (context, event) => {
        const button = context.figure.buttons.get(event.type)
        if (button !== undefined) {
          button.style.backgroundColor = orangeMathaleaLight
        }
        context.figure.buttons.forEach((value, key) => {
          if (key !== event.type) {
            value.style.backgroundColor = 'white'
          }
        })
      },
      eraseUserMessage: (context) => {
        userMessage('', context.figure)
      },
    },
  },
)

function getExistingPointOrCreatedPoint(
  context: MyContext,
  event: MyEvent,
): Point | undefined {
  if (event.element instanceof Point) {
    return event.element
  }
  const [x, y] = [event.x as number, event.y as number]
  if (
    event.possibleElements !== undefined &&
    event.possibleElements.length === 2
  ) {
    if (
      event.possibleElements[0] instanceof Segment &&
      event.possibleElements[1] instanceof Segment
    ) {
      return context.figure.create('PointIntersectionLL', {
        line1: event.possibleElements[0],
        line2: event.possibleElements[1],
      })
    }
  }
  if (event.element instanceof Segment) {
    const [point1, point2] = [event.element.point1, event.element.point2]
    const k =
      ((x - point1.x) * (point2.x - point1.x) +
        (y - point1.y) * (point2.y - point1.y)) /
      distance(point1, point2) ** 2
    return context.figure.create('PointOnLine', { k, line: event.element })
  }
  if (event.waitingWithModal === true) return undefined
  return context.figure.create('Point', { x, y })
}

function getExistingPointOrCreatetPoindWasASuccess(event: MyEvent): boolean {
  return event.waitingWithModal === false
}

function userMessage(text: string, figure: Figure): void {
  if (!figure.options.automaticUserMessage) return
  const div = figure.divUserMessage
  if (div != null) div.innerHTML = figure.isDynamic ? text : ''
}

function testRadiusIsSegment(
  radius: string,
  figure: Figure,
): [Point, Point] | undefined {
  if (radius === undefined || typeof radius !== 'string') return
  const [label1, label2] = radius.split('')
  const points = [...figure.elements.values()].filter(
    (e) => e.type !== 'pointer' && e.type?.includes('Point'),
  ) as Point[]
  const matchPoints1 = points.filter((p) => p.label === label1)
  const matchPoints2 = points.filter((p) => p.label === label2)
  if (matchPoints1.length !== 1 || matchPoints2.length !== 1) return
  return [matchPoints1[0], matchPoints2[0]]
}

export default ui
