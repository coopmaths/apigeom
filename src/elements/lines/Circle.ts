import type Figure from '../../Figure'
import type Point from '../points/Point'
import type Segment from './Segment'

import Element2D from '../Element2D'
import type { OptionsCircle, OptionsElement2D } from '../interfaces'

/**
 * Trace un cercle dont on connait le centre et le rayon
 */
class Circle extends Element2D {
  /** Couleur de remplissage du cercle */
  private _fillColor: 'none' | string
  /** Opacité de remplissage entre 0 et 1 */
  private _fillOpacity?: number
  /** Rayon du cercle */
  _radius: number
  /** Pointeur vers la première extrémité */
  center: Point
  constructor(
    figure: Figure,
    {
      center,
      fillOpacity = figure.options.fillOpacity,
      radius,
      ...options
    }: OptionsCircle & OptionsElement2D,
  ) {
    super(figure, options)
    this.type = 'Circle'
    this._radius = radius
    this.center = center
    if (options?.fillColor !== undefined) this._fillColor = options.fillColor
    else this._fillColor = this.figure.options.fillColor
    this.fillOpacity = fillOpacity || this.figure.options.fillOpacity
    if (options?.isDashed !== undefined) this._isDashed = options.isDashed
  }

  /** Créé un nouveau cercle image d'une homothétie */
  dilate(center: Point, factor: number, options?: OptionsElement2D): Circle {
    const newCenter = this.center.dilate(center, factor, {
      isChild: true,
      isVisible: false,
    })
    const newCircle = this.figure.create('Circle', {
      center: newCenter,
      radius: Math.abs(factor) * this.radius,
      ...options,
    })
    newCircle.transformation = { center, factor, origin: this, type: 'dilate' }
    return newCircle
  }

  distancePointer(x: number, y: number): number {
    if (
      (this.figure.currentState === 'FILL' || (this.figure.currentState = 'SET_OPTIONS' && this.fillColor !== 'none')) &&
      this.figure.pointer.isInCircle(this)
    )
      return 0
    const distanceToCenter =
      Math.hypot(
        (x - this.center.x) * this.figure.xScale,
        (y - this.center.y) * this.figure.yScale,
      ) * this.figure.scale
    return Math.abs(distanceToCenter - this.radius * this.figure.scale)
  }

  draw(): void {
    this.groupSvg = document.createElementNS(
      'http://www.w3.org/2000/svg',
      'circle',
    )
    this.center.subscribe(this)
    this.setVisibilityColorThicknessAndDashed()
    this.update()
  }

  /** Créé un nouveau cercle image d'une symétrie centrale */
  reflect(center: Point, options?: OptionsElement2D): Circle {
    const newCenter = this.center.reflect(center, {
      isChild: true,
      isVisible: false,
    })
    const newCircle = this.figure.create('Circle', {
      center: newCenter,
      radius: this.radius,
      ...options,
    })
    newCircle.transformation = { center, origin: this, type: 'reflect' }
    return newCircle
  }

  /** Créé un nouveau cercle image d'une symétrie axiale */
  reflectOverLine(line: Segment, options?: OptionsElement2D): Circle {
    const newCenter = this.center.reflectOverLine(line, {
      isChild: true,
      isVisible: false,
    })
    const newCircle = this.figure.create('Circle', {
      center: newCenter,
      radius: this.radius,
      ...options,
    })
    newCircle.transformation = { line, origin: this, type: 'reflectOverLine' }
    return newCircle
  }

  /** Créé un nouveau cercle image d'une rotation */
  rotate(center: Point, angle: number, options?: OptionsElement2D): Circle {
    const newCenter = this.center.rotate(center, angle, {
      isChild: true,
      isVisible: false,
    })
    const newCircle = this.figure.create('Circle', {
      center: newCenter,
      radius: this.radius,
      ...options,
    })
    newCircle.transformation = { angle, center, origin: this, type: 'rotation' }
    return newCircle
  }

  /** Modifie la couleur et l'épaisseur de l'élément */
  setVisibilityColorThicknessAndDashed(): void {
    this.color = this._color
    this.fillColor = this._fillColor
    if (this._fillOpacity !== undefined) this.fillOpacity = this._fillOpacity
    if (this._isDashed !== undefined) this.isDashed = this._isDashed
    this.isDashed = this._isDashed
    this.isVisible = this._isVisible
  }

  toJSON(): object {
    return {
      ...this.jsonOptions(),
      fillColor: this.fillColor,
      fillOpacity: this.fillOpacity,
      idCenter: this.center.id,
      radius: this._radius,
    }
  }

  translate(point1: Point, point2: Point, options?: OptionsElement2D): Circle {
    const newCenter = this.center.translate(point1, point2, {
      isChild: true,
      isVisible: false,
    })
    const newCircle = this.figure.create('Circle', {
      center: newCenter,
      radius: this.radius,
      ...options,
    })
    newCircle.transformation = {
      origin: this,
      point1,
      point2,
      type: 'translation',
    }
    return newCircle
  }

  update(): void {
    if (
      this.center.x === undefined ||
      this.center.y === undefined ||
      Number.isNaN(this.center.x) ||
      Number.isNaN(this.center.y)
    ) {
      this.groupSvg.removeAttribute('r')
    } else {
      const xSvg = this.figure.xToSx(this.center.x)
      const ySvg = this.figure.yToSy(this.center.y)
      const rSvg = this.figure.pixelsPerUnit * this._radius * this.figure.scale
      this.groupSvg.setAttribute('cx', `${xSvg}`)
      this.groupSvg.setAttribute('cy', `${ySvg}`)
      this.groupSvg.setAttribute('r', `${rSvg}`)
    }
    this.notify()
  }

  get description(): string {
    if (this.transformation?.type === 'reflect') {
      return `Image du cercle $${(this.transformation.origin as Circle).notation}$ dans la symétrie de centre $${this.transformation?.center.notation}$`
    }
    if (this.transformation?.type === 'reflectOverLine') {
      return `Image du cercle $${(this.transformation.origin as Circle).notation}$ dans la symétrie d'axe  $${this.transformation?.line.notation}$`
    }
    if (this.transformation?.type === 'dilate') {
      return `Image du cercle $${(this.transformation.origin as Circle).notation}$ dans l'homothétie de centre $${this.transformation?.center.notation}$ et de rapport $${this.transformation.factor}$`
    }
    if (this.transformation?.type === 'rotation') {
      return `Image du cercle $${(this.transformation.origin as Circle).notation}$ dans la rotation de centre $${this.transformation.center.notation}$ et d'angle $${Math.abs(this.transformation.angle)}\\degree$ dans le sens ${this.transformation.angle > 0 ? 'anti-horaire' : 'horaire'}`
    }
    if (this.transformation?.type === 'translation') {
      return `Image du cercle $${(this.transformation.origin as Circle).notation}$ dans la translation qui transforme $${(this.transformation.point1 as Point).notation}$ en $${(this.transformation.point2 as Point).notation}$`
    }
    const centerName =
      this.center.label !== '' ? this.center.label : this.center.id
    return `Cercle de centre ${centerName} et de rayon ${this.radius} cm`
  }

  /** Couleur de remplissage au format HTML */
  get fillColor(): string {
    return this._fillColor
  }

  /** Change la couleur des tracés de l'élément */
  set fillColor(color: string) {
    this._fillColor = color
    this.groupSvg.setAttribute('fill', this._fillColor)
  }

  /** Couleur de remplissage au format HTML */
  get fillOpacity(): number | undefined {
    return this._fillOpacity
  }

  /** Change la couleur des tracés de l'élément */
  set fillOpacity(opacity: number | undefined) {
    this._fillOpacity = opacity
    if (opacity !== undefined)
      this.groupSvg.setAttribute('fill-opacity', opacity.toString())
  }

  get latex(): string {
    let result = `% Cercle ${this.id}`
    result += `\n\\draw${this.latexOptions} (${this.center.x}, ${this.center.y}) circle (${this.radius});`
    return result
  }

  get notation(): string {
    return this.id
  }

  get radius(): number {
    return this._radius
  }

  set radius(radius: number) {
    this._radius = radius
    this.update()
  }
}

export default Circle
