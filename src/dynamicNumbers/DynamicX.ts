import type Figure from '../Figure'
import type Point from '../elements/points/Point'
import type TextDynamicByPosition from '../elements/text/TextDynamicByPosition'

import DynamicNumber from './DynamicNumber'

class DynamicX extends DynamicNumber {
  dynamicText: TextDynamicByPosition
  point: Point
  constructor(figure: Figure, { point }: { point: Point }) {
    super(figure, {})
    this.point = point
    this.point.subscribe(this)
    this.dynamicText = this.figure.create('TextDynamicByPosition', {
      color: 'blue',
      dynamicNumber: this,
      isChild: true,
      maximumFractionDigits: 2,
      minimumFractionDigits: 0,
      textAfter: '',
      textBefore: '',
      x: point.x,
      y: -1,
    })
    this.type = 'DynamicX'
    this.update()
    this.dynamicText.div.style.fontWeight = 'bolder'
  }

  toJSON() {
    return {
      ...super.toJSON(),
      idPoint: this.point.id,
    }
  }
   

  update(): void {
    this.value = this.point.x
    this.dynamicText.x = this.point.x
    if (this.point.y > 0) {
      this.dynamicText.y = -1
    } else {
      this.dynamicText.y = 1
    }
    this.notify()
  }
}

export default DynamicX
