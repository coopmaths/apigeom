import type Figure from '../Figure'
import type Element2D from '../elements/Element2D'
import type Circle from '../elements/lines/Circle'
import type Line from '../elements/lines/Line'
import type Ray from '../elements/lines/Ray'
import type Segment from '../elements/lines/Segment'
import type Point from '../elements/points/Point'

interface PointElement {
  label: string
  type: 'Point'
}
interface LineElement {
  point1: string
  point2: string
  type: 'Line' | 'Ray' | 'Segment'
}
interface CircleElement {
  center: string
  radius: number
  type: 'Circle'
}
interface CircleCenterPointElement {
  center: string
  radius: number
  type: 'CircleCenterPoint'
}

type Element =
  | CircleCenterPointElement
  | CircleElement
  | LineElement
  | PointElement
type CheckElementExistResult =
  | { isValid: boolean; result: Circle[]; type: 'Circle' }
  | { isValid: boolean; result: Circle[]; type: 'CircleCenterPoint' }
  | { isValid: boolean; result: Line[]; type: 'Line' }
  | { isValid: boolean; result: Point[]; type: 'Point' }
  | { isValid: boolean; result: Ray[]; type: 'Ray' }
  | { isValid: boolean; result: Segment[]; type: 'Segment' }
  | { isValid: false; result: []; type: '' }

export default function checkElementExist(
  element: Element,
  figure: Figure,
): CheckElementExistResult {
  const elements = [...figure.elements.values()].filter(
    (e) => e.type !== 'pointer',
  ) as Element2D[]
  let filteredElements = elements.filter((e) => e.type === element.type) as
    | Circle[]
    | Line[]
    | Point[]
    | Ray[]
    | Segment[]
  switch (element.type) {
    case 'Point':
      filteredElements = filteredElements.filter(
        (e) => e.type === 'Point',
      ) as Point[]
      filteredElements = filteredElements.filter(
        (e) => e.label === element.label,
      )
      return {
        isValid: filteredElements.length === 1,
        result: filteredElements,
        type: element.type,
      }
    case 'Line':
      filteredElements = filteredElements.filter(
        (e) => e.type === 'Line',
      ) as Line[]
      filteredElements = filteredElements.filter(
        (e) =>
          (e.point1.label === element.point1 &&
            e.point2.label === element.point2) ||
          (e.point1.label === element.point2 &&
            e.point2.label === element.point1),
      )
      return {
        isValid: filteredElements.length === 1,
        result: filteredElements,
        type: element.type,
      }
    case 'Segment':
      filteredElements = filteredElements.filter(
        (e) => e.type === 'Segment',
      ) as Line[]
      filteredElements = filteredElements.filter(
        (e) =>
          (e.point1.label === element.point1 &&
            e.point2.label === element.point2) ||
          (e.point1.label === element.point2 &&
            e.point2.label === element.point1),
      )
      return {
        isValid: filteredElements.length === 1,
        result: filteredElements,
        type: element.type,
      }
    case 'Ray':
      filteredElements = filteredElements.filter(
        (e) => e.type === 'Ray',
      ) as Line[]
      filteredElements = filteredElements.filter(
        (e) =>
          e.point1.label === element.point1 &&
          e.point2.label === element.point2,
      )
      return {
        isValid: filteredElements.length === 1,
        result: filteredElements,
        type: element.type,
      }
    case 'Circle':
      filteredElements = filteredElements.filter(
        (e) => e.type === 'Circle',
      ) as Circle[]
      filteredElements = filteredElements.filter(
        (e) => e.center.label === element.center && e.radius === element.radius,
      )
      return {
        isValid: filteredElements.length === 1,
        result: filteredElements,
        type: element.type,
      }
    case 'CircleCenterPoint':
      filteredElements = filteredElements.filter(
        (e) => e.type === 'CircleCenterPoint',
      ) as Circle[]
      filteredElements = filteredElements.filter(
        (e) => e.center.label === element.center && e.radius === element.radius,
      )
      return {
        isValid: filteredElements.length === 1,
        result: filteredElements,
        type: element.type,
      }
    default:
      filteredElements = []
  }
  return { isValid: false, result: [], type: '' }
}
