import type Figure from '../../Figure'
import type Point from '../points/Point'
import type Segment from './Segment'

import { colorLatex } from '../../lib/handleColors'
import Element2D from '../Element2D'
import type { Coords } from '../calculus/Coords'
import type { OptionsElement2D } from '../interfaces'

/**
 * Trace une polygone
 */

class Polygon extends Element2D {
  _fillColor = 'none'
  _fillOpacity = 0.5
  /** Est-ce qu'on créé un segment pour chaque côté ? */
  isBuiltWithSegments: boolean
  /** Liste des sommets */
  points: Point[]
  /** Liste des côtés */
  readonly segments: Segment[]
  /** Forme des sommets */
  shape: '' | 'o' | 'x'
  constructor(
    figure: Figure,
    {
      fillColor = 'none',
      fillOpacity = figure.options.fillOpacity,
      isBuiltWithSegments = true,
      points,
      shape = '',
      ...options
    }: {
      fillColor?: string
      fillOpacity?: number
      isBuiltWithSegments?: boolean
      points: Point[] | Coords[]
      shape?: '' | 'o' | 'x'
    } & OptionsElement2D,
  ) {
    super(figure, options)
    this.type = 'Polygon'
    if (isStricCoords(points[0])) {
      this.points = []
      for (const coord of points as Coords[]) {
        this.points.push(
          this.figure.create('Point', {
            x: coord.x,
            y: coord.y,
            isVisible: false,
            isChild: false,
          }),
        )
      }
    } else {
      this.points = points as Point[]
    }
    this.shape = shape
    this.segments = []
    this.fillColor = fillColor
    this.fillOpacity = fillOpacity
    this.isBuiltWithSegments = isBuiltWithSegments
    for (const point of this.points) {
      point.subscribe(this)
    }
  }

  /** Créer un segment caché pour chacun des côtés du polygone et ainsi permettre de placer des milieux, tracer des perpendiculaires... */
  createSegments(): void {
    for (let i = 0; i < this.points.length; i++) {
      const point1 = this.points.at(i % this.points.length) as Point
      const point2 = this.points.at((i + 1) % this.points.length) as Point
      const segment = this.figure.create('Segment', {
        color: '',
        id: `${this.id}_segment${i.toString()}`,
        isChild: true,
        point1,
        point2,
      })
      this.segments.push(segment)
    }
    for (const segment of this.segments) {
      segment.createdBy = this
    }
  }

  dilate(center: Point, factor: number, options?: OptionsElement2D): Polygon {
    const newPoints = []
    for (const point of this.points) {
      newPoints.push(
        point.dilate(center, factor, { isChild: true, isVisible: false }),
      )
    }
    const newPolygon = this.figure.create('Polygon', {
      points: newPoints,
      ...options,
    })
    newPolygon.transformation = { center, factor, origin: this, type: 'dilate' }
    return newPolygon
  }

  distancePointer(x: number, y: number): number {
    if (
      this.figure.currentState === 'FILL' &&
      this.figure.pointer.isInPolygon(this)
    )
      return 0
    let minDistance = Number.POSITIVE_INFINITY
    for (const segment of this.segments) {
      const distance = segment.distancePointer(x, y)
      if (distance < minDistance) minDistance = distance
    }
    return minDistance
  }

  draw(): void {
    this.groupSvg = document.createElementNS(
      'http://www.w3.org/2000/svg',
      'polygon',
    )
    this.groupSvg.setAttribute('fill', this.fillColor ?? 'none')
    this.groupSvg.setAttribute('fill-opacity', this.fillOpacity.toString())
    this.setVisibilityColorThicknessAndDashed()
    if (this.isBuiltWithSegments) {
      this.createSegments()
    }
    this.update()
  }

  reflect(center: Point, options?: OptionsElement2D): Polygon {
    const newPoints = []
    for (const point of this.points) {
      newPoints.push(point.reflect(center, { isChild: true, isVisible: false }))
    }
    const newPolygon = this.figure.create('Polygon', {
      points: newPoints,
      ...options,
    })
    newPolygon.transformation = { center, origin: this, type: 'reflect' }
    return newPolygon
  }

  reflectOverLine(line: Segment, options?: OptionsElement2D): Polygon {
    const newPoints = []
    for (const point of this.points) {
      newPoints.push(
        point.reflectOverLine(line, { isChild: true, isVisible: false }),
      )
    }
    const newPolygon = this.figure.create('Polygon', {
      points: newPoints,
      ...options,
    })
    newPolygon.transformation = { line, origin: this, type: 'reflectOverLine' }
    return newPolygon
  }

  rotate(center: Point, angle: number, options?: OptionsElement2D): Polygon {
    const newPoints = []
    for (const point of this.points) {
      newPoints.push(
        point.rotate(center, angle, { isChild: true, isVisible: false }),
      )
    }
    const newPolygon = this.figure.create('Polygon', {
      points: newPoints,
      ...options,
    })
    newPolygon.transformation = {
      center,
      origin: this,
      angle,
      type: 'rotation',
    }
    return newPolygon
  }

  remove(): void {
    super.remove()
    for (const segment of this.segments) {
      segment.remove()
    }
  }

  toJSON(): object {
    const idPoints = []
    for (const point of this.points) {
      idPoints.push(point.id)
    }
    return {
      ...this.jsonOptions(),
      idPoints,
    }
  }

  translate(point1: Point, point2: Point, options?: OptionsElement2D): Polygon {
    const newPoints = []
    for (const point of this.points) {
      newPoints.push(
        point.translate(point1, point2, { isChild: true, isVisible: false }),
      )
    }
    const newPolygon = this.figure.create('Polygon', {
      points: newPoints,
      ...options,
    })
    newPolygon.transformation = {
      origin: this,
      point1,
      point2,
      type: 'translation',
    }
    return newPolygon
  }

  update(): void {
    this.notify()
    if (!allCoordsAreNumber(this.points)) {
      this.groupSvg.removeAttribute('points')
    } else {
      let pointsCoords = ''
      for (const point of this.points) {
        pointsCoords += `${this.figure.xToSx(point.x).toString()},${this.figure.yToSy(point.y).toString()} `
      }
      this.groupSvg.setAttribute('points', pointsCoords)
    }
  }

  get description(): string {
    if (this.transformation?.type === 'reflect') {
      return `Image du polygone $${(this.transformation.origin as Polygon).notation}$ dans la symétrie de centre $${this.transformation?.center.notation}$`
    }
    if (this.transformation?.type === 'reflectOverLine') {
      return `Image du polygone $${(this.transformation.origin as Polygon).notation}$ dans la symétrie d'axe $${this.transformation?.line.notation}$`
    }
    if (this.transformation?.type === 'dilate') {
      return `Image du polygone $${(this.transformation.origin as Polygon).notation}$ dans l'homothétie de centre $${this.transformation?.center.notation}$ et de rapport $${this.transformation.factor}$`
    }
    if (this.transformation?.type === 'rotation') {
      return `Image du polygone $${(this.transformation.origin as Polygon).notation}$ dans la rotation de centre $${this.transformation.center.notation}$ et d'angle $${Math.abs(this.transformation.angle)}\\degree$ dans le sens ${this.transformation.angle > 0 ? 'anti-horaire' : 'horaire'}`
    }
    if (this.transformation?.type === 'translation') {
      return `Image du polygone $${(this.transformation.origin as Polygon).notation}$ dans la translation qui transforme $${this.transformation.point1.notation}$ en $${this.transformation.point2.notation}$`
    }
    return `Polygone ${this.notation}`
  }

  /** Couleur de remplissage au format HTML */
  get fillColor(): string {
    return this._fillColor
  }

  /** Change la couleur des tracés de l'élément */
  set fillColor(color: string) {
    this._fillColor = color
    this.groupSvg.setAttribute('fill', this._fillColor)
  }

  /** Couleur de remplissage au format HTML */
  get fillOpacity(): number {
    return this._fillOpacity ?? this.figure.options.fillOpacity
  }

  /** Change la couleur des tracés de l'élément */
  set fillOpacity(opacity: number) {
    this._fillOpacity = opacity
    if (opacity !== undefined)
      this.groupSvg.setAttribute('fill-opacity', opacity.toString())
  }

  get latex(): string {
    let result = `% Polygone ${this.points.reduce((acc, point) => acc + point.label, '')}`
    result += `\n \\draw [color=${colorLatex(this.color ? this.color : 'none')},fill=${colorLatex(this.fillColor ? this.fillColor : 'none')},fill opacity=${this.fillOpacity},line width=${this.thickness}] `
    for (const point of this.points) {
      result += `(${point.x.toFixed(2)},${point.y.toFixed(2)}) -- `
    }
    result += ' cycle;'
    return result
  }

  get notation(): string {
    return this.points.reduce((acc, point) => {
      const pointName = point.label === '' ? point.id : point.label
      return acc + pointName
    }, '')
  }
}

export default Polygon

function allCoordsAreNumber(points: Point[] | Coords[]): boolean {
  for (const point of points) {
    if (!Number.isFinite(point.x) || !Number.isFinite(point.y)) return false
  }
  return true
}

/**
 * Vérifie si l'objet est uniquement de type Coords et n'a que les clés x et y
 * @param obj
 * @returns
 */
export function isStricCoords(obj: Coords): obj is { x: number; y: number } {
  if (
    typeof obj === 'object' &&
    obj !== null &&
    Object.keys(obj).length === 2
  ) {
    if (
      'x' in obj &&
      'y' in obj &&
      typeof obj.x === 'number' &&
      typeof obj.y === 'number'
    ) {
      return true
    }
  }
  return false
}
