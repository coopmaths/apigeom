import type Figure from "../../Figure";
import { distance, homothetieCoord, rotationCoord } from "../calculus/Coords";
import Element2D from "../Element2D"
import type { OptionsElement2D } from "../interfaces";
import type Point from "../points/Point";

export default class MarkRightAngle extends Element2D {
    /* Point qui indique la direction du 2e sommet du carré */
    directionPoint: Point
    /* Point qui indique le sommet du carré */
    point: Point
    /* Taille du carré en unités de la figure */
    size: number
    svgPolygon: SVGPolygonElement
    constructor(figure: Figure, { point, directionPoint, size = 0.4, ...options}: { point: Point, directionPoint: Point, size?: number } & OptionsElement2D) {
        super(figure, { ...options})
        this.directionPoint = directionPoint
        this.point = point
        this.size = size
        this.type = 'MarkRightAngle'
        this.point.subscribe(this)
        this.directionPoint.subscribe(this)
        this.svgPolygon = document.createElementNS('http://www.w3.org/2000/svg', 'polygon')
        this.groupSvg.appendChild(this.svgPolygon)
        this.update()
        this.setVisibilityColorThicknessAndDashed()
    }

    draw(): void {
    this.update()
  }

    update(): void {
        const x1 = this.figure.xToSx(this.point.x)
        const y1 = this.figure.yToSy(this.point.y)
        const sommet2 = homothetieCoord(this.directionPoint, this.point, this.size / distance(this.point, this.directionPoint))
        const x2 = this.figure.xToSx(sommet2.x)
        const y2 = this.figure.yToSy(sommet2.y)
        const sommet3 = rotationCoord(this.point, sommet2, -90)
        const x3 = this.figure.xToSx(sommet3.x)
        const y3 = this.figure.yToSy(sommet3.y)
        const sommet4 = rotationCoord(sommet2, this.point, 90)
        const x4 = this.figure.xToSx(sommet4.x)
        const y4 = this.figure.yToSy(sommet4.y)
        this.svgPolygon.setAttribute('points', `${x1},${y1} ${x2},${y2} ${x3},${y3} ${x4},${y4}`)
        this.svgPolygon.setAttribute('fill', 'none')
        this.notify()
      }
    
      toJSON(): object {
        return {
          ...this.jsonOptions(),
          color: this.color,
          idPoint: this.point.id,
          idDirectionPoint: this.directionPoint.id,
        }
      }

      get latex(): string {
        const sommet2 = homothetieCoord(this.directionPoint, this.point, this.size / distance(this.point, this.directionPoint))
        const sommet3 = rotationCoord(this.point, sommet2, -90)
        const sommet4 = rotationCoord(sommet2, this.point, 90)
        let result = `% Angle droit en ${this.point.label}`
        result += `\n\\draw${this.latexOptions} (${this.point.x},${this.point.y}) -- (${sommet2.x},${sommet2.y}) -- (${sommet3.x},${sommet3.y}) -- (${sommet4.x},${sommet4.y}) -- cycle;`
        return result
      }
}