import type Figure from '../../Figure'
import type { OptionsElement2D } from '../interfaces'
import type Circle from '../lines/Circle'
import type Line from '../lines/Line'
import type Point from './Point'

import Element2D from '../Element2D'

class PointsIntersectionLC extends Element2D {
  /** Deuxième cercle */
  circle: Circle
  /** Droite ou segment ou demi-droite */
  line: Line
  /** Point d'intersection avec la plus grande ordonnée */
  point1: Point
  /** Point d'intersection avec la plus petite ordonnée */
  point2: Point
  shape: '' | 'o' | 'x'
  sizeInPixels?: number
  constructor(
    figure: Figure,
    {
      circle,
      line,
      shape = 'x',
      sizeInPixels,
      ...options
    }: {
      circle: Circle
      line: Line
      shape?: '' | 'o' | 'x'
      sizeInPixels?: number
    } & OptionsElement2D,
  ) {
    super(figure, options)
    this.type = 'PointsIntersectionLC'
    sizeInPixels = sizeInPixels ?? figure.options.pointSize
    this.line = line
    this.circle = circle
    this.point1 = figure.create('PointIntersectionLC', {
      circle,
      line,
      n: 1,
      shape,
      sizeInPixels,
      ...options,
    })
    this.point2 = figure.create('PointIntersectionLC', {
      circle,
      line,
      n: 2,
      shape,
      sizeInPixels,
      ...options,
    })
    this.shape = shape
    this.sizeInPixels = sizeInPixels
  }

  draw(): void {}

  toJSON(): object {
    return {
      ...this.jsonOptions(),
      idCircle: this.circle.id,
      idLine: this.line.id,
      shape: this.shape,
      sizeInPixels: this.sizeInPixels,
    }
  }
}

export default PointsIntersectionLC
