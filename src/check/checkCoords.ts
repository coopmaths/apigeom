import type Figure from '../Figure'
import Element2D from '../elements/Element2D'
import type Point from '../elements/points/Point'

import { displayNumber, round } from '../lib/format'

export default function checkCoords({
  checkOnlyAbscissa = false,
  figure,
  label,
  x,
  y = 0,
  color,
}: {
  checkOnlyAbscissa?: boolean
  figure: Figure
  label?: string
  x: number
  y?: number
  color?: string
}): { isValid: boolean; message: string; points: Point[] } {
  let points: Point[] = []
  if (color !== undefined) {
    points = [...figure.elements.values()].filter(
      (e) =>
        e instanceof Element2D &&
        e.type !== 'pointer' &&
        e.type.includes('Point') &&
        e.color === color,
    ) as Point[]
  } else {
    points = [...figure.elements.values()].filter(
      (e) =>
        e.type !== 'pointer' &&
        e.type.startsWith('Point') &&
        e instanceof Element2D &&
        e.isSelectable,
    ) as Point[]
  }
  if (label !== undefined) {
    const matchPoints = points.filter((p) => p.label === label)
    if (matchPoints.length === 0) {
      return {
        isValid: false,
        message: `Il n'y a pas de point $${label}$ dans la figure.`,
        points: [],
      }
    }
    if (matchPoints.length > 1) {
      return {
        isValid: false,
        message: `Il y a ${matchPoints.length} points $${label}$ dans la figure.`,
        points: matchPoints,
      }
    }
    const point = matchPoints[0]
    const isValid = round(point.x) === round(x) && round(point.y) === round(y)
    let message: string
    if (checkOnlyAbscissa) {
      message = isValid
        ? ''
        : `Le point $${label}$ a pour abscisse $${displayNumber(point.x)}$ et non $${displayNumber(x)}$.`
    } else {
      message = isValid
        ? ''
        : `Le point $${label}$ a pour coordonnées $(${displayNumber(point.x)}\\;;\\;${displayNumber(point.y)})$ et non $(${displayNumber(x)}\\;;\\;${displayNumber(y)})$.`
    }
    return { isValid, message, points: [point] }
  }
  let matchPoints = points.filter(
    (p) => round(p.x) === round(x) && round(p.y) === round(y),
  )
  if (checkOnlyAbscissa) {
    matchPoints = points.filter((p) => round(p.x) === round(x))
  }
  if (matchPoints.length === 0) {
    if (checkOnlyAbscissa) {
      return {
        isValid: false,
        message: `Il n'y a pas de point d'abscisse $${displayNumber(x)}$ dans la figure.`,
        points: [],
      }
    }
    return {
      isValid: false,
      message: `Il n'y a pas de point en $(${displayNumber(x)}\\;;\\;${displayNumber(y)})$ dans la figure.`,
      points: [],
    }
  }
  if (matchPoints.length > 1) {
    if (checkOnlyAbscissa) {
      return {
        isValid: true,
        message: `Il y a ${matchPoints.length} points d'abscisse $${displayNumber(x)}$ dans la figure.`,
        points: matchPoints,
      }
    }
    return {
      isValid: true,
      message: `Il y a ${matchPoints.length} points de coordonnées $(${displayNumber(x)}\\;;\\;${displayNumber(y)})$ dans la figure.`,
      points: matchPoints,
    }
  }
  const point = matchPoints[0]
  return { isValid: true, message: '', points: [point] }
}
