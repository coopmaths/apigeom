import type Figure from '../../Figure'
import type Point from '../points/Point'

import Element2D from '../Element2D'
import { distance, similitudeCoord } from '../calculus/Coords'
import type { Binome, OptionsElement2D } from '../interfaces'

/**
 * Trace un segment qui a pour extrémités deux points donnés
 * Si un point a des coordonnées non définies alors le segment est effacé momentannément
 * C'est la classe parent de celles qui permettent de tracer une droite ou une demi-droite
 */
class Segment extends Element2D {
  borderSize: number
  /** Élément parent avec lequel il est lié pour le changement de style (comme les côtés d'un polygone) */
  createdBy?: Element2D
  /** Première extrémité du segment */
  point1: Point
  /** Deuxième extrémité du segment */
  point2: Point
  /** Détermine s'il faut afficher les deux extrémités, que celle de gauche ou que celle de droite */
  shape?: '->' | '-|' | '' | '<-' | '<->' | '<-|' | '|-' | '|->' | '|-|'
  svgArrow1!: SVGElement
  svgArrow2!: SVGElement
  svgBorder1!: SVGElement
  svgBorder2!: SVGElement
  svgLine!: SVGElement
  constructor(
    figure: Figure,
    {
      borderSize,
      color = figure.options.color,
      isDashed = figure.options.isDashed,
      point1,
      point2,
      shape = '',
      thickness = figure.options.thickness,
      ...options
    }: {
      borderSize?: number
      point1: Point
      point2: Point
      shape?: '->' | '-|' | '' | '<-' | '<->' | '<-|' | '|-' | '|->' | '|-|'
    } & OptionsElement2D,
  ) {
    super(figure, options)
    this.type = 'Segment'
    this.point1 = point1
    this.point2 = point2
    this.shape = shape
    this.borderSize = borderSize ?? figure.options.borderSize
    this.color = color
    this.thickness = thickness
    this.isDashed = isDashed
    this.point1.subscribe(this)
    this.point2.subscribe(this)
  }

  dilate(center: Point, factor: number, options?: OptionsElement2D): Segment {
    const newPoint1 = this.point1.dilate(center, factor, {
      isChild: true,
      isVisible: false,
    })
    const newPoint2 = this.point2.dilate(center, factor, {
      isChild: true,
      isVisible: false,
      ...options,
    })
    let type = this.type as 'Line' | 'Ray' | 'Segment' | 'LineParallel'
    if (type !== 'Ray' && type !== 'Segment') type = 'Line' // Gestion de toutes les droites particulières
    const newSegment = this.figure.create(type, {
      point1: newPoint1,
      point2: newPoint2,
    })
    newSegment.transformation = { center, factor, origin: this, type: 'dilate' }
    return newSegment
  }

  distancePointer(x: number, y: number): number {
    const pointerCoords = { x, y }
    // Calculer la distance entre le point donné et les extrémités du segment
    const distanceToP1 = distance(pointerCoords, this.point1)
    const distanceToP2 = distance(pointerCoords, this.point2)

    // Trouver la projection du point sur le segment
    const segmentLength = distance(this.point1, this.point2)
    const projection =
      ((x - this.point1.x) * (this.point2.x - this.point1.x) +
        (y - this.point1.y) * (this.point2.y - this.point1.y)) /
      segmentLength ** 2
    // La distance minimale est la distance à l'une des extrémités si la projection est en dehors du segment
    if (projection < 0) {
      return distanceToP1
    }
    if (projection > 1) {
      return distanceToP2
    }
    // Calculer la distance verticale entre le point et le segment
    const closestX =
      this.point1.x + projection * (this.point2.x - this.point1.x)
    const closestY =
      this.point1.y + projection * (this.point2.y - this.point1.y)
    const closest = { x: closestX, y: closestY }
    return distance(pointerCoords, closest)
  }

  draw(): void {
    this.svgLine = document.createElementNS(
      'http://www.w3.org/2000/svg',
      'line',
    )
    this.groupSvg.appendChild(this.svgLine)
    if (this.shape !== '') {
      this.svgBorder1 = document.createElementNS(
        'http://www.w3.org/2000/svg',
        'line',
      )
      this.svgBorder2 = document.createElementNS(
        'http://www.w3.org/2000/svg',
        'line',
      )
      this.svgArrow1 = document.createElementNS(
        'http://www.w3.org/2000/svg',
        'polyline',
      )
      this.svgArrow2 = document.createElementNS(
        'http://www.w3.org/2000/svg',
        'polyline',
      )
      this.groupSvg.appendChild(this.svgBorder1)
      this.groupSvg.appendChild(this.svgBorder2)
      this.groupSvg.appendChild(this.svgArrow1)
      this.groupSvg.appendChild(this.svgArrow2)
    }
    this.setVisibilityColorThicknessAndDashed()
    this.update()
  }

  drawArrow1(): void {
    const a = { x: this.point1.x, y: this.point1.y }
    const b = { x: this.point2.x, y: this.point2.y }
    const l = Math.hypot(
      this.point1.x - this.point2.x,
      this.point1.y - this.point2.y,
    )
    const { x: x1, y: y1 } = similitudeCoord(
      b,
      a,
      (1 / l) * this.borderSize * 2,
      45,
    )
    const { x: x2, y: y2 } = similitudeCoord(
      b,
      a,
      (1 / l) * this.borderSize * 2,
      -45,
    )
    const x1Svg = this.figure.xToSx(x1)
    const x2Svg = this.figure.xToSx(x2)
    const y1Svg = this.figure.yToSy(y1)
    const y2Svg = this.figure.yToSy(y2)
    const axSvg = this.figure.xToSx(a.x)
    const aySvg = this.figure.yToSy(a.y)
    if (allAreNumbers(x1Svg, x2Svg, y1Svg, y2Svg, axSvg, aySvg)) {
      this.svgArrow1.setAttribute(
        'points',
        `${x1Svg},${y1Svg} ${axSvg},${aySvg} ${x2Svg},${y2Svg}`,
      )
      this.groupSvg.appendChild(this.svgArrow1)
    }
  }

  drawArrow2(): void {
    const a = { x: this.point1.x, y: this.point1.y }
    const b = { x: this.point2.x, y: this.point2.y }
    const l = Math.hypot(
      this.point1.x - this.point2.x,
      this.point1.y - this.point2.y,
    )
    const { x: x1, y: y1 } = similitudeCoord(
      a,
      b,
      (1 / l) * this.borderSize * 2,
      45,
    )
    const { x: x2, y: y2 } = similitudeCoord(
      a,
      b,
      (1 / l) * this.borderSize * 2,
      -45,
    )
    const x1Svg = this.figure.xToSx(x1)
    const x2Svg = this.figure.xToSx(x2)
    const y1Svg = this.figure.yToSy(y1)
    const y2Svg = this.figure.yToSy(y2)
    const bxSvg = this.figure.xToSx(b.x)
    const bySvg = this.figure.yToSy(b.y)
    if (allAreNumbers(x1Svg, x2Svg, y1Svg, y2Svg, bxSvg, bySvg)) {
      this.svgArrow2.setAttribute(
        'points',
        `${x1Svg},${y1Svg} ${bxSvg},${bySvg} ${x2Svg},${y2Svg}`,
      )
      this.groupSvg.appendChild(this.svgArrow2)
    }
  }

  drawBorder1(): void {
    const a = { x: this.point1.x, y: this.point1.y }
    const b = { x: this.point2.x, y: this.point2.y }
    const l = Math.hypot(
      this.point1.x - this.point2.x,
      this.point1.y - this.point2.y,
    )
    const { x: x1, y: y1 } = similitudeCoord(
      b,
      a,
      (1 / l) * this.borderSize,
      90,
    )
    const { x: x2, y: y2 } = similitudeCoord(
      b,
      a,
      (1 / l) * this.borderSize,
      -90,
    )
    const x1Svg = this.figure.xToSx(x1).toString()
    const x2Svg = this.figure.xToSx(x2).toString()
    const y1Svg = this.figure.yToSy(y1).toString()
    const y2Svg = this.figure.yToSy(y2).toString()
    this.svgBorder1.setAttribute('x1', x1Svg)
    this.svgBorder1.setAttribute('y1', y1Svg)
    this.svgBorder1.setAttribute('x2', x2Svg)
    this.svgBorder1.setAttribute('y2', y2Svg)
    this.groupSvg.appendChild(this.svgBorder1)
  }

  drawBorder2(): void {
    const a = { x: this.point1.x, y: this.point1.y }
    const b = { x: this.point2.x, y: this.point2.y }
    const l = Math.hypot(
      this.point1.x - this.point2.x,
      this.point1.y - this.point2.y,
    )
    const { x: x1, y: y1 } = similitudeCoord(
      a,
      b,
      (1 / l) * this.borderSize,
      90,
    )
    const { x: x2, y: y2 } = similitudeCoord(
      a,
      b,
      (1 / l) * this.borderSize,
      -90,
    )
    const x1Svg = this.figure.xToSx(x1).toString()
    const x2Svg = this.figure.xToSx(x2).toString()
    const y1Svg = this.figure.yToSy(y1).toString()
    const y2Svg = this.figure.yToSy(y2).toString()
    this.svgBorder2.setAttribute('x1', x1Svg)
    this.svgBorder2.setAttribute('y1', y1Svg)
    this.svgBorder2.setAttribute('x2', x2Svg)
    this.svgBorder2.setAttribute('y2', y2Svg)
    this.groupSvg.appendChild(this.svgBorder2)
  }

  reflect(center: Point, options?: OptionsElement2D): Segment {
    const newPoint1 = this.point1.reflect(center, {
      isChild: true,
      isVisible: false,
    })
    const newPoint2 = this.point2.reflect(center, {
      isChild: true,
      isVisible: false,
    })
    let type = this.type as 'Line' | 'Ray' | 'Segment' | 'LineParallel'
    if (type !== 'Ray' && type !== 'Segment') type = 'Line' // Gestion de toutes les droites particulières
    const result = this.figure.create(type, {
      point1: newPoint1,
      point2: newPoint2,
      ...options,
    })
    result.transformation = { center, origin: this, type: 'reflect' }
    return result
  }

  reflectOverLine(line: Segment, options?: OptionsElement2D): Segment {
    const newPoint1 = this.point1.reflectOverLine(line, {
      isChild: true,
      isVisible: false,
    })
    const newPoint2 = this.point2.reflectOverLine(line, {
      isChild: true,
      isVisible: false,
    })
    let type = this.type as 'Line' | 'Ray' | 'Segment' | 'LineParallel'
    if (type !== 'Ray' && type !== 'Segment') type = 'Line' // Gestion de toutes les droites particulières
    const result = this.figure.create(type, {
      point1: newPoint1,
      point2: newPoint2,
      ...options,
    })
    result.transformation = { line, origin: this, type: 'reflectOverLine' }
    return result
  }

  rotate(center: Point, angle: number, options?: OptionsElement2D): Segment {
    const newPoint1 = this.point1.rotate(center, angle, {
      isChild: true,
      isVisible: false,
    })
    const newPoint2 = this.point2.rotate(center, angle, {
      isChild: true,
      isVisible: false,
      ...options,
    })
    let type = this.type as 'Line' | 'Ray' | 'Segment' | 'LineParallel'
    if (type !== 'Ray' && type !== 'Segment') type = 'Line' // Gestion de toutes les droites particulières
    const result = this.figure.create(type, {
      point1: newPoint1,
      point2: newPoint2,
    })
    result.transformation = { angle, center, origin: this, type: 'rotation' }
    return result
  }

  toJSON(): object {
    return {
      ...this.jsonOptions(),
      borderSize: this.borderSize,
      idPoint1: this.point1.id,
      idPoint2: this.point2.id,
      shape: this.shape,
    }
  }

  translate(point1: Point, point2: Point, options: OptionsElement2D) {
    const newPoint1 = this.point1.translate(point1, point2)
    const newPoint2 = this.point2.translate(point1, point2)
    let type = this.type as 'Line' | 'Ray' | 'Segment' | 'LineParallel'
    if (type !== 'Ray' && type !== 'Segment') type = 'Line' // Gestion de toutes les droites particulières
    const result = this.figure.create(type, {
      point1: newPoint1,
      point2: newPoint2,
      ...options,
    })
    result.transformation = {
      origin: this,
      point1,
      point2,
      type: 'translation',
    }
    return result
  }

  update(): void {
    this.notify()
    if (
      this.point1.x === undefined ||
      this.point1.y === undefined ||
      this.point2.x === undefined ||
      this.point2.y === undefined ||
      !Number.isFinite(this.point1.x) ||
      !Number.isFinite(this.point1.y) ||
      !Number.isFinite(this.point2.x) ||
      !Number.isFinite(this.point2.y)
    ) {
      this.svgLine.removeAttribute('x1')
      this.svgLine.removeAttribute('x2')
      this.svgLine.removeAttribute('y1')
      this.svgLine.removeAttribute('y2')
    } else {
      const x1Svg = this.figure.xToSx(this.point1.x)
      const x2Svg = this.figure.xToSx(this.point2.x)
      const y1Svg = this.figure.yToSy(this.point1.y)
      const y2Svg = this.figure.yToSy(this.point2.y)
      this.svgLine.setAttribute('x1', `${x1Svg}`)
      this.svgLine.setAttribute('y1', `${y1Svg}`)
      this.svgLine.setAttribute('x2', `${x2Svg}`)
      this.svgLine.setAttribute('y2', `${y2Svg}`)
    }
    if (this.shape?.at(0) === '|') {
      this.drawBorder1()
      this.svgArrow1?.remove()
    } else if (this.shape?.at(0) === '<') {
      this.drawArrow1()
      this.svgBorder1?.remove()
    } else {
      this.svgArrow1?.remove()
      this.svgBorder1?.remove()
    }
    if (this.shape?.at(-1) === '|') {
      this.drawBorder2()
      this.svgArrow2.remove()
    } else if (this.shape?.at(-1) === '>') {
      this.drawArrow2()
      this.svgBorder2?.remove()
    } else {
      this.svgArrow2?.remove()
      this.svgBorder2?.remove()
    }
  }

  get angleWithHorizontalInDegres(): number {
    const [a, b] = this.equation
    const angle = Math.atan2(-a, b)
    return (angle * 180) / Math.PI
  }

  get color(): string {
    return this._color
  }

  set color(color: string) {
    super.color = color
    for (const element of [this.svgArrow1, this.svgArrow2]) {
      if (element === undefined) continue
      element.setAttribute('stroke', color)
      element.setAttribute('fill', color)
    }
  }

  get description(): string {
    return `Segment ${this.notation}`
  }

  /** Renvoie [a, b, c] tels que ax + by + c = 0 est l'équation de la droite passant par point1 et point2 */
  get equation(): [number, number, number] {
    if (
      this.point1.x === undefined ||
      this.point1.y === undefined ||
      this.point2.x === undefined ||
      this.point2.y === undefined
    )
      return [Number.NaN, Number.NaN, Number.NaN]
    try {
      const a = this.point1.y - this.point2.y
      const b = this.point2.x - this.point1.x
      const c =
        (this.point1.x - this.point2.x) * this.point1.y +
        (this.point2.y - this.point1.y) * this.point1.x
      return [a, b, c]
    } catch (error) {
      console.error('Erreur dans Line.equation()', error)
      // this.exist = false
      return [Number.NaN, Number.NaN, Number.NaN]
    }
  }

  get latex(): string {
    let result = `%${this.description}`
    result += `\n\\draw${this.latexOptions} (${this.point1.x}, ${this.point1.y}) -- (${this.point2.x}, ${this.point2.y});`
    return result
  }

  get notation(): string {
    const point1Name =
      this.point1.label !== '' ? this.point1.label : this.point1.id
    const point2Name =
      this.point2.label !== '' ? this.point2.label : this.point2.id
    return `[${point1Name}${point2Name}]`
  }

  /**
   * Vecteur normal à la droite
   */
  get unitPerpendicularVectorBinome(): Binome {
    try {
      const [a, b] = this.equation
      return { x: a, y: b }
    } catch (error) {
      console.error('Erreur dans Line.directeur()', error)
      return { x: Number.NaN, y: Number.NaN }
    }
  }

  /**
   * Vecteur directeur de la droite
   */
  get unitVectorBinome(): Binome {
    try {
      const [a, b] = this.equation
      return { x: b, y: -a }
    } catch (error) {
      console.error('Erreur dans Line.normal()', error)
      return { x: Number.NaN, y: Number.NaN }
    }
  }
}

export default Segment

function allAreNumbers(...numbers: number[]): boolean {
  for (const number of numbers) {
    if (!Number.isFinite(number)) return false
  }
  return true
}
