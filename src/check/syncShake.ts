import type Figure from '../Figure'
import type Point from '../elements/points/Point'

import { round } from '../lib/format'

export default function syncShake({
  delta = 3,
  figure,
  repeat = 5,
  time = 1,
}: { delta?: number; figure: Figure; repeat?: number; time?: number }): void {
  const timePeranimation = time / repeat
  // Retirer l'animation
  figure.container.style.animation = 'none'
  void figure.container.offsetWidth
  // Remettre l'animation définie dans le css de index.html
  figure.container.style.animation = `shake ${timePeranimation}s`
  figure.container.style.animationIterationCount = repeat.toString()

  const nearX = (x: number): number => {
    const distanceFromBorder = 0.5
    const newX = round(x + (Math.random() - 0.5) * delta, 1)
    if (
      newX > figure.xMin + distanceFromBorder &&
      newX < figure.xMax - distanceFromBorder
    ) {
      return newX
    }
    return x
  }

  const nearY = (y: number): number => {
    const distanceFromBorder = 2
    const newY = round(y + (Math.random() - 0.5) * delta, 1)
    if (
      newY > figure.yMin + distanceFromBorder &&
      newY < figure.yMax - distanceFromBorder
    ) {
      return newY
    }
    return y
  }

  const points = [...figure.elements.values()].filter(
    (e) => e.type !== 'pointer' && e.type === 'Point',
  ) as Point[]
  const freePoints = points.filter((p) => p.isFree)
  for (let _ = 0; _ < repeat; _++) {
    for (const point of freePoints) {
      point.moveTo(nearX(point.x), nearY(point.y))
    }
  }
  figure.container.style.animation = 'none'
}
