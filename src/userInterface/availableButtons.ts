export type availableButtons =
  | 'BISECTOR_BY_POINTS'
  | 'CIRCLE_CENTER_POINT'
  | 'CIRCLE_RADIUS'
  | 'DESCRIPTION'
  | 'DILATE'
  | 'DISC_CENTER_POINT'
  | 'DISC_RADIUS'
  | 'DOWNLOAD_LATEX_SVG'
  | 'DRAG'
  | 'FILL'
  | 'GRID'
  | 'HIDE'
  | 'LINE_PARALLEL'
  | 'LINE_PERPENDICULAR'
  | 'LINE'
  | 'MARK_BETWEEN_POINTS'
  | 'MIDDLE'
  | 'MOVE_LABEL'
  | 'NAME_POINT'
  | 'OPEN'
  | 'PAUSE'
  | 'PERPENDICULAR_BISECTOR'
  | 'PLAY_SKIP_BACK'
  | 'PLAY_SKIP_FORWARD'
  | 'PLAY'
  | 'POINT_INTERSECTION'
  | 'POINT_ON'
  | 'POINT'
  | 'POLYGON'
  | 'RAY'
  | 'REDO'
  | 'REFLECTION_OVER_LINE'
  | 'REFLECTION'
  | 'REMOVE'
  | 'RESTART'
  | 'ROTATE'
  | 'SAVE'
  | 'SEGMENT'
  | 'SET_OPTIONS'
  | 'SHAKE'
  | 'TRANSLATION'
  | 'UNDO'
  | 'VECTOR'
  | 'ZOOM_IN'
  | 'ZOOM_OUT'
