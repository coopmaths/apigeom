import type Figure from '../../Figure'
import type Circle from '../lines/Circle'
import type Line from '../lines/Line'
import type Segment from '../lines/Segment'
import type Point from '../points/Point'

import { defaultMaxSlope, defaultMinSlope } from '../defaultValues'

/**
 * Classe de méthodes statiques pour des calculs sur les coordonnées
 */
export class Coords {
  x: number
  y: number
  constructor(x = 0, y = 0) {
    this.x = x
    this.y = y
  }
}

export function angleWithHorizontalInDegres(A: Coords, B: Coords): number {
  if (
    A.x === undefined ||
    A.y === undefined ||
    B.x === undefined ||
    B.y === undefined
  )
    return Number.NaN
  try {
    const angle = (Math.atan2(B.y - A.y, B.x - A.x) * 180) / Math.PI
    return angle
  } catch (error) {
    return Number.NaN
  }
}

export function intersectionCCCoord(
  circle1: Circle,
  circle2: Circle,
  n: 1 | 2 = 1,
): Coords {
  try {
    const O1 = circle1.center
    const O2 = circle2.center
    const r0 = circle1._radius
    const r1 = circle2._radius
    const x0 = O1.x
    const x1 = O2.x
    const y0 = O1.y
    const y1 = O2.y
    if (
      x0 === undefined ||
      x1 === undefined ||
      y0 === undefined ||
      y1 === undefined
    )
      return new Coords()
    const dx = x1 - x0
    const dy = y1 - y0
    const d = Math.sqrt(dy * dy + dx * dx)
    if (d > r0 + r1) {
      return { x: Number.NaN, y: Number.NaN }
    }
    if (d < Math.abs(r0 - r1)) {
      return { x: Number.NaN, y: Number.NaN }
    }
    const a = (r0 * r0 - r1 * r1 + d * d) / (2.0 * d)
    const x2 = x0 + (dx * a) / d
    const y2 = y0 + (dy * a) / d
    const h = Math.sqrt(r0 * r0 - a * a)
    const rx = -dy * (h / d)
    const ry = dx * (h / d)
    const xi = x2 + rx
    const xiPrime = x2 - rx
    const yi = y2 + ry
    const yiPrime = y2 - ry
    if (n === 1) {
      if (yiPrime > yi) {
        return new Coords(xiPrime, yiPrime)
      }
      return new Coords(xi, yi)
    }
    if (yiPrime > yi) {
      return new Coords(xi, yi)
    }
    return new Coords(xiPrime, yiPrime)
  } catch (error) {
    return new Coords(Number.NaN, Number.NaN)
  }
}

export function intersectionLLCoord(line1: Line, line2: Line): Coords {
  try {
    const [da, db, dc] = line1.equation
    const [fa, fb, fc] = line2.equation
    let x: number
    let y: number
    if (fa * db - fb * da === 0) {
      return { x: Number.NaN, y: Number.NaN }
    }
    y = (fc * da - dc * fa) / (fa * db - fb * da)

    if (da === 0) {
      // si d est horizontale alors f ne l'est pas donc fa<>0
      x = (-fc - fb * y) / fa
    } else {
      // d n'est pas horizontale donc ...
      x = (-dc - db * y) / da
    }
    // On teste le cas du segment ou de la demi-droite
    let isIntersectionExisting = true
    if (
      line1.type === 'Segment' &&
      (x < Math.min(line1.point1.x, line1.point2.x) ||
        x > Math.max(line1.point1.x, line1.point2.x) ||
        y < Math.min(line1.point1.y, line1.point2.y) ||
        y > Math.max(line1.point1.y, line1.point2.y))
    ) {
      isIntersectionExisting = false
    }
    if (
      line2.type === 'Segment' &&
      (x < Math.min(line2.point1.x, line2.point2.x) ||
        x > Math.max(line2.point1.x, line2.point2.x) ||
        y < Math.min(line2.point1.y, line2.point2.y) ||
        y > Math.max(line2.point1.y, line2.point2.y))
    ) {
      isIntersectionExisting = false
    }
    if (line1.type === 'Ray') {
      // Direction gauche droite
      if (line1.point1.x < line1.point2.x && x < line1.point1.x) {
        isIntersectionExisting = false
      }
      // Direction droite gauche
      if (line1.point1.x > line1.point2.x && x > line1.point1.x) {
        isIntersectionExisting = false
      }
      // Direction bas haut
      if (line1.point1.y < line1.point2.y && y < line1.point1.y) {
        isIntersectionExisting = false
      }
      // Direction haut bas
      if (line1.point1.y > line1.point2.y && y > line1.point1.y) {
        isIntersectionExisting = false
      }
    }
    if (line2.type === 'Ray') {
      // Direction gauche droite
      if (line2.point1.x < line2.point2.x && x < line2.point1.x) {
        isIntersectionExisting = false
      }
      // Direction droite gauche
      if (line2.point1.x > line2.point2.x && x > line2.point1.x) {
        isIntersectionExisting = false
      }
      // Direction bas haut
      if (line2.point1.y < line2.point2.y && y < line2.point1.y) {
        isIntersectionExisting = false
      }
      // Direction haut bas
      if (line2.point1.y > line2.point2.y && y > line2.point1.y) {
        isIntersectionExisting = false
      }
    }
    if (!isIntersectionExisting) {
      x = Number.NaN
      y = Number.NaN
    }
    return new Coords(x, y)
  } catch (error) {
    console.error(error)
    return new Coords(Number.NaN, Number.NaN)
  }
}

export function distance(A: Coords | Point, B: Coords | Point): number {
  return Math.hypot(A.x - B.x, A.y - B.y)
}

export function equationLine(
  point1: Coords | Point,
  point2: Coords | Point,
): [number, number, number] {
  try {
    const a = point1.y - point2.y
    const b = point2.x - point1.x
    const c =
      (point1.x - point2.x) * point1.y + (point2.y - point1.y) * point1.x
    return [a, b, c]
  } catch (error) {
    console.error('Erreur dans Line.equation()', error)
    return [Number.NaN, Number.NaN, Number.NaN]
  }
}

export function intersectionSCCoord(L: Segment, C: Circle): Coords {
  try {
    const { x } = intersectionLCCoord(L, C, 1)
    const [A, B] = [L.point1, L.point2]
    if (x !== undefined && distance(A, B) > C.radius) {
      if (x < Math.max(A.x, B.x) && x > Math.min(A.x, B.x)) {
        return intersectionLCCoord(L, C, 1)
      }
      return intersectionLCCoord(L, C, 2)
    }
    return new Coords(Number.NaN, Number.NaN)
  } catch (error) {
    return new Coords(Number.NaN, Number.NaN)
  }
}

export function intersectionLCCoord(D: Line, C: Circle, n: 1 | 2 = 1): Coords {
  try {
    const O = C.center
    const r = C.radius
    const [a, b, c] = D.equation
    const xO = O.x
    const yO = O.y
    let Delta: number
    let delta: number
    let xi: number
    let yi: number
    let xiPrime: number
    let yiPrime: number
    if (b === 0) {
      // la droite est verticale
      xi = -c / a
      xiPrime = xi
      Delta = 4 * (-xO * xO - (c * c) / (a * a) - (2 * xO * c) / a + r * r)
      if (Delta < 0) return new Coords(Number.NaN, Number.NaN)
      if (Math.abs(Delta) < 10 ** -6) {
        // un seul point d'intersection
        yi = yO + Math.sqrt(Delta) / 2
        yiPrime = yi
      } else {
        // deux points d'intersection
        yi = yO - Math.sqrt(Delta) / 2
        yiPrime = yO + Math.sqrt(Delta) / 2
      }
    } else if (a === 0) {
      // la droite est horizontale
      yi = -c / b
      yiPrime = yi
      Delta = 4 * (-yO * yO - (c * c) / (b * b) - (2 * yO * c) / b + r * r)
      if (Delta < 0) return new Coords(Number.NaN, Number.NaN)
      if (Math.abs(Delta) < 10 ** -6) {
        // un seul point d'intersection
        xi = xO + Math.sqrt(Delta) / 2
        xiPrime = xi
      } else {
        // deux points d'intersection
        xi = xO - Math.sqrt(Delta) / 2
        xiPrime = xO + Math.sqrt(Delta) / 2
      }
    } else {
      // cas général
      Delta =
        (2 * ((a * c) / (b * b) + (yO * a) / b - xO)) ** 2 -
        4 *
          (1 + (a / b) ** 2) *
          (xO * xO + yO * yO + (c / b) ** 2 + (2 * yO * c) / b - r * r)
      if (Delta < 0) return new Coords(Number.NaN, Number.NaN)
      if (Math.abs(Delta) < 10 ** -6) {
        // un seul point d'intersection
        delta = Math.sqrt(Delta)
        xi =
          (-2 * ((a * c) / (b * b) + (yO * a) / b - xO) - delta) /
          (2 * (1 + (a / b) ** 2))
        xiPrime = xi
        yi = (-a * xi - c) / b
        yiPrime = yi
      } else {
        // deux points d'intersection
        delta = Math.sqrt(Delta)
        xi =
          (-2 * ((a * c) / (b * b) + (yO * a) / b - xO) - delta) /
          (2 * (1 + (a / b) ** 2))
        xiPrime =
          (-2 * ((a * c) / (b * b) + (yO * a) / b - xO) + delta) /
          (2 * (1 + (a / b) ** 2))
        yi = (-a * xi - c) / b
        yiPrime = (-a * xiPrime - c) / b
      }
    }
    if (n === 1) {
      if (yiPrime > yi) {
        return new Coords(xiPrime, yiPrime)
      }
      return new Coords(xi, yi)
    }
    if (yiPrime > yi) {
      return new Coords(xi, yi)
    }
    return new Coords(xiPrime, yiPrime)
  } catch (error) {
    return new Coords(Number.NaN, Number.NaN)
  }
}

/**
 *
 * @param origin Point
 * @param line Droite
 * @returns { x, y } coordonnées du projeté orthogonale sur d
 * @author Jean-Claude Lhote
 */
export function orthogonalProjectionCoord(
  origin: Coords | Point,
  line: Line,
): Coords {
  try {
    const [a, b, c] = line.equation
    const k = 1 / (a * a + b * b)
    let x: number | undefined
    let y: number | undefined
    if (a === 0) {
      x = origin.x
      y = -c / b
    } else if (b === 0) {
      y = origin.y
      x = -c / a
    } else {
      if (origin.x === undefined || origin.y === undefined) return new Coords()
      x = k * (b * b * origin.x - a * b * origin.y - a * c)
      y = k * (-a * b * origin.x + a * a * origin.y + (a * a * c) / b) - c / b
    }
    return new Coords(x, y)
  } catch (error) {
    return new Coords(Number.NaN, Number.NaN)
  }
}

/**
 *
 * @param A Antécédent (Point ou[x,y])
 * @param O Centre (Point ou[x,y])
 * @param angle Image
 * @returns [x, y] coordonnées de l'image
 */
export function rotationCoord(
  A: Coords | Point,
  O: Coords | Point,
  angle: number,
): Coords {
  try {
    if (
      O.x === undefined ||
      O.y === undefined ||
      A.x === undefined ||
      A.y === undefined
    )
      return new Coords()
    const x =
      O.x +
      (A.x - O.x) * Math.cos((angle * Math.PI) / 180) -
      (A.y - O.y) * Math.sin((angle * Math.PI) / 180)
    const y =
      O.y +
      (A.x - O.x) * Math.sin((angle * Math.PI) / 180) +
      (A.y - O.y) * Math.cos((angle * Math.PI) / 180)
    return new Coords(x, y)
  } catch (error) {
    return new Coords(Number.NaN, Number.NaN)
  }
}

/**
 *
 * @param A Antécédent
 * @param O Centre
 * @param k Coefficient
 * @returns [x, y] coordonnées de l'image
 */
export function homothetieCoord(
  A: Coords | Point,
  O: Coords | Point,
  k: number,
): Coords {
  if (
    A.x === undefined ||
    A.y === undefined ||
    O.x === undefined ||
    O.y === undefined
  )
    return new Coords()
  try {
    const x = O.x + k * (A.x - O.x)
    const y = O.y + k * (A.y - O.y)
    return new Coords(x, y)
  } catch {
    return new Coords(Number.NaN, Number.NaN)
  }
}

/**
 *
 * @param A Antécédent
 * @param O Centre
 * @param k Coefficient
 * @param angle Angle en degrés
 * @returns [x, y] coordonnées de l'image
 */
export function similitudeCoord(
  A: Coords | Point,
  O: Coords | Point,
  k: number,
  angle: number,
): Coords {
  if (
    A.x === undefined ||
    A.y === undefined ||
    O.x === undefined ||
    O.y === undefined
  )
    return new Coords()
  try {
    const angleRadian = (angle * Math.PI) / 180
    const x =
      O.x +
      k *
        (Math.cos(angleRadian) * (A.x - O.x) -
          Math.sin(angleRadian) * (A.y - O.y))
    const y =
      O.y +
      k *
        (Math.cos(angleRadian) * (A.y - O.y) +
          Math.sin(angleRadian) * (A.x - O.x))
    return new Coords(x, y)
  } catch (error) {
    return new Coords(Number.NaN, Number.NaN)
  }
}

export function reflectOverLineCoord(origin: Point, line: Line): Coords {
  let x: number
  let y: number
  const [a, b, c] = line.equation
  const k = 1 / (a * a + b * b)
  if (a === 0) {
    x = origin.x
    y = -(origin.y + (2 * c) / b)
  } else if (b === 0) {
    y = origin.y
    x = -(origin.x + (2 * c) / a)
  } else {
    x = k * ((b * b - a * a) * origin.x - 2 * a * b * origin.y - 2 * a * c)
    y =
      k *
        ((a * a - b * b) * origin.y -
          2 * a * b * origin.x +
          (a * a * c) / b -
          b * c) -
      c / b
  }
  return { x, y }
}

export function translateCoords(
  origin: Coords,
  point1: Coords,
  point2: Coords,
): Coords {
  const x = origin.x + point2.x - point1.x
  const y = origin.y + point2.y - point1.y
  return { x, y }
}

export function angleOriented(
  A: Coords | Point,
  O: Coords | Point,
  B: Coords | Point,
): number {
  const v = { x: B.x - O.x, y: B.y - O.y }
  const u = { x: A.x - O.x, y: A.y - O.y }
  const s = u.x * v.y - v.x * u.y >= 0 ? 1 : -1 // composante z du produit vectoriel OA^OB
  return s * angle(A, O, B)
}

export function angle(
  A: Coords | Point,
  O: Coords | Point,
  B: Coords | Point,
): number {
  const OA = { norme: 0, x: A.x - O.x, y: A.y - O.y }
  OA.norme = Math.sqrt(OA.x ** 2 + OA.y ** 2)
  const OB = { norme: 0, x: B.x - O.x, y: B.y - O.y }
  OB.norme = Math.sqrt(OB.x ** 2 + OB.y ** 2)
  const scalaire = OA.x * OB.x + OA.y * OB.y
  if (OA.norme * OB.norme < defaultMinSlope) {
    return 0 // On évite de retouner un angle NaN, zéro, c'est toujours mieux que NaN.
  }
  return (Math.acos(scalaire / (OA.norme * OB.norme)) * 180) / Math.PI
}

/** Renvoie [x1, y1, x2, y2] les coordonnées des extrémités du tracé */
export function getCoordsOut(
  A: Coords | Point,
  B: Coords | Point,
  figure: Figure,
): [number, number, number, number] {
  if (
    A.x === undefined ||
    A.y === undefined ||
    B.x === undefined ||
    B.y === undefined ||
    Number.isNaN(A.x) ||
    Number.isNaN(A.y) ||
    Number.isNaN(B.x) ||
    Number.isNaN(B.y)
  )
    return [Number.NaN, Number.NaN, Number.NaN, Number.NaN]
  try {
    let pente = defaultMaxSlope
    if (B.x !== A.x) {
      pente = (B.y - A.y) / (B.x - A.x)
    }
    if (Math.abs(pente) >= defaultMaxSlope)
      return [A.x, figure.yMax, A.x, figure.yMin]
    if (Math.abs(pente) < defaultMinSlope)
      return [figure.xMin, A.y, figure.xMax, A.y]
    let xOutLeft: number
    let yOutLeft: number
    let n = 0
    while (true) {
      xOutLeft = A.x + n
      yOutLeft = A.y + n * pente
      n++
      if (
        xOutLeft > figure.xMax + 1 ||
        yOutLeft > figure.yMax + 1 ||
        yOutLeft < figure.yMin - 1
      )
        break
    }
    let xOutRight: number
    let yOutRight: number
    n = 0
    while (true) {
      xOutRight = A.x + n
      yOutRight = A.y + n * pente
      n--
      if (
        xOutRight < figure.xMin - 1 ||
        yOutRight > figure.yMax + 1 ||
        yOutRight < figure.yMin - 1
      )
        break
    }
    return [xOutLeft, yOutLeft, xOutRight, yOutRight]
  } catch (error) {
    console.error('Erreur dans Coords.getCoordsOut', error)
    return [Number.NaN, Number.NaN, Number.NaN, Number.NaN]
  }
}

export function getMinDistanceMinWithElements(coords: Coords, figure: Figure): number {
  const elements = [...figure.elements.values()].filter((e) =>
    'distancePointer' in e
  )
  let distance = Number.POSITIVE_INFINITY
  for (const element of elements) {
    const d = element.distancePointer(coords.x, coords.y)
    if (d < distance) {
      distance = d
    }
  }
  return distance
}

export function labelCoords(
  point1: Coords | Point,
  point2: Coords | Point,
): Coords {
  try {
    const midX = (point1.x + point2.x) / 2
    const midY = (point1.y + point2.y) / 2
    const dx = point2.x - point1.x
    const dy = point2.y - point1.y
    const perpX = -dy
    const perpY = dx
    const length = Math.sqrt(perpX * perpX + perpY * perpY)
    const normPerpX = perpX / length
    const normPerpY = perpY / length
    const offsetX = normPerpX * 0.5
    const offsetY = normPerpY * 0.5
    const x = midX + offsetX
    const y = midY + offsetY
    return new Coords(x, y)
  } catch (error) {
    return new Coords(Number.NaN, Number.NaN)
  }
}

/** Donne les coordonnées de deux points de la parallèle à line passant par point */
export function parallelCoords({
  line,
  point,
  figure,
}: { line: Line; point: Coords; figure: Figure }) {
  try {
    const x1 = point.x
    const y1 = point.y
    let x2: number
    let y2: number
    if (line.point2.x - line.point1.x === 0) {
      x2 = x1
      y2 = figure.yMax
    } else {
      x2 = x1 + 5
      y2 =
        y1 +
        (5 * (line.point2.y - line.point1.y)) / (line.point2.x - line.point1.x)
    }
    return { x1, y1, x2, y2 }
  } catch (error) {
    console.error('Erreur dans Coords.parallelCoords', error)
    return { x1: Number.NaN, y1: Number.NaN, x2: Number.NaN, y2: Number.NaN }
  }
}
