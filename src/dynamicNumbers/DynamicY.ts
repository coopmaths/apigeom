import type Figure from '../Figure'
import type Point from '../elements/points/Point'
import type TextDynamicByPosition from '../elements/text/TextDynamicByPosition'

import DynamicNumber from './DynamicNumber'

class DynamicY extends DynamicNumber {
  dynamicText: TextDynamicByPosition
  point: Point
  constructor(figure: Figure, { point }: { point: Point }) {
    super(figure, {})
    this.point = point
    this.point.subscribe(this)
    this.dynamicText = this.figure.create('TextDynamicByPosition', {
      color: 'blue',
      dynamicNumber: this,
      isChild: true,
      maximumFractionDigits: 2,
      minimumFractionDigits: 0,
      textAfter: '',
      textBefore: '',
      x: -1,
      y: point.y,
    })
    this.type = 'DynamicY'
    this.update()
    this.dynamicText.div.style.fontWeight = 'bolder'
  }

  toJSON() {
    return {
      ...super.toJSON(),
      idPoint: this.point.id,
    }
  }

  update(): void {
    this.value = this.point.y
    this.dynamicText.y = this.point.y
    if (this.point.x > 0) {
      this.dynamicText.x = -1
    } else {
      this.dynamicText.x = 1
    }
    this.notify()
  }
}

export default DynamicY
