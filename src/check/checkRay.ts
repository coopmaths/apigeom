import type Figure from '../Figure'
import { type Coords, equationLine } from '../elements/calculus/Coords'
import type Ray from '../elements/lines/Ray'

import { round } from '../lib/format'

export default function checkRay({
  figure,
  point1,
  point2,
  color,
}: { figure: Figure; point1: Coords; point2: Coords; color?: string }): {
  isValid: boolean
  message: string
} {
  let rays = [...figure.elements.values()].filter(
    (e) => e.type === 'Ray',
  ) as Ray[]
  const [a, b, c] = equationLine(point1, point2)
  if (rays.length === 0)
    return {
      isValid: false,
      message: "Il n'y a pas de demi-droite dans la figure.",
    }
  if (color !== undefined) {
    rays = rays.filter((e) => e.color === color)
    if (rays.length === 0)
      return {
        isValid: false,
        message:
          "Il n'y a pas de demi-droite de la couleur demandée dans la figure.",
      }
  }

  for (const ray of rays) {
    const [a2, b2, c2] = ray.equation
    if (
      areCoincident(a, b, c, a2, b2, c2) &&
      round(ray.point1.x) === round(point1.x) &&
      round(ray.point1.y) === round(point1.y)
    ) {
      const directionRay = {
        x: round(ray.point2.x - ray.point1.x),
        y: round(ray.point2.y - ray.point1.y),
      }
      const directionCheck = {
        x: round(point2.x - point1.x),
        y: round(point2.y - point1.y),
      }
      if (
        directionRay.x * directionCheck.x >= 0 &&
        directionRay.y * directionCheck.y >= 0
      ) {
        return { isValid: true, message: 'La demi-droite a bien été tracée.' }
      }
    }
  }

  if (color !== undefined) {
    rays = [...figure.elements.values()].filter(
      (e) => e.type === 'Ray',
    ) as Ray[]
    for (const ray of rays) {
      const [a2, b2, c2] = ray.equation
      if (
        areCoincident(a, b, c, a2, b2, c2) &&
        round(ray.point1.x) === round(point1.x) &&
        round(ray.point1.y) === round(point1.y)
      ) {
        const directionRay = {
          x: round(ray.point2.x - ray.point1.x),
          y: round(ray.point2.y - ray.point1.y),
        }
        const directionCheck = {
          x: round(point2.x) - round(point1.x),
          y: round(point2.y) - round(point1.y),
        }
        if (
          directionRay.x * directionCheck.x >= 0 &&
          directionRay.y * directionCheck.y >= 0
        ) {
          return {
            isValid: false,
            message:
              "Il n'y a pas de demi-droite de la couleur demandée dans la figure.",
          }
        }
      }
    }
  }

  return {
    isValid: false,
    message: 'Aucune demi-droite ne correspond à la réponse attendue.',
  }
}

function areCoincident(
  a1: number,
  b1: number,
  c1: number,
  a2: number,
  b2: number,
  c2: number,
  precision = 2,
): boolean {
  // Cas des droites verticales
  if (b1 === 0 && b2 === 0) {
    return c1 / a1 === c2 / a2
  }
  const slope1 = b1 !== 0 ? -a1 / b1 : Number.POSITIVE_INFINITY
  const slope2 = b2 !== 0 ? -a2 / b2 : Number.POSITIVE_INFINITY

  const intercept1 = b1 !== 0 ? -c1 / b1 : Number.NaN
  const intercept2 = b2 !== 0 ? -c2 / b2 : Number.NaN

  return (
    round(slope1, precision) === round(slope2, precision) &&
    round(intercept1, precision) === round(intercept2, precision)
  )
}
