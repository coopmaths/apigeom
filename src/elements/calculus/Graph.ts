import { parse } from 'mathjs'

import type Figure from '../../Figure'
import type { OptionsElement2D } from '../interfaces'

import Element2D from '../Element2D'
import { distance } from './Coords'

/**
 * Trace la courbe représentative d'une fonction à partir de son expression.
 * L'expression de la fonction est un string qui est analysé par mathjs
 */
class Graph extends Element2D {
  expression: string
  readonly f: (x: number) => number
  prohibitedValues: number[]
  step: number
  xMax: number
  xMin: number
  constructor(
    figure: Figure,
    {
      expression,
      prohibitedValues = [],
      step,
      xMax = figure.xMax,
      xMin = figure.xMin,
      ...options
    }: {
      expression: string
      prohibitedValues?: number[]
      step?: number
      xMax?: number
      xMin?: number
    } & OptionsElement2D,
  ) {
    super(figure, { ...options })
    this.type = 'Graph'
    this.xMin = xMin
    this.xMax = xMax
    this.expression = expression
    this.prohibitedValues = prohibitedValues
    if (step === undefined) {
      const unite = figure.pixelsPerUnit * figure.xScale * figure.scale
      step = 2 / unite
    }
    this.step = step
    const fNode = parse(expression)
    this.f = (x: number): number => fNode.evaluate({ x })
  }

  draw(): void {
    const points = getListOfPoints({
      f: this.f,
      prohibitedValues: this.prohibitedValues,
      step: this.step,
      xMax: this.xMax,
      xMin: this.xMin,
    })
    const pointsCoords = points
      .map(
        (point) =>
          `${this.figure.xToSx(point.x).toString()},${this.figure.yToSy(point.y).toString()}`,
      )
      .join(' ')
    this.groupSvg = document.createElementNS(
      'http://www.w3.org/2000/svg',
      'polyline',
    )
    this.groupSvg.setAttribute('fill', 'none')
    this.groupSvg.setAttribute('points', pointsCoords)
    this.setVisibilityColorThicknessAndDashed()
  }

  /**
   * Calcule la distance minimale entre un point et la courbe
   */
  distancePointer(x: number, y: number): number {
    // On échantillonne plusieurs points de la courbe pour trouver le plus proche
    let minDistance = Number.POSITIVE_INFINITY
    for (let xi = this.xMin; xi <= this.xMax; xi += this.step) {
      try {
        const yi = this.f(xi)
        if (!Number.isNaN(yi) && Number.isFinite(yi)) {
          const d = distance({ x, y }, { x: xi, y: yi })
          minDistance = Math.min(minDistance, d)
        }
      } catch (e) {
        // Si on tombe sur une erreur, on ignore le point
      }
    }
    return minDistance
  }

  toJSON(): object {
    return {
      ...this.jsonOptions(),
      expression: this.expression,
      isChild: this.isChild,
    }
  }

  /**
   * Description de la courbe pour l'interface utilisateur
   */
  get description(): string {
    return `Courbe représentative de la fonction ${this.notation}`
  }

  /**
   * Code LaTeX pour export
   */
  get latex(): string {
    return `\\draw[domain=${this.xMin}:${this.xMax}, smooth, thick, ${this.color}, ${this.isDashed ? 'dashed, ' : ''} line width=${this.thickness}] 
        plot (\\x, {${parse(this.expression).toString({ implicit: 'show' }).replaceAll('x', '(\\x)')}});`
  }

  /**
   * Notation mathématique de la fonction
   */
  get notation(): string {
    return `f(x) = ${this.expression}`
  }
}

function getListOfPoints({
  f,
  prohibitedValues,
  step,
  xMax,
  xMin,
}: {
  f: (x: number) => number
  prohibitedValues: number[]
  step: number
  xMax: number
  xMin: number
}): Array<{ x: number; y: number }> {
  const points: Array<{ x: number; y: number }> = []
  for (let x = xMin; x < xMax; x += step) {
    if (!prohibitedValues.includes(x)) {
      const point = { x, y: f(x) }
      points.push(point)
    }
  }
  return points
}
export default Graph
