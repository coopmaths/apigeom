import type Figure from '../Figure'
import type Point from '../elements/points/Point'

import { angle as calculAngle } from '../elements/calculus/Coords'
import { round } from '../lib/format'
import syncShake from './syncShake'

/**
 * Teste la valeur de l'angle label1label2label3
 * @returns {{isValid: boolean, message: string}} Un objet contenant deux propriétés :
 * `isValid`, un booléen indiquant si l'angle est valide, et `message`, une chaîne de caractères contenant un message descriptif.
 */
export default function checkAngle({
  angle,
  figure,
  label1,
  label2,
  label3,
}: {
  angle: number
  figure: Figure
  label1: string
  label2: string
  label3: string
}): { isValid: boolean; message: string } {
  const points = [...figure.elements.values()].filter(
    (e) => e.type !== 'pointer' && e.type.includes('Point'),
  ) as Point[]
  const matchPoints1 = points.filter((p) => p.label === label1)
  const matchPoints2 = points.filter((p) => p.label === label2)
  const matchPoints3 = points.filter((p) => p.label === label3)
  if (matchPoints1.length === 0) {
    return {
      isValid: false,
      message: `Il n'y a pas de point $${label1}$ dans la figure.`,
    }
  }
  if (matchPoints1.length > 1) {
    return {
      isValid: false,
      message: `Il y a plusieurs points nommés $${label1}$ dans la figure.`,
    }
  }
  if (matchPoints2.length === 0) {
    return {
      isValid: false,
      message: `Il n'y a pas de point $${label2}$ dans la figure.`,
    }
  }
  if (matchPoints2.length > 1) {
    return {
      isValid: false,
      message: `Il y a plusieurs points nommés $${label2}$ dans la figure.`,
    }
  }
  if (matchPoints3.length === 0) {
    return {
      isValid: false,
      message: `Il n'y a pas de point $${label3}$ dans la figure.`,
    }
  }
  if (matchPoints3.length > 1) {
    return {
      isValid: false,
      message: `Il y a plusieurs points nommés $${label3}$ dans la figure.`,
    }
  }
  let isValid = true
  const point1 = matchPoints1[0]
  const point2 = matchPoints2[0]
  const point3 = matchPoints3[0]
  for (let i = 0; i < 3; i++) {
    isValid =
      isValid &&
      round(calculAngle(point1, point2, point3), 2) === round(angle, 2)
    syncShake({ figure })
  }
  if (isValid) {
    return {
      isValid,
      message: `L'angle $\\widehat{${label1}${label2}${label3}}$ est bien toujours égal à ${angle}°.`,
    }
  }
  return {
    isValid,
    message: `L'angle $\\widehat{${label1}${label2}${label3}}$ n'est pas toujours égal à ${angle}°.`,
  }
}
