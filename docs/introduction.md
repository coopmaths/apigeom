---
title: Premiers pas
group: 
category: 
children:
---

## Création de la figure

apigeom s'appuie sur une figure dans laquelle on ajoute des éléments (points, segments, polygones...).

```js
const figure = new Figure()
```

On peut paramétrer cette figure avec un objet qui a les valeurs suivantes par défaut.

```js
const figure = new Figure({
    border: false, // Bordure autour de la figure
    dx: 1, // Si snapgrid est à true, c'est la largeur de la grille aimantée en unités de la figure
    dy: 1, // Si snapgrid est à true, c'est la hauteur de la grille aimantée en unités de la figure
    height: 600 // Hauteur de la figure en pixels pour la sortie SVG
    isDynamic: true, // À false, on ne peut déplacer aucun élément 
    pixelsPerUnit: 30, // Nombre de pixels pour une unité de la figure
    scale: 1, // Niveau de zoom
    snapGrid: false, // Présence d'une grille aimantée
    width: 800, // Largeur de la figure en pixels pour la sortie SVG
    xMin: -10, // Abscisse du coin en bas à gauche
    xScale: 1, // Niveau de zoom pour les abscisses
    yMin: -6, // Ordonnée du coin en bas à gauche
    yScale: 1, // Niveau de zoom pour les ordonnées
  })
```

Une fois la figure créée, il est possible de modifier certains paramètres :

```js
figure.scale = 2
figure.snapGrid = true
figure.isDynamic = false
```

Mais il est conseillé de fixer `xMin`, `yMin` et `widh` à la construction de la figure.

## Modifier les options par défaut de la figure

Une figure dynamique a des options par défaut qu'il est possible de modifier. Les options par défaut sont définies dans le fichier `defaultValues.ts`.

```js
const defaultOptions = {
  animationStepInterval: 3000, // Temps en ms entre 2 étapes d'une animation
  automaticUserMessage: true, // Message en haut à droite dans l'interface utilisateur qui dépend du bouton cliqué
  borderSize: 0.2, // Taille des extrémités d'un segment
  color: 'currentColor', // Couleur pour les constructions
  colorPointPolygon: 'none', // Couleur des sommets d'un polygone (avec 'none') ces sommets sont cachés
  displayGrid: false, // Affichage de la grille si elle est définie
  fillColor: 'none', // Couleur de remplissage pour les polygones et disques
  fillOpacity: 0.2, // Opacité de la couleur de remplissage
  gridWithTwoPointsOnSamePosition: true, // Quand snapGrid est à true, est-ce que l'on peut avoir deux points superposés ?
  fontSize: '1em', // Taille de police pour les textes
  isDashed: false, // Constructions en pointillés ?
  labelAutomaticForPoints: undefined, // Si c'est une lettre alors le premier point aura pour label cette lettre et les points suivants suivront dans l'ordre alphabétique
  labelDxInPixels: 15, // Écart horizontal entre un point et son label
  labelDyInPixels: 15, // Écart vertical entre un point et son label
  latexHeight: 12, // Hauteur de la figure pour la sortie LaTeX si yMin n'est pas défini dans la méthode latex()
  labelIsVisible: true, // Affichage du label des points
  latexWidth: 18, // Largeur de la figure pour la sortie LaTeX si xMax n'est pas défini dans la méthode latex()
  limitNumberOfElement: new Map(), // Permet d'ajouter des restrictions sur les constructions
  mark: '||', // Codage par défaut
  moveTextGrid: 15, // Taille de la grille autour du point pour le déplacement du label d'un point 
  pointDescriptionWithCoordinates: true, // Affichage des coordonnées des points dans la modale de choix des points
  pointSize: 5, // Taille des marques des points
  thickness: 1, // Épaisseur des traits
  shape: 'x' as '' | '|' | 'o' | 'x', // Marque des points
  shapeForPolygon: 'x' as '' | '|' | 'o' | 'x', // Marque des sommets des polygones
  thicknessForPoint: 2, // Épaisseur des marques des points
  tmpColor: 'gray', // Couleur des tracés temporaires (comme pour un polygone en construction)
  tmpFillColor: orangeMathaleaLight, // Couleur de remplissage par défaut pour les tracés temporaires (comme un cercle en construction)
  tmpFillOpacity: 0.2, // Opacité pour le remplissage des tracés temporaires
  tmpIsDashed: true, // Tracés temporaires en pointillés
  tmpThickness: 1, // Épaisseur des tracés temporaires 
  tmpShape: 'x' as '' | '|' | 'o' | 'x', // Marque des points temporaires
}
```

### Exemples

Si dans un figure, on souhaite que tous les tracés soient en bleu avec une épaisseur de 2 pixels :

```js
figure.options.color = 'blue'
figure.options.thickness = 2
```

Si dans une figure, on doit placer les points M, N, O, P, alors on peut mettre un label automatique qui suivra l'ordre alphabétique :

```js
figure.options.labelAutomaticForPoints = 'M'
```

Si dans une figure, on souhaite qu'il y ait au maximum que 3 points alors on ajoute la restriction ainsi :

```js
figure.options.limitNumberOfElement.set('Point', 3)
```

## Ajout d'éléments

## Styles des éléments
