import type Figure from '../Figure'
import type { eventName } from '../uiMachine'

import bisectorByPoints from '../assets/svg/bisectorByPoints.svg'
import circle from '../assets/svg/circle.svg'
import circleCompass from '../assets/svg/circleCompass.svg'
import circlePoint from '../assets/svg/circlePoint.svg'
import cursor from '../assets/svg/cursor.svg'
import description from '../assets/svg/description.svg'
import dilate from '../assets/svg/dilate.svg'
import disc from '../assets/svg/disc.svg'
import discPoint from '../assets/svg/discPoint.svg'
import drag from '../assets/svg/drag.svg'
import dragAll from '../assets/svg/dragAll.svg'
import fill from '../assets/svg/fill.svg'
import grid from '../assets/svg/grid.svg'
import hide from '../assets/svg/hide.svg'
import informations from '../assets/svg/informations.svg'
import latex from '../assets/svg/latex.svg'
import latexSvg from '../assets/svg/latexSvg.svg'
import line from '../assets/svg/line.svg'
import lineParallel from '../assets/svg/lineParallel.svg'
import linePerpendicular from '../assets/svg/linePerpendicular.svg'
import markAngle from '../assets/svg/markAngle.svg'
import matkSegment from '../assets/svg/markSegment.svg'
import mesureAngle from '../assets/svg/mesureAngle.svg'
import mesureSegment from '../assets/svg/mesureSegment.svg'
import middle from '../assets/svg/middle.svg'
import moveLabel from '../assets/svg/moveLabel.svg'
import namePoint from '../assets/svg/namePoint.svg'
import open from '../assets/svg/open.svg'
import pause from '../assets/svg/pause.svg'
import perpendicularBisector from '../assets/svg/perpendicularBisector.svg'
import play from '../assets/svg/play.svg'
import playSkipBack from '../assets/svg/playSkipBack.svg'
import playSkipForward from '../assets/svg/playSkipForward.svg'
import point from '../assets/svg/point.svg'
import pointIntersection from '../assets/svg/pointIntersection.svg'
import pointOn from '../assets/svg/pointOn.svg'
import polygon from '../assets/svg/polygon.svg'
import ray from '../assets/svg/ray.svg'
import redo from '../assets/svg/redo.svg'
import reflection from '../assets/svg/reflection.svg'
import reflectionOverLine from '../assets/svg/reflectionOverLine.svg'
import remove from '../assets/svg/remove.svg'
import restart from '../assets/svg/restart.svg'
import rotate from '../assets/svg/rotate.svg'
import save from '../assets/svg/save.svg'
import segment from '../assets/svg/segment.svg'
import setOptions from '../assets/svg/setOptions.svg'
import shake from '../assets/svg/shake.svg'
import translation from '../assets/svg/translation.svg'
import undo from '../assets/svg/undo.svg'
import vector from '../assets/svg/vector.svg'
import zoomIn from '../assets/svg/zoomIn.svg'
import zoomOut from '../assets/svg/zoomOut.svg'
import setButtonStyle from './setButtonStyle'

// La clé devrait être de type availableButtons
const availableIcons = new Map<string, { tooltip: string; url: string }>([
  ['BISECTOR_BY_POINTS', { tooltip: 'Bissectrice', url: bisectorByPoints }],
  ['CIRCLE_CENTER_POINT', { tooltip: 'Cercle centre-point', url: circlePoint }],
  [
    'CIRCLE_COMPASS',
    { tooltip: 'Cercle par report de longueur', url: circleCompass },
  ],
  ['CIRCLE_RADIUS', { tooltip: 'Cercle centre-rayon', url: circle }],
  ['CURSOR', { tooltip: 'Curseur', url: cursor }],
  [
    'DESCRIPTION',
    { tooltip: 'Afficher le programme de construction', url: description },
  ],
  ['DILATE', { tooltip: 'Homothétie', url: dilate }],
  ['DISC_CENTER_POINT', { tooltip: 'Disque centre-point', url: discPoint }],
  ['DISC_RADIUS', { tooltip: 'Disque centre-rayon', url: disc }],
  [
    'DOWNLOAD_LATEX_SVG',
    { tooltip: 'Exporter la figure au format LaTeX ou SVG', url: latexSvg },
  ],
  ['DRAG_ALL', { tooltip: 'Déplacer la figure', url: dragAll }],
  ['DRAG', { tooltip: 'Déplacer les points', url: drag }],
  ['FILL', { tooltip: 'Remplir / Vider', url: fill }],
  ['GRID', { tooltip: 'Afficher le repère', url: grid }],
  ['HIDE', { tooltip: 'Masquer un élément', url: hide }],
  [
    'INFORMATIONS',
    { tooltip: "Obtenir les informations sur l'élément", url: informations },
  ],
  ['LATEX', { tooltip: 'Export LaTeX', url: latex }],
  ['LINE_PARALLEL', { tooltip: 'Droite parallèle', url: lineParallel }],
  [
    'LINE_PERPENDICULAR',
    { tooltip: 'Droite perpendiculaire', url: linePerpendicular },
  ],
  ['LINE', { tooltip: 'Droite', url: line }],
  ['MARK_ANGLE', { tooltip: 'Coder un angle', url: markAngle }],
  ['MARK_BETWEEN_POINTS', { tooltip: 'Coder un segment', url: matkSegment }],
  ['MESURE_ANGLE', { tooltip: 'Mesurer un angle', url: mesureAngle }],
  ['MESURE_SEGMENT', { tooltip: 'Mesurer une distance', url: mesureSegment }],
  ['MIDDLE', { tooltip: 'Milieu', url: middle }],
  ['MOVE_LABEL', { tooltip: "Déplacer le nom d'un point", url: moveLabel }],
  ['NAME_POINT', { tooltip: 'Renommer un point', url: namePoint }],
  ['OPEN', { tooltip: 'Charger un fichier', url: open }],
  ['PAUSE', { tooltip: 'Pause', url: pause }],
  [
    'PERPENDICULAR_BISECTOR',
    { tooltip: 'Médiatrice', url: perpendicularBisector },
  ],
  ['PLAY_SKIP_BACK', { tooltip: 'Étape précédente', url: playSkipBack }],
  ['PLAY_SKIP_FORWARD', { tooltip: 'Étape suivante', url: playSkipForward }],
  ['PLAY', { tooltip: 'Lecture', url: play }],
  [
    'POINT_INTERSECTION',
    { tooltip: "Point à l'intersection", url: pointIntersection },
  ],
  ['POINT_ON', { tooltip: 'Point sur', url: pointOn }],
  ['POINT', { tooltip: 'Point', url: point }],
  ['POLYGON', { tooltip: 'Polygone', url: polygon }],
  ['RAY', { tooltip: 'Demi-droite', url: ray }],
  ['REDO', { tooltip: 'Rétablir', url: redo }],
  [
    'REFLECTION_OVER_LINE',
    { tooltip: 'Symétrie axiale', url: reflectionOverLine },
  ],
  ['REFLECTION', { tooltip: 'Symétrie centrale', url: reflection }],
  ['REMOVE', { tooltip: 'Supprimer', url: remove }],
  ['RESTART', { tooltip: 'Recommencer', url: restart }],
  ['ROTATE', { tooltip: 'Rotation', url: rotate }],
  ['SAVE', { tooltip: 'Sauvegarder', url: save }],
  ['SEGMENT', { tooltip: 'Segment', url: segment }],
  ['SET_OPTIONS', { tooltip: 'Modifier le style', url: setOptions }],
  ['SHAKE', { tooltip: 'Secouer la figure', url: shake }],
  ['TRANSLATION', { tooltip: 'Translation', url: translation }],
  ['UNDO', { tooltip: 'Annuler la dernière action', url: undo }],
  ['VECTOR', { tooltip: 'Vecteur', url: vector }],
  ['ZOOM_IN', { tooltip: 'Zoom avant', url: zoomIn }],
  ['ZOOM_OUT', { tooltip: 'Zoom arrière', url: zoomOut }],
])

export default function addButtons(
  list: string,
  figure: Figure,
): HTMLImageElement[] {
  const imgList = []
  const buttons = list.split(' ')
  for (const key of buttons) {
    const button = key.toUpperCase()
    if (!availableIcons.has(button)) {
      console.error(`Le bouton ${button} n'existe pas`)
      continue
    }
    const img = document.createElement('img')
    const icon = availableIcons.get(button)
    if (icon == null) {
      console.error(`L'icône pour le bouton ${button} n'existe pas`)
      continue
    }
    img.src = icon.url
    img.title = icon.tooltip
    setButtonStyle(img)
    img.onclick = () => {
      figure.ui?.send(button as eventName)
    }
    figure.buttons.set(button as eventName, img)
    imgList.push(img)
  }
  return imgList
}
