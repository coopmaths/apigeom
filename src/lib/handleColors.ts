/** Transforme #F15929 en {rgb,255:red,241; green,89; blue,41} : indispensable avec tikZ */
function hexToTikzRgb(hex: string): string {
  const hexWithoutHashtag = hex.replace(/^#/, '')
  const bigint = Number.parseInt(hexWithoutHashtag, 16)
  const r = (bigint >> 16) & 255
  const g = (bigint >> 8) & 255
  const b = bigint & 255
  return `{rgb,255:red,${r}; green,${g}; blue,${b}}`
}

/** Transforme rgba(241, 89, 41, 0.5) en {rgb,255:red,241; green,89; blue,41} : indispensable avec tikZ */
function rgbaToTikzRgb(rgba: string): string {
  const rgbaValues = rgba.match(/\d+(\.\d+)?/g)
  if (rgbaValues && rgbaValues.length >= 3) {
    const r = Number.parseInt(rgbaValues[0])
    const g = Number.parseInt(rgbaValues[1])
    const b = Number.parseInt(rgbaValues[2])
    return `{rgb,255:red,${r}; green,${g}; blue,${b}}`
  }
  throw new Error('Invalid RGBA color format')
}

export function colorLatex(color: string) {
  if (color.includes('#')) return hexToTikzRgb(color)
  if (color.includes('rgba')) return rgbaToTikzRgb(color)
  if (color === 'currentColor') return 'black'
  return color
}
