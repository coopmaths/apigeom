import type Figure from '../../Figure'
import type TextByPosition from '../text/TextByPosition'


import Element2D from '../Element2D'
import { orangeMathalea } from '../defaultValues'
import type { OptionsElement2D } from '../interfaces'
import type Vector from '../vector/Vector'

class Grid extends Element2D {
  readonly axeX: boolean
  readonly axeY: boolean
  grid: boolean
  labels: TextByPosition[]
  vectors: Vector[]
  readonly labelX: boolean
  readonly labelY: boolean
  readonly stepX: number
  readonly stepY: number
  readonly subGrid: boolean
  readonly subGridDivisions: number
  readonly xMax: number
  readonly xMin: number
  readonly yMax: number
  readonly yMin: number
  /** Présence du repère O, i, j*/
  readonly repereOij: boolean
  /** Épaisseur des traits de la grille principale */
  readonly strokeWidthGrid: number
  /** Épaisseur des thicks */
  readonly thickThickness: number
  /** Longueur des thicks */
  readonly thickHeight: number
  /** Groupe SVG des thicks */
  protected thicks: SVGElement
  constructor(
    figure: Figure,
    {
      axeX = true,
      axeY = true,
      color = 'black',
      grid = true,
      labelX = true,
      labelY = true,
      stepX = 1,
      stepY = 1,
      subGrid = false,
      subGridDivisions = 5,
      xMax = figure.xMax,
      xMin = figure.xMin,
      yMax = figure.yMax,
      yMin = figure.yMin,
      strokeWidthGrid = 0.5,
      thickThickness = 2,
      thickHeight = 4,
      repereOij = false,
      ...options
    }: {
      axeX?: boolean
      axeY?: boolean
      color?: string
      grid?: boolean
      labelX?: boolean
      labelY?: boolean
      stepX?: number
      stepY?: number
      subGrid?: boolean
      subGridDivisions?: number
      xMax?: number
      xMin?: number
      yMax?: number
      yMin?: number
      strokeWidthGrid?: number
      thickThickness?: number
      thickHeight?: number
      repereOij?: boolean
    } & OptionsElement2D = {},
  ) {
    super(figure, { ...options })
    this.type = 'Grid'
    this._color = color
    this.axeX = axeX
    this.axeY = axeY
    this.grid = grid
    this.subGrid = subGrid
    this.labelX = labelX
    this.labelY = labelY
    this.stepX = stepX
    this.stepY = stepY
    this.subGridDivisions = subGridDivisions
    this.xMin = xMin
    this.xMax = xMax
    this.yMin = yMin
    this.yMax = yMax
    this.labels = []
    this.vectors = []
    this.strokeWidthGrid = strokeWidthGrid
    this.thickThickness = thickThickness
    this.thickHeight = thickHeight
    this.repereOij = repereOij
    this.thicks = document.createElementNS('http://www.w3.org/2000/svg', 'g')
  }

  draw(): void {
    const sizeOfArrow = 20
    if (this.axeX) {
      // Définit la flèche
      const defs = document.createElementNS(
        'http://www.w3.org/2000/svg',
        'defs',
      )
      this.figure.svg.appendChild(defs)
      const marker = document.createElementNS(
        'http://www.w3.org/2000/svg',
        'marker',
      )
      marker.setAttribute('id', 'arrow')
      marker.setAttribute('markerWidth', sizeOfArrow.toString())
      marker.setAttribute('markerHeight', sizeOfArrow.toString())
      marker.setAttribute('refX', '0')
      marker.setAttribute('refY', '6')
      marker.setAttribute('orient', 'auto')
      marker.setAttribute('markerUnits', 'strokeWidth')
      const path = document.createElementNS(
        'http://www.w3.org/2000/svg',
        'path',
      )
      path.setAttribute('d', 'M0,0 L0,12 L18,6 z')
      marker.appendChild(path)
      defs.appendChild(marker)

      const svgAxeX = document.createElementNS(
        'http://www.w3.org/2000/svg',
        'line',
      )
      svgAxeX.setAttribute('x1', this.figure.xToSx(this.xMin).toString())
      svgAxeX.setAttribute('y1', this.figure.yToSy(0).toString())
      svgAxeX.setAttribute('x2', (this.figure.xToSx(this.xMax) - sizeOfArrow).toString())
      svgAxeX.setAttribute('y2', this.figure.yToSy(0).toString())
      svgAxeX.setAttribute('stroke', 'black')
      svgAxeX.setAttribute('stroke-width', '1')
      // Ajoute la flèche comme une mark
      svgAxeX.setAttribute('marker-end', 'url(#arrow)')

      // Thicks
      for (let x = 0; x < this.xMax; x += this.stepX) {
        const svgLine = document.createElementNS(
          'http://www.w3.org/2000/svg',
          'line',
        )
        svgLine.setAttribute('x1', this.figure.xToSx(x).toString())
        svgLine.setAttribute('y1', (-this.thickHeight).toString())
        svgLine.setAttribute('x2', this.figure.xToSx(x).toString())
        svgLine.setAttribute('y2', this.thickHeight.toString())
        svgLine.setAttribute('stroke', this._color)
        svgLine.setAttribute('stroke-width', this.thickThickness.toString())
        this.thicks.appendChild(svgLine)
      }
      for (let x = 0; x >= this.xMin; x -= this.stepX) {
        const svgLine = document.createElementNS(
          'http://www.w3.org/2000/svg',
          'line',
        )
        svgLine.setAttribute('x1', this.figure.xToSx(x).toString())
        svgLine.setAttribute('y1', (-this.thickHeight).toString())
        svgLine.setAttribute('x2', this.figure.xToSx(x).toString())
        svgLine.setAttribute('y2', this.thickHeight.toString())
        svgLine.setAttribute('stroke', this._color)
        svgLine.setAttribute('stroke-width', this.thickThickness.toString())
        this.thicks.appendChild(svgLine)
      }
      this.groupSvg.appendChild(svgAxeX)
    }
    if (this.axeY) {
      // Définit la flèche
      const defs = document.createElementNS(
        'http://www.w3.org/2000/svg',
        'defs',
      )
      this.figure.svg.appendChild(defs)
      const marker = document.createElementNS(
        'http://www.w3.org/2000/svg',
        'marker',
      )
      marker.setAttribute('id', 'arrow')
      marker.setAttribute('markerWidth', sizeOfArrow.toString()) // Augmenter la largeur
      marker.setAttribute('markerHeight', sizeOfArrow.toString()) // Augmenter la hauteur
      marker.setAttribute('refX', '0')
      marker.setAttribute('refY', '6') // Ajuster la référence Y
      marker.setAttribute('orient', 'auto')
      marker.setAttribute('markerUnits', 'strokeWidth')
      const path = document.createElementNS(
        'http://www.w3.org/2000/svg',
        'path',
      )
      path.setAttribute('d', 'M0,0 L0,12 L18,6 z') // Augmenter les dimensions du chemin
      marker.appendChild(path)
      defs.appendChild(marker)

      const svgAxeY = document.createElementNS(
        'http://www.w3.org/2000/svg',
        'line',
      )
      svgAxeY.setAttribute('x1', this.figure.xToSx(0).toString())
      svgAxeY.setAttribute('y1', this.figure.yToSy(this.yMin).toString())
      svgAxeY.setAttribute('x2', this.figure.xToSx(0).toString())
      svgAxeY.setAttribute('y2', (this.figure.yToSy(this.yMax) + sizeOfArrow).toString())
      svgAxeY.setAttribute('stroke', 'black')
      svgAxeY.setAttribute('stroke-width', '1')
      // Ajoute la flèche comme une mark
      svgAxeY.setAttribute('marker-end', 'url(#arrow)')
      // Thicks
      for (let y = 0; y < this.yMax; y += this.stepY) {
        const svgLine = document.createElementNS(
          'http://www.w3.org/2000/svg',
          'line',
        )
        svgLine.setAttribute('x1', (-this.thickHeight).toString())
        svgLine.setAttribute('y1', this.figure.yToSy(y).toString())
        svgLine.setAttribute('x2', this.thickHeight.toString())
        svgLine.setAttribute('y2', this.figure.yToSy(y).toString())
        svgLine.setAttribute('stroke', this._color)
        svgLine.setAttribute('stroke-width', this.thickThickness.toString())
        this.thicks.appendChild(svgLine)
      }
      for (let y = 0; y >= this.yMin; y -= this.stepY) {
        const svgLine = document.createElementNS(
          'http://www.w3.org/2000/svg',
          'line',
        )
        svgLine.setAttribute('x1', (-this.thickHeight).toString())
        svgLine.setAttribute('y1', this.figure.yToSy(y).toString())
        svgLine.setAttribute('x2', this.thickHeight.toString())
        svgLine.setAttribute('y2', this.figure.yToSy(y).toString())
        svgLine.setAttribute('stroke', this._color)
        svgLine.setAttribute('stroke-width', this.thickThickness.toString())
        this.thicks.appendChild(svgLine)
      }
      this.groupSvg.appendChild(svgAxeY)
    }
    if (this.grid) {
      const svgGrid = document.createElementNS(
        'http://www.w3.org/2000/svg',
        'g',
      )
      svgGrid.setAttribute('stroke', 'gray')
      svgGrid.setAttribute('stroke-opacity', '0.5')
      this.groupSvg.appendChild(svgGrid)
      //for (let x = 0; x <= this.xMax; x += this.stepX) {
      for (
        let x = this.xMin <= 0 ? 0 : this.xMin;
        x <= this.xMax;
        x += this.stepX
      ) {
        const svgLine = document.createElementNS(
          'http://www.w3.org/2000/svg',
          'line',
        )
        svgLine.setAttribute('x1', this.figure.xToSx(x).toString())
        svgLine.setAttribute('y1', this.figure.yToSy(this.yMin).toString())
        svgLine.setAttribute('x2', this.figure.xToSx(x).toString())
        svgLine.setAttribute('y2', this.figure.yToSy(this.yMax).toString())
        svgLine.setAttribute('stroke-width', this.strokeWidthGrid.toString())
        svgGrid.appendChild(svgLine)
      }
      // for (let x = 0; x >= this.xMin; x -= this.stepX) {
      for (
        let x = this.xMax < 0 ? this.xMax : this.xMax < this.stepX ? 0 : -1;
        x >= this.xMin;
        x -= this.stepX
      ) {
        const svgLine = document.createElementNS(
          'http://www.w3.org/2000/svg',
          'line',
        )
        svgLine.setAttribute('x1', this.figure.xToSx(x).toString())
        svgLine.setAttribute('y1', this.figure.yToSy(this.yMin).toString())
        svgLine.setAttribute('x2', this.figure.xToSx(x).toString())
        svgLine.setAttribute('y2', this.figure.yToSy(this.yMax).toString())
        svgLine.setAttribute('stroke-width', '0.5')
        svgGrid.appendChild(svgLine)
      }
      // for (let y = 0; y <= this.yMax; y += this.stepY) {
      for (
        let y = this.yMin <= 0 ? 0 : this.yMin;
        y <= this.yMax;
        y += this.stepY
      ) {
        const svgLine = document.createElementNS(
          'http://www.w3.org/2000/svg',
          'line',
        )
        svgLine.setAttribute('x1', this.figure.xToSx(this.xMin).toString())
        svgLine.setAttribute('y1', this.figure.yToSy(y).toString())
        svgLine.setAttribute('x2', this.figure.xToSx(this.xMax).toString())
        svgLine.setAttribute('y2', this.figure.yToSy(y).toString())
        svgLine.setAttribute('stroke-width', this.strokeWidthGrid.toString())
        svgGrid.appendChild(svgLine)
      }
      // for (let y = 0; y >= this.yMin; y -= this.stepY) {
      for (
        let y = this.yMax < 0 ? this.yMax : this.yMax < this.stepY ? 0 : -1;
        y >= this.yMin;
        y -= this.stepY
      ) {
        const svgLine = document.createElementNS(
          'http://www.w3.org/2000/svg',
          'line',
        )
        svgLine.setAttribute('x1', this.figure.xToSx(this.xMin).toString())
        svgLine.setAttribute('y1', this.figure.yToSy(y).toString())
        svgLine.setAttribute('x2', this.figure.xToSx(this.xMax).toString())
        svgLine.setAttribute('y2', this.figure.yToSy(y).toString())
        svgLine.setAttribute('stroke-width', '0.5')
        svgGrid.appendChild(svgLine)
      }
    }
    if (this.subGrid) {
      const svgSubGrid = document.createElementNS(
        'http://www.w3.org/2000/svg',
        'g',
      )
      svgSubGrid.setAttribute('stroke', 'gray')
      svgSubGrid.setAttribute('stroke-opacity', '0.2')
      this.groupSvg.appendChild(svgSubGrid)
      for (
        let x = this.xMin;
        x <= this.xMax;
        x += this.stepX / this.subGridDivisions
      ) {
        const svgLine = document.createElementNS(
          'http://www.w3.org/2000/svg',
          'line',
        )
        svgLine.setAttribute('x1', this.figure.xToSx(x).toString())
        svgLine.setAttribute('y1', this.figure.yToSy(this.yMin).toString())
        svgLine.setAttribute('x2', this.figure.xToSx(x).toString())
        svgLine.setAttribute('y2', this.figure.yToSy(this.yMax).toString())
        svgLine.setAttribute('stroke-width', '0.25')
        svgSubGrid.appendChild(svgLine)
      }
      for (
        let y = this.yMin;
        y <= this.yMax;
        y += this.stepY / this.subGridDivisions
      ) {
        const svgLine = document.createElementNS(
          'http://www.w3.org/2000/svg',
          'line',
        )
        svgLine.setAttribute('x1', this.figure.xToSx(this.xMin).toString())
        svgLine.setAttribute('y1', this.figure.yToSy(y).toString())
        svgLine.setAttribute('x2', this.figure.xToSx(this.xMax).toString())
        svgLine.setAttribute('y2', this.figure.yToSy(y).toString())
        svgLine.setAttribute('stroke-width', '0.25')
        svgSubGrid.appendChild(svgLine)
      }
    }
    if (this.labelX) {
      for (
        let x = this.labelY ? this.stepX : 0;
        x < this.xMax;
        x += this.stepX
      ) {
        this.labels.push(
          this.figure.create('TextByPosition', {
            anchor: 'topCenter',
            dyInPixels: -this.thickHeight - 2,
            fontSize: '0.67em', // '8pt',
            isChild: true,
            isSelectable: true,
            text: `$${x.toString()}$`,
            x,
            y: 0,
          }),
        )
      }
      for (let x = -this.stepX; x >= this.xMin; x -= this.stepX) {
        this.labels.push(
          this.figure.create('TextByPosition', {
            anchor: 'topCenter',
            dyInPixels: -this.thickHeight - 2,
            fontSize: '0.67em', // '8pt',
            isChild: true,
            isSelectable: true,
            text: `$${x.toString()}$`,
            x,
            y: 0,
          }),
        )
      }
    }
    if (this.labelY) {
      for (let y = this.stepY; y < this.yMax; y += this.stepY) {
        this.labels.push(
          this.figure.create('TextByPosition', {
            anchor: 'middleRight',
            dxInPixels: -this.thickHeight - 5,
            dyInPixels: 0,
            fontSize: '0.67em', // '8pt',
            isChild: true,
            isSelectable: true,
            text: `$${y.toString()}$`,
            x: 0,
            y,
          }),
        )
      }
      for (let y = -this.stepY; y >= this.yMin; y -= this.stepY) {
        this.labels.push(
          this.figure.create('TextByPosition', {
            anchor: 'middleRight',
            dxInPixels: -this.thickHeight - 5,
            dyInPixels: 2,
            fontSize: '0.67em', // '8pt',
            isChild: true,
            isSelectable: true,
            text: `$${y.toString()}$`,
            x: 0,
            y: y - 0.1,
          }),
        )
      }
      if (!this.labelX) {
        this.labels.push(
          this.figure.create('TextByPosition', {
            anchor: 'middleRight',
            dxInPixels: this.thickHeight + 10,
            dyInPixels: 2,
            fontSize: '0.67em', // '8pt',
            isChild: true,
            isSelectable: true,
            text: '$0$',
            x: -1.2,
            y: 0,
          }),
        )
      }
    }
    this.groupSvg.appendChild(this.thicks)
    this.setVisibilityColorThicknessAndDashed()
    if (this.repereOij) {
      const origin = this.figure.create('Point', {
        x: 0,
        y: 0,
        isVisible: false,
        isChild: true,
      })
      this.vectors = [
        this.figure.create('Vector', {
          origin,
          x: 1,
          y: 0,
          color: orangeMathalea,
          thickness: 3,
          isChild: true,
        }),
        this.figure.create('Vector', {
          origin,
          x: 0,
          y: 1,
          color: orangeMathalea,
          thickness: 3,
          isChild: true,
        }),
      ]
      this.labels.push(
        this.figure.create('TextByPosition', {
          text: '$O$',
          x: -0.25,
          y: -0.25,
          anchor: 'topRight',
          color: orangeMathalea,
          fontSize: '8pt',
          isChild: true,
        }),
        this.figure.create('TextByPosition', {
          text: '$\\vec \\imath$',
          x: 0.5,
          y: -0.25,
          anchor: 'topRight',
          color: orangeMathalea,
          fontSize: '8pt',
          isChild: true,
        }),
        this.figure.create('TextByPosition', {
          text: '$\\vec \\jmath$',
          x: -0.5,
          y: 0.25,
          anchor: 'bottomRight',
          color: orangeMathalea,
          fontSize: '8pt',
          isChild: true,
        }),
      )
    }
    this.update()
  }

  get latex(): string {
    let result = `%${this.description}`
    result += `\n\\tkzInit[xmin=${this.xMin}, xmax=${this.xMax}, ymin=${this.yMin}, ymax=${this.yMax}]`
    result += '\n\\tkzGrid[thick]'
    result += '\\tkzAxeXY'
    return result
  }

  get isVisible(): boolean {
    return super.isVisible
  }

  set isVisible(value: boolean) {
    super.isVisible = value
    for (const label of this.labels) {
      label.isVisible = value
    }
    for (const vector of this.vectors) {
      vector.isVisible = value
    }
  }

  toJSON(): object {
    return {
      ...this.jsonOptions(),
      xMin: this.xMin,
      xMax: this.xMax,
      yMin: this.yMin,
      yMax: this.yMax,
      axeX: this.axeX,
      axeY: this.axeY,
      grid: this.grid,
      labelX: this.labelX,
      labelY: this.labelY,
      repereOij: this.repereOij,
      stepX: this.stepX,
      stepY: this.stepY,
      subGrid: this.subGrid,
      thickHeight: this.thickHeight,
      thickThickness: this.thickThickness,
    }
  }
}

export default Grid
