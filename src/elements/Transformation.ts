import type Circle from './lines/Circle'
import type Polygon from './lines/Polygon'
import type Segment from './lines/Segment'
import type Point from './points/Point'
import type Vector from './vector/Vector'

type ElementOrigin = Circle | Point | Polygon | Segment

interface Dilate {
  center: Point
  factor: number
  origin: ElementOrigin
  type: 'dilate'
}
interface Reflect {
  center: Point
  origin: ElementOrigin
  type: 'reflect'
}

interface Rotation {
  angle: number
  center: Point
  origin: ElementOrigin
  type: 'rotation'
}

interface ReflectOverLine {
  line: Segment
  origin: ElementOrigin
  type: 'reflectOverLine'
}

interface Translation {
  origin: ElementOrigin
  point1: Point
  point2: Point
  type: 'translation'
}

interface TranslationVector {
  origin: Point
  vector: Vector
  type: 'translationVector'
}

type Transformation =
  | Dilate
  | Reflect
  | ReflectOverLine
  | Rotation
  | Translation
  | TranslationVector

export default Transformation
