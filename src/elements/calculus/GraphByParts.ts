import { parse } from 'mathjs'

import type Figure from '../../Figure'
import type { OptionsElement2D } from '../interfaces'

import Element2D from '../Element2D'
import { distance } from './Coords'

/**
 * Trace la courbe représentative d'une fonction à partir de son expression.
 * L'expression de la fonction est un string qui est analysé par mathjs
 */
class GraphByParts extends Element2D {
  parts: {expression: string, xMin: number, xMax: number}[]
  fonctions: ((x:number)=>number)[]
  prohibitedValues: number[]
  step: number
  xMax: number
  xMin: number
  constructor(
    figure: Figure,
    {
      parts,
      prohibitedValues = [],
      step,
      xMax = figure.xMax,
      xMin = figure.xMin,
      ...options
    }: {
parts: {expression: string, xMin: number, xMax: number}[]
      prohibitedValues?: number[]
      step?: number
      xMax?: number
      xMin?: number
    } & OptionsElement2D,
  ) {
    super(figure, { ...options })
    this.type = 'GraphByParts'
    this.xMin = Math.min(...parts.map(el=>el.xMin))
    this.xMax = Math.max(...parts.map(el=>el.xMax))
    this.fonctions = []
    this.parts = parts
    for (let i = 0; i < parts.length; i++) {
      const fNode = parse(parts[i].expression)
      this.fonctions.push((x: number): number => fNode.evaluate({ x }))
    }
    this.prohibitedValues = prohibitedValues
    if (step === undefined) {
      const unite = figure.pixelsPerUnit * figure.xScale * figure.scale
      step = 2 / unite
    }
    this.step = step
    this.f = this.f.bind(this)
  }

   f(x:number){
    let i=0
    while (x > this.parts[i].xMax && i<this.parts.length){
      i++
    }
    if (i>=this.parts.length){
      return NaN
    }
    let f = this.fonctions[i]
    return f(x)
  }

  draw(): void {
    const groups: SVGElement[] = []
    for (const part of this.parts) {
      let groupSvg
    const points = getListOfPoints({
      f: this.f,
      prohibitedValues: this.prohibitedValues,
      step: this.step,
      xMax: part.xMax,
      xMin: part.xMin,
    })
    const pointsCoords = points
      .map(
        (point) =>
          `${this.figure.xToSx(point.x).toString()},${this.figure.yToSy(point.y).toString()}`,
      )
      .join(' ')
    groupSvg = document.createElementNS(
      'http://www.w3.org/2000/svg',
      'polyline',
    )
    groupSvg.setAttribute('fill', 'none')
    groupSvg.setAttribute('points', pointsCoords)
    groups.push(groupSvg)
    }
    this.groupSvg = document.createElementNS(
      'http://www.w3.org/2000/svg',
      'g',
    )
    this.groupSvg.innerHTML = groups.map(group => group.outerHTML).join('\n')
    this.setVisibilityColorThicknessAndDashed()
  }

  /**
   * Calcule la distance minimale entre un point et la courbe
   */
  distancePointer(x: number, y: number): number {
    // On échantillonne plusieurs points de la courbe pour trouver le plus proche
    let minDistance = Number.POSITIVE_INFINITY
    for (let xi = this.xMin; xi <= this.xMax; xi += this.step) {
      try {
        const yi = this.f(xi)
        if (!Number.isNaN(yi) && Number.isFinite(yi)) {
          const d = distance({ x, y }, { x: xi, y: yi })
          minDistance = Math.min(minDistance, d)
        }
      } catch (e) {
        // Si on tombe sur une erreur, on ignore le point
      }
    }
    return minDistance
  }

  toJSON(): object {
    return {
      ...this.jsonOptions(),
      parts: this.parts,
      xMax: this.xMax,
      xMin: this.xMin
    }
  }

  /**
   * Description de la courbe pour l'interface utilisateur
   */
  get description(): string {
    return `Courbe représentative de la fonction ${this.notation}`
  }

  /**
   * Code LaTeX pour export
   */
  get latex(): string {
    let latex = ''
    for (const part of this.parts) {
      latex += `\\draw[domain=${part.xMin}:${part.xMax}, smooth, thick, ${this.color}, ${this.isDashed ? 'dashed, ' : ''} line width=${this.thickness}] 
        plot (\\x, {${parse(part.expression).toString({ implicit: 'show' }).replaceAll('x', '(\\x)')}});\n`
    }
    return latex
  }

  /**
   * Notation mathématique de la fonction
   */
  get notation(): string {
    let notation = ''
    for (const part of this.parts) {
      notation += `f(x) = ${part.expression}\\text{ pour }${part.xMin} ≤ x ≤ ${part.xMax}~;~\n`
    }
    return notation
  }
}

function getListOfPoints({
  f,
  prohibitedValues,
  step,
  xMax,
  xMin,
}: {
  f: (x: number) => number
  prohibitedValues: number[]
  step: number
  xMax: number
  xMin: number
}): Array<{ x: number; y: number }> {
  const points: Array<{ x: number; y: number }> = []
  for (let x = xMin; x < xMax; x += step) {
    if (!prohibitedValues.includes(x)) {
      const point = { x, y: f(x) }
      points.push(point)
    }
  }
  return points
}
export default GraphByParts
