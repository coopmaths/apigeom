import type Figure from '../../Figure'
import type ArcByCenterRadiusAndAngles from '../lines/ArcByCenterRadiusAndAngles'
import type Point from '../points/Point'

import Element2D from '../Element2D'
import type { OptionsElement2D } from '../interfaces'

/**
 * Créé plusieurs disques fractionnés que l'on peut colorier
 * Soit on définit le numérateur à N et les N premières portions sont coloriées soit on définit les indices des portions à colorier dans indicesSectorsInColor s'il y a les deux c'est indicesSectorsInColor qui l'emporte
 */
class CircleFractionDiagram extends Element2D {
  /** Centre du diagramme */
  center: Point
  /** Centre des cercles */
  centers: Point[] = []
  /** Nombre de portions */
  denominator: number
  /** Espace entre les cercles */
  gap: number
  /** Nombre de cercles */
  numberOfCircle: number
  /** Rayon des cercles */
  radius: number
  /** Arcs de cercles */
  sectors: ArcByCenterRadiusAndAngles[] = []
  /** Indices des portions coloriées */
  _indicesSectorsInColor: Set<number> = new Set()
  constructor(
    figure: Figure,
    {
      denominator,
      gap = 1,
      indicesSectorsInColor = new Set(),
      numberOfCircle = 1,
      numerator = 0,
      radius,
      x = 0,
      y = 0,
      ...options
    }: {
      /** Nombre de portions */
      denominator: number
      /** Espace entre les cercles */
      gap?: number
      /** Indices des portions coloriées */
      indicesSectorsInColor?: Set<number>
      /** Nombre de cercles */
      numberOfCircle?: number
      /** Nombre de portions coloriées si indicesSectorsInColor n'est pas défini */
      numerator?: number
      /** Rayon des cercles */
      radius: number
      /** Abscisse du centre */
      x?: number
      /** Ordonnée du centre */
      y?: number
    } & OptionsElement2D,
  ) {
    super(figure, { ...options })
    this.center = this.figure.create('Point', {
      shape: '',
      x,
      y,
      isChild: true,
    })
    this.radius = radius
    this.denominator = denominator
    this.gap = gap
    this.numberOfCircle = numberOfCircle
    if (numerator !== undefined && indicesSectorsInColor.size === 0) {
      for (let i = 0; i < numerator; i++) {
        this.indicesSectorsInColor.add(i)
      }
    } else {
      this.indicesSectorsInColor = indicesSectorsInColor
    }
    this.type = 'CircleFractionDiagram'
  }

  draw(): void {
    this.centers = [this.center]
    this.sectors = []
    const angle = 360 / this.denominator
    for (let n = 0; n < this.numberOfCircle; n++) {
      if (n > 0)
        this.centers.push(
          this.figure.create('Point', {
            isChild: true,
            isSelectable: false,
            shape: '',
            x: this.center.x + n * (2 * this.radius + this.gap),
            y: this.center.y,
          }),
        )
      for (let i = 0; i < this.denominator; i++) {
        this.sectors.push(
          this.figure.create('ArcByCenterRadiusAndAngles', {
            center: this.centers[n],
            endAngle: angle * (i + 1),
            isChild: true,
            radius: this.radius,
            startAngle: angle * i,
            color: this.color,
            thickness: this.thickness,
            isDashed: this.isDashed,
          }),
        )
      }
    }
    this.indicesSectorsInColor = this._indicesSectorsInColor
  }

  redraw(): void {
    for (const center of this.centers) {
      center.remove()
    }
    for (const sector of this.sectors) {
      sector.remove()
    }
    this.draw()
  }

  toJSON(): object {
    return {
      ...this.jsonOptions(),
      denominator: this.denominator,
      gap: this.gap,
      arrayIndicesSectorsInColor: Array.from(this.indicesSectorsInColor),
      numberOfCircle: this.numberOfCircle,
      numerator: this.numerator,
      radius: this.radius,
      x: this.center.x,
      y: this.center.y,
    }
  }

  get indicesSectorsInColor(): Set<number> {
    for (let i = 0; i < this.sectors.length; i++) {
      if (this.sectors[i].fillColor !== 'none') {
        this._indicesSectorsInColor.add(i)
      } else {
        this._indicesSectorsInColor.delete(i)
      }
    }
    return this._indicesSectorsInColor
  }

  set indicesSectorsInColor(indicesSectorsInColor: Set<number>) {
    this._indicesSectorsInColor = indicesSectorsInColor
    this.sectors.forEach((sector, index) => {
      if (this._indicesSectorsInColor.has(index)) {
        sector.fillColor = this.color
      } else {
        sector.fillColor = 'none'
      }
    })
  }

  get numerator(): number {
    return this.sectors.filter((sector) => sector.fillColor !== 'none').length
  }

  set numerator(numerator: number) {
    this.sectors.forEach((sector, index) => {
      if (index < numerator) {
        sector.fillColor = this.color
      } else {
        sector.fillColor = 'none'
      }
    })
  }
}

export default CircleFractionDiagram
