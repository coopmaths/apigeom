import { expect, test } from 'vitest'
import { describe } from 'vitest'
import Figure from '../src/Figure'

describe('Homothétie', () => {
  test("Image d'un point après déplacement", () => {
    const figure = new Figure()
    const A = figure.create('Point', { x: -2, y: 0, label: 'A' })
    const B = figure.create('Point', { x: -2, y: 3, label: 'B' })
    const O = figure.create('Point', { x: 0, y: 0, label: 'M' })
    const A2 = A.dilate(O, -2)
    const B2 = B.dilate(O, -2)
    const M = figure.create('Middle', { point1: A2, point2: B2 })
    expect(A.x).toBe(-2)
    expect(A2.x).toBe(4)
    expect(M.x).toBe(4)
    expect(M.y).toBe(-3)
    A.moveTo(-4, 2)
    B.moveTo(-3, 5)
    expect(M.x).toBe(7)
    expect(M.y).toBe(-7)
  })

  test('Cercle circonscrit avec deux symétries centrales', () => {
    const figure = new Figure()
    const A = figure.create('Point', { x: -2, y: 0, label: 'A' })
    const B = figure.create('Point', { x: 6, y: 2, label: 'B' })
    const C = figure.create('Point', { x: 3, y: 8, label: 'C' })
    const triangle = figure.create('Polygon', { points: [A, B, C] })
    const medAB = figure.create('PerpendicularBisectorByPoints', {
      point1: A,
      point2: B,
    })
    const medBC = figure.create('PerpendicularBisectorByPoints', {
      point1: B,
      point2: C,
    })
    const medAC = figure.create('PerpendicularBisectorByPoints', {
      point1: A,
      point2: C,
    })
    const O1 = figure.create('PointIntersectionLL', {
      line1: medAB,
      line2: medBC,
    })
    const c = figure.create('CircleCenterPoint', { center: O1, point: A })

    const O = figure.create('Point', { x: 8, y: 5, label: 'O' })
    const O2 = figure.create('Point', { x: 5, y: 9, label: "O'" })

    figure.options.color = 'green'
    const triangle2 = triangle.dilate(O, -2)
    const medAB2 = medAB.dilate(O, -2)
    const medBC2 = medBC.dilate(O, -2)
    const medAC2 = medAC.dilate(O, -2)
    const c2 = c.dilate(O, -2)

    figure.options.color = 'orange'

    const triangle3 = triangle2.dilate(O, -2)
    const medAB3 = medAB2.dilate(O, -2)
    const medBC3 = medBC2.dilate(O, -2)
    const medAC3 = medAC2.dilate(O, -2)
    const c3 = c2.dilate(O, -2)

    const A3B3 = triangle3.segments[0]
    A3B3.point1.label = 'R'
    A3B3.point2.label = 'S'

    const isParallel = figure.checkParallel({
      label1: 'RS',
      label2: 'AB',
    }).isValid
    O.moveTo(O.x + 1, O.y + 1)
    O2.moveTo(O2.x + 1, O2.y + 1)
    expect(isParallel).toBe(true)
    expect(c3.radius).toBeCloseTo(c.radius * 4, 6)
  })
})
