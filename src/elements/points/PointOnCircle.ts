import type Figure from '../../Figure'
import type { OptionsElement2D } from '../interfaces'
import type Circle from '../lines/Circle'

import Point from './Point'

class PointOnCircle extends Point {
  angleWithHorizontal: number
  circle: Circle
  figure: Figure
  constructor(
    figure: Figure,
    {
      angleWithHorizontal,
      circle,
      label,
      ...options
    }: {
      angleWithHorizontal?: number
      circle: Circle
      label?: string
    } & OptionsElement2D,
  ) {
    if (angleWithHorizontal === undefined)
      angleWithHorizontal = Math.random() * 2 * Math.PI
    super(figure, {
      label,
      x: circle.center.x + circle.radius * Math.cos(angleWithHorizontal),
      y: circle.center.y + circle.radius * Math.sin(angleWithHorizontal),
      ...options,
    })
    this.figure = figure
    this.circle = circle
    this.angleWithHorizontal = angleWithHorizontal
    this.isFree = true
    this.type = 'PointOnCircle'
    this.circle.subscribe(this)
  }

  moveTo(x: number, y: number): void {
    const angleWithHorizontal = Math.atan2(
      y - this.circle.center.y,
      x - this.circle.center.x,
    )
    this.angleWithHorizontal = angleWithHorizontal
    this.x =
      this.circle.center.x + this.circle.radius * Math.cos(angleWithHorizontal)
    this.y =
      this.circle.center.y + this.circle.radius * Math.sin(angleWithHorizontal)
    this.update()
  }

  toJSON(): object {
    return {
      ...this.jsonOptions(),
      angleWithHorizontal: this.angleWithHorizontal,
      idCircle: this.circle.id,
      label: this.label,
    }
  }

  update(): void {
    this._x =
      this.circle.center.x +
      this.circle.radius * Math.cos(this.angleWithHorizontal)
    this._y =
      this.circle.center.y +
      this.circle.radius * Math.sin(this.angleWithHorizontal)
    super.update()
  }

  get description(): string {
    return `Point sur le cercle $${this.circle.id}$`
  }
}

export default PointOnCircle
