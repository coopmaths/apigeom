import { expect, test } from 'vitest'
import Figure from '../src/Figure'
import fs from 'node:fs/promises'
import sharp from 'sharp'
import nodeHtmlToImage from 'node-html-to-image'

const figure = new Figure({ xMin: -1.6, yMin: -1.6, width: 336, height: 95 })
figure.divUserMessage.style.display = 'none'
figure.options.automaticUserMessage = false
figure.options.color = 'blue'

const den = 6
figure.create('CircleFractionDiagram', { denominator: den, numberOfCircle: 3, radius: 1.5 })
// 3 x 3 + 2 gaps =   11 unités,
// pixelsPerUnit = 30,
// 11.2 * 30 * scale * scalex = 336

const container = document.createElement('div')
figure.isDynamic = false
figure.setContainer(container)  




// pnpm test ./tests/circleFractionDiagram.test.js
test('CircleFractionDiagram', async () => {
  let zoom = 1
  figure.zoom(zoom, { changeHeight: true, changeWidth: true, changeLeft: false, changeBottom: false })
  await exportPNG(zoom)
  await exportHTML(zoom)

  zoom = 2
  figure.zoom(zoom, { changeHeight: true, changeWidth: true, changeLeft: false, changeBottom: false })
  await exportPNG(zoom)
  await exportHTML(zoom)

  zoom = 3
  figure.zoom(zoom, { changeHeight: true, changeWidth: true, changeLeft: false, changeBottom: false })
  await exportPNG(zoom)
  await exportHTML(zoom)

}, 3 * 30000)


async function exportHTML(zoom, filename) {
    const IMA_FOLDER = 'imag_png'
    try {
        await fs.access(IMA_FOLDER)
    } catch (err) {
        await fs.mkdir(IMA_FOLDER)
    }

    const style =`<head>
      <style>
        body {
          width: ${figure.width}px;
          height: ${figure.height}px;
        }
      </style>
    </head>`

    await nodeHtmlToImage({
        output: `${IMA_FOLDER}/CircleFractionDiagram_HTML_z_${zoom}.png`,
        html: `<html><body>${style}<div>${figure.container.innerHTML}</div></body></html>`
    }).then(() => console.info('The image was created successfully!'))
}

async function exportPNG(zoom) {
    const IMA_FOLDER = 'imag_png'
    try {
        await fs.access(IMA_FOLDER)
    } catch (err) {
        await fs.mkdir(IMA_FOLDER)
    }
    const serializer = new XMLSerializer()
    const svgString = serializer.serializeToString(figure.svg)
    await sharp(Buffer.from(svgString))
        .png() // Conversion en PNG
        .toFile(`${IMA_FOLDER}/CircleFractionDiagram_z_${zoom}.png`) // Sauvegarde le fichier en PNG
        .then(info => {
            console.info(`Image PNG créée avec succès : ${info}`)
        })
        .catch(err => {
            console.error(`Erreur lors de la conversion : ${err}`)
        })
}

