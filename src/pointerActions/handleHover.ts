import type Figure from '../Figure'

import Element2D from '../elements/Element2D'
import { defaultDistanceClick } from '../elements/defaultValues'
import Point from '../elements/points/Point'

export default function handleHover(
  figure: Figure,
  pointerX: number,
  pointerY: number,
): void {
  const elements = [...figure.elements.values()].filter(
    (e) => e instanceof Element2D,
  ) as Element2D[]
  figure.hoverPoints.clear()
  for (const element of elements) {
    element.isHover =
      element.type !== 'pointer' &&
      element.isVisible &&
      element.isSelectable &&
      figure.filter(element) &&
      element.distancePointer(pointerX, pointerY) * figure.pixelsPerUnit <
        defaultDistanceClick
    if (element.isHover && element instanceof Point && element.isFree) {
      figure.hoverPoints.add(element)
    }
    if (
      figure.hoverPoints.size > 0 &&
      // @ts-expect-error
      typeof figure?.ui?.state?.value === 'object' &&
      // @ts-expect-error
      'POINT' in figure?.ui?.state?.value
    ) {
      figure.divFigure.style.cursor = 'move'
    } else {
      figure.divFigure.style.cursor = 'default'
    }
  }
}
