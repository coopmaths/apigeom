import { expect, test } from 'vitest'
import { JestAssertion } from 'vitest'
import Figure from '../src/Figure'

const figure = new Figure()
const A = figure.create('Point', { x: 0, y: 0, label: 'A' })
const B = figure.create('Point', { x: 5, y: 0, label: 'B' })
const AB = figure.create('Line', { point1: A, point2: B })
const C = figure.create('Point', { x: 10, y: 10, label: 'C' })
const d = figure.create('LineParallel', { line: AB, point: C })
const D = figure.create('PointOnLine', { line: d, label: 'D' })

test('Parallèles', () => {
  const { isValid } = figure.checkParallel({ label1: 'AB', label2: 'CD' })
  expect(isValid).toBe(true)
  C.moveTo(3, 5)
  const { isValid: isValid2 } = figure.checkParallel({
    label1: 'AB',
    label2: 'CD',
  })
  expect(isValid2).toBe(true)
  A.moveTo(3, 4)
  const { isValid: isValid3 } = figure.checkParallel({
    label1: 'AB',
    label2: 'CD',
  })
  expect(isValid3).toBe(true)
})

test('Parallèles verticales', () => {
  const A = figure.create('Point', { x: -2, y: 0, label: 'A' })
  const B = figure.create('Point', { x: -2, y: 3, label: 'B' })
  const M = figure.create('Point', { x: 0, y: 0, label: 'O' })
  const d = figure.create('Line', { point1: A, point2: B, label: 'd' })
  const d2 = figure.create('LineParallel', { line: d, point: M, label: 'd2' })
  expect(Number.isFinite(d2.point2.y)).toBe(true)
  const [a, b, c] = d.equation
  const [a2, b2, c2] = d2.equation
  expect(a * b2 - a2 * b).toBeCloseTo(0, 6)
})
