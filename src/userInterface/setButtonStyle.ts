export default function setButtonStyle(element: HTMLElement): void {
  element.style.width = '40px'
  element.style.height = '40px'
  element.style.margin = '4px'
  element.style.cursor = 'pointer'
  element.style.borderRadius = '5px'
  element.style.border = '1px solid black'
  element.style.backgroundColor = 'white'
  element.style.boxShadow = '0px 0px 2px 0px rgba(0,0,0,0.75)'
  element.style.padding = '2px'
  element.onmouseover = () => {
    element.style.boxShadow = '0px 0px 5px 0px rgba(0,0,0,0.75)'
  }
  element.onmouseout = () => {
    element.style.boxShadow = '0px 0px 2px 0px rgba(0,0,0,0.75)'
  }
}
