import { expect, test } from 'vitest'
import Figure from '../src/Figure'
import fs from 'node:fs/promises'
import sharp from 'sharp'
import nodeHtmlToImage from 'node-html-to-image'

const figure = new Figure({ xMin: -0.5, yMin: -2, width: 750, height: 120 })
figure.divUserMessage.style.display = 'none'
figure.options.automaticUserMessage = false
figure.options.color = 'blue'

figure.create('RectangleFractionDiagram',  { denominator: 2, numberOfRectangles: 5, width: 4 })
// 5 x 4 = 20 unités, plus 4 gaps donc 24 unités
// pixelsPerUnit = 30,
// 25 * 30 * scale * scalex = 750

figure.create('TextByPosition', { text: `L'unité est partagée en 2 parts égales.`, x: 0, y: -1.5, anchor: 'bottomLeft', isChild: false })

const container = document.createElement('div')
figure.isDynamic = false
figure.setContainer(container)  




// pnpm test ./tests/rectangleFractionDiagram.test.js
test('RectangleFractionDiagram', async () => {

  let zoom = 1
  figure.zoom(zoom, { changeHeight: true, changeWidth: true, changeLeft: false, changeBottom: false })
  await exportPNG(zoom)
  await exportHTML(zoom)

  zoom = 2
  figure.zoom(zoom, { changeHeight: true, changeWidth: true, changeLeft: false, changeBottom: false })
  await exportPNG(zoom)
  await exportHTML(zoom)

  zoom = 3
  figure.zoom(zoom, { changeHeight: true, changeWidth: true, changeLeft: false, changeBottom: false })
  await exportPNG(zoom)
  await exportHTML(zoom)

}, 3 * 30000)


async function exportHTML(zoom) {
    const IMA_FOLDER = 'imag_png'
    try {
        await fs.access(IMA_FOLDER)
    } catch (err) {
        await fs.mkdir(IMA_FOLDER)
    }

    const style =`<head>
      <style>
        body {
          width: ${figure.width}px;
          height: ${figure.height}px;
        }
      </style>
    </head>`

    await nodeHtmlToImage({
        output: `${IMA_FOLDER}/RectangleFractionDiagram_HTML_z_${zoom}.png`,
        html: `<html><body>${style}<div>${figure.container.innerHTML}</div></body></html>`
    }).then(() => console.info('The image was created successfully!'))
}

async function exportPNG(zoom) {
    const IMA_FOLDER = 'imag_png'
    try {
        await fs.access(IMA_FOLDER)
    } catch (err) {
        await fs.mkdir(IMA_FOLDER)
    }
    const serializer = new XMLSerializer()
    const svgString = serializer.serializeToString(figure.svg)
    await sharp(Buffer.from(svgString))
        .png() // Conversion en PNG
        .toFile(`${IMA_FOLDER}/LRectangleFractionDiagram_z_${zoom}.png`) // Sauvegarde le fichier en PNG
        .then(info => {
            console.info('Image PNG créée avec succès :', info)
        })
        .catch(err => {
            console.error('Erreur lors de la conversion :', err)
        })
}

