import type Point from '../points/Point'

export function distance(
  point1: { x: number; y: number },
  point2: { x: number; y: number },
): number {
  return Math.hypot(point1.x - point2.x, point1.y - point2.y)
}

export function getLargeSweep(angle: number): [number, number] {
  try {
    let large: 0 | 1
    let sweep: 0 | 1
    if (angle > 180) {
      large = 1
      sweep = 0
    } else if (angle < -180) {
      large = 1
      sweep = 1
    } else {
      large = 0
      sweep = angle > 0 ? 0 : 1
    }
    return [large, sweep]
  } catch (error) {
    return [Number.NaN, Number.NaN]
  }
}

export function normalizeAngle(angle: number): number {
  return angle - 2 * Math.PI * Math.floor((angle + Math.PI) / (2 * Math.PI))
}

export function polarToCartesian(
  center: Point,
  radius: number,
  angleInDegrees: number,
): { x: number; y: number } {
  const angleInRadians = (angleInDegrees * Math.PI) / 180.0

  return {
    x: center.x + radius * Math.cos(angleInRadians),
    y: center.y + radius * Math.sin(angleInRadians),
  }
}
