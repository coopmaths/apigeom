import type Figure from '../../Figure'
import type DynamicNumber from '../../dynamicNumbers/DynamicNumber'

import { round } from '../../lib/format'
import { displayDigits } from '../defaultValues'
import type { OptionsDynamicText } from '../interfaces'
import TextByPosition from './TextByPosition'

/**
 * Créé un div contenant un texte qui est mis au dessus du svg
 * par défaut KaTeX s'occupe du rendu du div
 */
class TextDynamicByPosition extends TextByPosition {
  private readonly dynamicNumber: DynamicNumber
  maximumFractionDigits: number
  minimumFractionDigits: number
  textAfter: string
  textBefore: string

  constructor(
    figure: Figure,
    {
      color = 'black',
      dynamicNumber,
      isChild = false,
      maximumFractionDigits = displayDigits,
      minimumFractionDigits = 0,
      textAfter = '',
      textBefore = '',
      x,
      y,
    }: OptionsDynamicText,
  ) {
    const value =
      textBefore +
      Intl.NumberFormat('fr-FR', {
        maximumFractionDigits,
        minimumFractionDigits,
      }).format(dynamicNumber.value) +
      textAfter
    super(figure, { color, isChild, text: value, x, y })
    this.type = 'TextDynamicByPosition'
    this.dynamicNumber = dynamicNumber
    this.textBefore = textBefore
    this.textAfter = textAfter
    this.minimumFractionDigits = minimumFractionDigits
    this.maximumFractionDigits = maximumFractionDigits
    dynamicNumber.subscribe(this)
  }

  toJSON(): object {
    return {
      ...this.jsonOptions(),
      idDynamicNumber: this.dynamicNumber.id,
      maximumFractionDigits: this.maximumFractionDigits,
      minimumFractionDigits: this.minimumFractionDigits,
      textAfter: this.textAfter,
      textBefore: this.textBefore,
      x: round(this.x),
      y: round(this.y),
    }
  }

  update(): void {
    this.text =
      this.textBefore +
      Intl.NumberFormat('fr-FR', {
        maximumFractionDigits: this.maximumFractionDigits,
        minimumFractionDigits: this.minimumFractionDigits,
      }).format(this.dynamicNumber.value) +
      this.textAfter
  }
}

export default TextDynamicByPosition
