import type Figure from '../Figure'
import type Polygon from '../elements/lines/Polygon'

export default function checkPolygonByLabels({
  figure,
  labels,
  color,
}: { figure: Figure; labels: string[]; color?: string }): {
  isValid: boolean
  message: string
} {
  let polygons = [...figure.elements.values()].filter((e) =>
    e.type.includes('Polygon'),
  ) as Polygon[]
  if (color !== undefined) {
    polygons = polygons.filter((e) => e.color === color)
  }
  if (polygons.length === 0)
    return {
      isValid: false,
      message:
        color === undefined
          ? "Il n'y a pas de polygones dans la figure."
          : `Il n'y a pas de polygones de couleur ${color} dans la figure.`,
    }
  let samePolygone = false
  for (const polygon of polygons) {
    if (polygon.points.length !== labels.length) {
      continue
    }
    // Index du premier point du polygone
    const index = labels.findIndex((label) => label === polygon.points[0].label)
    if (index === -1) {
      break
    }
    const newLabels = labels.slice(index).concat(labels.slice(0, index))
    samePolygone = newLabels.every(
      (label, i) => label === polygon.points[i].label,
    )
    if (samePolygone) {
      break
    }
    const reveseLabels = labels.reverse()
    const indexReverse = reveseLabels.findIndex(
      (label) => label === polygon.points[0].label,
    )
    const newLabelsReverse = reveseLabels
      .slice(indexReverse)
      .concat(reveseLabels.slice(0, indexReverse))
    samePolygone = newLabelsReverse.every(
      (label, i) => label === polygon.points[i].label,
    )
    if (samePolygone) {
      break
    }
  }
  return { isValid: samePolygone, message: '' }
}
