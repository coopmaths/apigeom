import type Figure from '../../Figure'
import { labelCoords } from '../calculus/Coords'
import type { OptionsElement2D } from '../interfaces'
import Segment from '../lines/Segment'
import type Point from '../points/Point'
import type TextByPosition from '../text/TextByPosition'

class VectorByPoints extends Segment {
  _label?: string
  elementTextLabel?: TextByPosition
  _colorLabel?: string
  labelDxInPixels?: number
  labelDyInPixels?: number
  constructor(
    figure: Figure,
    {
      point1,
      point2,
      label,
      ...options
    }: {
      isChild?: boolean
      isFree?: boolean
      isSelectable?: boolean
      isVisible?: boolean
      label?: string
      labelDxInPixels?: number
      labelDyInPixels?: number
      point1: Point
      point2: Point
    } & OptionsElement2D,
  ) {
    super(figure, { point1, point2, ...options })
    this.type = 'VectorByPoints'
    this.shape = '->'
    this.label = label
    this.labelDxInPixels = options.labelDxInPixels ?? 0
    this.labelDyInPixels = options.labelDyInPixels ?? 0
  }

  get label(): string {
    if (this._label === undefined) return ''
    return this._label
  }

  set label(label: string | undefined) {
    this._label = label
    if (label !== undefined) {
      if (this.elementTextLabel === undefined) {
        const { x, y } = labelCoords(this.point1, this.point2)
        this.elementTextLabel = this.figure.create('TextByPosition', {
          color: this._colorLabel ?? this.color,
          dxInPixels: this.labelDxInPixels,
          dyInPixels: this.labelDyInPixels,
          id: `${this.id}_label`,
          isChild: true,
          isSelectable: true,
          x,
          y,
          text: `$${label}$`,
        })
      } else {
        this.elementTextLabel.text = `$${label}$`
        this.elementTextLabel.color = this._colorLabel ?? 'currentColor'
      }
    }
  }

  get x() {
    return this.point2.x - this.point1.x
  }

  get y() {
    return this.point2.y - this.point1.y
  }

  update(): void {
    super.update()
    if (this.elementTextLabel !== undefined) {
      const { x, y } = labelCoords(this.point1, this.point2)
      this.elementTextLabel.x = x
      this.elementTextLabel.y = y
    }
  }

  toJSON(): object {
    return {
      ...this.jsonOptions(),
      idPoint1: this.point1.id,
      idPoint2: this.point2.id,
      label: this.label,
    }
  }
}

export default VectorByPoints
