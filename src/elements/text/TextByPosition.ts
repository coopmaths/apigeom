import renderMathInElement from 'katex/dist/contrib/auto-render.js'
import 'katex/dist/katex.min.css'

import type Figure from '../../Figure'

import { round } from '../../lib/format'
import Element2D from '../Element2D'
import type Grid from '../grid/Grid'

/**
 * Créé un div contenant un texte qui est mis au dessus du svg
 * par défaut KaTeX s'occupe du rendu du div
 */
class TextByPosition extends Element2D {
  /** Angle avec l'horizontal exprimé en degrés */
  _angleWithHorizontalInDegres = 0
  private _backgroundColor?: string
  _color: string
  private _fontSize: string
  private _text!: string
  private _x!: number
  private _y!: number
  /** Ancrage du texte */
  anchor:
    | 'bottomCenter'
    | 'bottomLeft'
    | 'bottomRight'
    | 'middleCenter'
    | 'middleLeft'
    | 'middleRight'
    | 'topCenter'
    | 'topLeft'
    | 'topRight'
  /** Le texte est mis dans un div qui s'affichera par dessus le SVG */
  div!: HTMLDivElement
  dxInPixels: number
  dyInPixels: number

  constructor(
    figure: Figure,
    {
      anchor = 'middleCenter',
      backgroundColor,
      color = 'black',
      dxInPixels = 0,
      dyInPixels = 0,
      fontSize = figure.options.fontSize,
      id,
      isChild = false,
      isVisible = true,
      text,
      x,
      y,
    }: {
      anchor?:
        | 'bottomCenter'
        | 'bottomLeft'
        | 'bottomRight'
        | 'middleCenter'
        | 'middleLeft'
        | 'middleRight'
        | 'topCenter'
        | 'topLeft'
        | 'topRight'
      backgroundColor?: string
      color?: string
      dx?: number
      dxInPixels?: number
      dy?: number
      dyInPixels?: number
      fontSize?: string
      id?: string
      isChild?: boolean
      isSelectable?: boolean
      isVisible?: boolean
      text: string
      x: number
      y: number
    },
  ) {
    super(figure, { id, isChild })
    this.type = 'TextByPosition'
    this._x = x
    this._y = y
    this.div = document.createElement('div')
    this._fontSize = fontSize
    this.dxInPixels = dxInPixels
    this.dyInPixels = dyInPixels
    this._text = text
    this.backgroundColor = backgroundColor
    this._color = color
    this.anchor = anchor
    this.isVisible = isVisible
  }

  draw(): void {
    if (
      !this.isVisible ||
      this.x > this.figure.xMax ||
      this.x < this.figure.xMin ||
      this.y > this.figure.yMax ||
      this.y < this.figure.yMin
    ) {
      return
    }
    this.div.style.position = 'absolute'
    this.div.style.pointerEvents = 'none'
    this.figure.divFigure?.appendChild(this.div)
    if (this.color !== 'black') this.div.style.color = this.color
    if (this.backgroundColor !== undefined)
      this.div.style.backgroundColor = this.backgroundColor
    this.text = this._text
    this.x = this._x
    this.y = this._y
    if (this._angleWithHorizontalInDegres !== 0) {
      this.div.style.transform = this.div.style.transform.replace(
        /rotate\([0-9.]+deg\)/,
        '',
      )
      this.div.style.transformOrigin = 'center'
      this.div.style.transform += ` rotate(${this._angleWithHorizontalInDegres}deg)`
      this.div.style.transform += 'translateY(-50%)'
      this.div.style.verticalAlign = 'middle'
    }
    this.div.style.alignItems = 'center'
    if (this.figure.scale - this.figure.initial.scale !== 0) {
      /* MGU essaie de augmnter le texte avec le zoom */
      if (this._fontSize.includes('em')) {
        // rien à faire, c'est en fonction de la taille du caractère m
      } else if (this._fontSize.includes('pt')) {
        this.fontSize =  `${Number.parseFloat(this._fontSize) *  (1 + (this.figure.scale - this.figure.initial.scale))}pt`
      }
    }
  }

  /** Déplace le texte aux coordonnées données */
  moveTo(x: number, y: number): void {
    this.x = x
    this.y = y
  }

  remove(): void {
    this.figure.elements.delete(this.id)
    this.div.remove()
    for (const element of this.observers) {
      element.remove()
    }
  }

  get isVisible(): boolean {
    return this._isVisible
  }

  set isVisible(isVisible: boolean) {
    this._isVisible = isVisible
    if (isVisible) {
      this.figure.divFigure?.appendChild(this.div)
    } else {
      this.div.remove()
    }
  }

  get opacity(): number {
    return this._opacity
  }

  set opacity(opacity: number) {
    this._opacity = opacity
    this.div.style.opacity = opacity.toString()
  }

  toJSON(): object {
    return {
      ...this.jsonOptions(),
      backgroundColor: this.backgroundColor,
      color: this.color,
      name: this.id,
      text: this.text,
      x: round(this.x),
      y: round(this.y),
      anchor: this.anchor,
    }
  }

  set angleWithHorizontalInDegres(angle: number) {
    this._angleWithHorizontalInDegres = angle
    this.div.style.transform = this.div.style.transform.replace(
      /rotate\([0-9.]+deg\)/,
      '',
    )
    this.div.style.transformOrigin = 'center'
    this.div.style.transform += ` rotate(${angle}deg)`
  }

  get angleWithHorizontalInDegres(): number {
    return this._angleWithHorizontalInDegres
  }

  get backgroundColor(): string | undefined {
    return this._backgroundColor
  }

  set backgroundColor(color: string | undefined) {
    this._backgroundColor = color
    if (color !== undefined) {
      this.div.style.backgroundColor = color
    }
  }

  get color(): string {
    return this._color
  }

  set color(color: string) {
    this._color = color
    this.div.style.color = color
  }

  get fontSize(): string {
    return this._fontSize
  }

  set fontSize(fontSize: string) {
    this._fontSize = fontSize
    this.div.style.fontSize = fontSize
  }

  get latex(): string {
    // On exclue les labels de la grille car ils sont gérés par l'environnement axis
    const grid = [...this.figure.elements.values()].filter(
      (e) => e.type === 'Grid',
    )[0] as Grid
    if (grid && grid.labels.indexOf(this) !== -1) return ''
    return this.text !== '$$' && this.text !== ''
      ? `\\node[${this.latexOptions}] at (${this.x},${this.y}) {${this.text}};`
      : ''
  }

  get latexOptions(): string {
    const superOptions = super.latexOptions
    const options =
      superOptions === ''
        ? []
        : superOptions.substring(1, superOptions.length - 1).split(',')
    const alignment = []
    if (this.dyInPixels > 0) alignment.push('above')
    else if (this.dyInPixels < 0) alignment.push('below')
    if (this.dxInPixels > 0) alignment.push('right')
    else if (this.dxInPixels < 0) alignment.push('left')
    const alignmentString = alignment.join(' ')
    if (alignment.length > 0) options.push(alignmentString)
    return options.join(',')
  }

  get text(): string {
    return this._text
  }

  set text(text: string) {
    this._text = text
    this.div.innerHTML = text
    renderMathInElement(this.div, {
      delimiters: [
        { display: true, left: '\\[', right: '\\]' },
        { display: false, left: '$', right: '$' },
      ],
      errorColor: '#CC0000',
      preProcess: (chaine: string) =>
        chaine.replaceAll(String.fromCharCode(160), '\\,'),
      strict: 'warn',
      throwOnError: true,
      trust: false,
    })
  }

  get x(): number {
    return this._x
  }

  set x(x: number) {
    let styleTransform = ''
    if (this.anchor.includes('Left')) {
      this.div.style.left = `${this.figure.xToSx(x - this.figure.xMin).toString()}px`
    } else if (this.anchor.includes('Right')) {
      // this.div.style.right = this.figure.xToSx(-x + this.figure.xMax).toString() + 'px'
      if (
        this.figure.divFigure?.clientWidth !== undefined &&
        this.figure.divFigure.clientWidth > this.figure.width
      ) {
        this.div.style.right = `${(this.figure.xToSx(-x + this.figure.xMax) + Math.max(0, this.figure.divFigure.clientWidth - this.figure.width)).toString()}px`
      } else {
        this.div.style.right = `${this.figure.xToSx(-x + this.figure.xMax).toString()}px`
      }
    } else if (this.anchor.includes('Center')) {
      this.div.style.left = `${this.figure.xToSx(x - this.figure.xMin).toString()}px`
      styleTransform += 'translateX(-50%)'
    }
    if (this.anchor.includes('middle')) {
      styleTransform += 'translateY(-50%)'
    }
    if (this.dxInPixels !== 0 || this.dyInPixels !== 0) {
      styleTransform += ` translate(${this.dxInPixels.toString()}px, ${-this.dyInPixels.toString()}px)`
    }
    this.div.style.fontSize = this._fontSize
    this.div.style.transform = styleTransform
    this._x = x
  }

  get y(): number {
    return this._y
  }

  set y(y: number) {
    if (this.anchor.includes('top')) {
      this.div.style.top = `${this.figure.yToSy(y - this.figure.yMax).toString()}px`
    } else if (this.anchor.includes('bottom')) {
      this.div.style.bottom = `${this.figure.yToSy(-y + this.figure.yMin).toString()}px`
    } else if (this.anchor.includes('middle')) {
      this.div.style.top = `${this.figure.yToSy(y - this.figure.yMax).toString()}px`
    }
    this._y = y
  }
}

export default TextByPosition
