import type DynamicNumber from '../dynamicNumbers/DynamicNumber'
import type Element2D from './Element2D'
import type Circle from './lines/Circle'
import type Line from './lines/Line'
import type Point from './points/Point'
import type Vector from './vector/Vector'

export interface Binome {
  x: number
  y: number
}

export interface FigureOptions {
  animationStepInterval: number
  automaticUserMessage: boolean
  borderSize: number
  color: string
  /** Couleur des sommets après les tracés (par défaut transparent) */
  colorPointPolygon: string
  changeColorChangeActionToSetOptions: boolean
  displayGrid: boolean
  discFillOpacity: number
  distanceWithoutNewPoint: number
  fillColor: string
  fillColorAndBorderColorAreSame: boolean
  fillOpacity: number
  fontSize: string
  /** Dans une Grid avec snapGrid à true, permettre à deux points d'être confondus (par défaut à true) */
  gridWithTwoPointsOnSamePosition: boolean
  isDashed: boolean
  labelAutomaticBeginsWith?: string
  labelDxInPixels: number
  labelDyInPixels: number
  labelIsVisible?: boolean
  latexHeight: number
  latexWidth: number
  limitNumberOfElement: Map<string, number>
  mark: string
  moveTextGrid: number
  pointDescriptionWithCoordinates: boolean
  pointSize: number
  shape: '' | '|' | 'o' | 'x'
  shapeForPolygon: '' | '|' | 'o' | 'x'
  thickness: number
  thicknessForPoint: number
  tmpColor: string
  tmpFillColor: string
  tmpFillOpacity: number
  tmpIsDashed: boolean
  tmpShape: '' | '|' | 'o' | 'x'
  tmpThickness: number
}

export interface OptionsElement2D {
  color?: string
  fillColor?: 'none' | string
  fillOpacity?: number
  id?: string
  isChild?: boolean
  isDashed?: boolean
  isSelectable?: boolean
  isDeletable?: boolean
  isVisible?: boolean
  label?: string
  opacity?: number
  thickness?: number
}

export interface OptionsPoint extends OptionsElement2D {
  isFree: boolean
  label?: string
  labelDxInPixels?: number
  labelDyInPixels?: number
  shape: '' | '|' | 'o' | 'x'
  sizeInPixels?: number
  x: number
  y: number
}

export interface OptionsPointWithoutCoords extends OptionsElement2D {
  isFree?: boolean
  label?: string
  labelDxInPixels?: number
  labelDyInPixels?: number
  shape?: '' | '|' | 'o' | 'x'
  sizeInPixels?: number
}

export interface OptionsPointBy extends OptionsElement2D {
  isFree?: boolean
  label?: string
  labelDxInPixels?: number
  labelDyInPixels?: number
  shape?: '' | '|' | 'o' | 'x'
  sizeInPixels?: number
}

export interface OptionsPointByTranslation extends OptionsElement2D {
  isChild?: boolean
  label?: string
  labelDxInPixels?: number
  labelDyInPixels?: number
  origin: Point
  shape: '' | '|' | 'o' | 'x'
  sizeInPixels?: number
  vector: Vector
}
export interface OptionsElementByTranslation extends OptionsElement2D {
  isChild?: boolean
  label?: string
  labelDxInPixels?: number
  labelDyInPixels?: number
  origin: Element2D
  shape: '' | '|' | 'o' | 'x'
  sizeInPixels?: number
  vector: Vector
}

export interface OptionsMiddle extends OptionsElement2D {
  isChild?: boolean
  label?: string
  labelDxInPixels?: number
  labelDyInPixels?: number
  point1: Point
  point2: Point
  shape: '' | '|' | 'o' | 'x'
  sizeInPixels?: number
}

export interface OptionsIntersectionLL extends OptionsElement2D {
  InPixels?: number
  line1: Line
  line2: Line
  shape?: '' | '|' | 'o' | 'x'
}

export interface OptionsIntersectionCC extends OptionsElement2D {
  circle1: Circle
  circle2: Circle
  n?: 1 | 2
  shape?: '' | '|' | 'o' | 'x'
  sizeInPixels?: number
}

export interface OptionsIntersectionLC extends OptionsElement2D {
  circle: Circle
  line: Line
  n?: 1 | 2
  shape?: '' | '|' | 'o' | 'x'
  sizeInPixels?: number
}

export interface OptionsCircle extends OptionsElement2D {
  center: Point
  radius: number
}

export interface OptionsCircleCenterDynamicRadius extends OptionsElement2D {
  center: Point
  radius: DynamicNumber
}

/**
 * @param {Point} center
 * @param {Point} point
 */
export interface OptionsCircleCenterPoint extends OptionsElement2D {
  center: Point
  point: Point
}

export interface OptionsPolyline extends OptionsElement2D {
  points: Point[]
}

export interface OptionsPolygon extends OptionsElement2D {
  points: Point[]
}

export interface OptionsLine extends OptionsElement2D {
  borderSize?: number
  point1: Point
  point2: Point
  shape?: '-|' | '' | '|-' | '|-|'
}

export interface OptionsLineParallel extends OptionsElement2D {
  line: Line
  point: Point
}

export interface OptionsLinePerpendicular extends OptionsElement2D {
  line: Line
  point: Point
}

export interface OptionsText extends OptionsElement2D {
  anchor?:
    | 'bottomCenter'
    | 'bottomLeft'
    | 'bottomRight'
    | 'middleCenter'
    | 'middleLeft'
    | 'middleRight'
    | 'topCenter'
    | 'topLeft'
    | 'topRight'
  dx?: number
  dxInPixels?: number
  dy?: number
  dyInPixels?: number
  fontSize?: string
  text: string
  x: number
  y: number
}

export interface OptionsDynamicNumber extends OptionsElement2D {
  isChild?: boolean
  textAfter?: string
  textBefore?: string
}

export interface OptionsDynamicText extends OptionsElement2D {
  dx?: number
  dy?: number
  dynamicNumber: DynamicNumber
  maximumFractionDigits?: number
  minimumFractionDigits?: number
  textAfter: string
  textBefore: string
  x: number
  y: number
}

export interface OptionsRestrictedText extends OptionsElement2D {
  dxInPixels?: number
  dyInPixels?: number
  point: Point
  text: string
}

export interface OptionsVector extends OptionsElement2D {
  x: number
  y: number
}

export interface OptionsVectorByPoints extends OptionsElement2D {
  origin?: Point
  point1: Point
  point2: Point
}

export interface OptionsVectorPerpendicular extends OptionsElement2D {
  line: Line
  origin: Point
}

export type typeElement2D =
  | ''
  | 'Angle'
  | 'Arc'
  | 'ArcBy3PointsAndRadius'
  | 'ArcByCenterRadiusAndAngles'
  | 'Barycenter'
  | 'BisectorByPoints'
  | 'Circle'
  | 'CircleCenterPoint'
  | 'CircleDynamicRadius'
  | 'CircleFractionDiagram'
  | 'DisplayDistance'
  | 'Distance'
  | 'DynamicCalcul'
  | 'DynamicX'
  | 'DynamicY'
  | 'ElementByReflect'
  | 'ElementByReflectOverLine'
  | 'ElementByRotation'
  | 'ElementByTranslationByPoints'
  | 'GraduatedLine'
  | 'Graph'
  | 'Graph2'
  | 'GraphByParts'
  | 'Grid'
  | 'Line'
  | 'LineByPointVector'
  | 'LineFractionDiagram'
  | 'LineParallel'
  | 'LinePerpendicular'
  | 'MarkBetweenPoints'
  | 'MarkRightAngle'
  | 'MarkSegment'
  | 'Middle'
  | 'PerpendicularBisector'
  | 'PerpendicularBisectorByPoints'
  | 'Point'
  | 'PointByProjection'
  | 'PointByProjectionOnAxisX'
  | 'PointByProjectionOnAxisY'
  | 'pointer'
  | 'PointIntersectionCC'
  | 'PointIntersectionLC'
  | 'PointIntersectionLL'
  | 'PointOnCircle'
  | 'PointOnGraph'
  | 'PointOnLine'
  | 'PointOnPolyline'
  | 'PointOnLineAtDistance'
  | 'PointsIntersectionCC'
  | 'PointsIntersectionLC'
  | 'Polygon'
  | 'Polyline'
  | 'Ray'
  | 'RectangleFractionDiagram'
  | 'Segment'
  | 'TextByPoint'
  | 'TextByPosition'
  | 'TextCyclic'
  | 'TextDynamicByPosition'
  | 'Vector'
  | 'VectorByPoints'
  | 'VectorPerpendicular'

export interface SaveJson {
  options: FigureOptions
  history: string[]
}
