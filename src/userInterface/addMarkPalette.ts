import type Figure from '../Figure'
import { marks, orangeMathaleaLight } from '../elements/defaultValues'

export default function addMarkPalette({
  figure,
  container,
}: { figure: Figure; container: HTMLDivElement }): void {
  const buttonsMark: HTMLElement[] = []
  for (const mark of marks) {
    const button = figure.addCustomButton({
      record: false,
      text: mark,
      action: () => {
        figure.options.mark = mark
      },
      container: container,
    })
    if (mark === figure.options.mark) {
      button.style.backgroundColor = orangeMathaleaLight
    }
    buttonsMark.push(button)
    button.addEventListener('click', handleClick)
  }
  function handleClick(this: HTMLElement): void {
    for (const button of buttonsMark) {
      button.style.backgroundColor = 'white'
    }
    this.style.backgroundColor = orangeMathaleaLight
  }
}
