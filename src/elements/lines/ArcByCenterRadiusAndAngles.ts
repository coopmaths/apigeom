import type Figure from '../../Figure'
import type Point from '../points/Point'

import Const from '../../dynamicNumbers/Const'
import { polarToCartesian } from '../calculus/utils'
import type { OptionsElement2D } from '../interfaces'
import Arc from './Arc'

class ArcByCenterRadiusAndAngles extends Arc {
  endAngle: number
  radius: number
  startAngle: number
  constructor(
    figure: Figure,
    {
      addBorders = true,
      center,
      color,
      endAngle,
      fillColor,
      fillOpacity,
      radius,
      startAngle,
      ...options
    }: {
      addBorders?: boolean
      center: Point
      endAngle: number
      radius: number
      startAngle: number
    } & OptionsElement2D,
  ) {
    const dynamicAngle = new Const(figure, {
      isChild: true,
      value: endAngle - startAngle,
    })
    const { x, y } = polarToCartesian(center, radius, startAngle)
    const start = figure.create('Point', {
      isChild: true,
      isFree: false,
      shape: '',
      x,
      y,
    })
    super(figure, {
      addBorders,
      center,
      color,
      dynamicAngle,
      fillColor,
      fillOpacity,
      start,
      ...options,
    })
    this.radius = radius
    this.startAngle = startAngle
    this.endAngle = endAngle
    this.type = 'ArcByCenterRadiusAndAngles'
  }

  toJSON(): object {
    return {
      ...this.jsonOptions(),
      endAngle: this.endAngle,
      fillColor: this.fillColor,
      idCenter: this.center.id,
      radius: this.radius,
      startAngle: this.startAngle,
    }
  }

  get latex(): string {
    let result = `% Arc de cercle ${this.id}`
    const start = polarToCartesian(this.center, this.radius, this.startAngle)
    if (this.fillColor && this.fillColor !== 'none') {
      result += `\n\\fill${this.latexOptions} (${this.center.x}, ${this.center.y}) -- ({${this.center.x} + ${this.radius} * cos(${this.startAngle})}, {${this.center.y} + ${this.radius} * sin(${this.startAngle})}) arc(${this.startAngle}:${this.endAngle}:${this.radius}) -- 
    cycle;`
    } else {
      result += `\n\\draw${this.latexOptions} (${start.x}, ${start.y}) arc (${this.startAngle}:${this.endAngle}:${this.radius});`
    }
    if (this.addBorders) {
      result += `\n\\draw${this.latexOptions} (${this.center.x}, ${this.center.y}) -- ({${this.center.x} + ${this.radius} * cos(${this.startAngle})}, {${this.center.y} + ${this.radius} * sin(${this.startAngle})});`
      result += `\n\\draw${this.latexOptions} (${this.center.x}, ${this.center.y}) -- ({${this.center.x} + ${this.radius} * cos(${this.endAngle})}, {${this.center.y} + ${this.radius} * sin(${this.endAngle})});`
    }
    return result
  }
}

export default ArcByCenterRadiusAndAngles
