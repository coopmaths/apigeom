import type Figure from '../Figure'
import type Point from '../elements/points/Point'

import { distance as calculDistance } from '../elements/calculus/Coords'
import syncShake from './syncShake'

export default function checkDistance({
  distance,
  figure,
  label1,
  label2,
  shake = true,
  time = 0,
}: {
  distance: number
  figure: Figure
  label1: string
  label2: string
  shake?: boolean
  time?: number
}): { isValid: boolean; message: string } {
  const points = [...figure.elements.values()].filter(
    (e) => e.type !== 'pointer' && e.type.includes('Point'),
  ) as Point[]
  const matchPoints1 = points.filter((p) => p.label === label1)
  if (matchPoints1.length === 0) {
    return {
      isValid: false,
      message: `Il n'y a pas de point $${label1}$ dans la figure.`,
    }
  }
  if (matchPoints1.length > 1) {
    return {
      isValid: false,
      message: `Il y a ${matchPoints1.length} points $${label1}$ dans la figure.`,
    }
  }
  const matchPoints2 = points.filter((p) => p.label === label2)
  if (matchPoints2.length === 0) {
    return {
      isValid: false,
      message: `Il n'y a pas de point $${label2}$ dans la figure.`,
    }
  }
  if (matchPoints2.length > 1) {
    return {
      isValid: false,
      message: `Il y a ${matchPoints2.length} points $${label2}$ dans la figure.`,
    }
  }
  const point1 = matchPoints1[0]
  const point2 = matchPoints2[0]
  let isValid = Math.abs(calculDistance(point1, point2) - distance) < 0.001
  if (shake) {
    syncShake({ figure, time })
    isValid =
      isValid && Math.abs(calculDistance(point1, point2) - distance) < 0.001
    syncShake({ figure, time })
    isValid =
      isValid && Math.abs(calculDistance(point1, point2) - distance) < 0.001
  }
  let message = ''
  if (isValid) {
    message = `La distance entre les points $${label1}$ et $${label2}$ est bien toujours égale à ${distance}.`
  } else {
    message = `La distance entre les points $${label1}$ et $${label2}$ n'est pas toujours égale à ${distance}.`
  }
  return { isValid, message }
}
