import type Figure from '../../Figure'
import type { OptionsElement2D, OptionsPointWithoutCoords } from '../interfaces'
import type Arc from '../lines/Arc'
import type Circle from '../lines/Circle'
import type Line from '../lines/Line'
import type Polygon from '../lines/Polygon'
import type Segment from '../lines/Segment'
import type TextByPoint from '../text/TextByPoint'

import { round } from '../../lib/format'
import Element2D from '../Element2D'
import {
  reflectOverLineCoord,
  rotationCoord,
  translateCoords,
} from '../calculus/Coords'
import { distance, normalizeAngle } from '../calculus/utils'
import Grid from '../grid/Grid'
import type Vector from '../vector/Vector'

/**
 * Trace un point et ajoute un éventuel label à partir de la propriété label
 */
class Point extends Element2D {
  /** Couleur du label */
  private _colorLabel: string
  /** Nom que l'on affiche à côté du point */
  private _label?: string
  /** Croix, rond ou rien */
  private _shape: '' | '|' | 'o' | 'x'
  /** Taille du point, correspond à ce qui est ajouté dans les 4 directions pour faire la croix ou au rayon du rond */
  private _sizeInPixels: number
  protected _x: number
  protected _y: number
  /** Elément SVG pour rond */
  private svgCircle!: SVGCircleElement
  /** Affichage du nom du point */
  elementTextLabel?: TextByPoint
  /** Le point est-il librement déplaçable ? */
  isFree: boolean
  /** Décalage vertical pour le nom du point */
  labelDxInPixels: number
  /** Décalage horizontal pour le nom du point */
  labelDyInPixels: number
  /** Affichage ou non du label */
  labelIsVisible: boolean
  /** Elément SVG pour le premier trait de la croix */
  protected svgLine1!: SVGLineElement
  /** Elément SVG pour le deuxième trait de la croix */
  protected svgLine2!: SVGLineElement
  constructor(
    figure: Figure,
    {
      color = figure.options.color,
      colorLabel = figure.options.color,
      id,
      isChild,
      isFree = true,
      isSelectable,
      isVisible = true,
      label,
      labelIsVisible = figure.options.labelIsVisible ?? true,
      labelDxInPixels,
      labelDyInPixels,
      shape = figure.options.shape,
      sizeInPixels,
      thickness,
      x = 0,
      y = 0,
    }: {
      color?: string
      colorLabel?: string
      id?: string
      isChild?: boolean
      isFree?: boolean
      isSelectable?: boolean
      isVisible?: boolean
      label?: string
      labelDxInPixels?: number
      labelDyInPixels?: number
      labelIsVisible?: boolean
      shape?: '' | '|' | 'o' | 'x'
      sizeInPixels?: number
      thickness?: number
      x?: number
      y?: number
    } & OptionsElement2D,
  ) {
    if (id === undefined) {
      let cpt = 0
      while (figure.elements.has(`point${cpt.toString()}`)) {
        cpt++
      }
      id = `point${cpt.toString()}`
    }
    super(figure, { color, id, isChild, isSelectable, thickness })
    this.type = 'Point'
    this._shape = shape ?? 'x'
    this._sizeInPixels = sizeInPixels ?? figure.options.pointSize
    this._thickness = thickness ?? figure.options.thicknessForPoint
    this.labelDxInPixels = labelDxInPixels ?? figure.options.labelDxInPixels
    this.labelDyInPixels = labelDyInPixels ?? figure.options.labelDyInPixels
    if (this.figure.snapGrid && this.isVisible) {
      // Les points invisible comme ceux qui définissent la droite perpendiculaire n'ont pas besoin d'être sur la grille
      this._x = Math.round(x / this.figure.dx) * this.figure.dx
      this._y = Math.round(y / this.figure.dy) * this.figure.dy
    } else {
      this._x = x
      this._y = y
    }
    this.label = label
    this.labelIsVisible = labelIsVisible
    this._colorLabel = colorLabel
    this.isFree = isFree
    this._isVisible = isVisible ?? true
  }

  private redraw(): void {
    this.notify()
    if (Number.isNaN(this._x) || Number.isNaN(this._y)) {
      this.svgCircle?.remove()
      this.svgLine1?.remove()
      this.svgLine2?.remove()
      this.elementTextLabel?.hide()
      return
    }
    if (
      this.elementTextLabel === undefined ||
      this.elementTextLabel.x > this.figure.xMax ||
      this.elementTextLabel.y > this.figure.yMax ||
      this.elementTextLabel.x < this.figure.xMin ||
      this.elementTextLabel.y < this.figure.yMin
    ) {
      this.elementTextLabel?.hide()
    } else
      this.isVisible && this.labelIsVisible && this.elementTextLabel?.show()
    if (this._shape === 'x') {
      const x1Svg = this.figure.xToSx(this._x) - this._sizeInPixels
      const x2Svg = this.figure.xToSx(this._x) + this._sizeInPixels
      const x3Svg = this.figure.xToSx(this._x) + this._sizeInPixels
      const x4Svg = this.figure.xToSx(this._x) - this._sizeInPixels
      const y1Svg = this.figure.yToSy(this._y) - this._sizeInPixels
      const y2Svg = this.figure.yToSy(this._y) + this._sizeInPixels
      const y3Svg = this.figure.yToSy(this._y) - this._sizeInPixels
      const y4Svg = this.figure.yToSy(this._y) + this._sizeInPixels
      this.svgLine1.setAttribute('x1', `${x1Svg}`)
      this.svgLine1.setAttribute('y1', `${y1Svg}`)
      this.svgLine1.setAttribute('x2', `${x2Svg}`)
      this.svgLine1.setAttribute('y2', `${y2Svg}`)
      this.svgLine2.setAttribute('x1', `${x3Svg}`)
      this.svgLine2.setAttribute('y1', `${y3Svg}`)
      this.svgLine2.setAttribute('x2', `${x4Svg}`)
      this.svgLine2.setAttribute('y2', `${y4Svg}`)
      this.svgCircle?.remove()
      this.groupSvg.appendChild(this.svgLine1)
      this.groupSvg.appendChild(this.svgLine2)
    } else if (this._shape === 'o') {
      const xSvg = this.figure.xToSx(this._x)
      const ySvg = this.figure.yToSy(this._y)
      const rSvg = this._sizeInPixels
      this.svgCircle.setAttribute('cx', `${xSvg}`)
      this.svgCircle.setAttribute('cy', `${ySvg}`)
      this.svgCircle.setAttribute('r', `${rSvg}`)
      this.svgLine1?.remove()
      this.svgLine2?.remove()
      this.groupSvg.appendChild(this.svgCircle)
    } else if (this._shape === '') {
      this.svgLine1?.remove()
      this.svgLine2?.remove()
      this.svgCircle?.remove()
    }
  }

  createSegmentToAxeX(): Segment {
    const M = this.figure.create('PointByProjectionOnAxisX', {
      isVisible: false,
      origin: this,
    })
    return this.figure.create('Segment', {
      isDashed: true,
      point1: this,
      point2: M,
    })
  }

  createSegmentToAxeY(): Segment {
    const M = this.figure.create('PointByProjectionOnAxisY', {
      isVisible: false,
      origin: this,
    })
    return this.figure.create('Segment', {
      isDashed: true,
      point1: this,
      point2: M,
    })
  }

  dilate(center: Point, factor: number, options?: OptionsPointWithoutCoords): Point {
    const dilated = this.figure.create('Point', options)
    dilated.transformation = { center, factor, origin: this, type: 'dilate' }
    this.subscribe(dilated)
    center.subscribe(dilated)
    dilated.isFree = false
    dilated.update()
    return dilated
  }

  /** Distance entre un point et le pointeur de la souris exprimée dans les unités du repère */
  distancePointer(pointerX: number, pointerY: number): number {
    if (this._x === undefined || this._y === undefined)
      return Number.POSITIVE_INFINITY
    return Math.hypot(
      (this._x - pointerX) * this.figure.scale * this.figure.xScale,
      (this._y - pointerY) * this.figure.scale * this.figure.yScale,
    )
  }

  draw(): void {
    // Les deux traits qui forment la croix du point
    this.svgLine1 = document.createElementNS(
      'http://www.w3.org/2000/svg',
      'line',
    )
    this.svgLine2 = document.createElementNS(
      'http://www.w3.org/2000/svg',
      'line',
    )
    // Le cercle, si le point est représenté par un rond
    this.svgCircle = document.createElementNS(
      'http://www.w3.org/2000/svg',
      'circle',
    )
    // Le groupe parent de la représentation du point
    this.groupSvg = document.createElementNS('http://www.w3.org/2000/svg', 'g')
    this.setVisibilityColorThicknessAndDashed()
    if (this.isVisible) {
      this.shape = this._shape
      this.label = this._label
    }
    this.update()
  }

  isInArc(arc: Arc): boolean {
    const [x, y] = [this.x, this.y]
    const distanceToCenter = distance(this, arc.center)
    const radius = distance(arc.center, arc.start)
    if (distanceToCenter > radius) return false
    const angle = normalizeAngle(Math.atan2(y - arc.center.y, x - arc.center.x))
    const angleStart = normalizeAngle(
      Math.atan2(arc.start.y - arc.center.y, arc.start.x - arc.center.x),
    )
    const angleEnd = normalizeAngle(
      angleStart + (arc.dynamicAngle.value * Math.PI) / 180,
    )
    if (angleStart < angleEnd) {
      return angleStart < angle && angle < angleEnd
    }
    return angleStart < angle || angle < angleEnd
  }

  isInCircle(circle: Circle): boolean {
    return (
      (this.x - circle.center.x) ** 2 + (this.y - circle.center.y) ** 2 <=
      circle.radius ** 2
    )
  }

  isInPolygon(polygon: Polygon): boolean {
    const x = this.x
    const y = this.y
    let result = false
    for (let i = 0; i < polygon.points.length; i++) {
      const point1 = polygon.points.at(i % polygon.points.length) as Point
      const point2 = polygon.points.at((i + 1) % polygon.points.length) as Point
      if (
        point1.y > y !== point2.y > y &&
        x <
          ((point2.x - point1.x) * (y - point1.y)) / (point2.y - point1.y) +
            point1.x
      ) {
        result = !result
      }
    }
    return result
  }

  isOnline(line: Line): boolean {
    const [a, b, c] = line.equation
    let result = Math.abs(a * this.x + b * this.y + c) < 10 ** -6
    if (
      line.type === 'Segment' &&
      (this.x < Math.min(line.point1.x, line.point2.x) ||
        this.x > Math.max(line.point1.x, line.point2.x) ||
        this.y < Math.min(line.point1.y, line.point2.y) ||
        this.y > Math.max(line.point1.y, line.point2.y))
    ) {
      result = false
    }
    if (line.type === 'Ray') {
      // Direction gauche droite
      if (line.point1.x < line.point2.x && this.x < line.point1.x) {
        result = false
      }
      // Direction droite gauche
      if (line.point1.x > line.point2.x && this.x > line.point1.x) {
        result = false
      }
      // Direction bas haut
      if (line.point1.y < line.point2.y && this.y < line.point1.y) {
        result = false
      }
      // Direction haut bas
      if (line.point1.y > line.point2.y && this.y > line.point1.y) {
        result = false
      }
    }
    return result
  }

  jsonOptions(): object {
    return {
      ...super.jsonOptions(),
      colorLabel: this.colorLabel,
      label: this.label,
      labelDxInPixels: this.labelDxInPixels,
      labelDyInPixels: this.labelDyInPixels,
      shape: this.shape,
      sizeInPixels: this.sizeInPixels,
      x: round(this.x),
      y: round(this.y),
    }
  }

  /** Déplace le point */
  moveTo(x: number, y: number): void {
    this.x = x
    this.y = y
  }

  reflect(center: Point, options?: OptionsPointWithoutCoords): Point {
    const reflected = this.figure.create('Point', options)
    reflected.transformation = { center, origin: this, type: 'reflect' }
    this.subscribe(reflected)
    center.subscribe(reflected)
    reflected.isFree = false
    reflected.update()
    return reflected
  }

  reflectOverLine(line: Segment, options?: OptionsPointWithoutCoords): Point {
    const reflected = this.figure.create('Point', options)
    reflected.transformation = { line, origin: this, type: 'reflectOverLine' }
    this.subscribe(reflected)
    line.subscribe(reflected)
    reflected.isFree = false
    reflected.update()
    return reflected
  }

  rotate(center: Point, angle: number, options?: OptionsPointWithoutCoords): Point {
    const rotated = this.figure.create('Point', options)
    rotated.transformation = { angle, center, origin: this, type: 'rotation' }
    this.subscribe(rotated)
    center.subscribe(rotated)
    rotated.isFree = false
    rotated.update()
    return rotated
  }

  toJSON(): object {
    return this.jsonOptions()
  }

  translate(
    point1: Point,
    point2: Point,
    options?: OptionsPointWithoutCoords,
  ): Point {
    const translated = this.figure.create('Point', options)
    translated.transformation = {
      origin: this,
      point1,
      point2,
      type: 'translation',
    }
    this.subscribe(translated)
    point1.subscribe(translated)
    point2.subscribe(translated)
    translated.isFree = false
    translated.update()
    return translated
  }

  translateVector(vector: Vector, options: OptionsPointWithoutCoords): Point {
    const translated = this.figure.create('Point', options)
    translated.transformation = {
      origin: this,
      vector,
      type: 'translationVector',
    }
    this.subscribe(translated)
    vector.subscribe(translated)
    translated.isFree = false
    translated.update()
    return translated
  }

  update(): void {
    if (this.transformation?.type === 'reflect') {
      this._x =
        2 * this.transformation.center.x -
        (this.transformation.origin as Point).x
      this._y =
        2 * this.transformation.center.y -
        (this.transformation.origin as Point).y
    } else if (this.transformation?.type === 'dilate') {
      this._x =
        this.transformation.center.x +
        this.transformation.factor *
          ((this.transformation.origin as Point).x -
            this.transformation.center.x)
      this._y =
        this.transformation.center.y +
        this.transformation.factor *
          ((this.transformation.origin as Point).y -
            this.transformation.center.y)
    } else if (this.transformation?.type === 'rotation') {
      const { x, y } = rotationCoord(
        this.transformation.origin as Point,
        this.transformation.center,
        this.transformation.angle,
      )
      this._x = x
      this._y = y
    } else if (this.transformation?.type === 'reflectOverLine') {
      const { x, y } = reflectOverLineCoord(
        this.transformation.origin as Point,
        this.transformation.line,
      )
      this._x = x
      this._y = y
    } else if (this.transformation?.type === 'translation') {
      const { x, y } = translateCoords(
        this.transformation.origin as Point,
        this.transformation.point1,
        this.transformation.point2,
      )
      this._x = x
      this._y = y
    } else if (this.transformation?.type === 'translationVector') {
      const x = this.transformation.origin.x + this.transformation.vector.x
      const y = this.transformation.origin.y + this.transformation.vector.y
      this._x = x
      this._y = y
    }

    this.redraw()
  }

  get color(): string {
    return this._color
  }

  set color(color) {
    this._color = color
    if (this.svgCircle !== undefined) {
      this.svgCircle.setAttribute('fill', color)
      this.svgCircle.setAttribute('stroke', color)
    }
    if (this.svgLine1 !== undefined) {
      this.svgLine1.setAttribute('stroke', color)
    }
    if (this.svgLine2 !== undefined) {
      this.svgLine2.setAttribute('stroke', color)
    }
  }

  get colorLabel(): string {
    return this._colorLabel
  }

  set colorLabel(color: string) {
    this._colorLabel = color
    if (this.elementTextLabel !== undefined) this.elementTextLabel.color = color
  }

  get description(): string {
    if (this.transformation?.type === 'reflect') {
      return `Point $${this.label}$ image du point $${(this.transformation.origin as Point).notation}$ dans la symétrie de centre $${this.transformation?.center.notation}$`
    }
    if (this.transformation?.type === 'reflectOverLine') {
      return `Point $${this.label}$ image du point $${(this.transformation.origin as Point).notation}$ dans la symétrie d'axe $${this.transformation?.line.notation}$`
    }
    if (this.transformation?.type === 'dilate') {
      return `Point $${this.label}$ image du point $${(this.transformation.origin as Point).notation}$ dans l'homothétie de centre $${this.transformation?.center.notation}$ et de rapport $${this.transformation.factor}$`
    }
    if (this.transformation?.type === 'rotation') {
      return `Point $${this.label}$ image du point $${(this.transformation.origin as Point).notation}$ dans la rotation de centre $${this.transformation.center.notation}$ et d'angle $${Math.abs(this.transformation.angle)}\\degree$ dans le sens ${this.transformation.angle > 0 ? 'anti-horaire' : 'horaire'}`
    }
    if (this.transformation?.type === 'translation') {
      return `Point $${this.label}$ image du point $${(this.transformation.origin as Point).notation}$ dans la translation qui transforme $${(this.transformation.point1 as Point).notation}$ en $${(this.transformation.point2 as Point).notation}$`
    }
    if (this.transformation?.type === 'translationVector') {
      return `Point $${this.label}$ image
      du point $${(this.transformation.origin as Point).notation}$
      par la translation de vecteur $${this.transformation.vector.notation}$
      `
    }
    if (this.figure.options.pointDescriptionWithCoordinates) {
      return `Point $${this.label}$ libre de coordonnées $(${round(this.x, 2).toString().replace('.', ',')}\\;;\\;${round(this.y, 2).toString().replace('.', ',')})$`
    }
    return `Point $${this.label}$`
  }

  get isVisible(): boolean {
    return this._isVisible
  }

  set isVisible(isVisible) {
    this._isVisible = isVisible
    this.isVisible
      ? this.elementTextLabel?.show()
      : this.elementTextLabel?.hide()
    if (!this.isVisible) this.labelIsVisible = false
    super.isVisible = isVisible
  }

  get label(): string {
    if (this._label === undefined) return ''
    return this._label
  }

  set label(label: string | undefined) {
    this._label = label
    if (!this.isVisible || !this.labelIsVisible) {
      this.elementTextLabel?.hide()
      return
    }
    if (label !== undefined && label.length > 0) {
      if (this.elementTextLabel === undefined) {
        this.elementTextLabel = this.figure.create('TextByPoint', {
          color: this.colorLabel,
          dxInPixels: this.labelDxInPixels * this.figure.scale,
          dyInPixels: this.labelDyInPixels * this.figure.scale,
          id: `${this.id}_label`,
          isChild: true,
          isSelectable: true,
          point: this,
          text: `$${label}$`,
        })
      } else {
        this.elementTextLabel.text = `$${label}$`
        this.elementTextLabel.color = this._colorLabel
      }
    }
  }

  get latex(): string {
    if (this.shape === '') return ''
    let result = `% Point ${this.label}`
    // todo : Mgu la taille de la croix est fixe qqlesoit le scale, sinon elle peu être invisible si scale=0.5 => il faudrait une option
    result += `\n\\draw${this.latexOptions} plot[mark=${this.shape}, mark size=${0.15 / this.figure.scale}cm] coordinates{(${this.x},${this.y})};`
    return result
  }

  get notation(): string {
    if (this.label !== '') return this.label
    return this.id
  }

  get shape(): '' | '|' | 'o' | 'x' {
    return this._shape
  }

  set shape(shape) {
    this._shape = shape
    if (this._shape === 'x') {
      this.figure.svg.appendChild(this.groupSvg)
      this.svgCircle?.remove()
      this.groupSvg.appendChild(this.svgLine1)
      this.groupSvg.appendChild(this.svgLine2)
    } else if (this._shape === 'o') {
      this.figure.svg.appendChild(this.groupSvg)
      this.svgLine1?.remove()
      this.svgLine2?.remove()
      this.groupSvg.appendChild(this.svgCircle)
    } else if (this._shape === '') {
      this.groupSvg?.remove()
      this.svgLine1?.remove()
      this.svgLine2?.remove()
      this.svgCircle?.remove()
    }
    this.update()
  }

  get sizeInPixels(): number {
    return this._sizeInPixels
  }

  set sizeInPixels(size) {
    this._sizeInPixels = size
    this.update()
  }

  get x(): number {
    return this._x
  }

  set x(x) {
    if (this.figure.snapGrid && this.isVisible) {
      this._x = Math.round(x / this.figure.dx) * this.figure.dx
      if (!this.figure.options.gridWithTwoPointsOnSamePosition) {
        this.goToClosestFreePosition()
      }
    } else {
      this._x = x
    }
    this.update()
  }

  get y(): number {
    return this._y
  }

  set y(y) {
    if (this.figure.snapGrid && this.isVisible) {
      this._y = Math.round(y / this.figure.dy) * this.figure.dy
      if (!this.figure.options.gridWithTwoPointsOnSamePosition) {
        this.goToClosestFreePosition()
      }
    } else {
      this._y = y
    }
    this.update()
  }

  /**
     * 
     * MGu cette fonction est compliquée, et je trouve son comportement non intuitif...
     * Je l'ai modifié pour empêcher que les points sortent de la grille..
     * En revanche, si on tombe sur un point existant, c'est un peu au bonheur la chance...
     * et ça, c'est risque de bug...
     * 
     */
  goToClosestFreePosition(): void {
    const MAX_DISTANCE = 10
    const { points } = this.figure.checkCoords({ x: this._x, y: this._y })
    const grid = [...this.figure.elements.values()].find((ele)=> { return (ele instanceof Grid)})
    const inFigure = (this._x >= this.figure.xMin && this._x <= this.figure.xMax && this._y >= this.figure.yMin && this._y <= this.figure.yMax)
    /*  point in grid only*/
    const inGrid = grid!== undefined && (this._x <= grid.xMax && this._x >= grid.xMin) && (this._y <= grid.yMax && this._y >= grid.yMin)
    if (points.length === 1 && inFigure && (!grid || inGrid)) {
      // console.log('x,y=' + this._x + ':' + this._y)
      return
    }
    // console.log('point NOT available : x,y=' + this._x + ':' + this._y)
    for (let distance = 1; distance <= MAX_DISTANCE; distance++) {
      for (const dy of [ 0, distance, -distance ]) {
        for (const dx of [0, distance, -distance]) {
          // [5 3 4]
          // [2 x 1] Indice de recherche
          // [8 6 7]
          if (dx=== 0 && dy=== 0) {
            continue // point de départ 
          } 
          const newX = this._x + dx * this.figure.dx // il faut utiliser les pas de la figure
          const newY = this._y + dy * this.figure.dy // il faut utiliser les pas de la figure
          const pointNotInFigure = (newX < this.figure.xMin || newX > this.figure.xMax || newY < this.figure.yMin && newY > this.figure.yMax)
          if (pointNotInFigure) {
            // le point n'est pas dans la figure
            // console.log('point NOT in figure : x,y=' + newX + ':' + newY)
            continue
          }
          const pointNotInGrid = (grid!==undefined ) && (newX < grid.xMin || newX > grid.xMax || newY < grid.yMin && newY > grid.yMax)
          if (pointNotInGrid) {
            // le point n'est pas dans la grille
            // console.log('point NOT in Grid : x,y=' + newX + ':' + newY)
            continue
          }
          if (this.checkAndMove(dx * this.figure.dx, dy * this.figure.dy))
            // trouver un point disponible...
            // console.log('x,y=' + newX + ':' + newY)
            return
        }
      }
    }
  }


  private checkAndMove(dx: number, dy: number): boolean {
    const newX = this._x + dx
    const newY = this._y + dy
    const { points } = this.figure.checkCoords({ x: newX, y: newY })
    if (points.length === 0) {
      this._x = newX
      this._y = newY
      return true
    }
    return false
  }
}

export default Point
