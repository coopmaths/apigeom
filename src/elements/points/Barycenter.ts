import type Figure from '../../Figure'

import type { OptionsElement2D } from '../interfaces'
import Point from './Point'

class Barycenter extends Point {
  points: Point[]
  constructor(
    figure: Figure,
    { points, ...options }: { points: Point[] } & OptionsElement2D,
  ) {
    const x = points.reduce((acc, point) => acc + point.x, 0) / points.length
    const y = points.reduce((acc, point) => acc + point.y, 0) / points.length
    super(figure, { x, y, ...options })
    this.type = 'Barycenter'
    this.points = points
    for (const point of this.points) {
      point.subscribe(this)
    }
  }

  update(): void {
    const x =
      this.points.reduce((acc, point) => acc + point.x, 0) / this.points.length
    const y =
      this.points.reduce((acc, point) => acc + point.y, 0) / this.points.length
    this._x = x
    this._y = y
    super.update()
  }
}

export default Barycenter
