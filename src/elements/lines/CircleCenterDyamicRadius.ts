import type Figure from '../../Figure'
import type DynamicNumber from '../../dynamicNumbers/DynamicNumber'
import type Point from '../points/Point'

import type {
  OptionsCircleCenterDynamicRadius,
  OptionsElement2D,
} from '../interfaces'
import Circle from './Circle'

class CircleCenterDynamicRadius extends Circle {
  /** Centre du cercle */
  center: Point
  /** Rayon du cercle */
  readonly radiusDynamic: DynamicNumber
  constructor(
    figure: Figure,
    {
      center,
      radius,
      ...options
    }: OptionsCircleCenterDynamicRadius & OptionsElement2D,
  ) {
    super(figure, { center, radius: radius.value, ...options })
    this.type = 'CircleDynamicRadius'
    this.center = center
    this.radiusDynamic = radius
    this.radiusDynamic.subscribe(this)
    this.update()
    this.setVisibilityColorThicknessAndDashed()
  }

  toJSON(): object {
    return {
      ...this.jsonOptions(),
      fillColor: this.fillColor,
      fillOpacity: this.fillOpacity,
      idCenter: this.center.id,
      idRadius: this.radiusDynamic.id,
    }
  }

  update(): void {
    if (
      this.radiusDynamic?.value === undefined ||
      this.center.x === undefined ||
      this.center.y === undefined
    )
      return
    const xSvg = this.figure.xToSx(this.center.x)
    const ySvg = this.figure.yToSy(this.center.y)
    const rSvg =
      this.figure.pixelsPerUnit * this.radiusDynamic?.value * this.figure.scale
    this._radius = this.radiusDynamic.value
    this.groupSvg.setAttribute('cx', `${xSvg}`)
    this.groupSvg.setAttribute('cy', `${ySvg}`)
    if (Number.isFinite(rSvg)) this.groupSvg.setAttribute('r', `${rSvg}`)
    this.notify()
  }
}

export default CircleCenterDynamicRadius
