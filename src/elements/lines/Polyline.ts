import type Figure from '../../Figure'
import type Point from '../points/Point'

import Element2D from '../Element2D'
import type { OptionsElement2D, OptionsPolyline } from '../interfaces'

/**
 * Trace une ligne brisée
 */
class Polyline extends Element2D {
  /** Liste des sommets */
  points: Point[]
  /** Détermine s'il faut afficher les deux extrémités, que celle de gauche ou que celle de droite */
  shape?: '' | '|-' | '|-' | '|-|'
  constructor(
    figure: Figure,
    { points, ...options }: OptionsPolyline & OptionsElement2D,
  ) {
    super(figure, options)
    this.type = 'Polyline'
    this.points = points
    for (const point of points) {
      point.subscribe(this)
    }
  }

  draw(): void {
    this.groupSvg = document.createElementNS(
      'http://www.w3.org/2000/svg',
      'polyline',
    )
    this.groupSvg.setAttribute('fill', 'none')
    this.setVisibilityColorThicknessAndDashed()
    this.update()
  }

  toJSON(): object {
    const idPoints = []
    for (const point of this.points) {
      idPoints.push(point.id)
    }
    return {
      ...this.jsonOptions(),
      idPoints,
    }
  }

  update(): void {
    this.notify()
    if (!allCoordsAreNumber(this.points)) {
      this.groupSvg.removeAttribute('points')
    } else {
      let pointsCoords = ''
      for (const point of this.points) {
        pointsCoords += `${this.figure.xToSx(point.x).toString()},${this.figure.yToSy(point.y).toString()} `
      }
      this.groupSvg.setAttribute('points', pointsCoords)
    }
  }
}

export default Polyline

function allCoordsAreNumber(points: Point[]): boolean {
  for (const point of points) {
    if (!Number.isFinite(point.x) || !Number.isFinite(point.y)) return false
  }
  return true
}
