import type Figure from '../Figure'

import DynamicNumber from './DynamicNumber'

class Const extends DynamicNumber {
  constructor(
    figure: Figure,
    { isChild, value }: { isChild?: boolean; value: number },
  ) {
    super(figure, { isChild })
    this.type = 'Const'
    this.value = value
  }

  toJSON(): object {
    return {
      id: this.id,
      isChild: this.isChild,
      type: this.type,
      value: this.value,
    }
  }
}

export default Const
