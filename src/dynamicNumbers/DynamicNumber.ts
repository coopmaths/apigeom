import type Figure from '../Figure'
import type Element2D from '../elements/Element2D'
import type TextDynamicByPosition from '../elements/text/TextDynamicByPosition'

import type { OptionsDynamicNumber } from '../elements/interfaces'

/**
 * Pour les valeurs numériques variables qui seront les « parents » de constructions
 */
abstract class DynamicNumber {
  private readonly _label?: string
  protected _value: number
  figure: Figure
  readonly id: string
  isChild: boolean
  observers: Array<DynamicNumber | Element2D>
  textAfter: string
  textBefore: string
  type: string
  constructor(
    figure: Figure,
    { isChild = false, textAfter = '', textBefore = '' }: OptionsDynamicNumber,
  ) {
    this.figure = figure
    this.isChild = isChild
    if (this.isChild) {
      let cpt = 0
      while (this.figure.elements.has(`elementTmp${cpt.toString()}`)) {
        cpt++
      }
      this.id = `elementTmp${cpt.toString()}`
    } else {
      let cpt = 0
      while (this.figure.elements.has(`element${cpt.toString()}`)) {
        cpt++
      }
      this.id = `element${cpt.toString()}`
    }
    this.figure.elements.set(this.id, this)
    this.observers = []
    this._value = Number.NaN
    this.type = ''
    this.textBefore = textBefore
    this.textAfter = textAfter
  }

  display({
    color,
    maximumFractionDigits,
    minimumFractionDigits,
    textAfter = '',
    textBefore = '',
    x,
    y,
  }: {
    color?: string
    maximumFractionDigits?: number
    minimumFractionDigits?: number
    textAfter?: string
    textBefore?: string
    x: number
    y: number
  }): TextDynamicByPosition {
    return this.figure.create('TextDynamicByPosition', {
      color,
      dynamicNumber: this,
      id: `${this.id}_display`,
      maximumFractionDigits,
      minimumFractionDigits,
      textAfter,
      textBefore,
      x,
      y,
    })
  }

  draw(): void {}

  /** Prévenir les enfants qu'ils doivent se mettre à jour */
  notify(): void {
    for (const element of this.observers) {
      element.update()
    }
  }

  remove(): void {
    this.figure.elements.delete(this.id)
    for (const element of this.observers) {
      element.remove()
    }
  }

  /** S'abonner aux modifications des éléments parents
   * Par exemple le segment s'abonnera aux modifications de ses deux extrémités
   */
  subscribe(element: DynamicNumber | Element2D): void {
    this.observers.push(element)
  }

  /** Personnalise la sortie JSON de l'élément pour la sauvegarde */
  toJSON(): object {
    return {
      id: this.id,
      isChild: this.isChild,
      type: this.type,
    }
  }

  /**
   * Annuler l'abonnement
   */
  unsubscribe(element: Element2D): void {
    this.observers = this.observers.filter((observer) => observer !== element)
  }

  update(): void {
    this.notify()
  }

  get label(): string | undefined {
    return this._label
  }

  get string(): string {
    return this.textBefore + this.value.toString() + this.textAfter
  }

  set value(x: number) {
    this._value = x
    this.notify()
  }

  get value(): number {
    return this._value
  }
}

export default DynamicNumber
