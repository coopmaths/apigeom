import type Figure from '../Figure'
import type { Coords } from '../elements/calculus/Coords'
import type Circle from '../elements/lines/Circle'
import { round } from '../lib/format'

export default function checkCircle({
  center,
  figure,
  labelCenter,
  radius,
}: {
  center: Coords
  figure: Figure
  labelCenter?: string
  radius: number
}) {
  const allCircles = [...figure.elements.values()].filter((e) =>
    e.type.includes('Circle'),
  ) as Circle[]
  let circlesWithGoodLabelCenter: Circle[] = []
  if (labelCenter) {
    circlesWithGoodLabelCenter = allCircles.filter(
      (c) => c.center.label === labelCenter,
    )
  }
  if (allCircles.length === 0) {
    return {
      isValid: false,
      message: `Il n'y a pas de cercle dans la figure.`,
    }
  }
  const goodCircle = allCircles.find(
    (c) =>
      round(c.center.x) === round(center.x) &&
      round(c.center.y) === round(center.y) &&
      round(c.radius) === round(radius),
  )
  if (!goodCircle) {
    return {
      isValid: false,
      message: `Le cercle de centre $(${round(center.x, 2)}\\;;\\;${round(center.y, 2)})$ et de rayon ${round(radius, 1)} n'existe pas dans la figure.`,
    }
  }
  if (labelCenter) {
    const goodCircleWithGoodLabelCenter = circlesWithGoodLabelCenter.find(
      (c) =>
        round(c.center.x) === round(center.x) &&
        round(c.center.y) === round(center.y) &&
        round(c.radius) === round(radius),
    )
    if (!goodCircleWithGoodLabelCenter) {
      return {
        isValid: false,
        message: `Le cercle de centre $(${round(center.x, 2)}\\;;\\;${round(center.y, 2)})$ et de rayon ${round(radius, 2)} est bien tracé mais son centre n\'est pas ${labelCenter}.`,
        element: goodCircle,
      }
    }
    return {
      isValid: true,
      message: '',
      element: goodCircleWithGoodLabelCenter,
    }
  }

  return {
    isValid: true,
    message: '',
    element: goodCircle,
  }
}
