import type Figure from '../Figure'
import { type Coords, equationLine } from '../elements/calculus/Coords'
import type Segment from '../elements/lines/Segment'

import { round } from '../lib/format'

export default function checkLine({
  figure,
  point1,
  point2,
  color,
}: { figure: Figure; point1: Coords; point2: Coords; color?: string }): {
  isValid: boolean
  message: string
} {
  let lines: Segment[]
  let lines2: Segment[]

  lines = [...figure.elements.values()].filter((e) =>
    e.type.includes('Line'),
  ) as Segment[]
  if (lines.length === 0)
    return { isValid: false, message: "Il n'y a pas de droite dans la figure." }

  if (color !== undefined) {
    lines2 = lines.filter((e) => e.color === color)
    if (lines2.length === 0)
      return {
        isValid: false,
        message:
          "Il n'y a pas de droite de la couleur demandée dans la figure.",
      }

    const [aa, bb, cc] = equationLine(point1, point2)
    for (const line of lines2) {
      const [a2, b2, c2] = line.equation
      if (areCoincident(aa, bb, cc, a2, b2, c2)) {
        return { isValid: true, message: 'La droite a bien été tracée.' }
      }
    }
  }

  const [a, b, c] = equationLine(point1, point2)
  for (const line of lines) {
    const [a2, b2, c2] = line.equation
    if (areCoincident(a, b, c, a2, b2, c2)) {
      if (color === undefined)
        return { isValid: true, message: 'La droite a bien été tracée.' }
      return {
        isValid: false,
        message: "La droite n'est pas tracée de la bonne couleur.",
      }
    }
  }

  return {
    isValid: false,
    message: 'Aucune droite ne correspond à la réponse attendue.',
  }
}

function areCoincident(
  a1: number,
  b1: number,
  c1: number,
  a2: number,
  b2: number,
  c2: number,
  precision = 2,
): boolean {
  // Cas des droites verticales
  if (b1 === 0 && b2 === 0) {
    return c1 / a1 === c2 / a2
  }
  const slope1 = b1 !== 0 ? -a1 / b1 : Number.POSITIVE_INFINITY
  const slope2 = b2 !== 0 ? -a2 / b2 : Number.POSITIVE_INFINITY

  const intercept1 = b1 !== 0 ? -c1 / b1 : Number.NaN
  const intercept2 = b2 !== 0 ? -c2 / b2 : Number.NaN

  return (
    round(slope1, precision) === round(slope2, precision) &&
    round(intercept1, precision) === round(intercept2, precision)
  )
}
