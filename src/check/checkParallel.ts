import type Figure from '../Figure'
import type Point from '../elements/points/Point'

import { round } from '../lib/format'
import syncShake from './syncShake'

/** Les labels sont de la forme AB avec A et B les labels de deux points de la droite */
export default function checkParallel({
  figure,
  label1,
  label2,
  time = 0,
}: { figure: Figure; label1: string; label2: string; time?: number }): {
  isValid: boolean
  message: string
} {
  const [a, b] = label1.split('')
  const [c, d] = label2.split('')
  const points = [...figure.elements.values()].filter(
    (e) => e.type !== 'pointer' && e.type.includes('Point'),
  ) as Point[]
  const matchPointsA = points.filter((p) => p.label === a)
  if (matchPointsA.length === 0) {
    return {
      isValid: false,
      message: `Il n'y a pas de point $${a}$ dans la figure.`,
    }
  }
  if (matchPointsA.length > 1) {
    return {
      isValid: false,
      message: `Il y a ${matchPointsA.length} points $${a}$ dans la figure.`,
    }
  }
  const matchPointsB = points.filter((p) => p.label === b)
  if (matchPointsB.length === 0) {
    return {
      isValid: false,
      message: `Il n'y a pas de point $${b}$ dans la figure.`,
    }
  }
  if (matchPointsB.length > 1) {
    return {
      isValid: false,
      message: `Il y a ${matchPointsB.length} points $${b}$ dans la figure.`,
    }
  }
  const matchPointsC = points.filter((p) => p.label === c)
  if (matchPointsC.length === 0) {
    return {
      isValid: false,
      message: `Il n'y a pas de point $${c}$ dans la figure.`,
    }
  }
  if (matchPointsC.length > 1) {
    return {
      isValid: false,
      message: `Il y a ${matchPointsC.length} points $${c}$ dans la figure.`,
    }
  }
  const matchPointsD = points.filter((p) => p.label === d)
  if (matchPointsD.length === 0) {
    return {
      isValid: false,
      message: `Il n'y a pas de point $${d}$ dans la figure.`,
    }
  }
  if (matchPointsD.length > 1) {
    return {
      isValid: false,
      message: `Il y a ${matchPointsD.length} points $${d}$ dans la figure.`,
    }
  }
  const pointA = matchPointsA[0]
  const pointB = matchPointsB[0]
  const pointC = matchPointsC[0]
  const pointD = matchPointsD[0]
  let isValid =
    round((pointB.y - pointA.y) / (pointB.x - pointA.x), 2) ===
    round((pointD.y - pointC.y) / (pointD.x - pointC.x), 2)
  syncShake({ figure, time })
  isValid =
    isValid &&
    round((pointB.y - pointA.y) / (pointB.x - pointA.x), 2) ===
      round((pointD.y - pointC.y) / (pointD.x - pointC.x), 2)
  syncShake({ figure, time })
  isValid =
    isValid &&
    round((pointB.y - pointA.y) / (pointB.x - pointA.x), 2) ===
      round((pointD.y - pointC.y) / (pointD.x - pointC.x), 2)
  const message = isValid
    ? `Les droites $(${a}${b})$ et $(${c}${d})$ sont bien toujours parallèles.`
    : `Les droites $(${a}${b})$ et $(${c}${d})$ ne sont pas toujours parallèles.`
  return { isValid, message }
}
