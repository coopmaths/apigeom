import type Figure from '../Figure'
import type Point from '../elements/points/Point'

import { distance } from '../elements/calculus/Coords'
import { round } from '../lib/format'
import syncShake from './syncShake'

export default function checkSameDistance({
  figure,
  label1,
  label2,
  time = 0,
}: { figure: Figure; label1: string; label2: string; time?: number }): {
  isValid: boolean
  message: string
} {
  const [a, b] = label1.split('')
  const [c, d] = label2.split('')
  const points = [...figure.elements.values()].filter(
    (e) => e.type !== 'pointer' && e.type.includes('Point'),
  ) as Point[]
  const matchPointsA = points.filter((p) => p.label === a)
  if (matchPointsA.length === 0) {
    return {
      isValid: false,
      message: `Il n'y a pas de point $${a}$ dans la figure.`,
    }
  }
  if (matchPointsA.length > 1) {
    return {
      isValid: false,
      message: `Il y a ${matchPointsA.length} points $${a}$ dans la figure.`,
    }
  }
  const matchPointsB = points.filter((p) => p.label === b)
  if (matchPointsB.length === 0) {
    return {
      isValid: false,
      message: `Il n'y a pas de point $${b}$ dans la figure.`,
    }
  }
  if (matchPointsB.length > 1) {
    return {
      isValid: false,
      message: `Il y a ${matchPointsB.length} points $${b}$ dans la figure.`,
    }
  }
  const matchPointsC = points.filter((p) => p.label === c)
  if (matchPointsC.length === 0) {
    return {
      isValid: false,
      message: `Il n'y a pas de point $${c}$ dans la figure.`,
    }
  }
  if (matchPointsC.length > 1) {
    return {
      isValid: false,
      message: `Il y a ${matchPointsC.length} points $${c}$ dans la figure.`,
    }
  }
  const matchPointsD = points.filter((p) => p.label === d)
  if (matchPointsD.length === 0) {
    return {
      isValid: false,
      message: `Il n'y a pas de point $${d}$ dans la figure.`,
    }
  }
  if (matchPointsD.length > 1) {
    return {
      isValid: false,
      message: `Il y a ${matchPointsD.length} points $${d}$ dans la figure.`,
    }
  }
  const pointA = matchPointsA[0]
  const pointB = matchPointsB[0]
  const pointC = matchPointsC[0]
  const pointD = matchPointsD[0]
  let isValid =
    round(distance(pointA, pointB), 2) === round(distance(pointC, pointD), 2)
  syncShake({ figure, time })
  isValid =
    isValid &&
    round(distance(pointA, pointB), 2) === round(distance(pointC, pointD), 2)
  syncShake({ figure, time })
  isValid =
    isValid &&
    round(distance(pointA, pointB), 2) === round(distance(pointC, pointD), 2)
  syncShake({ figure, time })
  isValid =
    isValid &&
    round(distance(pointA, pointB), 2) === round(distance(pointC, pointD), 2)
  const message = `Les longueurs $${a}${b}$ et $${c}${d}$ sont bien toujours égales.`
  return { isValid, message }
}
