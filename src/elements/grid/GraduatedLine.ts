import type Figure from '../../Figure'
import type TextByPosition from '../text/TextByPosition'

import { displayNumber, round } from '../../lib/format'
import Element2D from '../Element2D'

class GraduatedLine extends Element2D {
  private _labelSize = '0.8em'
  private readonly addInPixels = 5
  /** La droite dépasse du min de addLeft */
  addLeft: number
  /** La droite dépasse du max de addRight */
  addRight: number
  /** Distance entre 2 labels à partir de 0 ou du min */
  labelInterval: number
  labels: TextByPosition[] = []
  max: number
  min: number
  points: Array<{ label: string; x: number }> = []
  /** Distance entre deux graduations */
  step: number
  /** Distance entre deux graduations secondaires */
  stepBis?: number
  svgLine!: SVGLineElement
  /** Épaisseur des graduations */
  thick: number
  /** Ordonnée des labels en pixels */
  yLabel: number

  constructor(
    figure: Figure,
    {
      addLeft = 0,
      addRight = 0,
      color,
      labelInterval,
      max = 6,
      min = -6,
      step = 1,
      stepBis,
      thick = 0.2,
      yLabel = -0.5,
    }: {
      addLeft?: number
      addRight?: number
      color?: string
      labelInterval?: number
      max?: number
      min?: number
      scale?: number
      step?: number
      stepBis?: number
      thick?: number
      yLabel?: number
    } = {},
  ) {
    super(figure, {})
    this.addLeft = addLeft
    this.addRight = addRight
    this._color = color ?? figure.options.color
    this.labelInterval = labelInterval ?? step
    this.min = min
    this.max = max
    this.step = step
    this.stepBis = stepBis
    this.thick = thick
    this.yLabel = yLabel
    this.type = 'GraduatedLine'
  }

  draw(): void {
    this.svgLine = document.createElementNS(
      'http://www.w3.org/2000/svg',
      'line',
    )
    this.groupSvg.appendChild(this.svgLine)
    this.groupSvg.setAttribute('color', this.color)
    this.setVisibilityColorThicknessAndDashed()
    this.drawLine()
    this.drawThicks()
    this.drawThicksBis()
    this.drawLabels()
  }

  drawLabels(): void {
    const start = round(
      Math.ceil(this.min / this.labelInterval) * this.labelInterval,
    )
    const end = round(
      Math.floor(this.max / this.labelInterval) * this.labelInterval,
    )
    for (let i = start; i <= end; ) {
      const text = `$${displayNumber(i)}$`
      this.labels.push(
        this.figure.create('TextByPosition', {
          anchor: 'topCenter',
          dxInPixels: 0,
          dyInPixels: 0,
          isChild: true,
          text,
          x: i,
          y: this.yLabel,
        }),
      )
      i = round(i + this.labelInterval)
    }
  }

  drawLine(): void {
    const x1Svg = this.figure.xToSx(this.min - this.addLeft)
    const x2Svg = this.figure.xToSx(this.max + this.addRight)
    const y1Svg = this.figure.yToSy(0)
    const y2Svg = this.figure.yToSy(0)
    this.svgLine.setAttribute('x1', `${x1Svg - this.addInPixels}`)
    this.svgLine.setAttribute('y1', `${y1Svg}`)
    this.svgLine.setAttribute('x2', `${x2Svg + this.addInPixels}`)
    this.svgLine.setAttribute('y2', `${y2Svg}`)
    this.svgLine.setAttribute('stroke', 'currentColor')

    // Définit la flèche
    const defs = document.createElementNS('http://www.w3.org/2000/svg', 'defs')
    this.figure.svg.appendChild(defs)
    const marker = document.createElementNS(
      'http://www.w3.org/2000/svg',
      'marker',
    )
    marker.setAttribute('id', 'arrow')
    marker.setAttribute('markerWidth', '10')
    marker.setAttribute('markerHeight', '10')
    marker.setAttribute('refX', '0')
    marker.setAttribute('refY', '3')
    marker.setAttribute('orient', 'auto')
    marker.setAttribute('markerUnits', 'strokeWidth')
    const path = document.createElementNS('http://www.w3.org/2000/svg', 'path')
    path.setAttribute('d', 'M0,0 L0,6 L9,3 z')
    path.setAttribute('fill', this.color)
    marker.appendChild(path)
    defs.appendChild(marker)

    // Ajoute la flèche comme une mark
    this.svgLine.setAttribute('marker-end', 'url(#arrow)')
  }

  drawThicks(): void {
    const start = round(Math.ceil(this.min / this.step) * this.step)
    const end = round(Math.floor(this.max / this.step) * this.step)
    for (let i = start; i <= end; ) {
      const xSvg = this.figure.xToSx(i)
      const y1Svg = this.figure.yToSy(-this.thick)
      const y2Svg = this.figure.yToSy(this.thick)
      const svgLine = document.createElementNS(
        'http://www.w3.org/2000/svg',
        'line',
      )
      svgLine.setAttribute('x1', `${xSvg}`)
      svgLine.setAttribute('y1', `${y1Svg}`)
      svgLine.setAttribute('x2', `${xSvg}`)
      svgLine.setAttribute('y2', `${y2Svg}`)
      svgLine.setAttribute('stroke', 'currentColor')
      svgLine.setAttribute('stroke-width', '1')
      this.groupSvg.appendChild(svgLine)
      i = round(i + this.step)
    }
  }

  drawThicksBis(): void {
    if (this.stepBis === undefined) return
    const start = round(Math.ceil(this.min / this.stepBis) * this.stepBis)
    const end = round(Math.floor(this.max / this.stepBis) * this.stepBis)
    let cpt = 0
    // Mille graduations aux maximum
    for (let i = start; i <= end && cpt < 1000; ) {
      const xSvg = this.figure.xToSx(i)
      const y1Svg = this.figure.yToSy(-this.thick / 2)
      const y2Svg = this.figure.yToSy(this.thick / 2)
      const svgLine = document.createElementNS(
        'http://www.w3.org/2000/svg',
        'line',
      )
      svgLine.setAttribute('x1', `${xSvg}`)
      svgLine.setAttribute('y1', `${y1Svg}`)
      svgLine.setAttribute('x2', `${xSvg}`)
      svgLine.setAttribute('y2', `${y2Svg}`)
      svgLine.setAttribute('stroke', 'currentColor')
      svgLine.setAttribute('stroke-width', '1')
      this.groupSvg.appendChild(svgLine)
      i = round(i + this.stepBis)
      cpt++
    }
  }

  toJSON(): object {
    return {
      ...super.toJSON(),
      type: this.type,
      color: this.color,
      addLeft: this.addLeft,
      addRight: this.addRight,
      labelInterval: this.labelInterval,
      max: this.max,
      min: this.min,
      step: this.step,
      stepBis: this.stepBis,
      thick: this.thick,
      yLabel: this.yLabel,
    }
  }

  get labelSize(): string {
    return this._labelSize
  }

  set labelSize(labelSize: string) {
    this._labelSize = labelSize
    for (const label of this.labels) {
      label.fontSize = labelSize
    }
  }

  get latex(): string {
    let result = `\n\\tkzInit[xmin=${this.min}, xmax=${this.max}, xstep=${this.step}]`
    result += '\n\\tkzDrawX[label={}, tickup=3pt, tickdn=3pt]'
    if (this.stepBis !== undefined) {
      result += `\\foreach \\i in {${this.min},${round(this.min + this.stepBis, 6)},...,${this.max}} {`
      result += '\n\\tkzHTick[mark=|,mark size=2pt]{\\i}\n}'
    }
    return result
  }
}

export default GraduatedLine
