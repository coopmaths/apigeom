import type Figure from '../../Figure'
import type Polygon from '../lines/Polygon'
import type Point from '../points/Point'

import Element2D from '../Element2D'

export default class LineFractionDiagram extends Element2D {
  denominator: number
  height: number
  _indicesRectanglesInColor: Set<number> = new Set()
  min: number
  max: number
  origin: Point
  origins: Point[] = []
  rectangles: Polygon[] = []
  thickHeightInPixels: number
  thickThickness: number
  verticalDistanceForClick: number
  width: number
  constructor(
    figure: Figure,
    {
      denominator = 4,
      height = 0.2,
      indicesRectanglesInColor = new Set(),
      min = 0,
      max = 5,
      numerator,
      thickHeightInPixels = 10,
      thickThickness = 4,
      verticalDistanceForClick = 1,
      width = 4,
      x = 0,
      y = 0,
      ...options
    }: {
      denominator?: number
      height?: number
      indicesRectanglesInColor?: Set<number>
      min?: number
      max?: number
      numerator?: number
      thickHeightInPixels?: number
      thickThickness?: number
      verticalDistanceForClick?: number
      width?: number
      x?: number
      y?: number
    } = {},
  ) {
    super(figure, { ...options })
    this.origin = this.figure.create('Point', {
      isChild: true,
      isFree: false,
      shape: '',
      x,
      y: y - height / 2,
    })
    this.width = width
    this.height = height
    this.min = min
    this.denominator = denominator
    this.max = max
    this.thickHeightInPixels = thickHeightInPixels
    this.thickThickness = thickThickness
    if (numerator !== undefined && indicesRectanglesInColor.size === 0) {
      for (let i = 0; i < numerator; i++) {
        this.indicesRectanglesInColor.add(i)
      }
    } else {
      this.indicesRectanglesInColor = indicesRectanglesInColor
    }
    this.type = 'LineFractionDiagram'
    this.verticalDistanceForClick = verticalDistanceForClick
  }

  draw(): void {
    this.figure.svg.appendChild(this.groupSvg)
    this.origins = [this.origin]
    this.rectangles = []
    const fractionWidth = this.width / this.denominator
    for (let n = 0; n < this.max; n++) {
      if (n > 0)
        this.origins.push(
          this.figure.create('Point', {
            isChild: true,
            isSelectable: false,
            shape: '',
            x: this.origin.x + n * this.width,
            y: this.origin.y,
          }),
        )
      for (let i = 0; i < this.denominator; i++) {
        this.rectangles.push(
          createRectangleByOriginWidthAndHeight({
            height: this.height,
            origin: this.origins[n],
            width: fractionWidth,
            xTranslate: i * fractionWidth,
            color: this.color,
            thickness: this.thickness,
            isDashed: this.isDashed,
          }),
        )
      }
    }
    const verticalDistanceForClick = this.verticalDistanceForClick
    for (const rectangle of this.rectangles) {
      rectangle.distancePointer = function (_: number, __: number): number {
        if (
          this.figure.currentState === 'FILL' &&
          this.figure.pointer.x >= this.points[0].x &&
          this.figure.pointer.x <= this.points[1].x &&
          this.figure.pointer.y - this.points[0].y < verticalDistanceForClick &&
          this.figure.pointer.y - this.points[0].y > -verticalDistanceForClick
        )
          return 0
        return Number.POSITIVE_INFINITY
      }
    }
    // Thicks
    for (let i = 0; i <= this.max; i++) {
      const x = this.origin.x + i * this.width
      const svgLine = document.createElementNS(
        'http://www.w3.org/2000/svg',
        'line',
      )
      svgLine.setAttribute('x1', this.figure.xToSx(x).toString())
      svgLine.setAttribute(
        'y1',
        (
          this.figure.yToSy(this.origin.y + this.height / 2) -
          this.thickHeightInPixels * this.figure.yScale * this.figure.scale
        ).toString(),
      )
      svgLine.setAttribute('x2', this.figure.xToSx(x).toString())
      svgLine.setAttribute(
        'y2',
        (
          this.figure.yToSy(this.origin.y + this.height / 2) +
          this.thickHeightInPixels * this.figure.yScale * this.figure.scale
        ).toString(),
      )
      svgLine.setAttribute('stroke', this._color)
      svgLine.setAttribute('stroke-width', this.thickThickness.toString())
      svgLine.setAttribute('stroke-readonly', '1')
      this.groupSvg.appendChild(svgLine)
      this.figure.create('TextByPosition', {
        isChild: true,
        x,
        y:
          this.origin.y +
          1 +
          this.thickHeightInPixels /
            (this.figure.yScale *
              this.figure.scale *
              this.figure.pixelsPerUnit),
        text: (this.min + i).toString(),
      })
    }
    this.indicesRectanglesInColor = this._indicesRectanglesInColor
  }

  redraw(): void {
    for (const origin of this.origins) {
      origin.remove()
    }
    for (const rectangle of this.rectangles) {
      rectangle.remove()
    }
    this.draw()
  }

  toJSON(): object {
    return {
      ...this.jsonOptions(),
      denominator: this.denominator,
      height: this.height,
      arrayIndicesRectanglesInColor: Array.from(this.indicesRectanglesInColor),
      min: this.min,
      max: this.max,
      thickHeightInPixels: this.thickHeightInPixels,
      thickThickness: this.thickThickness,
      verticalDistanceForClick: this.verticalDistanceForClick,
      width: this.width,
      x: this.origin.x,
      y: this.origin.y + this.height / 2,
    }
  }

  get numerator(): number {
    return this.rectangles.filter((rectangle) => rectangle.fillColor !== 'none')
      .length
  }

  get indicesRectanglesInColor(): Set<number> {
    for (let i = 0; i < this.rectangles.length; i++) {
      if (this.rectangles[i].fillColor !== 'none') {
        this._indicesRectanglesInColor.add(i)
      } else {
        this._indicesRectanglesInColor.delete(i)
      }
    }
    return this._indicesRectanglesInColor
  }

  set indicesRectanglesInColor(indicesRectanglesInColor: Set<number>) {
    this._indicesRectanglesInColor = indicesRectanglesInColor
    this.rectangles.forEach((rectangle, index) => {
      if (this._indicesRectanglesInColor.has(index)) {
        rectangle.fillColor = this.color
      } else {
        rectangle.fillColor = 'none'
      }
    })
  }

  get indiceLastInColor(): number {
    const indice = this.rectangles.findIndex(
      (rectangle) => rectangle.fillColor === 'none',
    )
    return indice === -1 ? this.max * this.denominator : indice
  }

  set numerator(numerator: number) {
    this.rectangles.forEach((rectangle, index) => {
      if (index < numerator) {
        rectangle.fillColor = this.color
      } else {
        rectangle.fillColor = 'none'
      }
    })
  }
}

function createRectangleByOriginWidthAndHeight({
  height,
  origin,
  width,
  xTranslate,
  color,
  thickness,
  isDashed,
}: {
  color?: string
  thickness?: number
  isDashed?: boolean
  height: number
  origin: Point
  width: number
  xTranslate: number
}): Polygon {
  const newPoints = []
  const figure = origin.figure
  newPoints.push(
    figure.create('Point', {
      isChild: true,
      isFree: false,
      shape: '',
      x: origin.x + xTranslate,
      y: origin.y,
    }),
  )
  newPoints.push(
    figure.create('Point', {
      isChild: true,
      isFree: false,
      shape: '',
      x: origin.x + xTranslate + width,
      y: origin.y,
    }),
  )
  newPoints.push(
    figure.create('Point', {
      isChild: true,
      isFree: false,
      shape: '',
      x: origin.x + xTranslate + width,
      y: origin.y + height,
    }),
  )
  newPoints.push(
    figure.create('Point', {
      isChild: true,
      isFree: false,
      shape: '',
      x: origin.x + xTranslate,
      y: origin.y + height,
    }),
  )
  return figure.create('Polygon', {
    isChild: true,
    points: newPoints,
    color,
    thickness,
    isDashed,
  })
}
