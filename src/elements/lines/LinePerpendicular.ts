import type Figure from '../../Figure'
import type Point from '../points/Point'

import {
  type Coords,
  orthogonalProjectionCoord,
  rotationCoord,
} from '../calculus/Coords'
import type { OptionsElement2D, OptionsLinePerpendicular } from '../interfaces'
import Line from './Line'

class LinePerpendicular extends Line {
  /** Droite perpendiculaire qui la définit */
  line: Line
  /** Point par lequel passe cette droite */
  point: Point
  constructor(
    figure: Figure,
    { line, point, ...options }: OptionsLinePerpendicular & OptionsElement2D,
  ) {
    const x1 = point.x
    const y1 = point.y
    let result: Coords
    if (x1 === line.point1.x && y1 === line.point1.y) {
      result = rotationCoord(line.point2, line.point1, 90)
    } else if (x1 === line.point2.x && y1 === line.point2.y) {
      result = rotationCoord(line.point1, point, 90)
    } else if (point.isOnline(line)) {
      result = rotationCoord(line.point1, point, 90)
    } else {
      result = orthogonalProjectionCoord(point, line)
    }
    const x2 = result.x
    const y2 = result.y
    const point1 = figure.create('Point', {
      isChild: true,
      isFree: false,
      isVisible: false,
      x: x1,
      y: y1,
    })
    const point2 = figure.create('Point', {
      isChild: true,
      isFree: false,
      isVisible: false,
      x: x2,
      y: y2,
    })
    super(figure, { point1, point2, ...options })
    this.type = 'LinePerpendicular'
    this.line = line
    this.point = point
    this.line.subscribe(this)
    this.point.subscribe(this)
    this.point1.unsubscribe(this)
    this.point2.unsubscribe(this)
  }

  toJSON(): object {
    return {
      ...this.jsonOptions(),
      idLine: this.line.id,
      idPoint: this.point.id,
    }
  }

  update(): void {
    const x1 = this.point.x
    const y1 = this.point.y
    let result: Coords
    if (x1 === this.line.point1.x && y1 === this.line.point1.y) {
      result = rotationCoord(this.line.point2, this.line.point1, 90)
    } else if (x1 === this.line.point2.x && y1 === this.line.point2.y) {
      result = rotationCoord(this.line.point1, this.point, 90)
    } else if (this.point.isOnline(this.line)) {
      result = rotationCoord(this.line.point1, this.point, 90)
    } else {
      result = orthogonalProjectionCoord(this.point, this.line)
    }
    const x2 = result.x
    const y2 = result.y
    this.point1.moveTo(x1, y1)
    this.point2.moveTo(x2, y2)
    super.update()
  }

  get description(): string {
    const pointName = this.point.label !== '' ? this.point.label : this.point.id
    if (
      this.line.point1.label !== '' &&
      this.line.point2.label !== '' &&
      this.point.label !== ''
    ) {
      return `Droite perpendiculaire à $${this.line.notation}$ et passant par le point $${pointName}$`
    }
    const perpendicularLines = [...this.figure.elements.values()].filter(
      (element) => element instanceof LinePerpendicular,
    ) as LinePerpendicular[]
    const index = perpendicularLines.indexOf(this)
    return `Droite perpendiculaire n°${index + 1}`
  }
}

export default LinePerpendicular
