import type Figure from '../../Figure'
import type Point from '../points/Point'

import { parallelCoords } from '../calculus/Coords'
import type { OptionsElement2D, OptionsLineParallel } from '../interfaces'
import Line from './Line'

class LineParallel extends Line {
  /** Droite parallèle qui la définit */
  line: Line
  /** Point par lequel passe cette droite */
  point: Point
  constructor(
    figure: Figure,
    { line, point, ...options }: OptionsLineParallel & OptionsElement2D,
  ) {
    const { x1, y1, x2, y2 } = parallelCoords({ line, point, figure })
    const point1 = figure.create('Point', {
      isChild: true,
      isFree: false,
      isVisible: false,
      x: x1,
      y: y1,
    })
    const point2 = figure.create('Point', {
      isChild: true,
      isFree: false,
      isVisible: false,
      x: x2,
      y: y2,
    })
    super(figure, { point1, point2, ...options })
    this.type = 'LineParallel'
    this.line = line
    this.point = point
    this.line.subscribe(this)
    this.point.subscribe(this)
    this.point1.unsubscribe(this)
    this.point2.unsubscribe(this)
  }

  toJSON(): object {
    return {
      ...this.jsonOptions(),
      idLine: this.line.id,
      idPoint: this.point.id,
    }
  }

  update(): void {
    const { x1, y1, x2, y2 } = parallelCoords({
      line: this.line,
      point: this.point,
      figure: this.figure,
    })
    this.point1.moveTo(x1, y1)
    this.point2.moveTo(x2, y2)
    super.update()
  }

  get description(): string {
    const pointName = this.point.label !== '' ? this.point.label : this.point.id
    if (
      this.line.point1.label !== '' &&
      this.line.point2.label !== '' &&
      this.point.label !== ''
    ) {
      return `Droite parallèle à $${this.line.notation}$ et passant par le point $${pointName}$`
    }
    const parallelLines = [...this.figure.elements.values()].filter(
      (element) => element instanceof LineParallel,
    ) as LineParallel[]
    const index = parallelLines.indexOf(this)
    return `Droite parallèle n°${index + 1}`
  }
}

export default LineParallel
