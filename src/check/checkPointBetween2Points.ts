import type Figure from '../Figure'
import type Point from '../elements/points/Point'

export function areNumbersOrdered(a: number, b: number, c: number): boolean {
  // Vérifier si les nombres sont en ordre croissant
  const isIncreasing = a <= b && b <= c
  // Vérifier si les nombres sont en ordre décroissant
  const isDecreasing = a >= b && b >= c

  // Retourner true si l'un des deux ordres est respecté
  return isIncreasing || isDecreasing
}

/**
 * Vérifie si un point est sur le segment point1-point2 sans que le segment ne soit tracé mais la droite ou la demi-droite doit l'être.
 * @param {Figure} figure // La figure
 * @param {string} labelPt // Nom du point
 * @param {string} labelPt1 // Nom du point1
 * @param {string} labelPt2 // Nom du point2
 * @example Voir exemples plus adaptés dans Figure.ts
 * @author Eric Elter
 * @returns {{ isValid: boolean, message: string }}
 */
export default function checkPointBetween2Points({
  figure,
  labelPt,
  labelPt1, // Je passe par un label et pas par un pt car cela pourrait être un pointOnLine ou un PointIntersectionLL ou .....
  labelPt2,
}: {
  figure: Figure
  labelPt: string
  labelPt1: string
  labelPt2: string
}): { isValid: boolean; message: string } {
  const point = [...figure.elements.values()].filter(
    (e) =>
      e.type !== 'pointer' &&
      e.type.includes('Point') &&
      'label' in e &&
      e.label === labelPt,
  ) as Point[]
  if (point.length === 0)
    return {
      isValid: false,
      message: `Il n'existe pas de point ${labelPt} sur la figure.`,
    }
  if (point.length > 1)
    return {
      isValid: false,
      message: `Il existe trop de points ${labelPt} sur la figure.`,
    }

  const point1 = [...figure.elements.values()].filter(
    (e) =>
      e.type !== 'pointer' &&
      e.type.includes('Point') &&
      'label' in e &&
      e.label === labelPt1,
  ) as Point[]
  if (point1.length === 0)
    return {
      isValid: false,
      message: `Il n'existe pas de point ${labelPt1} sur la figure.`,
    }
  if (point1.length > 1)
    return {
      isValid: false,
      message: `Il existe trop de points ${labelPt1} sur la figure.`,
    }

  const point2 = [...figure.elements.values()].filter(
    (e) =>
      e.type !== 'pointer' &&
      e.type.includes('Point') &&
      'label' in e &&
      e.label === labelPt2,
  ) as Point[]
  if (point2.length === 0)
    return {
      isValid: false,
      message: `Il n'existe pas de point ${labelPt2} sur la figure.`,
    }
  if (point2.length > 1)
    return {
      isValid: false,
      message: `Il existe trop de points ${labelPt2} sur la figure.`,
    }

  if (
    !figure.checkPointOnLine({ labelPt, nameLine: `${labelPt1}${labelPt2}` })
      .isValid ||
    !areNumbersOrdered(point1[0].x, point[0].x, point2[0].x) ||
    !areNumbersOrdered(point1[0].y, point[0].y, point2[0].y)
  )
    return {
      isValid: false,
      message: `Le point ${labelPt} n'est pas sur le segment[${labelPt1}${labelPt2}].`,
    }

  return {
    isValid: true,
    message: `Le point ${labelPt} est sur le segment[${labelPt1}${labelPt2}].`,
  }
}
