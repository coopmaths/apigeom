import type Figure from '../Figure'
import type { Coords } from '../elements/calculus/Coords'
import type Polygon from '../elements/lines/Polygon'
import { round } from '../lib/format'

export default function checkPolygon({
  figure,
  points,
  color,
}: { figure: Figure; points: Coords[]; color?: string }): {
  isValid: boolean
  message: string
} {
  let polygons = [...figure.elements.values()].filter((e) =>
    e.type.includes('Polygon'),
  ) as Polygon[]
  if (color !== undefined) {
    polygons = polygons.filter((e) => e.color === color)
  }
  if (polygons.length === 0)
    return {
      isValid: false,
      message:
        color === undefined
          ? "Il n'y a pas de polygones dans la figure."
          : `Il n'y a pas de polygones de couleur ${color} dans la figure.`,
    }
  let samePolygone = false
  for (const polygon of polygons) {
    if (polygon.points.length !== points.length) {
      continue
    }
    // Index du premier point du polygone
    const index = points.findIndex(
      (point) =>
        round(point.x) === round(polygon.points[0].x) &&
        round(point.y) === round(polygon.points[0].y),
    )
    if (index !== -1) {
      const newPoints = points.slice(index).concat(points.slice(0, index))
      samePolygone = newPoints.every(
        (point, i) =>
          round(point.x) === round(polygon.points[i].x) &&
          round(point.y) === round(polygon.points[i].y),
      )
      if (samePolygone) {
        break
      }
      const revesePoints = points.reverse()
      const indexReverse = revesePoints.findIndex(
        (point) =>
          point.x === polygon.points[0].x && point.y === polygon.points[0].y,
      )
      const newPointsReverse = revesePoints
        .slice(indexReverse)
        .concat(revesePoints.slice(0, indexReverse))
      samePolygone = newPointsReverse.every(
        (point, i) =>
          point.x === polygon.points[i].x && point.y === polygon.points[i].y,
      )
      if (samePolygone) {
        break
      }
    }
  }
  return {
    isValid: samePolygone,
    message: samePolygone ? 'Le polygone a bien été tracé' : '',
  }
}
