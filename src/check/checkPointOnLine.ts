import type Figure from '../Figure'
import type Line from '../elements/lines/Line'
import type Point from '../elements/points/Point'
import PointOnLine from '../elements/points/PointOnLine'

/**
 * Vérifie si un point appartient à une ligne (droite, demi-droite ou segment)
 * @param {Figure} figure // La figure
 * @param {string} labelPt // Nom du point
 * @param {string[]|string} nameLine // Nom de la ligne ou bien tableau contenant les noms de la ligne
 * @example Voir exemples plus adaptés dans Figure.ts
 * @author Eric Elter
 * @returns {{ isValid: boolean, message: string }}
 */
export default function checkPointOnLine({
  figure,
  labelPt,
  nameLine,
}: {
  figure: Figure
  labelPt: string
  nameLine: string[] | string
}): { isValid: boolean; message: string } {
  let points = [...figure.elements.values()].filter(
    (e) =>
      e.type !== 'pointer' &&
      e.type.includes('Point') &&
      'label' in e &&
      e.label === labelPt,
  ) as Point[]
  if (points.length === 0)
    return {
      isValid: false,
      message: `Il n'existe pas de point ${labelPt} sur la figure.`,
    }
  if (points.length > 1)
    return {
      isValid: false,
      message: `Il existe trop de points ${labelPt} sur la figure.`,
    }

  let lines = [...figure.elements.values()].filter(
    (e) =>
      e.type.includes('Line') ||
      e.type.includes('Segment') ||
      e.type.includes('Ray'),
  ) as Line[]
  if (lines.length === 0)
    lines = lines.filter((e) => nameLine.includes(e.notation))

  const nomLigne = Array.isArray(nameLine) ? nameLine[0] : nameLine
  if (lines.length === 0)
    return {
      isValid: false,
      message: `${nomLigne} n'existe pas sur la figure.`,
    }

  points = [...figure.elements.values()].filter(
    (e) =>
      e instanceof PointOnLine &&
      e.type !== 'pointer' &&
      e.type.includes('PointOnLine') &&
      lines.some((line) => e.line === line),
  ) as PointOnLine[]
  if (points.length === 0)
    return {
      isValid: false,
      message: `Il n'existe pas de point sur ${nomLigne}.`,
    }

  points = points.filter((e) => e.label === labelPt)
  if (points.length === 0)
    return {
      isValid: false,
      message: `Il n'existe pas de point ${labelPt} sur ${nomLigne}.`,
    }

  return { isValid: true, message: `Le point ${labelPt} sur ${nomLigne}.` }
}
