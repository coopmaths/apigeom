import type Figure from '../../Figure'
import type Point from '../points/Point'

import { orangeMathalea } from '../defaultValues'
import type { OptionsElement2D } from '../interfaces'
import TextByPoint from './TextByPoint'

class TextCyclicBetween2Points extends TextByPoint {
  indiceCurrentText = 0
  point1: Point
  point2: Point
  texts: string[]
  constructor(
    figure: Figure,
    {
      point1,
      point2,
      texts,
      ...options
    }: {
      dxInPixels?: number
      dyInPixels?: number
      point1: Point
      point2: Point
      texts: string[]
    } & OptionsElement2D,
  ) {
    const M = figure.create('Middle', {
      isChild: true,
      isVisible: false,
      point1,
      point2,
    }) as Point
    super(figure, { point: M, text: texts[0], ...options })
    this.type = 'TextCyclic'
    this.texts = texts
    this.indiceCurrentText = 0
    this.div.style.pointerEvents = 'auto'
    this.point1 = point1
    this.point2 = point2
    this.anchor = 'bottomCenter'
    this.div.style.color = orangeMathalea
    this.div.style.fontWeight = 'bold'
    this.div.addEventListener('click', () => {
      this.indiceCurrentText = (this.indiceCurrentText + 1) % this.texts.length
      this.text = this.texts[this.indiceCurrentText]
    })
    this.div.addEventListener('mousedown', (e) => {
      e.preventDefault()
    })
  }

  draw(): void {
    super.draw()
    this.div.style.pointerEvents = 'auto'
    this.div.style.userSelect = 'none'
    this.div.style.cursor = 'pointer'
    this.update()
  }

  update(): void {
    super.update()
    this.angleWithHorizontalInDegres =
      (-Math.atan2(
        this.point2.y - this.point1.y,
        this.point2.x - this.point1.x,
      ) *
        180) /
      Math.PI
  }
}

export default TextCyclicBetween2Points
