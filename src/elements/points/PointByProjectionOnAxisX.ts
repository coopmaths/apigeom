import type Figure from '../../Figure'

import type { OptionsElement2D } from '../interfaces'
import Point from './Point'

class PointByProjectionOnAxisX extends Point {
  origin: Point
  constructor(
    figure: Figure,
    { origin, ...options }: { origin: Point } & OptionsElement2D,
  ) {
    super(figure, { isFree: false, x: origin.x, y: 0, ...options })
    this.type = 'PointByProjectionOnAxisX'
    this.origin = origin
    this.origin.subscribe(this)
  }

  toJSON(): object {
    return {
      ...this.jsonOptions(),
      idOrigin: this.origin.id,
      label: this.label,
      shape: this.shape,
      sizeInPixels: this.sizeInPixels,
    }
  }

  update(): void {
    this._x = this.origin.x
    this._y = 0
    super.update()
  }
}

export default PointByProjectionOnAxisX
