import renderMathInElement from 'katex/dist/contrib/auto-render.js'
import 'katex/dist/katex.min.css'

import type DynamicNumber from './dynamicNumbers/DynamicNumber'
import type { eventName, eventOptions } from './uiMachine'
import type { availableButtons } from './userInterface/availableButtons'

import { loadJson } from './actions/loadJson'
import checkAngle from './check/checkAngle'
import checkCircleRadius from './check/checkCircleRadius'
import checkCoords from './check/checkCoords'
import checkDistance from './check/checkDistance'
import checkLine from './check/checkLine'
import checkParallel from './check/checkParallel'
import checkPointBetween2Points from './check/checkPointBetween2Points'
import checkPointOnIntersectionLL from './check/checkPointOnIntersectionLL'
import checkPointOnLine from './check/checkPointOnLine'
import checkPolygon from './check/checkPolygon'
import checkPolygonByLabels from './check/checkPolygonByLabels'
import checkRay from './check/checkRay'
import checkSameDistance from './check/checkSameDistance'
import checkSegment from './check/checkSegment'
import checkVector from './check/checkVector'
import shake from './check/shake'
import Angle from './dynamicNumbers/Angle'
import Distance from './dynamicNumbers/Distance'
import DynamicCalcul from './dynamicNumbers/DynamicCalcul'
import DynamicX from './dynamicNumbers/DynamicX'
import DynamicY from './dynamicNumbers/DynamicY'
import Element2D from './elements/Element2D'
import type { Coords } from './elements/calculus/Coords'
import Graph from './elements/calculus/Graph'
import Graph2 from './elements/calculus/Graph2'
import defaultOptions, {
  defaultButtonsWidth,
  defaultDivUserMessageFontSize,
  defaultDivUserMessageHeight,
  defaultDragAllDelta,
  defaultFooterHeight,
  defaultHistorySize,
  defaultMinHeight,
  defaultMinWidth,
} from './elements/defaultValues'
import CircleFractionDiagram from './elements/diagrams/CircleFractionDiagram'
import LineFractionDiagram from './elements/diagrams/LineFractionDiagram'
import RectangleFractionDiagram from './elements/diagrams/RectangleFractionDiagram'
import GraduatedLine from './elements/grid/GraduatedLine'
import Grid from './elements/grid/Grid'
import type { FigureOptions, SaveJson } from './elements/interfaces'
import Arc from './elements/lines/Arc'
import ArcBy3PointsAndRadius from './elements/lines/ArcBy3PointsAndRadius'
import ArcByCenterRadiusAndAngles from './elements/lines/ArcByCenterRadiusAndAngles'
import BisectorByPoints from './elements/lines/BisectorByPoints'
import Circle from './elements/lines/Circle'
import CircleCenterDynamicRadius from './elements/lines/CircleCenterDyamicRadius'
import CircleCenterPoint from './elements/lines/CircleCenterPoint'
import Line from './elements/lines/Line'
import LineByPointVector from './elements/lines/LineByPointVector'
import LineParallel from './elements/lines/LineParallel'
import LinePerpendicular from './elements/lines/LinePerpendicular'
import PerpendicularBisector from './elements/lines/PerpendicularBisector'
import PerpendicularBisectorByPoints from './elements/lines/PerpendicularBisectorByPoints'
import Polygon from './elements/lines/Polygon'
import Polyline from './elements/lines/Polyline'
import Ray from './elements/lines/Ray'
import Segment from './elements/lines/Segment'
import MarkBetweenPoints from './elements/mark/MarkBetweenPoints'
import MarkRightAngle from './elements/mark/MarkRightAngle'
import Barycenter from './elements/points/Barycenter'
import Middle from './elements/points/Middle'
import PointOnGraph from './elements/points/PoinrOnGraph'
import Point from './elements/points/Point'
import PointByProjection from './elements/points/PointByProjection'
import PointByProjectionOnAxisX from './elements/points/PointByProjectionOnAxisX'
import PointByProjectionOnAxisY from './elements/points/PointByProjectionOnAxisY'
import PointIntersectionCC from './elements/points/PointIntersectionCC'
import PointIntersectionLC from './elements/points/PointIntersectionLC'
import PointIntersectionLL from './elements/points/PointIntersectionLL'
import PointOnCircle from './elements/points/PointOnCircle'
import PointOnLine from './elements/points/PointOnLine'
import PointOnLineAtDistance from './elements/points/PointOnLineAtDistance'
import PointsIntersectionCC from './elements/points/PointsIntersectionCC'
import PointsIntersectionLC from './elements/points/PointsIntersectionLC'
import TextByPoint from './elements/text/TextByPoint'
import TextByPosition from './elements/text/TextByPosition'
import TextDynamicByPosition from './elements/text/TextDynamicByPosition'
import Vector from './elements/vector/Vector'
import VectorByPoints from './elements/vector/VectorByPoints'
import VectorPerpendicular from './elements/vector/VectorPerpendicular'
import { isLocalStorageAvailable } from './lib/storage'
import handleHover from './pointerActions/handleHover'
import handlePointerAction from './pointerActions/handlePointerAction'
import addButtons from './userInterface/addButtons'
import addColorPalette from './userInterface/addColorPalette'
import addCustomButton from './userInterface/addCustomButton'
import addDashedChoice from './userInterface/addDashedChoice'
import addThicknessChoice from './userInterface/addThicknessChoice'
import setToolbar from './userInterface/setToolbar'
import GraphByParts from './elements/calculus/GraphByParts'
import PointOnPolyline from './elements/points/PointOnPolyline'

/**
 * Créé un espace de travail dans lequel on peut
 * générer des figures de géométrie statique ou dynamique
 */
class Figure {
  /** Tableau de callBacks à exécuter quand la figure est sauvegardée */
  private _actionsOnChange: Array<() => void> = []
  /** Boutons qui interagissent avec la figure */
  buttons: Map<string, HTMLDivElement> = new Map<string, HTMLDivElement>()
  /** Element parent divUserMessage, du svg et des différents div pours les texte */
  container!: HTMLElement
  /** Etat de la machine pour l'interface graphique */
  currentState = ''
  /** div dans lequel sont placés tous les boutons de l'interface graphique  */
  divButtons: HTMLDivElement
  /** div dans lequel s'affichera le svg et les div des textes en surimpression du svg */
  divFigure: HTMLDivElement
  /** parent de divFigure et de divUserMessage */
  divFigureAndUserMessage: HTMLDivElement
  /** div dans lequel sera écrit la dernière sauvegarde automatique au format JSON */
  divSave: HTMLDivElement | null
  /** div dans lequel on affiche l'aide pour l'action en cours */
  divUserMessage: HTMLDivElement
  /** div supplémentaire pour les options */
  divToolbar2: HTMLDivElement
  /** div avec le choix des marques */
  divMarkPalette?: HTMLDivElement
  /** Si l'option snapGrid est active, cela détermine la distance horizontale entre deux lieux de dépot du point */
  dx: number
  /** Si l'option snapGrid est active, cela détermine la distance verticale entre deux lieux de dépot du point */
  dy: number
  /** La clé est par défaut element0, element1, element2... ou le nom de l'élément et la valeur est l'élément géométrique (segment, point, polygone...) */
  elements: Map<string, DynamicNumber | Element2D>
  /** Filtre utilisé sur les éléments pour savoir ceux qui réagissent au clic */
  filter: (e: Element2D) => boolean
  /** Quadrillage de la figure */
  grid?: Grid
  /** Hauteur en pixels du SVG */
  height: number
  /** Points à proximité du pointeur utilisés pour proposer un déplacement au lieu d'une création de point */
  hoverPoints: Set<Element2D>
  /** Identifiant de la figure pour la gestion de l'interactivité dans MathALÉA */
  id = ''
  /** Point actuellement en train d'être déplacé par la souris */
  inDrag: Point | TextByPosition | undefined
  /** Dimensions initiales utilisées pour le zoom */
  initial: {
    height: number
    scale: number
    width: number
    xMin: number
    yMin: number
  }
  /** Figure dynamique ou statique */
  isDynamic: boolean
  /** Eventuel div dans lequel on attend une réponse de l'utilisateur */
  modal?: HTMLDivElement

  /** Options courante pour tous les tracés */
  options: FigureOptions

  /** Nombre de pixels poun une unité (soit 1 cm en sortie papier) par défaut à 30 */
  pixelsPerUnit: number
  /** Point virtuel qui suit le pointer */
  pointer: Point
  /** Abscisse du pointeur dans le repère de la figure */
  pointerX: null | number
  /** Ordonnée du pointeur dans le repère de la figure */
  pointerY: null | number
  /** div (ou p ou svg…) dans lequel sera inséré le SVG et les div avec le texte mathématiques */
  /** Échelle globale */
  _scale: number
  /** Tableau d'éléments sélectionnés (utilisé dans l'interface graphique pour faire une construction à plusieurs entrées) */
  selectedElements: Element2D[]
  /** Si l'option snapGrid est active, le point sera aimanté */
  snapGrid: boolean
  /** Un tableau des différentes sauvegardes automatiques utilisé pour les redo */
  stackRedo: string[]
  /** Un tableau des différentes sauvegardes automatiques utilisé pour les undo */
  stackUndo: string[]
  /** SVG de la figure géométrique */
  svg: SVGElement
  /** Élément temporaire (utilisé dans l'interface graphique pour donner un aperçu de le construction en cours) */
  tmpElements: Element2D[]
  /** Machine qui gère l'état de l'interface utilisateur */
  ui!: { send: (e: eventName, opt?: eventOptions) => void }
  /** Largeur en pixels du SVG */
  width: number
  /** Abscisse du coin en bas à droite */
  xMax: number
  /** Abscisse du coin en bas à gauche */
  xMin: number
  /** Échelle horizontale */
  xScale: number
  /** Ordonnée du point en haut à droite */
  yMax: number
  /** Ordonnée du coin en bas à gauche */
  yMin: number
  /** Échelle verticale */
  yScale: number
  /** Niveau de zoom */
  zoomLevel = 1

  /**
   * @param __namedParameters width - Largeur en pixels du SVG
   * @param __namedParameters height - Hauteur en pixels du SVG
   * @param pixelsPerUnit - Nombres de pixels pour une unité de la figure, par défaut 30 pixels (l'unité sera le cm en sortie LaTeX)
   * @param xMin - Abscisse du coin en bas à gauche
   * @param yMin - Ordonnée du coin en bas à gauche
   * @param isDynamic - Figure dynamique ou statique
   * @param dx - Si l'option snapGrid est activée, cela correspond à la distance horizontale du quadrillage sur lequel les points peuvent être déposés
   * @param dy - Si l'option snapGrid est activée, cela correspond à la distance verticale du quadrillage sur lequel les points peuvent être déposés
   */
  constructor({
    border = false,
    dx = 1,
    dy = 1,
    height = document.documentElement.clientHeight - defaultFooterHeight,
    isDynamic = true,
    pixelsPerUnit = 30,
    scale = 1,
    snapGrid = false,
    width = document.documentElement.clientWidth - defaultButtonsWidth,
    xMin = -10,
    xScale = 1,
    yMin = -6,
    yScale = 1,
  }: {
    border?: boolean
    dx?: number
    dy?: number
    height?: number
    isDynamic?: boolean
    pixelsPerUnit?: number
    scale?: number
    snapGrid?: boolean
    width?: number
    xMin?: number
    xScale?: number
    yMin?: number
    yScale?: number
  } = {}) {
    this.elements = new Map()
    this.stackUndo = []
    this.stackRedo = []
    this.width = width
    this.height = height
    this.pixelsPerUnit = pixelsPerUnit
    this.xMin = xMin
    this.xMax = xMin + width / pixelsPerUnit / scale / xScale
    this.yMin = yMin
    this.yMax = yMin + height / pixelsPerUnit / scale / yScale
    this.dx = dx
    this.dy = dy
    this.hoverPoints = new Set()
    this.xScale = xScale
    this.yScale = yScale
    this._scale = scale
    this.isDynamic = isDynamic
    this.pointerX = null
    this.pointerY = null
    this.selectedElements = []
    this.tmpElements = []
    this.options = { ...defaultOptions }
    this.snapGrid = snapGrid

    this.container = document.createElement('div')
    this.container.id = 'containerApigeom'
    this.divFigureAndUserMessage = document.createElement('div')
    this.divFigureAndUserMessage.id = 'divFigureAndUserMessage'
    this.divFigure = document.createElement('div')
    this.divFigure.id = 'divFigure'
    this.svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
    this.divSave = null
    this.divButtons = document.createElement('div')
    this.divButtons.id = 'divButtons'
    this.divUserMessage = document.createElement('div')
    this.divUserMessage.id = 'divUserMessage'
    this.divUserMessage.style.fontSize = defaultDivUserMessageFontSize
    this.divUserMessage.style.height = defaultDivUserMessageHeight
    this.divUserMessage.style.width = '90%'
    this.divUserMessage.style.textAlign = 'right'
    this.divUserMessage.style.right = '0'
    this.divUserMessage.style.color = 'blue'
    this.divUserMessage.style.margin = '3px'
    this.divUserMessage.style.padding = '0.5em'
    this.divUserMessage.style.fontSize = '1.5em'
    this.divToolbar2 = document.createElement('div')
    this.divToolbar2.id = 'divToolbar2'
    this.divToolbar2.style.display = 'flex'

    this.container.appendChild(this.divButtons)
    this.container.appendChild(this.divFigureAndUserMessage)
    this.divFigureAndUserMessage.appendChild(this.divToolbar2)
    this.divFigureAndUserMessage.appendChild(this.divUserMessage)
    this.divFigureAndUserMessage.appendChild(this.divFigure)
    this.divFigure.appendChild(this.svg)
    if (border) this.svg.style.border = 'solid'
    // Pour éviter le scroll quand on manipule la figure sur un écran tactile
    this.svg.style.touchAction = 'none'
    this.clearHtml()
    if (this.isDynamic) this.listenPointer()
    this.pointer = new Point(this, {
      isChild: true,
      isFree: false,
      isVisible: false,
      isSelectable: false,
      shape: '',
      x: 0,
      y: 0,
    })
    this.pointer.type = 'pointer'
    this.filter = (e) => e instanceof Point && e.isFree
    this.saveState()
    this.initial = {
      height: this.height,
      width: this.width,
      scale: this._scale,
      xMin: this.xMin,
      yMin: this.yMin,
    }
    // this.handleDragAll()
    // Les boutons n'existe pas encore
    setTimeout(() => {
      this.handleUndoRedoButtons()
    }, 100)
  }

  addButtons(buttons: string): HTMLImageElement[] {
    return addButtons(buttons, this)
  }

  addColorPalette(colors: string[]): HTMLDivElement {
    return addColorPalette(colors, this)
  }

  addCustomButton({
    action,
    container,
    record,
    text,
    tooltip = '',
    url,
    userMessage,
  }: {
    action: () => void
    tooltip?: string
    url?: string
    text?: string
    container?: HTMLDivElement
    record?: boolean
    userMessage?: string
  }): HTMLElement {
    return addCustomButton({
      action,
      figure: this,
      tooltip,
      url,
      text,
      container,
      record,
      userMessage,
    })
  }

  addDashedChoice(): HTMLDivElement {
    return addDashedChoice(this)
  }

  addThicknessChoice(): HTMLDivElement {
    return addThicknessChoice(this)
  }

  /** Ajuste la taille pour être la plus grande possible en tenant compte d'une barre d'outils sur le côté et un footer en dessous */
  adjustSize(): void {
    if (this.width < defaultMinWidth) this.width = defaultMinWidth
    if (this.height < defaultMinHeight) this.height = defaultMinHeight
    this.xMax =
      this.xMin + this.width / this.pixelsPerUnit / this.scale / this.xScale
    this.yMax =
      this.yMin + this.height / this.pixelsPerUnit / this.scale / this.yScale
    if (this.divFigure !== null && this.svg !== null) {
      this.divFigure.style.width = `${this.width.toString()}px`
      this.divFigure.style.height = `${this.height.toString()}px`
      this.svg.setAttribute('width', this.width.toString())
      this.svg.setAttribute('height', this.height.toString())
      this.svg.setAttribute(
        'viewBox',
        `${this.xToSx(this.xMin)} ${this.yToSy(this.yMax)} ${this.width} ${this.height}`,
      )
    }
    for (const e of this.elements.values()) {
      if (e instanceof Element2D) e.update()
    }
  }

  /** Met à jour la taille de l'espace de travail en cas de changements sur la fenêtre du navigateur */
  autoAdjustSize(): void {
    window.addEventListener('resize', () => {
      getMaxSize()
      this.adjustSize()
    })
    window.addEventListener('orientationchange', () => {
      getMaxSize()
      this.adjustSize()
    })
    window.addEventListener('fullscreenchange', () => {
      getMaxSize()
      this.adjustSize()
    })

    const getMaxSize = (): void => {
      this.width = document.documentElement.clientWidth - defaultButtonsWidth
      this.height = document.documentElement.clientHeight - defaultFooterHeight
    }
  }

  /**
   * Vérifie si un point appartient à une ligne (droite, demi-droite ou segment)
   * @param {string} labelPt // Nom du point
   * @param {string[]|string} nameLine // Nom de la ligne ou bien tableau contenant les noms de la ligne
   * @example figure.checkPointOnLine({labelPt:'A',nameline:'CD'}) -> Vérifie que le point A appartienne à tout segment, demi-droite ou droite dont le nom contient CD.
   * @example figure.checkPointOnLine({labelPt:'A',nameline:['(CD)','(DC)'}) -> Vérifie que le point A appartienne à la droite nommée (CD) ou (DC).
   * @author Eric Elter
   * @returns {{ isValid: boolean, message: string }}
   */
  checkPointOnLine({
    labelPt,
    nameLine,
  }: {
    labelPt: string
    nameLine: string[] | string
  }): { isValid: boolean; message: string } {
    return checkPointOnLine({ figure: this, labelPt, nameLine })
  }

  /**
   * Vérifie si un point appartient à une intersection de deux lignes (droites, demi-droites ou segments)
   * @param {string} labelPt // Nom du point
   * @param {string[]|string} nameLine1 // Nom de la ligne1 ou bien tableau contenant les noms de la ligne1
   * @param {string[]|string} nameLine2 // Nom de la ligne2 ou bien tableau contenant les noms de la ligne2
   * @example figure.checkPointOnIntersectionLL({labelPt:'A',nameline1:'CD',nameline2:'EF'}) -> Vérifie que le point A est le point d'intersection de tout segment, demi-droite ou droite dont le nom contient CD et de tout segment, demi-droite ou droite dont le nom contient EF
   * @example figure.checkPointOnIntersectionLL({labelPt:'A',nameline1:['(CD)','(DC)'},nameline1:['[EF)'}) -> Vérifie que le point A est le point d'intersection de la droite nommée (CD) ou (DC) et de la demi-droite [EF).
   * @author Eric Elter
   * @returns {{ isValid: boolean, message: string }}
   */
  checkPointOnIntersectionLL({
    labelPt,
    nameLine1,
    nameLine2,
  }: {
    figure: Figure
    labelPt: string
    nameLine1: string[] | string
    nameLine2: string[] | string
  }): { isValid: boolean; message: string } {
    return checkPointOnIntersectionLL({
      figure: this,
      labelPt,
      nameLine1,
      nameLine2,
    })
  }

  /**
   * Vérifie si un point est sur le segment point1-point2 sans que le segment ne soit tracé mais la droite ou la demi-droite doit l'être.
   * @param {string} labelPt // Nom du point
   * @param {string} labelPt1 // Nom du point1
   * @param {string} labelPt2 // Nom du point2
   * @example figure.checkPointBetween2Points({ labelPt: 'A', labelPt1: 'E', labelPt2: 'F'}) -> Vérifie que le point A est sur le segment [EF] sans que le segment ne soit tracé mais la droite ou la demi-droite doit l'être.
   * @author Eric Elter
   * @returns {{ isValid: boolean, message: string }}
   */
  checkPointBetween2Points({
    labelPt,
    labelPt1, // Je passe par un label et pas par un pt car cela pourrait être un pointOnLine ou un PointIntersectionLL ou .....
    labelPt2,
  }: {
    labelPt: string
    labelPt1: string
    labelPt2: string
  }): { isValid: boolean; message: string } {
    return checkPointBetween2Points({
      figure: this,
      labelPt,
      labelPt1,
      labelPt2,
    })
  }

  checkAngle({
    angle,
    label1,
    label2,
    label3,
  }: { angle: number; label1: string; label2: string; label3: string }): {
    isValid: boolean
    message: string
  } {
    return checkAngle({ angle, figure: this, label1, label2, label3 })
  }

  checkCircleRadius({
    center,
    labelCenter,
    radius,
  }: {
    center: Coords
    labelCenter?: string
    radius: number
  }): {
    isValid: boolean
    message: string
    element?: Circle
  } {
    return checkCircleRadius({ center, figure: this, labelCenter, radius })
  }

  checkCoords({
    checkOnlyAbscissa,
    label,
    x,
    y,
    color,
  }: {
    checkOnlyAbscissa?: boolean
    label?: string
    x: number
    y?: number
    color?: string
  }): {
    isValid: boolean
    message: string
    points: Point[]
  } {
    return checkCoords({ checkOnlyAbscissa, figure: this, label, x, y, color })
  }

  checkDistance({
    distance,
    label1,
    label2,
  }: { distance: number; label1: string; label2: string }): {
    isValid: boolean
    message: string
  } {
    return checkDistance({ distance, figure: this, label1, label2 })
  }

  checkLine({
    point1,
    point2,
    color,
  }: { point1: Coords; point2: Coords; color?: string }): {
    isValid: boolean
    message: string
  } {
    return checkLine({ figure: this, point1, point2, color })
  }

  checkSegment({
    point1,
    point2,
    color,
  }: {
    point1: Coords
    point2: Coords
    color?: string
  }): { isValid: boolean; message: string } {
    return checkSegment({ figure: this, point1, point2, color })
  }

  checkParallel({ label1, label2 }: { label1: string; label2: string }): {
    isValid: boolean
    message: string
  } {
    return checkParallel({ figure: this, label1, label2 })
  }

  checkPolygon({ points, color }: { points: Coords[]; color?: string }): {
    isValid: boolean
    message: string
  } {
    return checkPolygon({ figure: this, points, color })
  }

  checkPolygonByLabels({
    labels,
    color,
  }: { labels: string[]; color?: string }): {
    isValid: boolean
    message: string
  } {
    return checkPolygonByLabels({ figure: this, labels, color })
  }

  checkSameDistance({ label1, label2 }: { label1: string; label2: string }): {
    isValid: boolean
    message: string
  } {
    return checkSameDistance({ figure: this, label1, label2 })
  }

  checkRay({
    point1,
    point2,
    color,
  }: { point1: Coords; point2: Coords; color?: string }): {
    isValid: boolean
    message: string
  } {
    return checkRay({ figure: this, point1, point2, color })
  }

  checkVector({
    labelOrigin,
    labelPoint2,
    x,
    xOrigin,
    y,
    yOrigin,
  }: {
    labelOrigin?: string
    labelPoint2?: string
    x: number
    xOrigin?: number
    y: number
    yOrigin?: number
  }): { isValid: boolean; message: string; vectors: VectorByPoints[] } {
    return checkVector({
      figure: this,
      labelOrigin,
      labelPoint2,
      x,
      xOrigin,
      y,
      yOrigin,
    })
  }

  clearHtml(): void {
    this.svg.innerHTML = ''
    const style = document.createElementNS(
      'http://www.w3.org/2000/svg',
      'style',
    )
    this.svg.appendChild(style)
    style.type = 'text/css'
    style.innerHTML = '.onlyOver:hover { opacity: 1; }'
    this.options = { ...defaultOptions }
  }

  copy(): Figure {
    const save = this.getJsonWithHistory()
    const figure = new Figure()
    figure.loadJson(JSON.parse(save))
    if (this.elements.size !== figure.elements.size) {
      throw new Error('Erreur lors de la copie de la figure')
    }
    return figure
  }

  create<T extends keyof typeof classes>(
    typeStr: T,
    // Les constructeurs sont de type [figure, options], donc on récupère le type des deuxièmes arguments des constructeurs
    options?: ConstructorParameters<(typeof classes)[T]>[1],
  ): InstanceType<(typeof classes)[T]> {
    // @ts-expect-error Typage très complexe
    const element = new classes[typeStr](this, { ...options })
    if ('draw' in element) element.draw()
    // @ts-expect-error Typage très complexe
    return element
  }

  getContainer(): HTMLElement | null {
    return this.container
  }

  getJsonWithHistory(): string {
    const save = {
      apiGeomVersion: APP_VERSION,
      options: this.options,
      ...Object.fromEntries(this.elements),
      history: this.stackUndo,
    }
    return JSON.stringify(save, filter, 2)
  }

  /** Récupère les coordonnées du pointeur dans le repère de la figure */
  getPointerCoord(event: PointerEvent): [number, number] {
    event.preventDefault()
    const rect = this.svg.getBoundingClientRect()
    const pointerX =
      (event.clientX - rect.x) / this.pixelsPerUnit / this.xScale / this.scale +
      this.xMin
    const pointerY =
      -(event.clientY - rect.y) /
        this.pixelsPerUnit /
        this.yScale /
        this.scale +
      this.yMax
    return [pointerX, pointerY]
  }

  /** Renvoie un string avec le code HTML et SVG de la figure */
  getStaticHtml(): string {
    const currentIsDynamic = this.isDynamic
    this.isDynamic = false
    let result = ''
    if (this.container.childElementCount < 5) {
      const newContainer = document.createElement('div')
      this.setContainer(newContainer)
    } else {
      this.isDynamic = currentIsDynamic
    }
    this.divFigure.style.display = 'block'
    result = this.divFigure.outerHTML
    return result
  }

  /** Déplace le repère de la figure à l'aide des flèches */
  handleDragAll(): void {
    window.addEventListener('keydown', (e) => {
      const direction = { x: 0, y: 0 }
      const step = defaultDragAllDelta
      if (e.key === 'ArrowLeft') {
        direction.x = step
      }
      if (e.key === 'ArrowRight') {
        direction.x = -step
      }
      if (e.key === 'ArrowUp') {
        direction.y = -step
      }
      if (e.key === 'ArrowDown') {
        direction.y = +step
      }
      // Shift + flèche => 10x plus rapide
      if (e.shiftKey) {
        direction.x *= 10
        direction.y *= 10
      }
      if (!isModalOpen() && (direction.x !== 0 || direction.y !== 0)) {
        e.preventDefault()
        this.xMin += direction.x
        this.yMin += direction.y
        this.zoom(this.zoomLevel)
      }
    })
  }

  handleUndoRedoButtons(): void {
    const btnRedo = this.buttons.get('REDO')
    const btnUndo = this.buttons.get('UNDO')
    if (btnRedo !== undefined) {
      btnRedo.style.opacity = this.stackRedo.length === 0 ? '0.5' : '1'
    }
    if (btnUndo !== undefined) {
      btnUndo.style.opacity = this.stackUndo.length < 2 ? '0.5' : '1'
    }
  }

  /** Génère le code LaTeX de la figure (EE : Avec ou sans préambule */
  latex({
    xMax = this.xMin + this.options.latexWidth,
    xMin = this.xMin,
    yMax = this.yMax,
    yMin = this.yMax - this.options.latexHeight,
    includePreambule = true,
  }: {
    xMax?: number
    xMin?: number
    yMax?: number
    yMin?: number
    includePreambule?: boolean
  } = {}): string {
    let latex = ''
    if (includePreambule) {
      latex += '\\documentclass[french,a4paper]{article}'
      latex += '\n\\usepackage[francais]{babel}'
      latex += '\n\\usepackage[utf8]{inputenc}'
      latex += '\n\\usepackage{tkz-base}'
      latex += '\n\\usepackage{pgfplots}'
      latex += '\n\\begin{document}'
    }
    latex += this.tikz({ xMax, xMin, yMax, yMin })
    if (includePreambule) latex += '\n\\end{document}'
    // ToFix Il peut y avoir un problème si un nombre est en écriture scientifique
    latex = latex.replace(/\d+\.\d+/g, (number: string) =>
      (Math.round(1000000 * Number.parseFloat(number)) / 1000000).toString(),
    )
    return latex
  }

  /** Démarre les listenners sur la figure lorsqu'elle est dynamique */
  listenPointer(): void {
    // On créé des listenners pour envoyer les information à la machine à états this.ui
    this.svg.addEventListener('pointerdown', (event: PointerEvent) => {
      if (!this.isDynamic) return
      // MGU nécessaire de le faire car sur tablette, le pointermove ne fonctionne pas
      const [pointerX, pointerY] = this.getPointerCoord(event)
      this.pointer.moveTo(pointerX, pointerY)
      handlePointerAction(this, event)
    })

    const stopDrag = (): void => {
      if (!this.isDynamic) return
      /** MGu pour sortir de l'object sélectionné */
      this.pointer.moveTo(Number.NEGATIVE_INFINITY, Number.NEGATIVE_INFINITY)
      handleHover(this, Number.NEGATIVE_INFINITY, Number.NEGATIVE_INFINITY)
      if (this.inDrag !== undefined) {
        this.inDrag = undefined
        if (this.divFigure !== null) this.divFigure.style.cursor = 'auto'
        this.saveState()
      }
    }
    this.svg.addEventListener('pointerup', stopDrag)
    this.svg.addEventListener('pointerleave', stopDrag)

    this.svg.addEventListener('pointermove', (event) => {
      if (!this.isDynamic) return
      const [pointerX, pointerY] = this.getPointerCoord(event)
      this.pointer.moveTo(pointerX, pointerY)
      handleHover(this, pointerX, pointerY)
      if (this.inDrag === undefined) return
      this.inDrag.moveTo(pointerX, pointerY)
    })
  }

  /** Exécuter tous les callbacks enregistrés avec onChange */
  private _hasChanged(): void {
    for (const action of this._actionsOnChange) {
      action()
    }
  }

  /** Efface la figure actuelle et charge une nouvelle figure à partir du code généré par this.json  */
  loadJson(json: SaveJson, eraseHistory?: boolean): void {
    loadJson(this, json, eraseHistory)
  }

  /** Ajoute une fonction à la liste de celles qui seront exécutées lorsque l'état de la figure est modifiée */
  onChange(action: () => void): void {
    this._actionsOnChange.push(action)
  }

  /** Reharge la figure stockée redoStack   */
  redo(): void {
    const lastRedo = this.stackRedo.at(-1)
    if (lastRedo !== undefined) {
      this.stackUndo.push(lastRedo)
      this.stackRedo.pop()
      this.loadJson(JSON.parse(lastRedo))
      this.handleUndoRedoButtons()
    }
    this._hasChanged()
  }

  restart(level = 1): void {
    if (this.stackUndo.length === 0) return
    const stop = Number.isInteger(level) && level > 1 ? level : 1
    while (this.stackUndo.length > stop) {
      const lastUndo = this.stackUndo.at(-2)
      if (lastUndo !== undefined) {
        this.stackRedo.push(this.stackUndo.at(-1) as string)
        this.stackUndo.pop()
        if (this.stackUndo.length + 1 === stop) {
          // chargement une seule fois au moins...
          this.loadJson(JSON.parse(lastUndo))
          this.handleUndoRedoButtons()
        }
      }
    }
    this._hasChanged()
  }

  /**  - Sauvegarde la figure
   *   - Met à jour l'historique et l'inscrit dans le div this.divSave
   *   - Réinitialise les éléments temporaires */
  saveState(): void {
    const save = this.json
    if (isLocalStorageAvailable())
      // Sauvegarde dans le localStorage
      localStorage.setItem('apiGeom', save)
    if (this.divSave !== null) {
      this.divSave.textContent = save
    }
    this.stackUndo.push(save)
    this.stackRedo = []
    if (this.stackUndo.length > defaultHistorySize)
      this.stackUndo = this.stackUndo.slice(-defaultHistorySize)
    this.handleUndoRedoButtons()
    for (const e of this.tmpElements) {
      e.remove()
    }
    this.tmpElements = []
    this.selectedElements = []
    this._hasChanged()
  }

  setContainer(parentContainer: HTMLElement): void {
    if (!(parentContainer instanceof HTMLElement))
      throw Error('grandParentContainer doit être un HTMLElement')
    parentContainer.appendChild(this.container)
    this.divFigure.style.position = 'relative'
    // this.container.style.overflow = 'hidden'
    this.divFigure.style.display = 'inline-block'
    this.divFigure.style.margin = '0'
    this.divFigure.style.padding = '0'
    this.divFigure.style.boxSizing = 'border-box'
    this.divFigure.style.border = 'none'
    // Si this.divButtons est vide
    if (this.divButtons.childElementCount === 0 && this.isDynamic) {
      this.setToolbar()
    }
    this.adjustSize()
  }

  setToolbar({
    nbCols,
    position = 'left',
    showStyles,
    tools,
  }: {
    nbCols?: number
    position?: 'left' | 'top' | 'none'
    showStyles?: boolean
    tools?: availableButtons[]
  } = {}): void {
    setToolbar(this, { nbCols, position, showStyles, tools })
  }

  async shake({
    delta = 3,
    repeat = 5,
    time = 1,
  }: { delta?: number; repeat?: number; time?: number } = {}): Promise<void> {
    await shake({ delta, figure: this, repeat, time })
  }

  /** Abscisse du SVG converti dans nos coordonnées */
  sxTox(x: number): number {
    return x / this.pixelsPerUnit / this.xScale / this.scale
  }

  /** Abscisse du SVG converti dans nos coordonnées */
  syToy(y: number): number {
    return -y / this.pixelsPerUnit / this.yScale / this.scale
  }

  tempCreate(
    typeStr: string,
    options?: ConstructorParameters<(typeof classes)[keyof typeof classes]>[1],
  ): Element2D {
    // @ts-expect-error Typage très complexe
    const element = this.create(typeStr, {
      isChild: true,
      isSelectable: false,
      ...options,
    }) as Element2D
    element.color = this.options.tmpColor
    element.thickness = this.options.tmpThickness
    if ('isDashed' in element && !(element instanceof Point))
      element.isDashed = this.options.tmpIsDashed
    if ('fillColor' in element) element.fillColor = this.options.tmpFillColor
    if ('fillOpacity' in element)
      element.fillOpacity = this.options.tmpFillOpacity
    this.tmpElements.push(element)
    return element
  }

  /** Génère le code TikZ de la figure */
  tikz({
    xMax = Math.min(this.xMax, this.xMin + this.options.latexWidth),
    xMin = this.xMin,
    yMax = this.yMax,
    yMin = Math.max(this.yMin, this.yMax - this.options.latexHeight),
    anchor = 'north',
  }: {
    xMax?: number
    xMin?: number
    yMax?: number
    yMin?: number
    anchor?: 'north' | 'south' | 'east' | 'west' | 'center' | 'mid' | 'base'
  } = {}): string {
    const stringScale = this.scale !== 1 ? `, scale=${this.scale}` : ''
    const xScale = this.xScale !== 1 ? `, xScale=${this.xScale}` : ''
    const yScale = this.yScale !== 1 ? `, yScale=${this.yScale}` : ''
    let tikz = `\n\\begin{tikzpicture}[baseline={(current bounding box.${anchor})}${stringScale}${xScale}${yScale}]`
    tikz += `\n\\clip(${xMin}, ${yMin}) rectangle (${xMax}, ${yMax});`
    for (const element of this.elements.values()) {
      if (
        element instanceof Element2D &&
        element.latex !== undefined &&
        element.latex !== '' &&
        element.isVisible &&
        element.color !== '' &&
        element.color !== 'none'
      ) {
        tikz += `\n${element.latex}`
      }
    }
    tikz += '\n\\end{tikzpicture}'
    // ToFix Il peut y avoir un problème si un nombre est en écriture scientifique
    tikz = tikz.replace(/\d+\.\d+/g, (number: string) =>
      (Math.round(1000000 * Number.parseFloat(number)) / 1000000).toString(),
    )
    return tikz
  }

  /** Charge la figure stockée dans stackUndo */
  undo(): void {
    const lastUndo = this.stackUndo.at(-2)
    if (lastUndo !== undefined) {
      this.stackRedo.push(this.stackUndo.at(-1) as string)
      this.stackUndo.pop()
      this.loadJson(JSON.parse(lastUndo))
      this.handleUndoRedoButtons()
    }
    this._hasChanged()
  }

  async wait(milliseconds = 1500): Promise<void> {
    await new Promise((resolve) => {
      setTimeout(resolve, milliseconds)
    })
  }

  /** Abscisse dans nos coordonnées converti en abscisse du SVG */
  xToSx(x: number): number {
    return x * this.pixelsPerUnit * this.xScale * this.scale
  }

  /** Abscisse dans nos coordonnées converti en abscisse du SVG */
  yToSy(y: number): number {
    return -y * this.pixelsPerUnit * this.yScale * this.scale
  }

  zoom(
    factor: number,
    {
      changeWidth = true,
      changeHeight = true,
      changeLeft = true,
      changeBottom = true,
    } = {},
  ): void {
    if (factor <= 0) return
    if (changeWidth) this.width = this.initial.width * factor
    if (changeHeight) this.height = this.initial.height * factor
    this.scale = this.initial.scale * factor
    if (changeLeft) this.xMin = this.initial.xMin / factor
    if (changeBottom) this.yMin = this.initial.yMin / factor
    this.adjustSize()
    const json = this.getJsonWithHistory()
    const options = this.options
    this.loadJson(JSON.parse(json), false)
    this.options = options
  }

  get htmlDescription(): HTMLUListElement {
    const ul = document.createElement('ul')
    ul.style.lineHeight = '2'
    for (const element of this.elements.values()) {
      if ('description' in element && !element.isChild) {
        const li = document.createElement('li')
        li.textContent = element.description
        ul.appendChild(li)
      }
    }
    return ul
  }

  /** Génère le code JSON de la figure qui permettra de la recharger */
  get json(): string {
    // Le JSON est personnalisé avec la méthode toJSON() des éléments
    const save = {
      apiGeomVersion: APP_VERSION,
      options: this.options,
      ...Object.fromEntries(this.elements),
    }
    return JSON.stringify(save, filter, 2)
  }

  get scale(): number {
    return this._scale
  }

  set scale(scale: number) {
    this._scale = scale
    this.adjustSize()
  }

  get userMessage(): string {
    return this.divUserMessage.textContent ?? ''
  }

  set userMessage(message: string) {
    this.divUserMessage.textContent = message
    renderMathInElement(this.divUserMessage, {
      delimiters: [
        { display: true, left: '\\[', right: '\\]' },
        { display: false, left: '$', right: '$' },
      ],
      errorColor: '#CC0000',
      preProcess: (chaine: string) =>
        chaine.replaceAll(String.fromCharCode(160), '\\,'),
      strict: 'warn',
      throwOnError: true,
      trust: false,
    })
  }
}

const classes = {
  Angle,
  Arc,
  ArcBy3PointsAndRadius,
  ArcByCenterRadiusAndAngles,
  Barycenter,
  BisectorByPoints,
  Circle,
  CircleCenterDynamicRadius,
  CircleCenterPoint,
  CircleFractionDiagram,
  Distance,
  DynamicCalcul,
  DynamicX,
  DynamicY,
  GraduatedLine,
  Graph,
  Graph2,
  GraphByParts,
  Grid,
  Line,
  LineByPointVector,
  LineFractionDiagram,
  RectangleFractionDiagram,
  LineParallel,
  LinePerpendicular,
  MarkBetweenPoints,
  MarkRightAngle,
  Middle,
  PerpendicularBisector,
  PerpendicularBisectorByPoints,
  Point,
  PointByProjection,
  PointByProjectionOnAxisX,
  PointByProjectionOnAxisY,
  PointIntersectionCC,
  PointIntersectionLC,
  PointIntersectionLL,
  PointOnCircle,
  PointOnGraph,
  PointOnLine,
  PointOnPolyline,
  PointOnLineAtDistance,
  PointsIntersectionCC,
  PointsIntersectionLC,
  Polygon,
  Polyline,
  Ray,
  Segment,
  TextByPoint,
  TextByPosition,
  TextDynamicByPosition,
  Vector,
  VectorByPoints,
  VectorPerpendicular,
}

export default Figure

function filter(key: string, value: Element2D): Element2D | undefined {
  if (value?.isChild) {
    return undefined
  }
  if (key === 'isChild') {
    return undefined
  }
  return value
}

function isModalOpen(): boolean {
  const dialogs = Array.from(document.querySelectorAll('dialog'))
  return dialogs.some((modal) => {
    if (modal.open) {
      return true
    }
    return false
  })
}
