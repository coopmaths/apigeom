import type Figure from '../Figure'
import { type Coords, equationLine } from '../elements/calculus/Coords'
import type Segment from '../elements/lines/Segment'

import { round } from '../lib/format'

/**
 *
 * @param param0 Vérifie si un segment est tracé
 * Attention ! Si A, B, C, D sont alignés dans cet ordre et [AD] est tracé, alors [BC] sera compté comme tracé
 * @returns
 */
export default function checkSegment({
  figure,
  point1,
  point2,
  color,
}: {
  figure: Figure
  point1: Coords
  point2: Coords
  color?: string
}): { isValid: boolean; message: string } {
  let segments = [...figure.elements.values()].filter((e) =>
    e.type.includes('Segment'),
  ) as Segment[]
  if (segments.length === 0)
    return {
      isValid: false,
      message: "Il n'y a pas de segment dans la figure.",
    }
  if (color !== undefined) {
    segments = segments.filter((e) => e.color === color)
  }
  if (segments.length === 0)
    return {
      isValid: false,
      message: "Il n'y a pas de segment de la couleur demandée dans la figure.",
    }
  const [a, b, c] = equationLine(point1, point2)
  const segmentsCoincident = segments.filter((segment) => {
    const [a2, b2, c2] = segment.equation
    return areCoincident(a, b, c, a2, b2, c2)
  })
  if (segmentsCoincident.length === 0) {
    return { isValid: false, message: 'Les segments ne sont pas confondus.' }
  }
  for (const segment of segmentsCoincident) {
    if (
      (round(segment.point1.x) === round(point1.x) &&
        round(segment.point1.y) === round(point1.y) &&
        round(segment.point2.x) === round(point2.x) &&
        round(segment.point2.y) === round(point2.y)) ||
      (round(segment.point2.x) === round(point1.x) &&
        round(segment.point2.y) === round(point1.y) &&
        round(segment.point1.x) === round(point2.x) &&
        round(segment.point1.y) === round(point2.y))
    ) {
      return { isValid: true, message: 'Le segment a bien été tracé.' }
    }
    // Si [point1, point2] est inclus dans le segment
    if (
      segment.point1.x <= point1.x &&
      point1.x <= segment.point2.x &&
      segment.point1.y <= point1.y &&
      point1.y <= segment.point2.y &&
      segment.point1.x <= point2.x &&
      point2.x <= segment.point2.x &&
      segment.point1.y <= point2.y &&
      point2.y <= segment.point2.y
    ) {
      return { isValid: true, message: 'Un segment plus grand existe.' }
    }
  }
  if (color !== undefined) {
    segments = [...figure.elements.values()].filter((e) =>
      e.type.includes('Segment'),
    ) as Segment[]
    for (const segment of segmentsCoincident) {
      if (
        (round(segment.point1.x) === round(point1.x) &&
          round(segment.point1.y) === round(point1.y) &&
          round(segment.point2.x) === round(point2.x) &&
          round(segment.point2.y) === round(point2.y)) ||
        (round(segment.point2.x) === round(point1.x) &&
          round(segment.point2.y) === round(point1.y) &&
          round(segment.point1.x) === round(point2.x) &&
          round(segment.point1.y) === round(point2.y))
      ) {
        return {
          isValid: false,
          message: "Le segment n'est pas tracé de la bonne couleur.",
        }
      }
    }
  }
  return {
    isValid: false,
    message: 'Aucun segment ne correspond à la réponse attendue.',
  }
}

function areCoincident(
  a1: number,
  b1: number,
  c1: number,
  a2: number,
  b2: number,
  c2: number,
  precision = 2,
): boolean {
  // Cas des droites verticales
  if (b1 === 0 && b2 === 0) {
    return c1 / a1 === c2 / a2
  }
  const slope1 = b1 !== 0 ? -a1 / b1 : Number.POSITIVE_INFINITY
  const slope2 = b2 !== 0 ? -a2 / b2 : Number.POSITIVE_INFINITY

  const intercept1 = b1 !== 0 ? -c1 / b1 : Number.NaN
  const intercept2 = b2 !== 0 ? -c2 / b2 : Number.NaN

  return (
    round(slope1, precision) === round(slope2, precision) &&
    round(intercept1, precision) === round(intercept2, precision)
  )
}
