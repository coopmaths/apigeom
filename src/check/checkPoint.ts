import type Figure from '../Figure'
import Element2D from '../elements/Element2D'
import type Point from '../elements/points/Point'

import { round } from '../lib/format'
/**
 * Idem que checkCoords mais avec un feedback sans coordonnées. Test juste l'existance d'un point et éventuellement son label.
 * @param param0
 * @returns
 */
export default function checkPoint({
  figure,
  label,
  x,
  y,
  color,
}: {
  figure: Figure
  label?: string
  x: number
  y: number
  color?: string
}): { isValid: boolean; message: string; points: Point[] } {
  let points: Point[] = []
  if (color !== undefined) {
    points = [...figure.elements.values()].filter(
      (e) =>
        e instanceof Element2D &&
        e.type !== 'pointer' &&
        e.type === 'Point' &&
        e.color === color,
    ) as Point[]
  } else {
    points = [...figure.elements.values()].filter(
      (e) => e.type !== 'pointer' && e.type === 'Point',
    ) as Point[]
  }
  if (label !== undefined) {
    const matchPoints = points.filter((p) => p.label === label)
    if (matchPoints.length === 0) {
      return {
        isValid: false,
        message: `Il n'y a pas de point $${label}$ dans la figure.`,
        points: [],
      }
    }
    if (matchPoints.length > 1) {
      return {
        isValid: false,
        message: `Il y a ${matchPoints.length} points $${label}$ dans la figure.`,
        points: matchPoints,
      }
    }
    const point = matchPoints[0]
    const isValid = round(point.x) === round(x) && round(point.y) === round(y)
    const message = isValid
      ? ''
      : `Le point $${label}$ n'est pas  non bien placé dans la figure.`
    return { isValid, message, points: [point] }
  }
  const matchPoints = points.filter(
    (p) => round(p.x) === round(x) && round(p.y) === round(y),
  )
  if (matchPoints.length === 0) {
    return {
      isValid: false,
      message: `Un point attendu n'est pas à sa place dans la figure.`,
      points: [],
    }
  }
  if (matchPoints.length > 1) {
    return {
      isValid: true,
      message: `Il y a ${matchPoints.length} points confondus à la place d'un point attendu dans la figure.`,
      points: matchPoints,
    }
  }
  const point = matchPoints[0]
  return { isValid: true, message: '', points: [point] }
}
