import type Figure from '../../Figure'
import type Graph from '../calculus/Graph'
import type GraphByParts from '../calculus/GraphByParts'
import type { OptionsElement2D } from '../interfaces'

import Point from './Point'

class PointOnGraph extends Point {
  graph: Graph
  constructor(
    figure: Figure,
    {
      abscissa = false,
      graph,
      ordinate = false,
      x = 1,
      ...options
    }: {
      abscissa?: boolean
      color?: string
      graph: Graph|GraphByParts
      id?: string
      isChild?: boolean
      isFree?: boolean
      isVisible?: boolean
      label?: string
      labelDxInPixels?: number
      labelDyInPixels?: number
      ordinate?: boolean
      shape?: '' | '|' | 'o' | 'x'
      size?: number
      thickness?: number
      x?: number
    } & OptionsElement2D,
  ) {
    super(figure, { x, y: graph.f(x), ...options })
    this.type = 'PointOnGraph'
    this.graph = graph
    this.graph.subscribe(this)
  }

  moveTo(x: number): void {
    let newX = x
    if (x>this.graph.xMax) newX = this.graph.xMax
    if (x<this.graph.xMin) newX = this.graph.xMin
    this.x = newX
    // y est en lecture seule
  }

  toJSON(): object {
    return {
      ...this.jsonOptions(),
      idGraph: this.graph.id,
      label: this.label,
      shape: this.shape,
      sizeInPixels: this.sizeInPixels,
      x: this.x,
    }
  }

  get x(): number {
    return this._x
  }

  set x(x) {
    if (
      x > this.figure.xMax ||
      x < this.figure.xMin ||
      this.graph.f(x) > this.figure.yMax ||
      this.graph.f(x) < this.figure.yMin
    )
      return
    this._x = x
    this._y = this.graph.f(x)
    this.update()
  }

  get y(): number {
    return this.graph.f(this.x)
  }
}

export default PointOnGraph
