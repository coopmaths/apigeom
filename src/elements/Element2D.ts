import type Figure from '../Figure'
import type DynamicNumber from '../dynamicNumbers/DynamicNumber'
import { colorLatex } from '../lib/handleColors'
import type Transformation from './Transformation'

import { orangeMathalea } from './defaultValues'
import type { OptionsElement2D, typeElement2D } from './interfaces'

abstract class Element2D {
  /** Couleur de l'élément au format HTML */
  protected _color: string
  /** Tracé en pointillés ? */
  protected _isDashed: boolean
  /** Est-ce que le pointeur est au dessus de l'élément ? */
  protected _isHover = false
  /** Est-ce que l'élément est sélectionné ? */
  protected _isSelected = false
  /** Visible ? */
  protected _isVisible: boolean
  /** Opacité */
  protected _opacity = 1
  /** Épaisseur du tracé de l'élément */
  protected _thickness: number
  /** Espace de travail dans lequel l'élément sera représenté */
  figure: Figure
  /** Groupe SVG dans lequel sera déssiné l'élément */
  groupSvg: SVGElement
  /** Identifiant de l'objet qui servira de clé dans le Map de tous les éléments */
  readonly id: string
  /** Les élément qui ont isChild à true sont ceux qui sont construits par d'autres et qui n'ont pas à être dans la sauvegarde */
  readonly isChild: boolean
  /** Est-ce qu'un élément est caché ? */
  isHidden = false
  /** Est-ce que l'élément peut être supprimé ? */
  isDeletable = true
  /** Pour l'interface graphique, détermine si un élément apparaitra ou pas dans la liste */
  isSelectable: boolean
  /** Liste des enfants à notifier à chaque fois que l'élément est déplacé */
  observers: Array<DynamicNumber | Element2D>
  /** Indique la transformation subit par le point */
  transformation?: Transformation
  /** Type d'objet mathématique */
  type!: typeElement2D
  constructor(
    figure: Figure,
    {
      color,
      id,
      isChild,
      isDashed,
      isSelectable,
      isVisible,
      opacity,
      thickness,
    }: OptionsElement2D,
  ) {
    this.figure = figure
    this.isChild = isChild ?? false
    /** Certains éléments sont construits par d'autres (codages, points temporaires, labels...)
     *  on les nomme elementTmpX, on met this.child à true et on ne les sauvegarde pas dans le Json
     *  mais ils sont bien présents dans figure.elements
     */
    this.isSelectable = isSelectable ?? true
    if (id == null || this.figure.elements.has(id)) {
      if (id != null && this.figure.elements.has(id)) {
        throw Error(`id ${id} déjà utilisé`)
      }
      if (this.isChild) {
        let cpt = 0
        while (this.figure.elements.has(`elementTmp${cpt.toString()}`)) {
          cpt++
        }
        this.id = `elementTmp${cpt.toString()}`
      } else {
        let cpt = 0
        while (this.figure.elements.has(`element${cpt.toString()}`)) {
          cpt++
        }
        this.id = `element${cpt.toString()}`
      }
    } else {
      this.id = id
    }
    this.figure.elements.set(this.id, this)
    this._color = color ?? this.figure.options.color
    this._thickness = thickness ?? this.figure.options.thickness
    this._isDashed = isDashed ?? this.figure.options.isDashed
    this._isVisible = isVisible ?? true
    this._opacity = opacity ?? 1
    this.groupSvg = document.createElementNS('http://www.w3.org/2000/svg', 'g')
    this.observers = []
  }

  /** Change la couleur de manière temporaire sans changer les attributs de l'élément */
  changeColor(element: Element2D, color: string): void {
    const fixedColor = color === 'mathalea' ? orangeMathalea : color
    if (element.groupSvg.children.length > 0) {
      for (const line of Array.from(element.groupSvg.children)) {
        line.setAttribute('stroke', fixedColor)
      }
    } else {
      // Le segment ou le cercle ne sont pas des groupes, ce sont des éléments uniques sans children
      element.groupSvg.setAttribute('stroke', fixedColor)
    }
  }

  distancePointer(_: number, __: number): number {
    return Number.POSITIVE_INFINITY
  }

  draw(): void {}

  /** Cacher l'élément de façon temporaire et permettre à l'utilisateur d'apercevoir l'élément en cliquant sur l'icone Afficher/Cacher */
  hide(): void {
    this.isVisible = false
    this.isHidden = true
  }

  jsonOptions(): object {
    const defaultOptions = {
      color: this.color,
      id: this.id,
      isChild: this.isChild,
      isDashed: this.isDashed,
      isVisible: this.isVisible,
      isSelectable: this.isSelectable,
      isDeletable: this.isDeletable,
      opacity: this.opacity,
      thickness: this.thickness,
      type: this.type,
    }
    if (this.transformation?.type === 'dilate') {
      return {
        ...defaultOptions,
        idTransformationCenter: this.transformation.center.id,
        idTransformationOrigin: this.transformation.origin.id,
        transformationFactor: this.transformation.factor,
        type: 'ElementByDilate',
      }
    }
    if (this.transformation?.type === 'reflect') {
      return {
        ...defaultOptions,
        idTransformationCenter: this.transformation.center.id,
        idTransformationOrigin: this.transformation.origin.id,
        type: 'ElementByReflect',
      }
    }
    if (this.transformation?.type === 'reflectOverLine') {
      return {
        ...defaultOptions,
        idTransformationLine: this.transformation.line.id,
        idTransformationOrigin: this.transformation.origin.id,
        type: 'ElementByReflectOverLine',
      }
    }
    if (this.transformation?.type === 'rotation') {
      return {
        ...defaultOptions,
        idTransformationCenter: this.transformation.center.id,
        idTransformationOrigin: this.transformation.origin.id,
        transformationAngle: this.transformation.angle,
        type: 'ElementByRotation',
      }
    }
    if (this.transformation?.type === 'translation') {
      return {
        ...defaultOptions,
        idTransformationPoint1: this.transformation.point1.id,
        idTransformationPoint2: this.transformation.point2.id,
        idTransformationOrigin: this.transformation.origin.id,
        type: 'ElementByTranslation',
      }
    }
    return defaultOptions
  }

  /** Prévenir les enfants qu'ils doivent se mettre à jour */
  notify(): void {
    for (const element of this.observers) {
      element.update()
    }
  }

  remove(): void {
    if (!this.isDeletable) return
    const element = this.figure.elements.get(this.id)
    const observers = element?.observers
    if (observers !== undefined) {
      for (const observer of observers) {
        observer.remove()
      }
    }
    if (element !== undefined) this.figure.elements.delete(this.id)
    this.groupSvg.remove()
  }

  /** Modifie la couleur et l'épaisseur de l'élément */
  setVisibilityColorThicknessAndDashed(): void {
    this.isVisible = this._isVisible
    this.color = this._color
    this.thickness = this._thickness
    if (this.type.includes('Point')) return
    this.isDashed = this._isDashed
  }

  /** Afficher l'élément */
  show(): void {
    this.isVisible = true
  }

  /** S'abonner aux modifications des éléments parents
   * Par exemple le segment s'abonnera aux modifications de ses deux extrémités
   */
  subscribe(element: DynamicNumber | Element2D): void {
    this.observers.push(element)
  }

  temp(): Element2D {
    this.figure.tmpElements.push(this)
    this.color = this.figure.options.tmpColor
    this.thickness = this.figure.options.tmpThickness
    if ('isDashed' in this) this.isDashed = this.figure.options.tmpIsDashed
    if ('fillColor' in this) this.fillColor = this.figure.options.tmpFillColor
    if ('fillOpacity' in this)
      this.fillOpacity = this.figure.options.tmpFillOpacity
    return this
  }

  tempShow(): void {
    this.isVisible = true
    this.changeColor(this, this.figure.options.tmpColor)
    changeThickness(this, 0.5)
  }

  /** Personnalise la sortie JSON de l'élément pour la sauvegarde */
  toJSON(): object {
    return {
      ...this.jsonOptions,
    }
  }

  /**
   * Annuler l'abonnement
   */
  unsubscribe(element: Element2D): void {
    this.observers = this.observers.filter((observer) => observer !== element)
  }

  /** Créé ou met à jour le groupe SVG de l'élément */
  update(): void {}

  /** Change la couleur des tracés de l'élément */
  set color(color: string) {
    this._color = color
    this.changeColor(this, color)
  }

  /** Couleur au format HTML */
  get color(): string {
    return this._color
  }

  get description(): string {
    return this.id
  }

  set isDashed(isDashed) {
    if (isDashed) {
      this.groupSvg.setAttribute('stroke-dasharray', '4 3')
    } else {
      this.groupSvg.removeAttribute('stroke-dasharray')
    }
    this._isDashed = isDashed
  }

  /** Pointillés */
  get isDashed(): boolean {
    return this._isDashed
  }

  get isHover(): boolean {
    return this._isHover
  }

  set isHover(isHover: boolean) {
    if (this.figure.currentState === 'HIDE') {
      if (isHover) {
        this.changeColor(this, orangeMathalea)
        changeThickness(this, this._thickness + 2)
      } else if (!this._isSelected && !this.isHidden) {
        this.changeColor(this, this._color)
        changeThickness(this, this._thickness)
      } else {
        this.tempShow()
      }
    } else {
      if (isHover) {
        this.changeColor(this, orangeMathalea)
        changeThickness(this, this._thickness + 2)
      } else if (!this._isSelected && !this.isHidden) {
        this.changeColor(this, this._color)
        changeThickness(this, this._thickness)
      }
    }
    this._isHover = isHover
  }

  get isSelected(): boolean {
    return this._isSelected
  }

  set isSelected(isSelected: boolean) {
    if (isSelected) {
      this.changeColor(this, orangeMathalea)
      changeThickness(this, this._thickness + 2)
    } else if (!this._isHover) {
      this.changeColor(this, this._color)
      changeThickness(this, this._thickness)
    }
    this._isSelected = isSelected
  }

  /** Affiche ou masque l'élément sans permettre à l'utilisateur de modifier l'état avec l'interface graphique */
  set isVisible(isVisible) {
    if (isVisible) {
      this.figure.svg.appendChild(this.groupSvg)
    } else {
      this.groupSvg.remove()
    }
    this._isVisible = isVisible
  }

  /** Est-il visible ? Le groupe SVG est-il dans le code SVG ? */
  get isVisible(): boolean {
    return this._isVisible
  }

  /** Génère le code LaTeX de l'élément */
  get latex(): string {
    return `% ${this.type} ${this.id}} n'a pas de sortie LaTeX`
  }

  get latexOptions(): string {
    const options = []
    if (this.color !== 'black' && this.color !== '') {
      options.push(`color=${colorLatex(this.color)}`)
    }
    if (this.thickness !== 1) {
      options.push(`line width=${this.thickness}`)
    }
    if (this.isDashed) {
      options.push('dashed')
    }
    if (options.length > 0) {
      return `[${options.join(',')}]`
    }
    return ''
  }

  get opacity(): number {
    return this._opacity
  }

  set opacity(opacity: number) {
    this._opacity = opacity
    this.groupSvg.setAttribute('opacity', `${opacity}`)
  }

  /** Change l'épaisseur des tracés */
  set thickness(thickness: number) {
    this._thickness = thickness
    changeThickness(this, thickness)
  }

  /** Épaisseur des tracés */
  get thickness(): number {
    return this._thickness
  }
}

function changeThickness(element: Element2D, thickness: number): void {
  if (element.groupSvg.children.length > 0) {
    for (const line of Array.from(element.groupSvg.children)) {
      if (
        line.getAttribute('stroke-readonly') === null ||
        line.getAttribute('stroke-readonly') === '0'
      ) {
        line.setAttribute('stroke-width', `${thickness}`)
      }
    }
  } else {
    // Le segment par exemple n'est pas un groupe mais un élément unique sans children
    element.groupSvg.setAttribute('stroke-width', `${thickness}`)
  }
}
export default Element2D
