import type Figure from '../Figure'
import type VectorByPoints from '../elements/vector/VectorByPoints'

export default function checkVector({
  figure,
  labelOrigin,
  labelPoint2,
  x,
  xOrigin,
  y,
  yOrigin,
}: {
  figure: Figure
  labelOrigin?: string
  labelPoint2?: string
  x: number
  xOrigin?: number
  y: number
  yOrigin?: number
}): { isValid: boolean; message: string; vectors: VectorByPoints[] } {
  const vectors = [...figure.elements.values()].filter(
    (e) => e.type === 'Vector' || e.type === 'VectorByPoints',
  ) as VectorByPoints[]
  const matchVectors = vectors.filter(
    (vector) => vector.x === x && vector.y === y,
  )
  if (matchVectors.length === 0) {
    return {
      isValid: false,
      message: `Il n'y a aucun vecteur de coordonnées $(${x}\\;;\\;${y})$`,
      vectors: [],
    }
  }
  const verifOnlyLabelOrigin = (): {
    isValid: boolean
    message: string
    vectors: VectorByPoints[]
  } => {
    const matchVectors2 = matchVectors.filter(
      (vector) => vector.point1.label === labelOrigin,
    )
    if (matchVectors2.length === 0) {
      return {
        isValid: false,
        message: `Il n'y a aucun vecteur de coordonnées $(${x}\\;;\\;${y})$ dont l'origine est nommée $${labelOrigin as string}$`,
        vectors: [],
      }
    }
    return { isValid: true, message: '', vectors: matchVectors2 }
  }
  const verifOnlyLabelOriginAndPoint2 = (): {
    isValid: boolean
    message: string
    vectors: VectorByPoints[]
  } => {
    const matchVectors2 = matchVectors.filter(
      (vector) =>
        vector.point1.label === labelOrigin &&
        vector.point2.label === labelPoint2,
    )
    if (matchVectors2.length === 0) {
      return {
        isValid: false,
        message: `Il n'y a aucun vecteur de coordonnées $(${x}\\;;\\;${y})$ dont l'origine est nommée $${labelOrigin as string}$ et l'extrémité est nommée $${labelPoint2 as string}$`,
        vectors: [],
      }
    }
    return { isValid: true, message: '', vectors: matchVectors2 }
  }
  const verifOnlyOrigin = (): {
    isValid: boolean
    message: string
    vectors: VectorByPoints[]
  } => {
    const matchVectors2 = matchVectors.filter(
      (vector) => vector.point1.x === xOrigin && vector.point1.y === yOrigin,
    )
    if (matchVectors.length === 0 && matchVectors2.length === 0) {
      return {
        isValid: false,
        message: `Il n'y a aucun vecteur de coordonnées $(${x}\\;;\\;${y})$ dont l'origine a pour coordonnées $(${xOrigin as number}\\;;\\;${yOrigin as number})$`,
        vectors: [],
      }
    }
    if (matchVectors.length > 0 && matchVectors2.length === 0) {
      return {
        isValid: false,
        message: `Il y a bien un vecteur de coordonnées $(${x}\\;;\\;${y})$ mais son origine n'a pas pour coordonnées $(${xOrigin as number}\\;;\\;${yOrigin as number})$`,
        vectors: matchVectors,
      }
    }
    return { isValid: true, message: '', vectors: matchVectors2 }
  }
  const verifOnlyLabelOriginAndOrigin = (): {
    isValid: boolean
    message: string
    vectors: VectorByPoints[]
  } => {
    const matchVectors2 = matchVectors.filter(
      (vector) =>
        vector.point1.label === labelOrigin &&
        vector.point1.x === xOrigin &&
        vector.point1.y === yOrigin,
    )
    if (matchVectors2.length === 0) {
      return {
        isValid: false,
        message: `Il n'y a aucun vecteur de coordonnées $(${x}\\;;\\;${y})$ dont l'origine est nommée $${labelOrigin as string}$ et a pour coordonnées $(${xOrigin as number}\\;;\\;${yOrigin as number})$`,
        vectors: [],
      }
    }
    return { isValid: true, message: '', vectors: matchVectors2 }
  }
  const verifLabelOriginAndPoint2AndOrigin = (): {
    isValid: boolean
    message: string
    vectors: VectorByPoints[]
  } => {
    const matchVectors2 = matchVectors.filter(
      (vector) =>
        vector.point1.label === labelOrigin &&
        vector.point2.label === labelPoint2 &&
        vector.point1.x === xOrigin &&
        vector.point1.y === yOrigin,
    )
    if (matchVectors2.length === 0) {
      return {
        isValid: false,
        message: `Il n'y a aucun vecteur de coordonnées $(${x}\\;;\\;${y})$ dont l'origine est nommée $${labelOrigin as string}$, l'extrémité est nommée $${labelPoint2 as string}$ et a pour coordonnées $(${xOrigin as number}\\;;\\;${yOrigin as number})$`,
        vectors: [],
      }
    }
    return { isValid: true, message: '', vectors: matchVectors2 }
  }
  if (matchVectors.length === 1) {
    // On veut que l'origine ait un nom particulier
    if (
      labelOrigin !== undefined &&
      labelOrigin !== '' &&
      labelPoint2 === undefined &&
      xOrigin === undefined &&
      yOrigin === undefined
    ) {
      return verifOnlyLabelOrigin()
      // On veut que les 2 extrémités soient nommées
    }
    if (
      labelOrigin !== undefined &&
      labelOrigin !== '' &&
      labelPoint2 !== undefined &&
      labelPoint2 !== '' &&
      xOrigin === undefined &&
      yOrigin === undefined
    ) {
      return verifOnlyLabelOriginAndPoint2()
      // On veut que l'origine ait des coordonnées particulières
    }
    if (
      labelOrigin === undefined &&
      labelPoint2 === undefined &&
      xOrigin !== undefined &&
      yOrigin !== undefined
    ) {
      return verifOnlyOrigin()
      // On veut que l'origine ait des coordonnées particulières et que l'origine soit nommée
    }
    if (
      labelOrigin !== undefined &&
      labelOrigin !== '' &&
      labelPoint2 === undefined &&
      xOrigin !== undefined &&
      yOrigin !== undefined
    ) {
      return verifOnlyLabelOriginAndOrigin()
      // On veut que l'origine ait des coordonnées particulières et que les 2 extrémités soient nommées
    }
    if (
      labelOrigin !== undefined &&
      labelOrigin !== '' &&
      labelPoint2 !== undefined &&
      labelPoint2 !== '' &&
      xOrigin !== undefined &&
      yOrigin !== undefined
    ) {
      return verifLabelOriginAndPoint2AndOrigin()
    }
    return {
      isValid: true,
      message: `Il y a bien un vecteur de coordonnées $(${x}\\;;\\;${y})$`,
      vectors: matchVectors,
    }
  }
  return {
    isValid: false,
    message: `Il y a ${matchVectors.length} vecteurs de coordonnées $(${x}\\;;\\;${y})`,
    vectors: matchVectors,
  }
}
