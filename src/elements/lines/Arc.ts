import type Figure from '../../Figure'
import type DynamicNumber from '../../dynamicNumbers/DynamicNumber'
import type Point from '../points/Point'

import Element2D from '../Element2D'
import { rotationCoord } from '../calculus/Coords'
import { distance, getLargeSweep } from '../calculus/utils'
import type { OptionsElement2D } from '../interfaces'

class Arc extends Element2D {
  _fillColor!: string
  _fillOpacity!: number
  /** Faut-il tracer les segments pour fermer l'arc de cercle ? */
  addBorders: boolean
  center: Point
  dynamicAngle: DynamicNumber
  start: Point
  svgArc: SVGPathElement
  constructor(
    figure: Figure,
    {
      addBorders = false,
      center,
      color,
      dynamicAngle,
      fillColor,
      fillOpacity,
      start,
      ...options
    }: {
      addBorders: boolean
      center: Point
      dynamicAngle: DynamicNumber
      start: Point
    } & OptionsElement2D,
  ) {
    super(figure, { ...options })
    this.start = start
    this.center = center
    this.dynamicAngle = dynamicAngle
    this.addBorders = addBorders
    this._color = color ?? 'black'
    this._fillColor = fillColor ?? 'none'
    this._fillOpacity = this.fillOpacity ?? this.figure.options.fillOpacity
    this.svgArc = document.createElementNS('http://www.w3.org/2000/svg', 'path')
    this.groupSvg.appendChild(this.svgArc)
    this.start.subscribe(this)
    this.center.subscribe(this)
    this.dynamicAngle.subscribe(this)
  }

  distancePointer(_: number, __: number): number {
    if (this.figure.pointer.isInArc(this)) return 0
    return Number.POSITIVE_INFINITY
  }

  draw(): void {
    this.figure.svg.appendChild(this.groupSvg)
    this.update()
    this.setVisibilityColorThicknessAndDashed()
  }

  /** Modifie la couleur et l'épaisseur de l'élément */
  setVisibilityColorThicknessAndDashed(): void {
    this.color = this._color
    this.fillColor = this._fillColor
    if (this._fillOpacity !== undefined) this.fillOpacity = this._fillOpacity
    if (this._isDashed !== undefined) this.isDashed = this._isDashed
    this.isDashed = this._isDashed
    this.isVisible = this._isVisible
  }

  toJSON(): object {
    return {
      ...this.jsonOptions(),
      idCenter: this.center.id,
      idDynamicAngle: this.dynamicAngle.id,
      idStart: this.start.id,
    }
  }

  update(): void {
    this.notify()
    const radius = this.figure.xToSx(distance(this.start, this.center))
    const [large, sweep] = getLargeSweep(this.dynamicAngle.value)
    const end = rotationCoord(this.start, this.center, this.dynamicAngle.value)
    if (
      Number.isFinite(this.start.x) &&
      Number.isFinite(this.start.y) &&
      Number.isFinite(end.x) &&
      Number.isFinite(end.y)
    ) {
      let path = `M${this.figure.xToSx(this.start.x)} ${this.figure.yToSy(this.start.y)} A ${radius} ${radius} 0 ${large} ${sweep} ${this.figure.xToSx(end.x)} ${this.figure.yToSy(end.y)}`
      if (this.addBorders)
        path += `L ${this.figure.xToSx(this.center.x)} ${this.figure.yToSy(this.center.y)} Z`
      this.svgArc.setAttribute('d', path)
    }
  }

  /** Couleur de remplissage au format HTML */
  get fillColor(): string {
    return this._fillColor
  }

  /** Change la couleur des tracés de l'élément */
  set fillColor(color: string) {
    this._fillColor = color
    this.groupSvg.setAttribute('fill', this._fillColor)
  }

  /** Couleur de remplissage au format HTML */
  get fillOpacity(): number {
    return this._fillOpacity
  }

  /** Change la couleur des tracés de l'élément */
  set fillOpacity(opacity: number) {
    this._fillOpacity = opacity
    if (opacity !== undefined)
      this.groupSvg.setAttribute('fill-opacity', opacity.toString())
  }
}

export default Arc
